
/*
=================================================
        FileName:   Beeper.c
        Author:     YuanLong.
=================================================
*/

#define _BEEPER_SRC_
#include "Globals.h"
#include "System.h"
#include "Beeper.h"
  
static BEEP_INFO CROM * SRAM pBeep;
static U8 SRAM tBeep = 0;

void BeepInit( void )
  {
    BEEP_POWER_OFF( );
    BEEP_WAVE_OFF( );
    pBeep = NULL;
  }

void BeepCtrl( void )
  {
    if ( NULL == pBeep )
      {
        BEEP_POWER_OFF( );
        BEEP_WAVE_OFF( );
        tBeep = 0;
      }
    else
      {
        if ( 0 == tBeep )
          {
            //BEEP_POWER_ON( );
            BEEP_WAVE_ON( pBeep->nFreqCnt );
          }
        else if ( tBeep >= pBeep->tPowerOff )
          {
            //BEEP_POWER_OFF( );
            if ( tBeep >= pBeep->tWaveOff )
              {
                BEEP_WAVE_OFF( );
                if ( pBeep->tBeepNext == 0 )
                  {
                    pBeep = NULL;
                  }
                else if ( tBeep >= pBeep->tBeepNext )
                  {
                    pBeep++;
                    tBeep = 255;
                  }
              }
          }
        tBeep++;
      }
  }
  
void BeepStart( BEEP_INFO CROM * sBeep )
  {
    pBeep = sBeep;
    tBeep = 0;
  }
  


/*
=================================================
        FileName:   Beeper.h
        Author:     YuanLong.
=================================================
*/

#ifndef _BEEPER_H_
#define _BEEPER_H_

#ifdef EXTERN
#undef EXTERN
#endif
#ifdef _BEEPER_SRC_
#define EXTERN
#else
#define EXTERN extern
#endif

// Turn on beeper circuit power.
#define BEEP_POWER_ON( ) \
    do { /* Add code here. */ \
    } while(0)
    
// Turn off beeper circuit power.
#define BEEP_POWER_OFF( ) \
    do {  /* Add code here. */ \
    } while(0)

#define BEEP_WAVE_ON( N ) \
    do { if( N == 1 ){TIM3_ARRH  = 0;TIM3_ARRL  = 237;TIM3_CCR2H = 0;TIM3_CCR2L = 119;TIM3_CR1   = 0X81;}/* Add code here. */ \
         else {TIM3_ARRH  = 0x09;TIM3_ARRL  = 0x86;TIM3_CCR2H = 0x04;TIM3_CCR2L = 0xC3;TIM3_CR1   = 0X81;}\
    } while(0)
    
// Turn off beeper buzzering wave.
#define BEEP_WAVE_OFF( ) \
    do { TIM3_CCR2H = 0;TIM3_CCR2L = 0;/* Add code here. */ \
    } while(0)

    
typedef struct tag_BEEP_INFO
  {
    U8  nFreqCnt;
    U8  tPowerOff;
    U8  tWaveOff;
    U8  tBeepNext;
  } BEEP_INFO;
  
#ifndef _BEEPER_SRC_

EXTERN BEEP_INFO CROM Confirm_Sound[ ];
EXTERN BEEP_INFO CROM Reminder_Sound[ ];
EXTERN BEEP_INFO CROM Complete_Sound[ ];
EXTERN BEEP_INFO CROM Cancel_Sound[ ];
#else

EXTERN BEEP_INFO CROM Confirm_Sound[ ] =
  {
    { 1, 0, 50, 0 }
  };
EXTERN BEEP_INFO CROM Reminder_Sound[ ] =
  {
    { 2, 0, 50, 0 }
  };
EXTERN BEEP_INFO CROM Complete_Sound[ ] =
  {
    //{ 1, 0, 50, 100 },
    //{ 1, 0, 50, 100 },
    //{ 1, 0, 50, 100 },
   // { 1, 0, 50, 100 },
    { 1, 0, 50,   0 }
  };
EXTERN BEEP_INFO CROM Cancel_Sound[ ] =
  {
    { 2, 0, 40, 60 }, // 0.4sON 0.2SOFF
    { 2, 0, 40, 60 },
    { 2, 0, 40,  0 }
  };
#endif

#ifndef _BEEPER_SRC_

#else

#endif
EXTERN void BeepInit( void ); // RESET subroutine.
EXTERN void BeepCtrl( void ); // 10mS procedure.
EXTERN void BeepStart( BEEP_INFO CROM * sBeep ); // API function.

#endif


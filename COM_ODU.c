
///////////////////////////////////////////////////////////
//                                                       //
//          FileName:   COM_ODU.c                        //
//                                                       //
///////////////////////////////////////////////////////////

#define  _COM_ODU_SRC_
#include "Globals.h"
#include "COM_ODU.h"
#include "KEY.h"
#include "LED_DISP.h"


#define COMM_ACK   0X09
#define COMM_NACK  0X10
void COM_ODU_Init( void )
  {
    Tx_Num       = 0;
    Tx_Length    = 0;
    Tx_Delay     = 0;
    
    Rx_Delay     = 0;
    Rx_Num       = 0;
    memset( Tx_Buffer, 0x00, sizeof( Tx_Buffer ) );
    memset( Rx_Buffer, 0x00, sizeof( Rx_Buffer ) );

    memset( &debugMessage, 0x00, sizeof( debugMessage ));
    F_KEY_COFFEE_BASKET = 0;
    F_KEY_TEA_BASKET    = 0;
    CTrl_Board_Brew_Status = 0;
    Brew_Status = IDLE;
  }

#if 1
static U8 Frame_Check(U8 *pdata,U8 num)
{
  U8 check_code = 0;
  U8 i          = 0;
  check_code = *pdata;
  for( i = 1; i< num; i++ )
  {
    check_code ^= *( pdata + i );
  }
  return check_code;
}
#endif


//===========send data to ctrl board every 100ms=============================//
void Tx_Contrl( void )
{
      static U8 state = 0;
      static U16 tx_delay = 0;
      if( tx_delay )
      {
        tx_delay--;
        return;
      }
      tx_delay = 100;//100ms
      Tx_Buffer[ 0 ] = 0x2A;
      Tx_Buffer[ 1 ] = 0x00;
      Tx_Buffer[ 2 ] = 0x00;
      Tx_Buffer[ 3 ] = 0x00;
      Tx_Buffer[ 4 ] = 0x00;
      Tx_Buffer[ 5 ] = 0x00;
      Tx_Buffer[ 6 ] = 0x00;
      Tx_Num = 0; 
       if( System_Status == ACTIVE )
          Tx_Buffer[ 1 ] |= 0x08;
       #if 0
       if(( KEEP_WARM_FUNC ) && ( F_Brew_complete || F_Brew_abnormal_end ))
          Tx_Buffer[ 1 ] |= 0x10;
       if(( WARM_LEVEL_FUNC ) && ( F_Brew_complete || F_Brew_abnormal_end ))
          Tx_Buffer[ 1 ] |= 0x20;
       #endif
       if( KEEP_WARM_FUNC )
       {
         if( Keep_Warm_Class == WARM_LO )
          Tx_Buffer[ 1 ] |= 0x20;
         else if( Keep_Warm_Class == WARM_HI || Keep_Warm_Class == WARM_OFF )
          Tx_Buffer[ 1 ] |= 0x40;
       }
       if( F_Brew_complete ||  F_Brew_abnormal_end )
       {
        //Start_Status = OFF;
        Tx_Buffer[ 1 ] |= 0X80;
       } 
       else if( Brew_Status == CANCEL )
       {
          Start_Status = OFF;
       }
       else if( errorCode0 )
       {
        Tx_Buffer[ 6 ] |= 0X80;
       }
       else if( Brew_Status == IDLE )
       {
       }
       switch( Start_Status )
       {
           case OFF:      Tx_Buffer[ 1 ] |= 0X00;break;
           case ON:       Tx_Buffer[ 1 ] |= 0X01;break;
           case PAUSE:    Tx_Buffer[ 1 ] |= 0X02;break;
           case CANCEL: Tx_Buffer[ 1 ] |= 0X03;break;
           case UNPAUSE:  Tx_Buffer[ 1 ] |= 0X04;break;
           default:       Tx_Buffer[ 1 ] |= 0X00;break;
       }
       if( CLEAN_FUNC_FLAG )
       {
         Tx_Buffer[ 2 ] = 3;
       }
       else if( COFFEE == Brew_Select)
       {
         Tx_Buffer[ 2 ] = 1;
       }
       else if( TEA == Brew_Select )
       {
         Tx_Buffer[ 2 ] = 2;
       }
             
      Tx_Buffer[ 3 ] = Knob0_count [ Brew_Select ];
      Tx_Buffer[ 4 ] = Knob1_count [ Brew_Select ];
      Tx_Buffer[ 5 ] = Knob2_count;
      if( forcePump )
      Tx_Buffer[ 6 ] |= 0x01;
           
      Tx_Length = 7;
      Tx_Buffer[ Tx_Length ] = Frame_Check( Tx_Buffer,Tx_Length );
      Tx_Length++;
      while(!(UART2_SR & 0x80)) // 等待发送数据寄存器为空
      {}
      UART2_DR = Tx_Buffer[ 0 ];
      UART2_CR2 |= 0X80;// 发送中断允许
}

//==========receive data from ctrl board dealwith===========================//
void Rx_Control( void )
{
  U8 rx_length  = 0;
  U8 check_code = 0;
  if( Rx_Delay )
  {
    Rx_Delay--;
    return;
  }
  if(( Rx_Num < 3 ) ||( Rx_Buffer[ 0 ] != 0xAA ))
  {
    Rx_Num = 0;
    return;
  }
  rx_length = Rx_Num;
  Rx_Num    = 0;
  check_code = Frame_Check( Rx_Buffer,rx_length - 1 );
  if( Rx_Buffer[ rx_length - 1 ] == check_code)
  {
    U8 length = sizeof( debugMessage );
	  U8 *p_data;
	  U8 *p_data1;
    U8 i = 0;
	  //p_data1 = &Wifi_Write_Status.ctl1_f.CTLBYTE.CTL_BYTE_L; 

            
     switch( Rx_Buffer[ 1 ] & 0x03 )
     {
       default: F_KEY_TEA_BASKET = 0;F_KEY_COFFEE_BASKET = 0;break;
       case 1:  F_KEY_TEA_BASKET = 0;F_KEY_COFFEE_BASKET = 1;break;
       case 2:  F_KEY_TEA_BASKET = 1;F_KEY_COFFEE_BASKET = 0;break;
     }
     switch(( Rx_Buffer[ 1 ] >> 2 ) & 0x07)
     {
       default: Brew_Status = IDLE;        break;
       case 1:  Brew_Status = PREHEAT;      break;
       case 2:  Brew_Status = RUNING;      break;
       case 3:  Brew_Status = PAUSE;      break;
       case 4:  Brew_Status = CANCELED;    break;
       case 5:  Brew_Status = RESUME;      break;
       case 6:  Brew_Status = NORMAL_END;  
                F_Brew_complete     = 1;
                F_KEEP_WARM_START   = 1;
                Complete_Sound_Delay = 5;  break;
       case 7:  Brew_Status = ABNORMAL_END;
                F_Brew_abnormal_end = 1;
                F_KEEP_WARM_START   = 1;
                Complete_Sound_Delay = 5; break;
       
     }
     //if( !errorCode0 )
     errorCode0 = Rx_Buffer[ 2 ];
     //T_BOIL     = Rx_Buffer[ 3 ];
     Brew_Bar = Rx_Buffer[ 1 ] >> 5; 

     
     p_data1 = (U8 *)&debugMessage.commHeadID;
     p_data = &Rx_Buffer[ 3 ];
     
     for(i = 0;i<length;i++ )
     {
       *(p_data1+i)  =  *(p_data+i);
     }
  }
}
void COM_ODU_Ctrl( void )
{
  Tx_Contrl( );
  Rx_Control( );
}
 




///////////////////////////////////////////////////////////
//                                                       //
//          FileName:   COM_ODU.h                        //
//                                                       //
///////////////////////////////////////////////////////////

#ifndef _COM_ODU_H_
#define _COM_ODU_H_

#ifdef  EXTERN
#undef  EXTERN
#endif
#ifdef  _COM_ODU_SRC_
#define EXTERN
#else
#define EXTERN extern
#endif

EXTERN U8 Tx_Num;
EXTERN U8 Tx_Length;
EXTERN U16 Tx_Delay;
EXTERN U8 Tx_Buffer[ 20 ];

EXTERN U8 Rx_Delay;
EXTERN U8 Rx_Num;
EXTERN U8 Rx_Buffer[ 50 ];



//#define F_KEY_COFFEE_BASKET            UART_OUT_data.Key_Flag[ 1 ].bits.b1
//#define F_Brew_complete                UART_OUT_data.Key_Flag[ 1 ].bits.b2 // 正常结束
//#define F_Brew_abnormal_end            UART_OUT_data.Key_Flag[ 1 ].bits.b3 //异常结束
//#define F_KEY_TEA_BASKET               UART_OUT_data.Key_Flag[ 1 ].bits.b4

EXTERN BYTE   Error_Flag0;
#define      BOIL_NTC_ERR              Error_Flag0.bits.b0
#define      PTC_NTC_ERR               Error_Flag0.bits.b1
#define      BOIL_NTC_OVERHEAT         Error_Flag0.bits.b2
#define      PTC_NTC_OVERHEAT          Error_Flag0.bits.b3

EXTERN BYTE   Ctrl_Board_Status;

#define F_KEY_COFFEE_BASKET            Ctrl_Board_Status.bits.b0
#define F_Brew_complete                Ctrl_Board_Status.bits.b1
#define F_Brew_abnormal_end            Ctrl_Board_Status.bits.b2
#define F_KEY_TEA_BASKET               Ctrl_Board_Status.bits.b3
#define F_KEEP_WARM_START              Ctrl_Board_Status.bits.b4

EXTERN U8 CTrl_Board_Brew_Status;

typedef enum 
{
  IDLE,
  PREHEAT,
  RUNING,
  PAUSED,
  CANCELED,
  RESUME,
  NORMAL_END,
  ABNORMAL_END
  
}BREW_STATUS;

typedef struct
{
  U8 commHeadID;
  U8 commBlock;
  S16 commTempTarget;
  S16 commTempWaterout;
  S16 commTempErr;
  FLOAT commWaterD;
  S16 commTempBoiler;
  FLOAT commboilerD;
  S16 commFlowTarget;
  FLOAT commFlowTemp;
  U16 commflowRate;
  FLOAT commFlowTempError;
  FLOAT commFlowRateError;
  U8 commPumpDuty;
  U8 commBoilerDuty;
  U8 commSoftVersion;
  S16 commTempWarmPlate;
  S16 commTempInlet;
}DEBUG_MESSAGE;
EXTERN DEBUG_MESSAGE   debugMessage;

EXTERN BREW_STATUS Brew_Status;


EXTERN U8 Brew_Bar;
EXTERN U8 CLean_Class; // 0: 无提示，1 :一般提示,  2:严重提示
EXTERN U8 CLean_Blink_10count;




EXTERN U8 Digital_Index;
EXTERN U8 errorCode0;
EXTERN U8 T_BOIL;

EXTERN void COM_ODU_Init( void );
EXTERN void COM_ODU_Ctrl( void );

#endif


   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   3                     ; Optimizer V4.2.4 - 18 Dec 2007
2777                     .const:	section	.text
2778  0000               _Confirm_Sound:
2779  0000 01            	dc.b	1
2780  0001 00            	dc.b	0
2781  0002 32            	dc.b	50
2782  0003 00            	dc.b	0
2783  0004               _Reminder_Sound:
2784  0004 02            	dc.b	2
2785  0005 00            	dc.b	0
2786  0006 32            	dc.b	50
2787  0007 00            	dc.b	0
2788  0008               _Complete_Sound:
2789  0008 01            	dc.b	1
2790  0009 00            	dc.b	0
2791  000a 32            	dc.b	50
2792  000b 00            	dc.b	0
2793  000c               _Cancel_Sound:
2794  000c 02            	dc.b	2
2795  000d 00            	dc.b	0
2796  000e 28            	dc.b	40
2797  000f 3c            	dc.b	60
2798  0010 02            	dc.b	2
2799  0011 00            	dc.b	0
2800  0012 28            	dc.b	40
2801  0013 3c            	dc.b	60
2802  0014 02            	dc.b	2
2803  0015 00            	dc.b	0
2804  0016 28            	dc.b	40
2805  0017 00            	dc.b	0
2909                     	bsct
2910  0000               L1402_tBeep:
2911  0000 00            	dc.b	0
2944                     ; 17 void BeepInit( void )
2944                     ; 18   {
2946                     	switch	.text
2947  0000               _BeepInit:
2951                     ; 20     BEEP_WAVE_OFF( );
2953  0000 725f532f      	clr	_TIM3_CCR2H
2956  0004 725f5330      	clr	_TIM3_CCR2L
2957                     ; 21     pBeep = NULL;
2959  0008 5f            	clrw	x
2960  0009 bf00          	ldw	L7302_pBeep,x
2961                     ; 22   }
2964  000b 81            	ret	
2994                     ; 24 void BeepCtrl( void )
2994                     ; 25   {
2995                     	switch	.text
2996  000c               _BeepCtrl:
3000                     ; 26     if ( NULL == pBeep )
3002  000c be00          	ldw	x,L7302_pBeep
3003  000e 260b          	jrne	L1012
3004                     ; 29         BEEP_WAVE_OFF( );
3006  0010 725f532f      	clr	_TIM3_CCR2H
3009  0014 725f5330      	clr	_TIM3_CCR2L
3010                     ; 30         tBeep = 0;
3012  0018 3f00          	clr	L1402_tBeep
3015  001a 81            	ret	
3016  001b               L1012:
3017                     ; 34         if ( 0 == tBeep )
3019  001b b600          	ld	a,L1402_tBeep
3020  001d 2630          	jrne	L5112
3021                     ; 37             BEEP_WAVE_ON( pBeep->nFreqCnt );
3023  001f 92c600        	ld	a,[L7302_pBeep.w]
3024  0022 4a            	dec	a
3025  0023 2614          	jrne	L3212
3028  0025 c7532b        	ld	_TIM3_ARRH,a
3031  0028 35ed532c      	mov	_TIM3_ARRL,#237
3034  002c c7532f        	ld	_TIM3_CCR2H,a
3037  002f 35775330      	mov	_TIM3_CCR2L,#119
3040  0033 35815320      	mov	_TIM3_CR1,#129
3042  0037 2040          	jra	L7212
3043  0039               L3212:
3046  0039 3509532b      	mov	_TIM3_ARRH,#9
3049  003d 3586532c      	mov	_TIM3_ARRL,#134
3052  0041 3504532f      	mov	_TIM3_CCR2H,#4
3055  0045 35c35330      	mov	_TIM3_CCR2L,#195
3058  0049 35815320      	mov	_TIM3_CR1,#129
3059  004d 202a          	jra	L7212
3060  004f               L5112:
3061                     ; 39         else if ( tBeep >= pBeep->tPowerOff )
3063  004f e601          	ld	a,(1,x)
3064  0051 b100          	cp	a,L1402_tBeep
3065  0053 2224          	jrugt	L7212
3066                     ; 42             if ( tBeep >= pBeep->tWaveOff )
3068  0055 e602          	ld	a,(2,x)
3069  0057 b100          	cp	a,L1402_tBeep
3070  0059 221e          	jrugt	L7212
3071                     ; 44                 BEEP_WAVE_OFF( );
3073  005b 725f532f      	clr	_TIM3_CCR2H
3076  005f 725f5330      	clr	_TIM3_CCR2L
3077                     ; 45                 if ( pBeep->tBeepNext == 0 )
3079  0063 e603          	ld	a,(3,x)
3080  0065 2605          	jrne	L1412
3081                     ; 47                     pBeep = NULL;
3083  0067 5f            	clrw	x
3084  0068 bf00          	ldw	L7302_pBeep,x
3086  006a 200d          	jra	L7212
3087  006c               L1412:
3088                     ; 49                 else if ( tBeep >= pBeep->tBeepNext )
3090  006c b100          	cp	a,L1402_tBeep
3091  006e 2209          	jrugt	L7212
3092                     ; 51                     pBeep++;
3094  0070 1c0004        	addw	x,#4
3095  0073 bf00          	ldw	L7302_pBeep,x
3096                     ; 52                     tBeep = 255;
3098  0075 35ff0000      	mov	L1402_tBeep,#255
3099  0079               L7212:
3100                     ; 56         tBeep++;
3102  0079 3c00          	inc	L1402_tBeep
3103                     ; 58   }
3106  007b 81            	ret	
3180                     ; 60 void BeepStart( BEEP_INFO CROM * sBeep )
3180                     ; 61   {
3181                     	switch	.text
3182  007c               _BeepStart:
3186                     ; 62     pBeep = sBeep;
3188  007c bf00          	ldw	L7302_pBeep,x
3189                     ; 63     tBeep = 0;
3191  007e 3f00          	clr	L1402_tBeep
3192                     ; 64   }
3195  0080 81            	ret	
3231                     	switch	.ubsct
3232  0000               L7302_pBeep:
3233  0000 0000          	ds.b	2
3234                     	xdef	_BeepStart
3235                     	xdef	_BeepCtrl
3236                     	xdef	_BeepInit
3237                     	xdef	_Cancel_Sound
3238                     	xdef	_Complete_Sound
3239                     	xdef	_Reminder_Sound
3240                     	xdef	_Confirm_Sound
3260                     	end

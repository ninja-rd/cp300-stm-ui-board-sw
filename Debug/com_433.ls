   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   3                     ; Optimizer V4.2.4 - 18 Dec 2007
2814                     	bsct
2815  0000               L3102_codeDelay:
2816  0000 00            	dc.b	0
2877                     ; 27 static U8   FrameCheck( void )
2877                     ; 28   {
2879                     	switch	.text
2880  0000               L5102_FrameCheck:
2882  0000 5203          	subw	sp,#3
2883       00000003      OFST:	set	3
2886                     ; 30     U8  r = 1;
2888  0002 a601          	ld	a,#1
2889  0004 6b02          	ld	(OFST-1,sp),a
2890                     ; 31     if ( buffer[ 0 ] != 0xF7 )
2892  0006 b604          	ld	a,L5002_buffer
2893  0008 a1f7          	cp	a,#247
2894  000a 2706          	jreq	L3502
2895                     ; 33         buffer[ 0 ] = 0xF7;
2897  000c 35f70004      	mov	L5002_buffer,#247
2898                     ; 34         r = 0;
2900  0010 0f02          	clr	(OFST-1,sp)
2901  0012               L3502:
2902                     ; 36     if ( buffer[ 1 ] != 0xF8 )
2904  0012 b605          	ld	a,L5002_buffer+1
2905  0014 a1f8          	cp	a,#248
2906  0016 2706          	jreq	L5502
2907                     ; 38         buffer[ 1 ] = 0xF8;
2909  0018 35f80005      	mov	L5002_buffer+1,#248
2910                     ; 39         r = 0;
2912  001c 0f02          	clr	(OFST-1,sp)
2913  001e               L5502:
2914                     ; 41     if ( count != buffer[ 2 ] + 4 )
2916  001e b618          	ld	a,L3002_count
2917  0020 5f            	clrw	x
2918  0021 97            	ld	xl,a
2919  0022 bf01          	ldw	c_x+1,x
2920  0024 b606          	ld	a,L5002_buffer+2
2921  0026 905f          	clrw	y
2922  0028 9097          	ld	yl,a
2923  002a 72a90004      	addw	y,#4
2924  002e 90b301        	cpw	y,c_x+1
2925  0031 2706          	jreq	L7502
2926                     ; 43         count = buffer[ 2 ] + 4;
2928  0033 ab04          	add	a,#4
2929  0035 b718          	ld	L3002_count,a
2930                     ; 44         r = 0;
2932  0037 0f02          	clr	(OFST-1,sp)
2933  0039               L7502:
2934                     ; 46     for ( s = 0, i = 2; i < count - 2; i++ )
2936  0039 0f01          	clr	(OFST-2,sp)
2937  003b a602          	ld	a,#2
2938  003d 6b03          	ld	(OFST+0,sp),a
2940  003f 200a          	jra	L5602
2941  0041               L1602:
2942                     ; 48         s += buffer[ i ];
2944  0041 5f            	clrw	x
2945  0042 97            	ld	xl,a
2946  0043 7b01          	ld	a,(OFST-2,sp)
2947  0045 eb04          	add	a,(L5002_buffer,x)
2948  0047 6b01          	ld	(OFST-2,sp),a
2949                     ; 46     for ( s = 0, i = 2; i < count - 2; i++ )
2951  0049 0c03          	inc	(OFST+0,sp)
2952  004b               L5602:
2955  004b b618          	ld	a,L3002_count
2956  004d 5f            	clrw	x
2957  004e 97            	ld	xl,a
2958  004f 1d0002        	subw	x,#2
2959  0052 7b03          	ld	a,(OFST+0,sp)
2960  0054 905f          	clrw	y
2961  0056 9097          	ld	yl,a
2962  0058 90bf01        	ldw	c_y+1,y
2963  005b b301          	cpw	x,c_y+1
2964  005d 2ce2          	jrsgt	L1602
2965                     ; 50     if ( buffer[ i ] != s )
2967  005f 5f            	clrw	x
2968  0060 97            	ld	xl,a
2969  0061 e604          	ld	a,(L5002_buffer,x)
2970  0063 1101          	cp	a,(OFST-2,sp)
2971  0065 270a          	jreq	L1702
2972                     ; 52         buffer[ i ] = s;
2974  0067 7b03          	ld	a,(OFST+0,sp)
2975  0069 5f            	clrw	x
2976  006a 97            	ld	xl,a
2977  006b 7b01          	ld	a,(OFST-2,sp)
2978  006d e704          	ld	(L5002_buffer,x),a
2979                     ; 53         r = 0;
2981  006f 0f02          	clr	(OFST-1,sp)
2982  0071               L1702:
2983                     ; 55     i++;
2985  0071 0c03          	inc	(OFST+0,sp)
2986                     ; 56     if ( buffer[ i ] != 0xFD )
2988  0073 7b03          	ld	a,(OFST+0,sp)
2989  0075 5f            	clrw	x
2990  0076 97            	ld	xl,a
2991  0077 e604          	ld	a,(L5002_buffer,x)
2992  0079 a1fd          	cp	a,#253
2993  007b 270a          	jreq	L3702
2994                     ; 58         buffer[ i ] = 0xFD;
2996  007d 7b03          	ld	a,(OFST+0,sp)
2997  007f 5f            	clrw	x
2998  0080 97            	ld	xl,a
2999  0081 a6fd          	ld	a,#253
3000  0083 e704          	ld	(L5002_buffer,x),a
3001                     ; 59         r = 0;
3003  0085 0f02          	clr	(OFST-1,sp)
3004  0087               L3702:
3005                     ; 61     return r;
3007  0087 7b02          	ld	a,(OFST-1,sp)
3010  0089 5b03          	addw	sp,#3
3011  008b 81            	ret	
3036                     ; 64 static U8   AddressCheck( void )
3036                     ; 65   {
3037                     	switch	.text
3038  008c               L5702_AddressCheck:
3042                     ; 66     if ( buffer[ 3 ] != 0x25 ) return 0;
3044  008c b607          	ld	a,L5002_buffer+3
3045  008e a125          	cp	a,#37
3046  0090 2702          	jreq	L7012
3049  0092 4f            	clr	a
3052  0093 81            	ret	
3053  0094               L7012:
3054                     ; 67     if ( buffer[ 4 ] != addrRMT[ 0 ] ) return 0;
3056  0094 b608          	ld	a,L5002_buffer+4
3057  0096 b100          	cp	a,L1102_addrRMT
3058  0098 2702          	jreq	L1112
3061  009a 4f            	clr	a
3064  009b 81            	ret	
3065  009c               L1112:
3066                     ; 68     if ( buffer[ 5 ] != addrRMT[ 1 ] ) return 0;
3068  009c b609          	ld	a,L5002_buffer+5
3069  009e b101          	cp	a,L1102_addrRMT+1
3070  00a0 2702          	jreq	L3112
3073  00a2 4f            	clr	a
3076  00a3 81            	ret	
3077  00a4               L3112:
3078                     ; 69     if ( buffer[ 6 ] != addrRMT[ 2 ] ) return 0;
3080  00a4 b60a          	ld	a,L5002_buffer+6
3081  00a6 b102          	cp	a,L1102_addrRMT+2
3082  00a8 2702          	jreq	L5112
3085  00aa 4f            	clr	a
3088  00ab 81            	ret	
3089  00ac               L5112:
3090                     ; 70     return 1;
3092  00ac a601          	ld	a,#1
3095  00ae 81            	ret	
3139                     ; 73 static void CommandSettings( void )
3139                     ; 74   {
3140                     	switch	.text
3141  00af               L7112_CommandSettings:
3143  00af 5204          	subw	sp,#4
3144       00000004      OFST:	set	4
3147                     ; 76     sysTprSet = buffer[ 9 ];
3149  00b1 450d00        	mov	_sysTprSet,L5002_buffer+9
3150                     ; 77     if ( BTST( buffer[ 11 ], 0 ) ) sysAC_On = 0;
3152  00b4 7201000f02    	btjf	L5002_buffer+11,#0,L7312
3155  00b9 3f00          	clr	_sysAC_On
3156  00bb               L7312:
3157                     ; 78     if ( BTST( buffer[ 11 ], 1 ) ) sysAC_On = 1;
3159  00bb 7203000f04    	btjf	L5002_buffer+11,#1,L1412
3162  00c0 35010000      	mov	_sysAC_On,#1
3163  00c4               L1412:
3164                     ; 79     sysAC_Mode = ( buffer[ 11 ] & 0x0C ) >> 2;
3166  00c4 b60f          	ld	a,L5002_buffer+11
3167  00c6 a40c          	and	a,#12
3168  00c8 44            	srl	a
3169  00c9 44            	srl	a
3170  00ca b700          	ld	_sysAC_Mode,a
3171                     ; 80     if ( BTST( buffer[ 11 ], 4 ) ) sysFreshOn = 0;
3173  00cc 7209000f02    	btjf	L5002_buffer+11,#4,L3412
3176  00d1 3f00          	clr	_sysFreshOn
3177  00d3               L3412:
3178                     ; 81     if ( BTST( buffer[ 11 ], 5 ) ) sysFreshOn = 1;
3180  00d3 720b000f04    	btjf	L5002_buffer+11,#5,L5412
3183  00d8 35010000      	mov	_sysFreshOn,#1
3184  00dc               L5412:
3185                     ; 82     if ( BTST( buffer[ 11 ], 6 ) ) sysSwingOn = 1;
3187  00dc 720d000f04    	btjf	L5002_buffer+11,#6,L7412
3190  00e1 35010000      	mov	_sysSwingOn,#1
3191  00e5               L7412:
3192                     ; 83     if ( BTST( buffer[ 11 ], 7 ) ) sysSwingOn = 0;
3194  00e5 720f000f02    	btjf	L5002_buffer+11,#7,L1512
3197  00ea 3f00          	clr	_sysSwingOn
3198  00ec               L1512:
3199                     ; 84     if ( buffer[ 12 ] & 0x07 )
3201  00ec b610          	ld	a,L5002_buffer+12
3202  00ee a507          	bcp	a,#7
3203  00f0 2706          	jreq	L3512
3204                     ; 86         sysAC_FanLvel = ( buffer[ 12 ] & 0x07 );
3206  00f2 a407          	and	a,#7
3207  00f4 b700          	ld	_sysAC_FanLvel,a
3208  00f6 b610          	ld	a,L5002_buffer+12
3209  00f8               L3512:
3210                     ; 88     if ( buffer[ 12 ] & 0x18 )
3212  00f8 a518          	bcp	a,#24
3213  00fa 2707          	jreq	L5512
3214                     ; 90         sysFreshFanLevel = ( buffer[ 12 ] & 0x18 ) >> 3;
3216  00fc a418          	and	a,#24
3217  00fe 44            	srl	a
3218  00ff 44            	srl	a
3219  0100 44            	srl	a
3220  0101 b700          	ld	_sysFreshFanLevel,a
3221  0103               L5512:
3222                     ; 92     t = 0;
3224  0103 5f            	clrw	x
3225  0104 1f03          	ldw	(OFST-1,sp),x
3226  0106 1f01          	ldw	(OFST-3,sp),x
3227                     ; 93     if ( BTST( buffer[ 13 ], 0 ) ) t += 3600UL/2;
3229  0108 720100110d    	btjf	L5002_buffer+13,#0,L7512
3232  010d ae0708        	ldw	x,#1800
3233  0110 bf02          	ldw	c_lreg+2,x
3234  0112 5f            	clrw	x
3235  0113 bf00          	ldw	c_lreg,x
3236  0115 96            	ldw	x,sp
3237  0116 5c            	incw	x
3238  0117 cd0000        	call	c_lgadd
3240  011a               L7512:
3241                     ; 94     if ( BTST( buffer[ 13 ], 1 ) ) t += 3600UL*1;
3243  011a 720300110d    	btjf	L5002_buffer+13,#1,L1612
3246  011f ae0e10        	ldw	x,#3600
3247  0122 bf02          	ldw	c_lreg+2,x
3248  0124 5f            	clrw	x
3249  0125 bf00          	ldw	c_lreg,x
3250  0127 96            	ldw	x,sp
3251  0128 5c            	incw	x
3252  0129 cd0000        	call	c_lgadd
3254  012c               L1612:
3255                     ; 95     if ( BTST( buffer[ 13 ], 2 ) ) t += 3600UL*2;
3257  012c 720500110d    	btjf	L5002_buffer+13,#2,L3612
3260  0131 ae1c20        	ldw	x,#7200
3261  0134 bf02          	ldw	c_lreg+2,x
3262  0136 5f            	clrw	x
3263  0137 bf00          	ldw	c_lreg,x
3264  0139 96            	ldw	x,sp
3265  013a 5c            	incw	x
3266  013b cd0000        	call	c_lgadd
3268  013e               L3612:
3269                     ; 96     if ( BTST( buffer[ 13 ], 3 ) ) t += 3600UL*4;
3271  013e 720700110d    	btjf	L5002_buffer+13,#3,L5612
3274  0143 ae3840        	ldw	x,#14400
3275  0146 bf02          	ldw	c_lreg+2,x
3276  0148 5f            	clrw	x
3277  0149 bf00          	ldw	c_lreg,x
3278  014b 96            	ldw	x,sp
3279  014c 5c            	incw	x
3280  014d cd0000        	call	c_lgadd
3282  0150               L5612:
3283                     ; 97     if ( BTST( buffer[ 13 ], 4 ) ) t += 3600UL*8;
3285  0150 720900110d    	btjf	L5002_buffer+13,#4,L7612
3288  0155 ae7080        	ldw	x,#28800
3289  0158 bf02          	ldw	c_lreg+2,x
3290  015a 5f            	clrw	x
3291  015b bf00          	ldw	c_lreg,x
3292  015d 96            	ldw	x,sp
3293  015e 5c            	incw	x
3294  015f cd0000        	call	c_lgadd
3296  0162               L7612:
3297                     ; 98     if ( BTST( buffer[ 13 ], 5 ) ) t += 3600UL*12;
3299  0162 720b00110d    	btjf	L5002_buffer+13,#5,L1712
3302  0167 aea8c0        	ldw	x,#43200
3303  016a bf02          	ldw	c_lreg+2,x
3304  016c 5f            	clrw	x
3305  016d bf00          	ldw	c_lreg,x
3306  016f 96            	ldw	x,sp
3307  0170 5c            	incw	x
3308  0171 cd0000        	call	c_lgadd
3310  0174               L1712:
3311                     ; 99     sysAC_Timer = t;
3313  0174 1e03          	ldw	x,(OFST-1,sp)
3314  0176 bf02          	ldw	_sysAC_Timer+2,x
3315  0178 1e01          	ldw	x,(OFST-3,sp)
3316  017a bf00          	ldw	_sysAC_Timer,x
3317                     ; 100     t = 0;
3319  017c 5f            	clrw	x
3320  017d 1f03          	ldw	(OFST-1,sp),x
3321  017f 1f01          	ldw	(OFST-3,sp),x
3322                     ; 101     if ( BTST( buffer[ 14 ], 0 ) ) t += 3600UL/2;
3324  0181 720100120d    	btjf	L5002_buffer+14,#0,L3712
3327  0186 ae0708        	ldw	x,#1800
3328  0189 bf02          	ldw	c_lreg+2,x
3329  018b 5f            	clrw	x
3330  018c bf00          	ldw	c_lreg,x
3331  018e 96            	ldw	x,sp
3332  018f 5c            	incw	x
3333  0190 cd0000        	call	c_lgadd
3335  0193               L3712:
3336                     ; 102     if ( BTST( buffer[ 14 ], 1 ) ) t += 3600UL*1;
3338  0193 720300120d    	btjf	L5002_buffer+14,#1,L5712
3341  0198 ae0e10        	ldw	x,#3600
3342  019b bf02          	ldw	c_lreg+2,x
3343  019d 5f            	clrw	x
3344  019e bf00          	ldw	c_lreg,x
3345  01a0 96            	ldw	x,sp
3346  01a1 5c            	incw	x
3347  01a2 cd0000        	call	c_lgadd
3349  01a5               L5712:
3350                     ; 103     if ( BTST( buffer[ 14 ], 2 ) ) t += 3600UL*2;
3352  01a5 720500120d    	btjf	L5002_buffer+14,#2,L7712
3355  01aa ae1c20        	ldw	x,#7200
3356  01ad bf02          	ldw	c_lreg+2,x
3357  01af 5f            	clrw	x
3358  01b0 bf00          	ldw	c_lreg,x
3359  01b2 96            	ldw	x,sp
3360  01b3 5c            	incw	x
3361  01b4 cd0000        	call	c_lgadd
3363  01b7               L7712:
3364                     ; 104     if ( BTST( buffer[ 14 ], 3 ) ) t += 3600UL*4;
3366  01b7 720700120d    	btjf	L5002_buffer+14,#3,L1022
3369  01bc ae3840        	ldw	x,#14400
3370  01bf bf02          	ldw	c_lreg+2,x
3371  01c1 5f            	clrw	x
3372  01c2 bf00          	ldw	c_lreg,x
3373  01c4 96            	ldw	x,sp
3374  01c5 5c            	incw	x
3375  01c6 cd0000        	call	c_lgadd
3377  01c9               L1022:
3378                     ; 105     if ( BTST( buffer[ 14 ], 4 ) ) t += 3600UL*8;
3380  01c9 720900120d    	btjf	L5002_buffer+14,#4,L3022
3383  01ce ae7080        	ldw	x,#28800
3384  01d1 bf02          	ldw	c_lreg+2,x
3385  01d3 5f            	clrw	x
3386  01d4 bf00          	ldw	c_lreg,x
3387  01d6 96            	ldw	x,sp
3388  01d7 5c            	incw	x
3389  01d8 cd0000        	call	c_lgadd
3391  01db               L3022:
3392                     ; 106     if ( BTST( buffer[ 14 ], 5 ) ) t += 3600UL*12;
3394  01db 720b00120d    	btjf	L5002_buffer+14,#5,L5022
3397  01e0 aea8c0        	ldw	x,#43200
3398  01e3 bf02          	ldw	c_lreg+2,x
3399  01e5 5f            	clrw	x
3400  01e6 bf00          	ldw	c_lreg,x
3401  01e8 96            	ldw	x,sp
3402  01e9 5c            	incw	x
3403  01ea cd0000        	call	c_lgadd
3405  01ed               L5022:
3406                     ; 107     sysFreshTimer = t;
3408  01ed 1e03          	ldw	x,(OFST-1,sp)
3409  01ef bf02          	ldw	_sysFreshTimer+2,x
3410  01f1 1e01          	ldw	x,(OFST-3,sp)
3411  01f3 bf00          	ldw	_sysFreshTimer,x
3412                     ; 108   }
3415  01f5 5b04          	addw	sp,#4
3416  01f7 81            	ret	
3461                     .const:	section	.text
3462  0000               L61:
3463  0000 00000384      	dc.l	900
3464  0004               L02:
3465  0004 0000a8c0      	dc.l	43200
3466  0008               L22:
3467  0008 00007080      	dc.l	28800
3468  000c               L42:
3469  000c 00003840      	dc.l	14400
3470  0010               L62:
3471  0010 00001c20      	dc.l	7200
3472  0014               L03:
3473  0014 00000e10      	dc.l	3600
3474  0018               L23:
3475  0018 00000708      	dc.l	1800
3476                     ; 110 static void AnswerForInquiry( void )
3476                     ; 111   {
3477                     	switch	.text
3478  01f8               L7022_AnswerForInquiry:
3480  01f8 5204          	subw	sp,#4
3481       00000004      OFST:	set	4
3484                     ; 113     buffer[ 2 ] = 0x0E;
3486  01fa 350e0006      	mov	L5002_buffer+2,#14
3487                     ; 114     buffer[ 3 ] = 0x26;
3489  01fe 35260007      	mov	L5002_buffer+3,#38
3490                     ; 115     buffer[ 7 ] = 0x81;
3492  0202 3581000b      	mov	L5002_buffer+7,#129
3493                     ; 116     buffer[ 8 ] = ( U8 )( ( tprRoom.val + 128 ) >> 8 );
3495  0206 be00          	ldw	x,_tprRoom
3496  0208 1c0080        	addw	x,#128
3497  020b 4f            	clr	a
3498  020c 5d            	tnzw	x
3499  020d 2a01          	jrpl	L41
3500  020f 43            	cpl	a
3501  0210               L41:
3502  0210 97            	ld	xl,a
3503  0211 5e            	swapw	x
3504  0212 9f            	ld	a,xl
3505  0213 b70c          	ld	L5002_buffer+8,a
3506                     ; 117     buffer[ 9 ] = ( U8 )sysTprSet;
3508  0215 45000d        	mov	L5002_buffer+9,_sysTprSet
3509                     ; 118     buffer[ 10 ] = 0;
3511  0218 3f0e          	clr	L5002_buffer+10
3512                     ; 119     buffer[ 11 ] = 0;
3514  021a 3f0f          	clr	L5002_buffer+11
3515                     ; 120     if ( sysAC_On )
3517  021c b600          	ld	a,_sysAC_On
3518  021e 2706          	jreq	L7222
3519                     ; 122         BSET( buffer[ 11 ], 1 );
3521  0220 7212000f      	bset	L5002_buffer+11,#1
3523  0224 2004          	jra	L1322
3524  0226               L7222:
3525                     ; 126         BSET( buffer[ 11 ], 0 );
3527  0226 7210000f      	bset	L5002_buffer+11,#0
3528  022a               L1322:
3529                     ; 128     buffer[ 11 ] |= ( sysAC_Mode << 2 );
3531  022a b600          	ld	a,_sysAC_Mode
3532  022c 48            	sll	a
3533  022d 48            	sll	a
3534  022e ba0f          	or	a,L5002_buffer+11
3535  0230 b70f          	ld	L5002_buffer+11,a
3536                     ; 129     if ( sysFreshOn )
3538  0232 b600          	ld	a,_sysFreshOn
3539  0234 2706          	jreq	L3322
3540                     ; 131         BSET( buffer[ 11 ], 5 );
3542  0236 721a000f      	bset	L5002_buffer+11,#5
3544  023a 2004          	jra	L5322
3545  023c               L3322:
3546                     ; 135         BSET( buffer[ 11 ], 4 );
3548  023c 7218000f      	bset	L5002_buffer+11,#4
3549  0240               L5322:
3550                     ; 137     if ( sysSwingOn )
3552  0240 b600          	ld	a,_sysSwingOn
3553  0242 2706          	jreq	L7322
3554                     ; 139         BSET( buffer[ 11 ], 6 );
3556  0244 721c000f      	bset	L5002_buffer+11,#6
3558  0248 2004          	jra	L1422
3559  024a               L7322:
3560                     ; 143         BSET( buffer[ 11 ], 7 );
3562  024a 721e000f      	bset	L5002_buffer+11,#7
3563  024e               L1422:
3564                     ; 145     buffer[ 12 ] = sysAC_FanLvel | ( sysFreshFanLevel << 3 );
3566  024e b600          	ld	a,_sysFreshFanLevel
3567  0250 48            	sll	a
3568  0251 48            	sll	a
3569  0252 48            	sll	a
3570  0253 ba00          	or	a,_sysAC_FanLvel
3571  0255 b710          	ld	L5002_buffer+12,a
3572                     ; 146     buffer[ 13 ] = 0;
3574  0257 3f11          	clr	L5002_buffer+13
3575                     ; 147     t = sysAC_Timer + 3600UL/4;
3577  0259 ae0000        	ldw	x,#_sysAC_Timer
3578  025c cd0000        	call	c_ltor
3580  025f ae0000        	ldw	x,#L61
3581  0262 cd0000        	call	c_ladd
3583  0265 96            	ldw	x,sp
3584  0266 5c            	incw	x
3585  0267 cd0000        	call	c_rtol
3587                     ; 148     if ( t >= 3600UL*12 )
3589  026a 96            	ldw	x,sp
3590  026b 5c            	incw	x
3591  026c cd0000        	call	c_ltor
3593  026f ae0004        	ldw	x,#L02
3594  0272 cd0000        	call	c_lcmp
3596  0275 2511          	jrult	L3422
3597                     ; 150         t -= 3600UL*12;
3599  0277 aea8c0        	ldw	x,#43200
3600  027a bf02          	ldw	c_lreg+2,x
3601  027c 5f            	clrw	x
3602  027d bf00          	ldw	c_lreg,x
3603  027f 96            	ldw	x,sp
3604  0280 5c            	incw	x
3605  0281 cd0000        	call	c_lgsub
3607                     ; 151         BSET( buffer[ 13 ], 5 );
3609  0284 721a0011      	bset	L5002_buffer+13,#5
3610  0288               L3422:
3611                     ; 153     if ( t >= 3600UL*8 )
3613  0288 96            	ldw	x,sp
3614  0289 5c            	incw	x
3615  028a cd0000        	call	c_ltor
3617  028d ae0008        	ldw	x,#L22
3618  0290 cd0000        	call	c_lcmp
3620  0293 2511          	jrult	L5422
3621                     ; 155         t -= 3600UL*8;
3623  0295 ae7080        	ldw	x,#28800
3624  0298 bf02          	ldw	c_lreg+2,x
3625  029a 5f            	clrw	x
3626  029b bf00          	ldw	c_lreg,x
3627  029d 96            	ldw	x,sp
3628  029e 5c            	incw	x
3629  029f cd0000        	call	c_lgsub
3631                     ; 156         BSET( buffer[ 13 ], 4 );
3633  02a2 72180011      	bset	L5002_buffer+13,#4
3634  02a6               L5422:
3635                     ; 158     if ( t >= 3600UL*4 )
3637  02a6 96            	ldw	x,sp
3638  02a7 5c            	incw	x
3639  02a8 cd0000        	call	c_ltor
3641  02ab ae000c        	ldw	x,#L42
3642  02ae cd0000        	call	c_lcmp
3644  02b1 2511          	jrult	L7422
3645                     ; 160         t -= 3600UL*4;
3647  02b3 ae3840        	ldw	x,#14400
3648  02b6 bf02          	ldw	c_lreg+2,x
3649  02b8 5f            	clrw	x
3650  02b9 bf00          	ldw	c_lreg,x
3651  02bb 96            	ldw	x,sp
3652  02bc 5c            	incw	x
3653  02bd cd0000        	call	c_lgsub
3655                     ; 161         BSET( buffer[ 13 ], 3 );
3657  02c0 72160011      	bset	L5002_buffer+13,#3
3658  02c4               L7422:
3659                     ; 163     if ( t >= 3600UL*2 )
3661  02c4 96            	ldw	x,sp
3662  02c5 5c            	incw	x
3663  02c6 cd0000        	call	c_ltor
3665  02c9 ae0010        	ldw	x,#L62
3666  02cc cd0000        	call	c_lcmp
3668  02cf 2511          	jrult	L1522
3669                     ; 165         t -= 3600UL*2;
3671  02d1 ae1c20        	ldw	x,#7200
3672  02d4 bf02          	ldw	c_lreg+2,x
3673  02d6 5f            	clrw	x
3674  02d7 bf00          	ldw	c_lreg,x
3675  02d9 96            	ldw	x,sp
3676  02da 5c            	incw	x
3677  02db cd0000        	call	c_lgsub
3679                     ; 166         BSET( buffer[ 13 ], 2 );
3681  02de 72140011      	bset	L5002_buffer+13,#2
3682  02e2               L1522:
3683                     ; 168     if ( t >= 3600UL*1 )
3685  02e2 96            	ldw	x,sp
3686  02e3 5c            	incw	x
3687  02e4 cd0000        	call	c_ltor
3689  02e7 ae0014        	ldw	x,#L03
3690  02ea cd0000        	call	c_lcmp
3692  02ed 2511          	jrult	L3522
3693                     ; 170         t -= 3600UL*1;
3695  02ef ae0e10        	ldw	x,#3600
3696  02f2 bf02          	ldw	c_lreg+2,x
3697  02f4 5f            	clrw	x
3698  02f5 bf00          	ldw	c_lreg,x
3699  02f7 96            	ldw	x,sp
3700  02f8 5c            	incw	x
3701  02f9 cd0000        	call	c_lgsub
3703                     ; 171         BSET( buffer[ 13 ], 1 );
3705  02fc 72120011      	bset	L5002_buffer+13,#1
3706  0300               L3522:
3707                     ; 173     if ( t >= 3600UL/2 )
3709  0300 96            	ldw	x,sp
3710  0301 5c            	incw	x
3711  0302 cd0000        	call	c_ltor
3713  0305 ae0018        	ldw	x,#L23
3714  0308 cd0000        	call	c_lcmp
3716  030b 2511          	jrult	L5522
3717                     ; 175         t -= 3600UL/2;
3719  030d ae0708        	ldw	x,#1800
3720  0310 bf02          	ldw	c_lreg+2,x
3721  0312 5f            	clrw	x
3722  0313 bf00          	ldw	c_lreg,x
3723  0315 96            	ldw	x,sp
3724  0316 5c            	incw	x
3725  0317 cd0000        	call	c_lgsub
3727                     ; 176         BSET( buffer[ 13 ], 0 );
3729  031a 72100011      	bset	L5002_buffer+13,#0
3730  031e               L5522:
3731                     ; 178     buffer[ 14 ] = 0;
3733  031e 3f12          	clr	L5002_buffer+14
3734                     ; 179     t = sysFreshTimer + 3600UL/4;
3736  0320 ae0000        	ldw	x,#_sysFreshTimer
3737  0323 cd0000        	call	c_ltor
3739  0326 ae0000        	ldw	x,#L61
3740  0329 cd0000        	call	c_ladd
3742  032c 96            	ldw	x,sp
3743  032d 5c            	incw	x
3744  032e cd0000        	call	c_rtol
3746                     ; 180     if ( t >= 3600UL*12 )
3748  0331 96            	ldw	x,sp
3749  0332 5c            	incw	x
3750  0333 cd0000        	call	c_ltor
3752  0336 ae0004        	ldw	x,#L02
3753  0339 cd0000        	call	c_lcmp
3755  033c 2511          	jrult	L7522
3756                     ; 182         t -= 3600UL*12;
3758  033e aea8c0        	ldw	x,#43200
3759  0341 bf02          	ldw	c_lreg+2,x
3760  0343 5f            	clrw	x
3761  0344 bf00          	ldw	c_lreg,x
3762  0346 96            	ldw	x,sp
3763  0347 5c            	incw	x
3764  0348 cd0000        	call	c_lgsub
3766                     ; 183         BSET( buffer[ 14 ], 5 );
3768  034b 721a0012      	bset	L5002_buffer+14,#5
3769  034f               L7522:
3770                     ; 185     if ( t >= 3600UL*8 )
3772  034f 96            	ldw	x,sp
3773  0350 5c            	incw	x
3774  0351 cd0000        	call	c_ltor
3776  0354 ae0008        	ldw	x,#L22
3777  0357 cd0000        	call	c_lcmp
3779  035a 2511          	jrult	L1622
3780                     ; 187         t -= 3600UL*8;
3782  035c ae7080        	ldw	x,#28800
3783  035f bf02          	ldw	c_lreg+2,x
3784  0361 5f            	clrw	x
3785  0362 bf00          	ldw	c_lreg,x
3786  0364 96            	ldw	x,sp
3787  0365 5c            	incw	x
3788  0366 cd0000        	call	c_lgsub
3790                     ; 188         BSET( buffer[ 14 ], 4 );
3792  0369 72180012      	bset	L5002_buffer+14,#4
3793  036d               L1622:
3794                     ; 190     if ( t >= 3600UL*4 )
3796  036d 96            	ldw	x,sp
3797  036e 5c            	incw	x
3798  036f cd0000        	call	c_ltor
3800  0372 ae000c        	ldw	x,#L42
3801  0375 cd0000        	call	c_lcmp
3803  0378 2511          	jrult	L3622
3804                     ; 192         t -= 3600UL*4;
3806  037a ae3840        	ldw	x,#14400
3807  037d bf02          	ldw	c_lreg+2,x
3808  037f 5f            	clrw	x
3809  0380 bf00          	ldw	c_lreg,x
3810  0382 96            	ldw	x,sp
3811  0383 5c            	incw	x
3812  0384 cd0000        	call	c_lgsub
3814                     ; 193         BSET( buffer[ 14 ], 3 );
3816  0387 72160012      	bset	L5002_buffer+14,#3
3817  038b               L3622:
3818                     ; 195     if ( t >= 3600UL*2 )
3820  038b 96            	ldw	x,sp
3821  038c 5c            	incw	x
3822  038d cd0000        	call	c_ltor
3824  0390 ae0010        	ldw	x,#L62
3825  0393 cd0000        	call	c_lcmp
3827  0396 2511          	jrult	L5622
3828                     ; 197         t -= 3600UL*2;
3830  0398 ae1c20        	ldw	x,#7200
3831  039b bf02          	ldw	c_lreg+2,x
3832  039d 5f            	clrw	x
3833  039e bf00          	ldw	c_lreg,x
3834  03a0 96            	ldw	x,sp
3835  03a1 5c            	incw	x
3836  03a2 cd0000        	call	c_lgsub
3838                     ; 198         BSET( buffer[ 14 ], 2 );
3840  03a5 72140012      	bset	L5002_buffer+14,#2
3841  03a9               L5622:
3842                     ; 200     if ( t >= 3600UL*1 )
3844  03a9 96            	ldw	x,sp
3845  03aa 5c            	incw	x
3846  03ab cd0000        	call	c_ltor
3848  03ae ae0014        	ldw	x,#L03
3849  03b1 cd0000        	call	c_lcmp
3851  03b4 2511          	jrult	L7622
3852                     ; 202         t -= 3600UL*1;
3854  03b6 ae0e10        	ldw	x,#3600
3855  03b9 bf02          	ldw	c_lreg+2,x
3856  03bb 5f            	clrw	x
3857  03bc bf00          	ldw	c_lreg,x
3858  03be 96            	ldw	x,sp
3859  03bf 5c            	incw	x
3860  03c0 cd0000        	call	c_lgsub
3862                     ; 203         BSET( buffer[ 14 ], 1 );
3864  03c3 72120012      	bset	L5002_buffer+14,#1
3865  03c7               L7622:
3866                     ; 205     if ( t >= 3600UL/2 )
3868  03c7 96            	ldw	x,sp
3869  03c8 5c            	incw	x
3870  03c9 cd0000        	call	c_ltor
3872  03cc ae0018        	ldw	x,#L23
3873  03cf cd0000        	call	c_lcmp
3875  03d2 2511          	jrult	L1722
3876                     ; 207         t -= 3600UL/2;
3878  03d4 ae0708        	ldw	x,#1800
3879  03d7 bf02          	ldw	c_lreg+2,x
3880  03d9 5f            	clrw	x
3881  03da bf00          	ldw	c_lreg,x
3882  03dc 96            	ldw	x,sp
3883  03dd 5c            	incw	x
3884  03de cd0000        	call	c_lgsub
3886                     ; 208         BSET( buffer[ 14 ], 0 );
3888  03e1 72100012      	bset	L5002_buffer+14,#0
3889  03e5               L1722:
3890                     ; 210     buffer[ 15 ] = 0;
3892  03e5 3f13          	clr	L5002_buffer+15
3893                     ; 211   }
3896  03e7 5b04          	addw	sp,#4
3897  03e9 81            	ret	
3925                     ; 213 void COM_433_Init( void )
3925                     ; 214   {
3926                     	switch	.text
3927  03ea               _COM_433_Init:
3931                     ; 215     COM_433_SET( );
3934  03ea 72165014      	bset	_PE_ODR,#3
3936                     ; 216     addrSetFlag = 0;
3938  03ee 3f03          	clr	L7002_addrSetFlag
3939                     ; 217     addrRMT[ 0 ] = EEP_SAVE_PTR[ 0 ];
3941  03f0 5540000000    	mov	L1102_addrRMT,16384
3942                     ; 218     addrRMT[ 1 ] = EEP_SAVE_PTR[ 1 ];
3944  03f5 5540010001    	mov	L1102_addrRMT+1,16385
3945                     ; 219     addrRMT[ 2 ] = EEP_SAVE_PTR[ 2 ];
3947  03fa 5540020002    	mov	L1102_addrRMT+2,16386
3948                     ; 220     rmtCodeEnable = 0;
3950  03ff 3f1a          	clr	_rmtCodeEnable
3951                     ; 221     rmtCodeSuccess = 0;
3953  0401 3f19          	clr	_rmtCodeSuccess
3954                     ; 222   }
3957  0403 81            	ret	
3960                     	bsct
3961  0001               L3032_state:
3962  0001 00            	dc.b	0
4012                     ; 224 void COM_433_Ctrl( void )
4012                     ; 225   {
4013                     	switch	.text
4014  0404               _COM_433_Ctrl:
4018                     ; 227     COM_433_SET( );
4021  0404 72165014      	bset	_PE_ODR,#3
4023                     ; 228     if ( addrSetFlag == 1 )
4025  0408 b603          	ld	a,L7002_addrSetFlag
4026  040a 4a            	dec	a
4027  040b 2633          	jrne	L1432
4028                     ; 230         if ( addrRMT[ 0 ] != EEP_SAVE_PTR[ 0 ] )
4030  040d b600          	ld	a,L1102_addrRMT
4031  040f c14000        	cp	a,16384
4032  0412 2708          	jreq	L3432
4033                     ; 232             EEP_WriteByte( &EEP_SAVE_PTR[ 0 ], addrRMT[ 0 ] );
4035  0414 3b0000        	push	L1102_addrRMT
4036  0417 ae4000        	ldw	x,#16384
4039  041a 201c          	jp	LC001
4040  041c               L3432:
4041                     ; 234         else if ( addrRMT[ 1 ] != EEP_SAVE_PTR[ 1 ] )
4043  041c b601          	ld	a,L1102_addrRMT+1
4044  041e c14001        	cp	a,16385
4045  0421 2708          	jreq	L7432
4046                     ; 236             EEP_WriteByte( &EEP_SAVE_PTR[ 1 ], addrRMT[ 1 ] );
4048  0423 3b0001        	push	L1102_addrRMT+1
4049  0426 ae4001        	ldw	x,#16385
4052  0429 200d          	jp	LC001
4053  042b               L7432:
4054                     ; 238         else if ( addrRMT[ 2 ] != EEP_SAVE_PTR[ 2 ] )
4056  042b b602          	ld	a,L1102_addrRMT+2
4057  042d c14002        	cp	a,16386
4058  0430 270c          	jreq	L3532
4059                     ; 240             EEP_WriteByte( &EEP_SAVE_PTR[ 2 ], addrRMT[ 2 ] );
4061  0432 3b0002        	push	L1102_addrRMT+2
4062  0435 ae4002        	ldw	x,#16386
4064  0438               LC001:
4065  0438 cd0000        	call	_EEP_WriteByte
4066  043b 84            	pop	a
4068  043c 2002          	jra	L1432
4069  043e               L3532:
4070                     ; 244             addrSetFlag = 0;
4072  043e 3f03          	clr	L7002_addrSetFlag
4073  0440               L1432:
4074                     ; 247     if ( rmtCodeEnable )
4076  0440 b61a          	ld	a,_rmtCodeEnable
4077  0442 2706          	jreq	L7532
4078                     ; 249         codeDelay = 100;
4080  0444 35640000      	mov	L3102_codeDelay,#100
4082  0448 2006          	jra	L1632
4083  044a               L7532:
4084                     ; 251     else if ( codeDelay )
4086  044a b600          	ld	a,L3102_codeDelay
4087  044c 2702          	jreq	L1632
4088                     ; 253         codeDelay--;
4090  044e 3a00          	dec	L3102_codeDelay
4091  0450               L1632:
4092                     ; 255     switch ( state )
4094  0450 b601          	ld	a,L3032_state
4095  0452 4a            	dec	a
4096  0453 2603cc051c    	jreq	L1232
4099                     ; 257         default:
4099                     ; 258             if ( count = SRL_RX_Gets( COM_433_PORT, buffer ) )
4101  0458 ae0004        	ldw	x,#L5002_buffer
4102  045b 89            	pushw	x
4103  045c 5f            	clrw	x
4104  045d cd0000        	call	_SRL_RX_Gets
4106  0460 b718          	ld	L3002_count,a
4107  0462 85            	popw	x
4108  0463 2603cc052e    	jreq	L7632
4109                     ; 260                 if ( FrameCheck( ) )
4111  0468 cd0000        	call	L5102_FrameCheck
4113  046b 4d            	tnz	a
4114  046c 27f7          	jreq	L7632
4115                     ; 262                     switch ( buffer[ 7 ] )
4117  046e b60b          	ld	a,L5002_buffer+7
4119                     ; 311                             break;
4120  0470 4a            	dec	a
4121  0471 2760          	jreq	L3132
4122  0473 a008          	sub	a,#8
4123  0475 2603cc04fd    	jreq	L7132
4124  047a a078          	sub	a,#120
4125  047c 2736          	jreq	L1132
4126  047e a007          	sub	a,#7
4127  0480 2704          	jreq	L7032
4128  0482 4a            	dec	a
4129  0483 275f          	jreq	L5132
4131  0485 81            	ret	
4132  0486               L7032:
4133                     ; 264                         case 0x88:
4133                     ; 265                             if ( buffer[ 2 ] != 0x06 ) break;
4135  0486 b606          	ld	a,L5002_buffer+2
4136  0488 a106          	cp	a,#6
4137  048a 26d9          	jrne	L7632
4140                     ; 266                             if ( !codeDelay ) break;
4142  048c b600          	ld	a,L3102_codeDelay
4143  048e 27d5          	jreq	L7632
4146                     ; 267                             rmtCodeEnable = 0;
4148  0490 3f1a          	clr	_rmtCodeEnable
4149                     ; 268                             addrRMT[ 0 ] = buffer[ 4 ];
4151  0492 450800        	mov	L1102_addrRMT,L5002_buffer+4
4152                     ; 269                             addrRMT[ 1 ] = buffer[ 5 ];
4154  0495 450901        	mov	L1102_addrRMT+1,L5002_buffer+5
4155                     ; 270                             addrRMT[ 2 ] = buffer[ 6 ];
4157  0498 450a02        	mov	L1102_addrRMT+2,L5002_buffer+6
4158                     ; 271                             rmtCodeSuccess = 1;
4160  049b 35010019      	mov	_rmtCodeSuccess,#1
4161                     ; 272                             addrSetFlag = 1;
4163  049f 35010003      	mov	L7002_addrSetFlag,#1
4164                     ; 273                             buffer[ 3 ] = 0x26;
4166  04a3 35260007      	mov	L5002_buffer+3,#38
4167                     ; 274                             FrameCheck( );
4169  04a7 cd0000        	call	L5102_FrameCheck
4171                     ; 275                             state = 1;
4173  04aa 35010001      	mov	L3032_state,#1
4174                     ; 276                             BEEP_Start( sBeepShort );
4176  04ae ae0000        	ldw	x,#_sBeepShort
4178                     ; 277                             break;
4181  04b1 cc0000        	jp	_BEEP_Start
4182  04b4               L1132:
4183                     ; 278                         case 0x81:
4183                     ; 279                             if ( buffer[ 2 ] != 0x0E ) break;
4185  04b4 b606          	ld	a,L5002_buffer+2
4186  04b6 a10e          	cp	a,#14
4187  04b8 2674          	jrne	L7632
4190                     ; 280                             if ( !AddressCheck( ) ) break;
4192  04ba cd008c        	call	L5702_AddressCheck
4194  04bd 4d            	tnz	a
4195  04be 276e          	jreq	L7632
4198                     ; 281                             CommandSettings( );
4200  04c0 cd00af        	call	L7112_CommandSettings
4202                     ; 282                             AnswerForInquiry( );
4204  04c3 cd01f8        	call	L7022_AnswerForInquiry
4206                     ; 283                             FrameCheck( );
4208  04c6 cd0000        	call	L5102_FrameCheck
4210                     ; 284                             state = 1;
4212  04c9 35010001      	mov	L3032_state,#1
4213                     ; 285                             BEEP_Start( sBeepShort );
4215  04cd ae0000        	ldw	x,#_sBeepShort
4217                     ; 286                             break;
4220  04d0 cc0000        	jp	_BEEP_Start
4221  04d3               L3132:
4222                     ; 287                         case 0x01:
4222                     ; 288                             if ( buffer[ 2 ] != 0x07 ) break;
4224  04d3 b606          	ld	a,L5002_buffer+2
4225  04d5 a107          	cp	a,#7
4226  04d7 2655          	jrne	L7632
4229                     ; 289                             if ( !AddressCheck( ) ) break;
4231  04d9 cd008c        	call	L5702_AddressCheck
4233  04dc 4d            	tnz	a
4234  04dd 274f          	jreq	L7632
4237                     ; 290                             AnswerForInquiry( );
4239  04df cd01f8        	call	L7022_AnswerForInquiry
4241                     ; 291                             FrameCheck( );
4243                     ; 292                             state = 1;
4244                     ; 293                             break;
4246  04e2 2030          	jp	LC002
4247  04e4               L5132:
4248                     ; 294                         case 0x89:
4248                     ; 295                             if ( buffer[ 2 ] != 0x07 ) break;
4250  04e4 b606          	ld	a,L5002_buffer+2
4251  04e6 a107          	cp	a,#7
4252  04e8 2644          	jrne	L7632
4255                     ; 296                             if ( !AddressCheck( ) ) break;
4257  04ea cd008c        	call	L5702_AddressCheck
4259  04ed 4d            	tnz	a
4260  04ee 273e          	jreq	L7632
4263                     ; 297                             buffer[ 3 ] = 0x26;
4265  04f0 35260007      	mov	L5002_buffer+3,#38
4266                     ; 298                             buffer[ 7 ] = 0x89;
4268  04f4 3589000b      	mov	L5002_buffer+7,#137
4269                     ; 299                             addr485 = buffer[ 8 ];
4271  04f8 450c00        	mov	_addr485,L5002_buffer+8
4272                     ; 300                             FrameCheck( );
4274                     ; 301                             state = 1;
4275                     ; 302                             break;
4277  04fb 2017          	jp	LC002
4278  04fd               L7132:
4279                     ; 303                         case 0x09:
4279                     ; 304                             if ( buffer[ 2 ] != 0x07 ) break;
4281  04fd b606          	ld	a,L5002_buffer+2
4282  04ff a107          	cp	a,#7
4283  0501 262b          	jrne	L7632
4286                     ; 305                             if ( !AddressCheck( ) ) break;
4288  0503 cd008c        	call	L5702_AddressCheck
4290  0506 4d            	tnz	a
4291  0507 2725          	jreq	L7632
4294                     ; 306                             buffer[ 3 ] = 0x26;
4296  0509 35260007      	mov	L5002_buffer+3,#38
4297                     ; 307                             buffer[ 7 ] = 0x89;
4299  050d 3589000b      	mov	L5002_buffer+7,#137
4300                     ; 308                             buffer[ 8 ] = addr485;
4302  0511 45000c        	mov	L5002_buffer+8,_addr485
4303                     ; 309                             FrameCheck( );
4306                     ; 310                             state = 1;
4308  0514               LC002:
4311  0514 cd0000        	call	L5102_FrameCheck
4314  0517 35010001      	mov	L3032_state,#1
4315                     ; 311                             break;
4318  051b 81            	ret	
4319  051c               L1232:
4320                     ; 316         case 1:
4320                     ; 317             if ( SRL_TX_Puts( COM_433_PORT, buffer, count ) )
4322  051c 3b0018        	push	L3002_count
4323  051f ae0004        	ldw	x,#L5002_buffer
4324  0522 89            	pushw	x
4325  0523 5f            	clrw	x
4326  0524 cd0000        	call	_SRL_TX_Puts
4328  0527 5b03          	addw	sp,#3
4329  0529 4d            	tnz	a
4330  052a 2702          	jreq	L7632
4331                     ; 319                 state = 0;
4333  052c 3f01          	clr	L3032_state
4334  052e               L7632:
4335                     ; 323   }
4338  052e 81            	ret	
4400                     	switch	.ubsct
4401  0000               L1102_addrRMT:
4402  0000 000000        	ds.b	3
4403  0003               L7002_addrSetFlag:
4404  0003 00            	ds.b	1
4405  0004               L5002_buffer:
4406  0004 000000000000  	ds.b	20
4407  0018               L3002_count:
4408  0018 00            	ds.b	1
4409                     	xref.b	_tprRoom
4410                     	xref	_BEEP_Start
4411                     	xref	_sBeepShort
4412                     	xref.b	_sysFreshTimer
4413                     	xref.b	_sysFreshFanLevel
4414                     	xref.b	_sysFreshOn
4415                     	xref.b	_sysAC_Timer
4416                     	xref.b	_sysAC_FanLvel
4417                     	xref.b	_sysAC_Mode
4418                     	xref.b	_sysAC_On
4419                     	xref.b	_sysSwingOn
4420                     	xref.b	_sysTprSet
4421                     	xref	_SRL_TX_Puts
4422                     	xref	_SRL_RX_Gets
4423                     	xref.b	_addr485
4424                     	xdef	_COM_433_Ctrl
4425                     	xdef	_COM_433_Init
4426  0019               _rmtCodeSuccess:
4427  0019 00            	ds.b	1
4428                     	xdef	_rmtCodeSuccess
4429  001a               _rmtCodeEnable:
4430  001a 00            	ds.b	1
4431                     	xdef	_rmtCodeEnable
4432                     	xref	_EEP_WriteByte
4433                     	xref.b	c_lreg
4434                     	xref.b	c_x
4435                     	xref.b	c_y
4455                     	xref	c_lgsub
4456                     	xref	c_lcmp
4457                     	xref	c_rtol
4458                     	xref	c_ladd
4459                     	xref	c_ltor
4460                     	xref	c_lgadd
4461                     	end

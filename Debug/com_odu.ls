   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   3                     ; Optimizer V4.2.4 - 18 Dec 2007
3362                     ; 17 void COM_ODU_Init( void )
3362                     ; 18   {
3364                     	switch	.text
3365  0000               _COM_ODU_Init:
3369                     ; 19     Tx_Num       = 0;
3371  0000 725f007e      	clr	_Tx_Num
3372                     ; 20     Tx_Length    = 0;
3374  0004 725f007d      	clr	_Tx_Length
3375                     ; 21     Tx_Delay     = 0;
3377  0008 5f            	clrw	x
3378  0009 cf007b        	ldw	_Tx_Delay,x
3379                     ; 23     Rx_Delay     = 0;
3381  000c 725f0066      	clr	_Rx_Delay
3382                     ; 24     Rx_Num       = 0;
3384  0010 725f0065      	clr	_Rx_Num
3385                     ; 25     memset( Tx_Buffer, 0x00, sizeof( Tx_Buffer ) );
3387  0014 ae0014        	ldw	x,#20
3388  0017               L6:
3389  0017 724f0066      	clr	(_Tx_Buffer-1,x)
3390  001b 5a            	decw	x
3391  001c 26f9          	jrne	L6
3392                     ; 26     memset( Rx_Buffer, 0x00, sizeof( Rx_Buffer ) );
3394  001e ae0032        	ldw	x,#50
3395  0021               L01:
3396  0021 724f0032      	clr	(_Rx_Buffer-1,x)
3397  0025 5a            	decw	x
3398  0026 26f9          	jrne	L01
3399                     ; 28     memset( &debugMessage, 0x00, sizeof( debugMessage ));
3401  0028 ae0029        	ldw	x,#41
3402  002b               L21:
3403  002b 724f0006      	clr	(_debugMessage-1,x)
3404  002f 5a            	decw	x
3405  0030 26f9          	jrne	L21
3406                     ; 29     F_KEY_COFFEE_BASKET = 0;
3408  0032 72110031      	bres	_Ctrl_Board_Status,#0
3409                     ; 30     F_KEY_TEA_BASKET    = 0;
3411  0036 72170031      	bres	_Ctrl_Board_Status,#3
3412                     ; 31     CTrl_Board_Brew_Status = 0;
3414  003a 725f0030      	clr	_CTrl_Board_Brew_Status
3415                     ; 32     Brew_Status = IDLE;
3417  003e 725f0006      	clr	_Brew_Status
3418                     ; 33   }
3421  0042 81            	ret	
3477                     ; 36 static U8 Frame_Check(U8 *pdata,U8 num)
3477                     ; 37 {
3478                     	switch	.text
3479  0043               L3032_Frame_Check:
3481  0043 89            	pushw	x
3482  0044 89            	pushw	x
3483       00000002      OFST:	set	2
3486                     ; 38   U8 check_code = 0;
3488                     ; 39   U8 i          = 0;
3490                     ; 40   check_code = *pdata;
3492  0045 1e03          	ldw	x,(OFST+1,sp)
3493  0047 f6            	ld	a,(x)
3494  0048 6b01          	ld	(OFST-1,sp),a
3495                     ; 41   for( i = 1; i< num; i++ )
3497  004a a601          	ld	a,#1
3498  004c 6b02          	ld	(OFST+0,sp),a
3500  004e 2014          	jra	L5332
3501  0050               L1332:
3502                     ; 43     check_code ^= *( pdata + i );
3504  0050 7b03          	ld	a,(OFST+1,sp)
3505  0052 97            	ld	xl,a
3506  0053 7b04          	ld	a,(OFST+2,sp)
3507  0055 1b02          	add	a,(OFST+0,sp)
3508  0057 2401          	jrnc	L61
3509  0059 5c            	incw	x
3510  005a               L61:
3511  005a 02            	rlwa	x,a
3512  005b 7b01          	ld	a,(OFST-1,sp)
3513  005d f8            	xor	a,(x)
3514  005e 6b01          	ld	(OFST-1,sp),a
3515                     ; 41   for( i = 1; i< num; i++ )
3517  0060 0c02          	inc	(OFST+0,sp)
3518  0062 7b02          	ld	a,(OFST+0,sp)
3519  0064               L5332:
3522  0064 1107          	cp	a,(OFST+5,sp)
3523  0066 25e8          	jrult	L1332
3524                     ; 45   return check_code;
3526  0068 7b01          	ld	a,(OFST-1,sp)
3529  006a 5b04          	addw	sp,#4
3530  006c 81            	ret	
3533                     	switch	.data
3534  0000               L1432_state:
3535  0000 00            	dc.b	0
3536  0001               L3432_tx_delay:
3537  0001 0000          	dc.w	0
3594                     ; 51 void Tx_Contrl( void )
3594                     ; 52 {
3595                     	switch	.text
3596  006d               _Tx_Contrl:
3600                     ; 55       if( tx_delay )
3602  006d ce0001        	ldw	x,L3432_tx_delay
3603  0070 2705          	jreq	L7732
3604                     ; 57         tx_delay--;
3606  0072 5a            	decw	x
3607  0073 cf0001        	ldw	L3432_tx_delay,x
3608                     ; 58         return;
3611  0076 81            	ret	
3612  0077               L7732:
3613                     ; 60       tx_delay = 100;//100ms
3615  0077 ae0064        	ldw	x,#100
3616  007a cf0001        	ldw	L3432_tx_delay,x
3617                     ; 61       Tx_Buffer[ 0 ] = 0x2A;
3619  007d 352a0067      	mov	_Tx_Buffer,#42
3620                     ; 62       Tx_Buffer[ 1 ] = 0x00;
3622  0081 725f0068      	clr	_Tx_Buffer+1
3623                     ; 63       Tx_Buffer[ 2 ] = 0x00;
3625  0085 725f0069      	clr	_Tx_Buffer+2
3626                     ; 64       Tx_Buffer[ 3 ] = 0x00;
3628  0089 725f006a      	clr	_Tx_Buffer+3
3629                     ; 65       Tx_Buffer[ 4 ] = 0x00;
3631  008d 725f006b      	clr	_Tx_Buffer+4
3632                     ; 66       Tx_Buffer[ 5 ] = 0x00;
3634  0091 725f006c      	clr	_Tx_Buffer+5
3635                     ; 67       Tx_Buffer[ 6 ] = 0x00;
3637  0095 725f006d      	clr	_Tx_Buffer+6
3638                     ; 68       Tx_Num = 0; 
3640  0099 725f007e      	clr	_Tx_Num
3641                     ; 69        if( System_Status == ACTIVE )
3643  009d c60000        	ld	a,_System_Status
3644  00a0 a102          	cp	a,#2
3645  00a2 2604          	jrne	L1042
3646                     ; 70           Tx_Buffer[ 1 ] |= 0x08;
3648  00a4 72160068      	bset	_Tx_Buffer+1,#3
3649  00a8               L1042:
3650                     ; 77        if( KEEP_WARM_FUNC )
3652  00a8 720300001a    	btjf	_Disp_Flag,#1,L3042
3653                     ; 79          if( Keep_Warm_Class == WARM_LO )
3655  00ad c60000        	ld	a,_Keep_Warm_Class
3656  00b0 a101          	cp	a,#1
3657  00b2 2606          	jrne	L5042
3658                     ; 80           Tx_Buffer[ 1 ] |= 0x20;
3660  00b4 721a0068      	bset	_Tx_Buffer+1,#5
3662  00b8 200d          	jra	L3042
3663  00ba               L5042:
3664                     ; 81          else if( Keep_Warm_Class == WARM_HI || Keep_Warm_Class == WARM_OFF )
3666  00ba a102          	cp	a,#2
3667  00bc 2705          	jreq	L3142
3669  00be c60000        	ld	a,_Keep_Warm_Class
3670  00c1 2604          	jrne	L3042
3671  00c3               L3142:
3672                     ; 82           Tx_Buffer[ 1 ] |= 0x40;
3674  00c3 721c0068      	bset	_Tx_Buffer+1,#6
3675  00c7               L3042:
3676                     ; 84        if( F_Brew_complete ||  F_Brew_abnormal_end )
3678  00c7 7202003105    	btjt	_Ctrl_Board_Status,#1,L7142
3680  00cc 7205003117    	btjf	_Ctrl_Board_Status,#2,L5142
3681  00d1               L7142:
3682                     ; 87         Tx_Buffer[ 1 ] |= 0X80;
3684  00d1 721e0068      	bset	_Tx_Buffer+1,#7
3686  00d5               L1242:
3687                     ; 100        switch( Start_Status )
3689  00d5 c60000        	ld	a,_Start_Status
3691                     ; 107            default:       Tx_Buffer[ 1 ] |= 0X00;break;
3692  00d8 2746          	jreq	L7342
3693  00da 4a            	dec	a
3694  00db 2729          	jreq	L7432
3695  00dd 4a            	dec	a
3696  00de 272c          	jreq	L1532
3697  00e0 4a            	dec	a
3698  00e1 272f          	jreq	L3532
3699  00e3 4a            	dec	a
3700  00e4 2736          	jreq	L5532
3705  00e6 2038          	jra	L7342
3706  00e8               L5142:
3707                     ; 89        else if( Brew_Status == CANCEL )
3709  00e8 c60006        	ld	a,_Brew_Status
3710  00eb a103          	cp	a,#3
3711  00ed 2606          	jrne	L3242
3712                     ; 91           Start_Status = OFF;
3714  00ef 725f0000      	clr	_Start_Status
3716  00f3 20e0          	jra	L1242
3717  00f5               L3242:
3718                     ; 93        else if( errorCode0 )
3720  00f5 c60001        	ld	a,_errorCode0
3721  00f8 2706          	jreq	L7242
3722                     ; 95         Tx_Buffer[ 6 ] |= 0X80;
3724  00fa 721e006d      	bset	_Tx_Buffer+6,#7
3726  00fe 20d5          	jra	L1242
3727  0100               L7242:
3728                     ; 97        else if( Brew_Status == IDLE )
3730  0100 725d0006      	tnz	_Brew_Status
3731  0104 20cf          	jra	L1242
3732                     ; 102            case OFF:      Tx_Buffer[ 1 ] |= 0X00;break;
3736  0106               L7432:
3737                     ; 103            case ON:       Tx_Buffer[ 1 ] |= 0X01;break;
3739  0106 72100068      	bset	_Tx_Buffer+1,#0
3742  010a 2014          	jra	L7342
3743  010c               L1532:
3744                     ; 104            case PAUSE:    Tx_Buffer[ 1 ] |= 0X02;break;
3746  010c 72120068      	bset	_Tx_Buffer+1,#1
3749  0110 200e          	jra	L7342
3750  0112               L3532:
3751                     ; 105            case CANCEL: Tx_Buffer[ 1 ] |= 0X03;break;
3753  0112 c60068        	ld	a,_Tx_Buffer+1
3754  0115 aa03          	or	a,#3
3755  0117 c70068        	ld	_Tx_Buffer+1,a
3758  011a 2004          	jra	L7342
3759  011c               L5532:
3760                     ; 106            case UNPAUSE:  Tx_Buffer[ 1 ] |= 0X04;break;
3762  011c 72140068      	bset	_Tx_Buffer+1,#2
3765  0120               L7342:
3766                     ; 109        if( CLEAN_FUNC_FLAG )
3768  0120 7203000006    	btjf	_Disp_Flag1,#1,L1442
3769                     ; 111          Tx_Buffer[ 2 ] = 3;
3771  0125 35030069      	mov	_Tx_Buffer+2,#3
3773  0129 2015          	jra	L3442
3774  012b               L1442:
3775                     ; 113        else if( COFFEE == Brew_Select)
3777  012b c60000        	ld	a,_Brew_Select
3778  012e a101          	cp	a,#1
3779  0130 2606          	jrne	L5442
3780                     ; 115          Tx_Buffer[ 2 ] = 1;
3782  0132 35010069      	mov	_Tx_Buffer+2,#1
3784  0136 2008          	jra	L3442
3785  0138               L5442:
3786                     ; 117        else if( TEA == Brew_Select )
3788  0138 a102          	cp	a,#2
3789  013a 2604          	jrne	L3442
3790                     ; 119          Tx_Buffer[ 2 ] = 2;
3792  013c 35020069      	mov	_Tx_Buffer+2,#2
3793  0140               L3442:
3794                     ; 122       Tx_Buffer[ 3 ] = Knob0_count [ Brew_Select ];
3796  0140 c60000        	ld	a,_Brew_Select
3797  0143 5f            	clrw	x
3798  0144 97            	ld	xl,a
3799  0145 d60000        	ld	a,(_Knob0_count,x)
3800  0148 c7006a        	ld	_Tx_Buffer+3,a
3801                     ; 123       Tx_Buffer[ 4 ] = Knob1_count [ Brew_Select ];
3803  014b c60000        	ld	a,_Brew_Select
3804  014e 5f            	clrw	x
3805  014f 97            	ld	xl,a
3806  0150 d60000        	ld	a,(_Knob1_count,x)
3807  0153 c7006b        	ld	_Tx_Buffer+4,a
3808                     ; 124       Tx_Buffer[ 5 ] = Knob2_count;
3810  0156 550000006c    	mov	_Tx_Buffer+5,_Knob2_count
3811                     ; 125       if( forcePump )
3813  015b c60000        	ld	a,_forcePump
3814  015e 2704          	jreq	L3542
3815                     ; 126       Tx_Buffer[ 6 ] |= 0x01;
3817  0160 7210006d      	bset	_Tx_Buffer+6,#0
3818  0164               L3542:
3819                     ; 128       Tx_Length = 7;
3821  0164 3507007d      	mov	_Tx_Length,#7
3822                     ; 129       Tx_Buffer[ Tx_Length ] = Frame_Check( Tx_Buffer,Tx_Length );
3824  0168 4b07          	push	#7
3825  016a ae0067        	ldw	x,#_Tx_Buffer
3826  016d cd0043        	call	L3032_Frame_Check
3828  0170 5b01          	addw	sp,#1
3829  0172 c7006e        	ld	_Tx_Buffer+7,a
3830                     ; 130       Tx_Length++;
3832  0175 725c007d      	inc	_Tx_Length
3834  0179               L7542:
3835                     ; 131       while(!(UART2_SR & 0x80)) // 等待发送数据寄存器为空
3837  0179 720f5240fb    	btjf	_UART2_SR,#7,L7542
3838                     ; 133       UART2_DR = Tx_Buffer[ 0 ];
3840  017e 5500675241    	mov	_UART2_DR,_Tx_Buffer
3841                     ; 134       UART2_CR2 |= 0X80;// 发送中断允许
3843  0183 721e5245      	bset	_UART2_CR2,#7
3844                     ; 135 }
3847  0187 81            	ret	
3930                     ; 138 void Rx_Control( void )
3930                     ; 139 {
3931                     	switch	.text
3932  0188               _Rx_Control:
3934  0188 5206          	subw	sp,#6
3935       00000006      OFST:	set	6
3938                     ; 140   U8 rx_length  = 0;
3940                     ; 141   U8 check_code = 0;
3942                     ; 142   if( Rx_Delay )
3944  018a c60066        	ld	a,_Rx_Delay
3945  018d 2706          	jreq	L3452
3946                     ; 144     Rx_Delay--;
3948  018f 725a0066      	dec	_Rx_Delay
3949                     ; 145     return;
3951  0193 2012          	jra	L43
3952  0195               L3452:
3953                     ; 147   if(( Rx_Num < 3 ) ||( Rx_Buffer[ 0 ] != 0xAA ))
3955  0195 c60065        	ld	a,_Rx_Num
3956  0198 a103          	cp	a,#3
3957  019a 2507          	jrult	L7452
3959  019c c60033        	ld	a,_Rx_Buffer
3960  019f a1aa          	cp	a,#170
3961  01a1 2707          	jreq	L5452
3962  01a3               L7452:
3963                     ; 149     Rx_Num = 0;
3965  01a3 725f0065      	clr	_Rx_Num
3966                     ; 150     return;
3967  01a7               L43:
3970  01a7 5b06          	addw	sp,#6
3971  01a9 81            	ret	
3972  01aa               L5452:
3973                     ; 152   rx_length = Rx_Num;
3975  01aa c60065        	ld	a,_Rx_Num
3976  01ad 6b06          	ld	(OFST+0,sp),a
3977                     ; 153   Rx_Num    = 0;
3979  01af 725f0065      	clr	_Rx_Num
3980                     ; 154   check_code = Frame_Check( Rx_Buffer,rx_length - 1 );
3982  01b3 4a            	dec	a
3983  01b4 88            	push	a
3984  01b5 ae0033        	ldw	x,#_Rx_Buffer
3985  01b8 cd0043        	call	L3032_Frame_Check
3987  01bb 5b01          	addw	sp,#1
3988  01bd 6b05          	ld	(OFST-1,sp),a
3989                     ; 155   if( Rx_Buffer[ rx_length - 1 ] == check_code)
3991  01bf 7b06          	ld	a,(OFST+0,sp)
3992  01c1 5f            	clrw	x
3993  01c2 97            	ld	xl,a
3994  01c3 5a            	decw	x
3995  01c4 d60033        	ld	a,(_Rx_Buffer,x)
3996  01c7 1105          	cp	a,(OFST-1,sp)
3997  01c9 26dc          	jrne	L43
3998                     ; 157     U8 length = sizeof( debugMessage );
4000  01cb a629          	ld	a,#41
4001  01cd 6b05          	ld	(OFST-1,sp),a
4002                     ; 160     U8 i = 0;
4004                     ; 164      switch( Rx_Buffer[ 1 ] & 0x03 )
4006  01cf c60034        	ld	a,_Rx_Buffer+1
4007  01d2 a403          	and	a,#3
4009                     ; 168        case 2:  F_KEY_TEA_BASKET = 1;F_KEY_COFFEE_BASKET = 0;break;
4010  01d4 4a            	dec	a
4011  01d5 2709          	jreq	L5642
4012  01d7 4a            	dec	a
4013  01d8 2710          	jreq	L7642
4014                     ; 166        default: F_KEY_TEA_BASKET = 0;F_KEY_COFFEE_BASKET = 0;break;
4016  01da 72170031      	bres	_Ctrl_Board_Status,#3
4020  01de 200e          	jp	LC001
4021  01e0               L5642:
4022                     ; 167        case 1:  F_KEY_TEA_BASKET = 0;F_KEY_COFFEE_BASKET = 1;break;
4024  01e0 72170031      	bres	_Ctrl_Board_Status,#3
4027  01e4 72100031      	bset	_Ctrl_Board_Status,#0
4030  01e8 2008          	jra	L5552
4031  01ea               L7642:
4032                     ; 168        case 2:  F_KEY_TEA_BASKET = 1;F_KEY_COFFEE_BASKET = 0;break;
4034  01ea 72160031      	bset	_Ctrl_Board_Status,#3
4037  01ee               LC001:
4039  01ee 72110031      	bres	_Ctrl_Board_Status,#0
4042  01f2               L5552:
4043                     ; 170      switch(( Rx_Buffer[ 1 ] >> 2 ) & 0x07)
4045  01f2 c60034        	ld	a,_Rx_Buffer+1
4046  01f5 a41c          	and	a,#28
4047  01f7 44            	srl	a
4048  01f8 44            	srl	a
4050                     ; 185                 Complete_Sound_Delay = 5; break;
4051  01f9 4a            	dec	a
4052  01fa 2718          	jreq	L3742
4053  01fc 4a            	dec	a
4054  01fd 271b          	jreq	L5742
4055  01ff 4a            	dec	a
4056  0200 271e          	jreq	L7742
4057  0202 4a            	dec	a
4058  0203 2721          	jreq	L1052
4059  0205 4a            	dec	a
4060  0206 2724          	jreq	L3052
4061  0208 4a            	dec	a
4062  0209 2727          	jreq	L5052
4063  020b 4a            	dec	a
4064  020c 272e          	jreq	L7052
4065                     ; 172        default: Brew_Status = IDLE;        break;
4067  020e 725f0006      	clr	_Brew_Status
4070  0212 2038          	jra	L1652
4071  0214               L3742:
4072                     ; 173        case 1:  Brew_Status = PREHEAT;      break;
4074  0214 35010006      	mov	_Brew_Status,#1
4077  0218 2032          	jra	L1652
4078  021a               L5742:
4079                     ; 174        case 2:  Brew_Status = RUNING;      break;
4081  021a 35020006      	mov	_Brew_Status,#2
4084  021e 202c          	jra	L1652
4085  0220               L7742:
4086                     ; 175        case 3:  Brew_Status = PAUSE;      break;
4088  0220 35020006      	mov	_Brew_Status,#2
4091  0224 2026          	jra	L1652
4092  0226               L1052:
4093                     ; 176        case 4:  Brew_Status = CANCELED;    break;
4095  0226 35040006      	mov	_Brew_Status,#4
4098  022a 2020          	jra	L1652
4099  022c               L3052:
4100                     ; 177        case 5:  Brew_Status = RESUME;      break;
4102  022c 35050006      	mov	_Brew_Status,#5
4105  0230 201a          	jra	L1652
4106  0232               L5052:
4107                     ; 178        case 6:  Brew_Status = NORMAL_END;  
4109  0232 35060006      	mov	_Brew_Status,#6
4110                     ; 179                 F_Brew_complete     = 1;
4112  0236 72120031      	bset	_Ctrl_Board_Status,#1
4113                     ; 180                 F_KEEP_WARM_START   = 1;
4114                     ; 181                 Complete_Sound_Delay = 5;  break;
4117  023a 2008          	jp	LC002
4118  023c               L7052:
4119                     ; 182        case 7:  Brew_Status = ABNORMAL_END;
4121  023c 35070006      	mov	_Brew_Status,#7
4122                     ; 183                 F_Brew_abnormal_end = 1;
4124  0240 72140031      	bset	_Ctrl_Board_Status,#2
4125                     ; 184                 F_KEEP_WARM_START   = 1;
4127                     ; 185                 Complete_Sound_Delay = 5; break;
4129  0244               LC002:
4131  0244 72180031      	bset	_Ctrl_Board_Status,#4
4133  0248 35050000      	mov	_Complete_Sound_Delay,#5
4136  024c               L1652:
4137                     ; 189      errorCode0 = Rx_Buffer[ 2 ];
4139  024c 5500350001    	mov	_errorCode0,_Rx_Buffer+2
4140                     ; 191      Brew_Bar = Rx_Buffer[ 1 ] >> 5; 
4142  0251 c60034        	ld	a,_Rx_Buffer+1
4143  0254 4e            	swap	a
4144  0255 a40e          	and	a,#14
4145  0257 44            	srl	a
4146  0258 c70005        	ld	_Brew_Bar,a
4147                     ; 194      p_data1 = (U8 *)&debugMessage.commHeadID;
4149  025b ae0007        	ldw	x,#_debugMessage
4150  025e 1f03          	ldw	(OFST-3,sp),x
4151                     ; 195      p_data = &Rx_Buffer[ 3 ];
4153  0260 ae0036        	ldw	x,#_Rx_Buffer+3
4154  0263 1f01          	ldw	(OFST-5,sp),x
4155                     ; 197      for(i = 0;i<length;i++ )
4157  0265 0f06          	clr	(OFST+0,sp)
4159  0267 201c          	jra	L7652
4160  0269               L3652:
4161                     ; 199        *(p_data1+i)  =  *(p_data+i);
4163  0269 7b03          	ld	a,(OFST-3,sp)
4164  026b 97            	ld	xl,a
4165  026c 7b04          	ld	a,(OFST-2,sp)
4166  026e 1b06          	add	a,(OFST+0,sp)
4167  0270 2401          	jrnc	L03
4168  0272 5c            	incw	x
4169  0273               L03:
4170  0273 02            	rlwa	x,a
4171  0274 89            	pushw	x
4172  0275 7b03          	ld	a,(OFST-3,sp)
4173  0277 97            	ld	xl,a
4174  0278 7b04          	ld	a,(OFST-2,sp)
4175  027a 1b08          	add	a,(OFST+2,sp)
4176  027c 2401          	jrnc	L23
4177  027e 5c            	incw	x
4178  027f               L23:
4179  027f 02            	rlwa	x,a
4180  0280 f6            	ld	a,(x)
4181  0281 85            	popw	x
4182  0282 f7            	ld	(x),a
4183                     ; 197      for(i = 0;i<length;i++ )
4185  0283 0c06          	inc	(OFST+0,sp)
4186  0285               L7652:
4189  0285 7b06          	ld	a,(OFST+0,sp)
4190  0287 1105          	cp	a,(OFST-1,sp)
4191  0289 25de          	jrult	L3652
4192                     ; 202 }
4194  028b cc01a7        	jra	L43
4219                     ; 203 void COM_ODU_Ctrl( void )
4219                     ; 204 {
4220                     	switch	.text
4221  028e               _COM_ODU_Ctrl:
4225                     ; 205   Tx_Contrl( );
4227  028e cd006d        	call	_Tx_Contrl
4229                     ; 206   Rx_Control( );
4232                     ; 207 }
4235  0291 cc0188        	jp	_Rx_Control
4248                     	xdef	_Rx_Control
4249                     	xdef	_Tx_Contrl
4250                     	xref	_Disp_Flag1
4251                     	xref	_Disp_Flag
4252                     	xref	_forcePump
4253                     	xref	_Complete_Sound_Delay
4254                     	xref	_Keep_Warm_Class
4255                     	xref	_Brew_Select
4256                     	xref	_Start_Status
4257                     	xref	_System_Status
4258                     	xref	_Knob2_count
4259                     	xref	_Knob1_count
4260                     	xref	_Knob0_count
4261                     	xdef	_COM_ODU_Ctrl
4262                     	xdef	_COM_ODU_Init
4263                     	switch	.bss
4264  0000               _T_BOIL:
4265  0000 00            	ds.b	1
4266                     	xdef	_T_BOIL
4267  0001               _errorCode0:
4268  0001 00            	ds.b	1
4269                     	xdef	_errorCode0
4270  0002               _Digital_Index:
4271  0002 00            	ds.b	1
4272                     	xdef	_Digital_Index
4273  0003               _CLean_Blink_10count:
4274  0003 00            	ds.b	1
4275                     	xdef	_CLean_Blink_10count
4276  0004               _CLean_Class:
4277  0004 00            	ds.b	1
4278                     	xdef	_CLean_Class
4279  0005               _Brew_Bar:
4280  0005 00            	ds.b	1
4281                     	xdef	_Brew_Bar
4282  0006               _Brew_Status:
4283  0006 00            	ds.b	1
4284                     	xdef	_Brew_Status
4285  0007               _debugMessage:
4286  0007 000000000000  	ds.b	41
4287                     	xdef	_debugMessage
4288  0030               _CTrl_Board_Brew_Status:
4289  0030 00            	ds.b	1
4290                     	xdef	_CTrl_Board_Brew_Status
4291  0031               _Ctrl_Board_Status:
4292  0031 00            	ds.b	1
4293                     	xdef	_Ctrl_Board_Status
4294  0032               _Error_Flag0:
4295  0032 00            	ds.b	1
4296                     	xdef	_Error_Flag0
4297  0033               _Rx_Buffer:
4298  0033 000000000000  	ds.b	50
4299                     	xdef	_Rx_Buffer
4300  0065               _Rx_Num:
4301  0065 00            	ds.b	1
4302                     	xdef	_Rx_Num
4303  0066               _Rx_Delay:
4304  0066 00            	ds.b	1
4305                     	xdef	_Rx_Delay
4306  0067               _Tx_Buffer:
4307  0067 000000000000  	ds.b	20
4308                     	xdef	_Tx_Buffer
4309  007b               _Tx_Delay:
4310  007b 0000          	ds.b	2
4311                     	xdef	_Tx_Delay
4312  007d               _Tx_Length:
4313  007d 00            	ds.b	1
4314                     	xdef	_Tx_Length
4315  007e               _Tx_Num:
4316  007e 00            	ds.b	1
4317                     	xdef	_Tx_Num
4318                     	xref	_memset
4338                     	end

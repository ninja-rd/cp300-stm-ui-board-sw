   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   3                     ; Optimizer V4.2.4 - 18 Dec 2007
2834                     ; 14 void FAN_Init( void )
2834                     ; 15   {
2836                     	switch	.text
2837  0000               _FAN_Init:
2841                     ; 16     fanLevel = FAN_OFF;
2843  0000 3f00          	clr	_fanLevel
2844                     ; 17   }
2847  0002 81            	ret	
2942                     ; 19 void FAN_Ctrl( void )
2942                     ; 20   {
2943                     	switch	.text
2944  0003               _FAN_Ctrl:
2946  0003 5206          	subw	sp,#6
2947       00000006      OFST:	set	6
2950                     ; 23     switch ( fanLevel )
2952  0005 b600          	ld	a,_fanLevel
2954                     ; 56             break;
2955  0007 4a            	dec	a
2956  0008 2714          	jreq	L7102
2957  000a 4a            	dec	a
2958  000b 2716          	jreq	L1202
2959  000d 4a            	dec	a
2960  000e 2743          	jreq	LC002
2961  0010 4a            	dec	a
2962  0011 2715          	jreq	L5202
2963  0013 4a            	dec	a
2964  0014 2733          	jreq	LC003
2965  0016 4a            	dec	a
2966  0017 2714          	jreq	L1302
2967  0019 4a            	dec	a
2968  001a 2716          	jreq	L3302
2969                     ; 25         default:
2969                     ; 26             ccr.word = 0;
2970                     ; 27             break;
2972  001c 2043          	jp	L5112
2973  001e               L7102:
2974                     ; 28         case FAN_LOW_1:
2974                     ; 29             ccr.word = 80;
2976  001e ae0050        	ldw	x,#80
2977                     ; 30             break;
2979  0021 203f          	jra	L3012
2980  0023               L1202:
2981                     ; 31         case FAN_LOW_2:
2981                     ; 32             ccr.word = 150;
2983  0023 ae0096        	ldw	x,#150
2984                     ; 33             break;
2986  0026 203a          	jra	L3012
2987                     ; 34         case FAN_MIDDLE_1:
2987                     ; 35             ccr.word = 250;
2988                     ; 36             break;
2990  0028               L5202:
2991                     ; 37         case FAN_MIDDLE_2:
2991                     ; 38             ccr.word = 350;
2993  0028 ae015e        	ldw	x,#350
2994                     ; 39             break;
2996  002b 2035          	jra	L3012
2997                     ; 40         case FAN_HIGH:
2997                     ; 41             ccr.word = 450;
2998                     ; 42             break;
3000  002d               L1302:
3001                     ; 43         case FAN_SUPER_HIGH:
3001                     ; 44             ccr.word = 600;
3003  002d ae0258        	ldw	x,#600
3004                     ; 45             break;
3006  0030 2030          	jra	L3012
3007  0032               L3302:
3008                     ; 46         case FAN_AUTO:
3008                     ; 47             tpr = tprRoom.val - ( ( S16 )sysTprSet << 8 );
3010  0032 5f            	clrw	x
3011  0033 b600          	ld	a,_sysTprSet
3012  0035 2a01          	jrpl	L01
3013  0037 53            	cplw	x
3014  0038               L01:
3015  0038 97            	ld	xl,a
3016  0039 4f            	clr	a
3017  003a 02            	rlwa	x,a
3018  003b 1f01          	ldw	(OFST-5,sp),x
3019  003d be00          	ldw	x,_tprRoom
3020  003f 72f001        	subw	x,(OFST-5,sp)
3021  0042 1f03          	ldw	(OFST-3,sp),x
3022                     ; 48             if ( tpr >= TPRREF( 4 ) )
3024  0044 a30400        	cpw	x,#1024
3025  0047 2f05          	jrslt	L5012
3026                     ; 49                 ccr.word = 450;
3028  0049               LC003:
3030  0049 ae01c2        	ldw	x,#450
3032  004c 2014          	jra	L3012
3033  004e               L5012:
3034                     ; 50             else if ( tpr >= TPRREF( 2 ) )
3036  004e a30200        	cpw	x,#512
3037  0051 2f05          	jrslt	L1112
3038                     ; 51                 ccr.word = 250;
3040  0053               LC002:
3042  0053 ae00fa        	ldw	x,#250
3044  0056 200a          	jra	L3012
3045  0058               L1112:
3046                     ; 52             else if ( tpr >= TPRREF( 0 ) )
3048  0058 1e03          	ldw	x,(OFST-3,sp)
3049  005a 2b05          	jrmi	L5112
3050                     ; 53                 ccr.word = 80;
3052  005c ae0050        	ldw	x,#80
3054  005f 2001          	jra	L3012
3055  0061               L5112:
3056                     ; 55                 ccr.word = 0;
3059  0061 5f            	clrw	x
3060  0062               L3012:
3061  0062 1f05          	ldw	(OFST-1,sp),x
3062                     ; 58     TIM1_CCR3H = ccr.byte.H;
3064  0064 7b05          	ld	a,(OFST-1,sp)
3065  0066 c75269        	ld	_TIM1_CCR3H,a
3066                     ; 59     TIM1_CCR3L = ccr.byte.L;
3068  0069 7b06          	ld	a,(OFST+0,sp)
3069  006b c7526a        	ld	_TIM1_CCR3L,a
3070                     ; 60     TIM1_EGR = 0X20;
3072  006e 35205257      	mov	_TIM1_EGR,#32
3073                     ; 61   }
3076  0072 5b06          	addw	sp,#6
3077  0074 81            	ret	
3090                     	xref.b	_tprRoom
3091                     	xref.b	_sysTprSet
3092                     	xdef	_FAN_Ctrl
3093                     	xdef	_FAN_Init
3094                     	switch	.ubsct
3095  0000               _fanLevel:
3096  0000 00            	ds.b	1
3097                     	xdef	_fanLevel
3117                     	end

   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   3                     ; Optimizer V4.2.4 - 18 Dec 2007
2886                     ; 26 void EEP_WriteByte( U8 * p, U8 byte )
2886                     ; 27   {
2888                     	switch	.text
2889  0000               _EEP_WriteByte:
2891  0000 89            	pushw	x
2892       00000000      OFST:	set	0
2895                     ; 28     FLASH_CR1   = 0X00;
2897  0001 725f505a      	clr	_FLASH_CR1
2898                     ; 29     FLASH_DUKR  = 0XAE;
2900  0005 35ae5064      	mov	_FLASH_DUKR,#174
2901                     ; 30     FLASH_DUKR  = 0X56;
2903  0009 35565064      	mov	_FLASH_DUKR,#86
2904                     ; 31     FLASH_CR2   = 0X00;
2906  000d 725f505b      	clr	_FLASH_CR2
2907                     ; 32     FLASH_NCR2  = 0XFF;
2909  0011 35ff505c      	mov	_FLASH_NCR2,#255
2910                     ; 33     *p = byte;
2912  0015 7b05          	ld	a,(OFST+5,sp)
2913  0017 1e01          	ldw	x,(OFST+1,sp)
2914  0019 f7            	ld	(x),a
2915                     ; 34     FLASH_CR2   = 0X00;
2917  001a 725f505b      	clr	_FLASH_CR2
2918                     ; 35     FLASH_NCR2  = 0XFF;
2920  001e 35ff505c      	mov	_FLASH_NCR2,#255
2921                     ; 36     FLASH_IAPSR = 0XE7;
2923  0022 35e7505f      	mov	_FLASH_IAPSR,#231
2924                     ; 37   }
2927  0026 85            	popw	x
2928  0027 81            	ret	
3067                     ; 39 void MCU_Init( void )
3067                     ; 40   {
3068                     	switch	.text
3069  0028               _MCU_Init:
3071  0028 5204          	subw	sp,#4
3072       00000004      OFST:	set	4
3075                     ; 41     __DI( );
3078  002a 9b            	sim	
3080                     ; 43     RST_SR      = 0x00;
3082  002b 725f50b3      	clr	_RST_SR
3083                     ; 44     CLK_ICKR    = 0x0D;
3085  002f 350d50c0      	mov	_CLK_ICKR,#13
3086                     ; 45     CLK_ECKR    = 0x00;
3088  0033 725f50c1      	clr	_CLK_ECKR
3089                     ; 46     CLK_SWCR    = 0x02;
3091  0037 350250c5      	mov	_CLK_SWCR,#2
3092                     ; 47     CLK_SWR     = 0xE1;
3094  003b 35e150c4      	mov	_CLK_SWR,#225
3095                     ; 48     CLK_CKDIVR  = 0x00;
3097  003f 725f50c6      	clr	_CLK_CKDIVR
3098                     ; 49     CLK_PCKENR1 = 0xFF;
3100  0043 35ff50c7      	mov	_CLK_PCKENR1,#255
3101                     ; 50     CLK_PCKENR2 = 0xFF;
3103  0047 35ff50ca      	mov	_CLK_PCKENR2,#255
3104                     ; 51     CLK_CSSR    = 0x01;
3106  004b 350150c8      	mov	_CLK_CSSR,#1
3107                     ; 52     CLK_CCOR    = 0x00;
3109  004f 725f50c9      	clr	_CLK_CCOR
3110                     ; 53     CLK_SWIMCCR = 0x00;
3112  0053 725f50cd      	clr	_CLK_SWIMCCR
3113                     ; 55     IWDG_KR     = 0x55;
3115  0057 355550e0      	mov	_IWDG_KR,#85
3116                     ; 56     IWDG_PR     = 0x04;
3118  005b 350450e1      	mov	_IWDG_PR,#4
3119                     ; 57     IWDG_RLR    = 250;
3121  005f 35fa50e2      	mov	_IWDG_RLR,#250
3122                     ; 58     IWDG_KR     = 0xAA;
3124  0063 35aa50e0      	mov	_IWDG_KR,#170
3125                     ; 59     IWDG_KR     = 0xCC;
3127  0067 35cc50e0      	mov	_IWDG_KR,#204
3128                     ; 61 #define CFG_PF_CR2      0X10
3131  006b ae2710        	ldw	x,#10000
3132  006e 1f03          	ldw	(OFST-1,sp),x
3133  0070 5f            	clrw	x
3134  0071 1f01          	ldw	(OFST-3,sp),x
3136  0073 2004          	jra	L7502
3137  0075               L3502:
3138                     ; 165 @inline void MCU_Delay( U32 n ) { while ( n-- ) __WDTC( ); }
3140  0075 35aa50e0      	mov	_IWDG_KR,#170
3141  0079               L7502:
3144  0079 96            	ldw	x,sp
3145  007a 5c            	incw	x
3146  007b cd0000        	call	c_ltor
3148  007e 96            	ldw	x,sp
3149  007f 5c            	incw	x
3150  0080 a601          	ld	a,#1
3151  0082 cd0000        	call	c_lgsbc
3153  0085 cd0000        	call	c_lrzmp
3155  0088 26eb          	jrne	L3502
3156                     ; 82     ADC_TDRL    = CFG_ADC_TDRL;
3159  008a 725f5407      	clr	_ADC_TDRL
3160                     ; 83     ADC_TDRH    = CFG_ADC_TDRH;
3162  008e 725f5406      	clr	_ADC_TDRH
3163                     ; 84     PA_ODR      = CFG_PA_ODR;
3165  0092 725f5000      	clr	_PA_ODR
3166                     ; 85     PA_DDR      = CFG_PA_DDR;
3168  0096 35045002      	mov	_PA_DDR,#4
3169                     ; 86     PA_CR1      = CFG_PA_CR1;
3171  009a 35045003      	mov	_PA_CR1,#4
3172                     ; 87     PA_CR2      = CFG_PA_CR2;
3174  009e 35045004      	mov	_PA_CR2,#4
3175                     ; 88     PB_ODR      = CFG_PB_ODR;
3177  00a2 35035005      	mov	_PB_ODR,#3
3178                     ; 89     PB_DDR      = CFG_PB_DDR;
3180  00a6 35035007      	mov	_PB_DDR,#3
3181                     ; 90     PB_CR1      = CFG_PB_CR1;
3183  00aa 353f5008      	mov	_PB_CR1,#63
3184                     ; 91     PB_CR2      = CFG_PB_CR2;
3186  00ae 35035009      	mov	_PB_CR2,#3
3187                     ; 92     PC_ODR      = CFG_PC_ODR;
3189  00b2 725f500a      	clr	_PC_ODR
3190                     ; 93     PC_DDR      = CFG_PC_DDR;
3192  00b6 35fe500c      	mov	_PC_DDR,#254
3193                     ; 94     PC_CR1      = CFG_PC_CR1;
3195  00ba 35fe500d      	mov	_PC_CR1,#254
3196                     ; 95     PC_CR2      = CFG_PC_CR2;
3198  00be 35fe500e      	mov	_PC_CR2,#254
3199                     ; 97     PD_ODR      = CFG_PD_ODR;
3201  00c2 35fe500f      	mov	_PD_ODR,#254
3202                     ; 98     PD_DDR      = CFG_PD_DDR;
3204  00c6 35bf5011      	mov	_PD_DDR,#191
3205                     ; 99     PD_CR1      = CFG_PD_CR1;
3207  00ca 35bf5012      	mov	_PD_CR1,#191
3208                     ; 100     PD_CR2      = CFG_PD_CR2;
3210  00ce 35bf5013      	mov	_PD_CR2,#191
3211                     ; 101     PE_ODR      = CFG_PE_ODR;
3213  00d2 725f5014      	clr	_PE_ODR
3214                     ; 102     PE_DDR      = CFG_PE_DDR;
3216  00d6 35205016      	mov	_PE_DDR,#32
3217                     ; 103     PE_CR1      = CFG_PE_CR1;
3219  00da 35205017      	mov	_PE_CR1,#32
3220                     ; 104     PE_CR2      = CFG_PE_CR2;
3222  00de 725f5018      	clr	_PE_CR2
3223                     ; 106     PF_ODR      = CFG_PF_ODR;
3225  00e2 35105019      	mov	_PF_ODR,#16
3226                     ; 107     PF_DDR      = CFG_PF_DDR;
3228  00e6 3510501b      	mov	_PF_DDR,#16
3229                     ; 108     PF_CR1      = CFG_PF_CR1;
3231  00ea 3510501c      	mov	_PF_CR1,#16
3232                     ; 109     PF_CR2      = CFG_PF_CR2;
3234  00ee 3510501d      	mov	_PF_CR2,#16
3235                     ; 111     EXTI_CR1    = CFG_EXTI_CR1;
3237  00f2 725f50a0      	clr	_EXTI_CR1
3238                     ; 112     EXTI_CR2    = CFG_EXTI_CR2;
3240  00f6 725f50a1      	clr	_EXTI_CR2
3241                     ; 114     ITC_SPR1    = CFG_ITC_SPR1;
3243  00fa 35557f70      	mov	_ITC_SPR1,#85
3244                     ; 115     ITC_SPR2    = CFG_ITC_SPR2;
3246  00fe 35557f71      	mov	_ITC_SPR2,#85
3247                     ; 116     ITC_SPR3    = CFG_ITC_SPR3;
3249  0102 35557f72      	mov	_ITC_SPR3,#85
3250                     ; 117     ITC_SPR4    = CFG_ITC_SPR4;
3252  0106 35457f73      	mov	_ITC_SPR4,#69
3253                     ; 118     ITC_SPR5    = CFG_ITC_SPR5;
3255  010a 35557f74      	mov	_ITC_SPR5,#85
3256                     ; 119     ITC_SPR6    = CFG_ITC_SPR6;
3258  010e 35517f75      	mov	_ITC_SPR6,#81
3259                     ; 120     ITC_SPR7    = CFG_ITC_SPR7;
3261  0112 35557f76      	mov	_ITC_SPR7,#85
3262                     ; 134     TIM1_CR1   = 0X80;
3264  0116 35805250      	mov	_TIM1_CR1,#128
3265                     ; 135     TIM1_CR2   = 0X00;
3267  011a 725f5251      	clr	_TIM1_CR2
3268                     ; 136     TIM1_SMCR  = 0X00;
3270  011e 725f5252      	clr	_TIM1_SMCR
3271                     ; 137     TIM1_ETR   = 0X00;
3273  0122 725f5253      	clr	_TIM1_ETR
3274                     ; 138     TIM1_SR1   = 0X00;
3276  0126 725f5255      	clr	_TIM1_SR1
3277                     ; 139     TIM1_SR2   = 0X00;
3279  012a 725f5256      	clr	_TIM1_SR2
3280                     ; 140     TIM1_IER   = 0X01;
3282  012e 35015254      	mov	_TIM1_IER,#1
3283                     ; 141     TIM1_EGR   = 0X00;
3285  0132 725f5257      	clr	_TIM1_EGR
3286                     ; 142     TIM1_CCMR1 = 0X00;
3288  0136 725f5258      	clr	_TIM1_CCMR1
3289                     ; 143     TIM1_CCMR2 = 0X00;
3291  013a 725f5259      	clr	_TIM1_CCMR2
3292                     ; 144     TIM1_CCMR3 = 0X6C;
3294  013e 356c525a      	mov	_TIM1_CCMR3,#108
3295                     ; 145     TIM1_CCMR4 = 0X00;
3297  0142 725f525b      	clr	_TIM1_CCMR4
3298                     ; 146     TIM1_CCER1 = 0X00;
3300  0146 725f525c      	clr	_TIM1_CCER1
3301                     ; 147     TIM1_CCER2 = 0X0C;
3303  014a 350c525d      	mov	_TIM1_CCER2,#12
3304                     ; 148     TIM1_CNTR  = 0;
3306  014e 5f            	clrw	x
3307  014f cf525e        	ldw	_TIM1_CNTR,x
3308                     ; 149     TIM1_PSCRH = 0;
3310  0152 725f5260      	clr	_TIM1_PSCRH
3311                     ; 150     TIM1_PSCRL = 15;
3313  0156 350f5261      	mov	_TIM1_PSCRL,#15
3314                     ; 153     TIM1_ARRH  = 0;
3316  015a 725f5262      	clr	_TIM1_ARRH
3317                     ; 155     TIM1_ARRL  = 19;
3319  015e 35135263      	mov	_TIM1_ARRL,#19
3320                     ; 156     TIM1_RCR   = 0;//7
3322  0162 725f5264      	clr	_TIM1_RCR
3323                     ; 157     TIM1_CCR1H = 0;
3325  0166 725f5265      	clr	_TIM1_CCR1H
3326                     ; 158     TIM1_CCR1L = 0;
3328  016a 725f5266      	clr	_TIM1_CCR1L
3329                     ; 159     TIM1_CCR2H = 0;
3331  016e 725f5267      	clr	_TIM1_CCR2H
3332                     ; 160     TIM1_CCR2L = 0;
3334  0172 725f5268      	clr	_TIM1_CCR2L
3335                     ; 161     TIM1_CCR3H = 0;
3337  0176 725f5269      	clr	_TIM1_CCR3H
3338                     ; 162     TIM1_CCR3L = 0;
3340  017a 725f526a      	clr	_TIM1_CCR3L
3341                     ; 163     TIM1_CCR4H = 0;
3343  017e 725f526b      	clr	_TIM1_CCR4H
3344                     ; 164     TIM1_CCR4L = 0;
3346  0182 725f526c      	clr	_TIM1_CCR4L
3347                     ; 165     TIM1_BKR   = 0X13;
3349  0186 3513526d      	mov	_TIM1_BKR,#19
3350                     ; 166     TIM1_DTR   = 16;
3352  018a 3510526e      	mov	_TIM1_DTR,#16
3353                     ; 167     TIM1_OISR  = 0X00;
3355  018e 725f526f      	clr	_TIM1_OISR
3356                     ; 168     TIM1_EGR   = 0X20;
3358  0192 35205257      	mov	_TIM1_EGR,#32
3359                     ; 169     TIM1_CR1  |= 0X01;
3361  0196 72105250      	bset	_TIM1_CR1,#0
3362                     ; 170     TIM1_BKR  |= 0X80;
3364  019a 721e526d      	bset	_TIM1_BKR,#7
3365                     ; 199     TIM3_CR1   = 0X00;
3367  019e 725f5320      	clr	_TIM3_CR1
3368                     ; 200     TIM3_SR1   = 0X00;
3370  01a2 725f5322      	clr	_TIM3_SR1
3371                     ; 201     TIM3_SR2   = 0X00;
3373  01a6 725f5323      	clr	_TIM3_SR2
3374                     ; 202     TIM3_IER   = 0X00;
3376  01aa 725f5321      	clr	_TIM3_IER
3377                     ; 203     TIM3_CCMR1 = 0X00;
3379  01ae 725f5325      	clr	_TIM3_CCMR1
3380                     ; 204     TIM3_CCMR2 = 0X68;
3382  01b2 35685326      	mov	_TIM3_CCMR2,#104
3383                     ; 205     TIM3_CCER1 = 0X10;
3385  01b6 35105327      	mov	_TIM3_CCER1,#16
3386                     ; 206     TIM3_CNTRH = 0;
3388  01ba 725f5328      	clr	_TIM3_CNTRH
3389                     ; 207     TIM3_CNTRL = 0;
3391  01be 725f5329      	clr	_TIM3_CNTRL
3392                     ; 208     TIM3_PSCR  = 4;        // 16M/ 2^4 = 1us
3394  01c2 3504532a      	mov	_TIM3_PSCR,#4
3395                     ; 210     TIM3_CCR1H = 0;
3397  01c6 725f532d      	clr	_TIM3_CCR1H
3398                     ; 211     TIM3_CCR1L = 0;
3400  01ca 725f532e      	clr	_TIM3_CCR1L
3401                     ; 212     TIM3_CCR2H = 0;
3403  01ce 725f532f      	clr	_TIM3_CCR2H
3404                     ; 213     TIM3_CCR2L = 0;
3406  01d2 725f5330      	clr	_TIM3_CCR2L
3407                     ; 214     TIM3_ARRH  = 0;
3409  01d6 725f532b      	clr	_TIM3_ARRH
3410                     ; 215     TIM3_ARRL  = 237;     // 238us /    4.2khz
3412  01da 35ed532c      	mov	_TIM3_ARRL,#237
3413                     ; 216     TIM3_EGR   = 0X0F;
3415  01de 350f5324      	mov	_TIM3_EGR,#15
3416                     ; 217     TIM3_CR1   = 0X81;/**/
3418  01e2 35815320      	mov	_TIM3_CR1,#129
3419                     ; 220     UART2_CR1 = 0x00;
3421  01e6 725f5244      	clr	_UART2_CR1
3422                     ; 221     UART2_CR2 = 0x2C;
3424  01ea 352c5245      	mov	_UART2_CR2,#44
3425                     ; 222     UART2_CR3 = 0x00;
3427  01ee 725f5246      	clr	_UART2_CR3
3428                     ; 223     UART2_CR4 = 0x00;// NA
3430  01f2 725f5247      	clr	_UART2_CR4
3431                     ; 224     UART2_CR5 = 0x00;// NA
3433  01f6 725f5248      	clr	_UART2_CR5
3434                     ; 225     UART2_CR6 = 0x00;// NA
3436  01fa 725f5249      	clr	_UART2_CR6
3437                     ; 226     UART2_BRR2 = 0x02;
3439  01fe 35025243      	mov	_UART2_BRR2,#2
3440                     ; 227     UART2_BRR1 = 0x68;// 682 ������9600
3442  0202 35685242      	mov	_UART2_BRR1,#104
3443                     ; 232   }
3446  0206 5b04          	addw	sp,#4
3447  0208 81            	ret	
3486                     ; 235 void MCU_Refresh( void )
3486                     ; 236   {
3487                     	switch	.text
3488  0209               _MCU_Refresh:
3492                     ; 237     ADC_TDRL    = CFG_ADC_TDRL;
3494  0209 725f5407      	clr	_ADC_TDRL
3495                     ; 238     ADC_TDRH    = CFG_ADC_TDRH;
3497  020d 725f5406      	clr	_ADC_TDRH
3498                     ; 239     PA_DDR      = CFG_PA_DDR;
3500  0211 35045002      	mov	_PA_DDR,#4
3501                     ; 240     PA_CR1      = CFG_PA_CR1;
3503  0215 35045003      	mov	_PA_CR1,#4
3504                     ; 241     PA_CR2      = CFG_PA_CR2;
3506  0219 35045004      	mov	_PA_CR2,#4
3507                     ; 242     PB_DDR      = CFG_PB_DDR;
3509  021d 35035007      	mov	_PB_DDR,#3
3510                     ; 243     PB_CR1      = CFG_PB_CR1;
3512  0221 353f5008      	mov	_PB_CR1,#63
3513                     ; 244     PB_CR2      = CFG_PB_CR2;
3515  0225 35035009      	mov	_PB_CR2,#3
3516                     ; 245     PC_DDR      = CFG_PC_DDR;
3518  0229 35fe500c      	mov	_PC_DDR,#254
3519                     ; 246     PC_CR1      = CFG_PC_CR1;
3521  022d 35fe500d      	mov	_PC_CR1,#254
3522                     ; 247     PC_CR2      = CFG_PC_CR2;
3524  0231 35fe500e      	mov	_PC_CR2,#254
3525                     ; 248     PD_DDR      = CFG_PD_DDR;
3527  0235 35bf5011      	mov	_PD_DDR,#191
3528                     ; 249     PD_CR1      = CFG_PD_CR1;
3530  0239 35bf5012      	mov	_PD_CR1,#191
3531                     ; 250     PD_CR2      = CFG_PD_CR2;
3533  023d 35bf5013      	mov	_PD_CR2,#191
3534                     ; 255     EXTI_CR1    = CFG_EXTI_CR1;
3536  0241 725f50a0      	clr	_EXTI_CR1
3537                     ; 256     EXTI_CR2    = CFG_EXTI_CR2;
3539  0245 725f50a1      	clr	_EXTI_CR2
3540                     ; 258   }
3543  0249 81            	ret	
3546                     	switch	.data
3547  0000               L3702_Led_Drive_Step:
3548  0000 00            	dc.b	0
3549  0001               L5702_Led_Drive_Cnt:
3550  0001 00            	dc.b	0
3573                     ; 272 void Led_Drive_Clr( void )
3573                     ; 273 {
3574                     	switch	.text
3575  024a               _Led_Drive_Clr:
3579                     ; 274     COM0_OFF( );
3582  024a 72125005      	bset	_PB_ODR,#1
3584                     ; 275     COM1_OFF( );
3587  024e 72105005      	bset	_PB_ODR,#0
3589                     ; 276     COM2_OFF( );
3592  0252 7214500f      	bset	_PD_ODR,#2
3594                     ; 277     COM3_OFF( );
3597  0256 7216500f      	bset	_PD_ODR,#3
3599                     ; 278     COM4_OFF( );
3602  025a 7218500f      	bset	_PD_ODR,#4
3604                     ; 279     SEG0_OFF( );
3607  025e 721f500a      	bres	_PC_ODR,#7
3609                     ; 280     SEG1_OFF( );
3612  0262 721d500a      	bres	_PC_ODR,#6
3614                     ; 281     SEG2_OFF( );
3617  0266 721b500a      	bres	_PC_ODR,#5
3619                     ; 282     SEG3_OFF( );
3622  026a 7219500a      	bres	_PC_ODR,#4
3624                     ; 283     SEG4_OFF( );
3627  026e 7217500a      	bres	_PC_ODR,#3
3629                     ; 284     SEG5_OFF( );
3632  0272 7215500a      	bres	_PC_ODR,#2
3634                     ; 285     SEG6_OFF( );
3637  0276 7213500a      	bres	_PC_ODR,#1
3639                     ; 286     Led_Drive_Step++;
3641  027a 725c0000      	inc	L3702_Led_Drive_Step
3642                     ; 287 }
3645  027e 81            	ret	
3679                     ; 289 void Com0_led_Init( void )
3679                     ; 290 {
3680                     	switch	.text
3681  027f               _Com0_led_Init:
3685                     ; 291     COM0_ON( );
3688  027f 72135005      	bres	_PB_ODR,#1
3690                     ; 292     Led_Drive_Cnt = 98;
3692  0283 35620001      	mov	L5702_Led_Drive_Cnt,#98
3693                     ; 293     Led_Drive_PWM[ 0 ] = Led_Type;
3695  0287 5500000000    	mov	L7702_Led_Drive_PWM,_Led_Type
3696                     ; 294     Led_Drive_PWM[ 1 ] = Led_Clean;
3698  028c 5500000001    	mov	L7702_Led_Drive_PWM+1,_Led_Clean
3699                     ; 295     Led_Drive_PWM[ 2 ] = Led_Power;
3701  0291 5500000002    	mov	L7702_Led_Drive_PWM+2,_Led_Power
3702                     ; 296     Led_Drive_PWM[ 3 ] = Led_BAR2;
3704  0296 5500000003    	mov	L7702_Led_Drive_PWM+3,_Led_BAR2
3705                     ; 297     Led_Drive_PWM[ 4 ] = Led_BAR3;
3707  029b 5500000004    	mov	L7702_Led_Drive_PWM+4,_Led_BAR3
3708                     ; 298     Led_Drive_PWM[ 5 ] = Led_BAR4;
3710  02a0 5500000005    	mov	L7702_Led_Drive_PWM+5,_Led_BAR4
3711                     ; 299     Led_Drive_PWM[ 6 ] = Led_BAR5;
3713  02a5 5500000006    	mov	L7702_Led_Drive_PWM+6,_Led_BAR5
3714                     ; 300     Led_Drive_Step++;
3716  02aa 725c0000      	inc	L3702_Led_Drive_Step
3717                     ; 301 }
3720  02ae 81            	ret	
3754                     ; 303 void Com1_led_Init( void )
3754                     ; 304 {
3755                     	switch	.text
3756  02af               _Com1_led_Init:
3760                     ; 305   COM1_ON( );
3763  02af 72115005      	bres	_PB_ODR,#0
3765                     ; 306   Led_Drive_Cnt = 98;
3767  02b3 35620001      	mov	L5702_Led_Drive_Cnt,#98
3768                     ; 307   Led_Drive_PWM[ 0 ] = Led_White;
3770  02b7 5500000000    	mov	L7702_Led_Drive_PWM,_Led_White
3771                     ; 308   Led_Drive_PWM[ 1 ] = Led_Speciality;
3773  02bc 5500000001    	mov	L7702_Led_Drive_PWM+1,_Led_Speciality
3774                     ; 309   Led_Drive_PWM[ 2 ] = Led_Green;
3776  02c1 5500000002    	mov	L7702_Led_Drive_PWM+2,_Led_Green
3777                     ; 310   Led_Drive_PWM[ 3 ] = Led_Immersion;
3779  02c6 5500000003    	mov	L7702_Led_Drive_PWM+3,_Led_Immersion
3780                     ; 311   Led_Drive_PWM[ 4 ] = Led_Delicate;
3782  02cb 5500000004    	mov	L7702_Led_Drive_PWM+4,_Led_Delicate
3783                     ; 312   Led_Drive_PWM[ 5 ] = Led_Style;
3785  02d0 5500000005    	mov	L7702_Led_Drive_PWM+5,_Led_Style
3786                     ; 313   Led_Drive_PWM[ 6 ] = Led_Start;
3788  02d5 5500000006    	mov	L7702_Led_Drive_PWM+6,_Led_Start
3789                     ; 314   Led_Drive_Step++;
3791  02da 725c0000      	inc	L3702_Led_Drive_Step
3792                     ; 315 }
3795  02de 81            	ret	
3829                     ; 317 void Com2_led_Init( void )
3829                     ; 318 {
3830                     	switch	.text
3831  02df               _Com2_led_Init:
3835                     ; 319   COM2_ON( );
3838  02df 7215500f      	bres	_PD_ODR,#2
3840                     ; 320   Led_Drive_Cnt = 98;
3842  02e3 35620001      	mov	L5702_Led_Drive_Cnt,#98
3843                     ; 321   Led_Drive_PWM[ 0 ] = Led_Classic;
3845  02e7 5500000000    	mov	L7702_Led_Drive_PWM,_Led_Classic
3846                     ; 322   Led_Drive_PWM[ 1 ] = Led_Herbal;
3848  02ec 5500000001    	mov	L7702_Led_Drive_PWM+1,_Led_Herbal
3849                     ; 323   Led_Drive_PWM[ 2 ] = Led_Rich;
3851  02f1 5500000002    	mov	L7702_Led_Drive_PWM+2,_Led_Rich
3852                     ; 324   Led_Drive_PWM[ 3 ] = Led_Black;
3854  02f6 5500000003    	mov	L7702_Led_Drive_PWM+3,_Led_Black
3855                     ; 325   Led_Drive_PWM[ 4 ] = Led_Overice;
3857  02fb 5500000004    	mov	L7702_Led_Drive_PWM+4,_Led_Overice
3858                     ; 326   Led_Drive_PWM[ 5 ] = Led_Oolong;
3860  0300 5500000005    	mov	L7702_Led_Drive_PWM+5,_Led_Oolong
3861                     ; 327   Led_Drive_PWM[ 6 ] = Led_Coldbrew;
3863  0305 5500000006    	mov	L7702_Led_Drive_PWM+6,_Led_Coldbrew
3864                     ; 328   Led_Drive_Step++;
3866  030a 725c0000      	inc	L3702_Led_Drive_Step
3867                     ; 329 }
3870  030e 81            	ret	
3904                     ; 331 void Com3_led_Init( void )
3904                     ; 332 {
3905                     	switch	.text
3906  030f               _Com3_led_Init:
3910                     ; 333   COM3_ON( );
3913  030f 7217500f      	bres	_PD_ODR,#3
3915                     ; 334   Led_Drive_Cnt = 98;
3917  0313 35620001      	mov	L5702_Led_Drive_Cnt,#98
3918                     ; 335   Led_Drive_PWM[ 0 ] = Led_Xltravel;
3920  0317 5500000000    	mov	L7702_Led_Drive_PWM,_Led_Xltravel
3921                     ; 336   Led_Drive_PWM[ 1 ] = Led_Xlcup;
3923  031c 5500000001    	mov	L7702_Led_Drive_PWM+1,_Led_Xlcup
3924                     ; 337   Led_Drive_PWM[ 2 ] = Led_Halfcarafe;
3926  0321 5500000002    	mov	L7702_Led_Drive_PWM+2,_Led_Halfcarafe
3927                     ; 338   Led_Drive_PWM[ 3 ] = Led_Cup;
3929  0326 5500000003    	mov	L7702_Led_Drive_PWM+3,_Led_Cup
3930                     ; 339   Led_Drive_PWM[ 4 ] = Led_Fullcarafe;
3932  032b 5500000004    	mov	L7702_Led_Drive_PWM+4,_Led_Fullcarafe
3933                     ; 340   Led_Drive_PWM[ 5 ] = Led_BAR1;
3935  0330 5500000005    	mov	L7702_Led_Drive_PWM+5,_Led_BAR1
3936                     ; 341   Led_Drive_PWM[ 6 ] = Led_Addone;
3938  0335 5500000006    	mov	L7702_Led_Drive_PWM+6,_Led_Addone
3939                     ; 342   Led_Drive_Step++;
3941  033a 725c0000      	inc	L3702_Led_Drive_Step
3942                     ; 343 }
3945  033e 81            	ret	
3979                     ; 345 void Com4_led_Init( void )
3979                     ; 346 {
3980                     	switch	.text
3981  033f               _Com4_led_Init:
3985                     ; 347   COM4_ON( );
3988  033f 7219500f      	bres	_PD_ODR,#4
3990                     ; 348   Led_Drive_Cnt = 98;
3992  0343 35620001      	mov	L5702_Led_Drive_Cnt,#98
3993                     ; 349   Led_Drive_PWM[ 0 ] = Led_Insertbasket;
3995  0347 5500000000    	mov	L7702_Led_Drive_PWM,_Led_Insertbasket
3996                     ; 350   Led_Drive_PWM[ 1 ] = Led_Delayset;
3998  034c 5500000001    	mov	L7702_Led_Drive_PWM+1,_Led_Delayset
3999                     ; 351   Led_Drive_PWM[ 2 ] = Led_Clean2;
4001  0351 5500000002    	mov	L7702_Led_Drive_PWM+2,_Led_Clean2
4002                     ; 352   Led_Drive_PWM[ 3 ] = Led_Keepwarm;
4004  0356 5500000003    	mov	L7702_Led_Drive_PWM+3,_Led_Keepwarm
4005                     ; 353   Led_Drive_PWM[ 4 ] = Led_Tea;
4007  035b 5500000004    	mov	L7702_Led_Drive_PWM+4,_Led_Tea
4008                     ; 354   Led_Drive_PWM[ 5 ] = Led_Coffee;
4010  0360 5500000005    	mov	L7702_Led_Drive_PWM+5,_Led_Coffee
4011                     ; 355   Led_Drive_PWM[ 6 ] = Led_Travel;
4013  0365 5500000006    	mov	L7702_Led_Drive_PWM+6,_Led_Travel
4014                     ; 356   Led_Drive_Step++;
4016  036a 725c0000      	inc	L3702_Led_Drive_Step
4017                     ; 357 }
4020  036e 81            	ret	
4047                     ; 359 void Seg_Led_Out( void )
4047                     ; 360 {
4048                     	switch	.text
4049  036f               _Seg_Led_Out:
4053                     ; 361    Led_Drive_Cnt--;
4055  036f 725a0001      	dec	L5702_Led_Drive_Cnt
4056                     ; 362    if( Led_Drive_Cnt < Led_Drive_PWM[ 0 ] ) SEG0_ON( );
4058  0373 c60001        	ld	a,L5702_Led_Drive_Cnt
4059  0376 c10000        	cp	a,L7702_Led_Drive_PWM
4060  0379 2404          	jruge	L1712
4064  037b 721e500a      	bset	_PC_ODR,#7
4066  037f               L1712:
4067                     ; 363    if( Led_Drive_Cnt < Led_Drive_PWM[ 1 ] ) SEG1_ON( );
4069  037f c10001        	cp	a,L7702_Led_Drive_PWM+1
4070  0382 2404          	jruge	L3712
4074  0384 721c500a      	bset	_PC_ODR,#6
4076  0388               L3712:
4077                     ; 364    if( Led_Drive_Cnt < Led_Drive_PWM[ 2 ] ) SEG2_ON( );
4079  0388 c10002        	cp	a,L7702_Led_Drive_PWM+2
4080  038b 2404          	jruge	L5712
4084  038d 721a500a      	bset	_PC_ODR,#5
4086  0391               L5712:
4087                     ; 365    if( Led_Drive_Cnt < Led_Drive_PWM[ 3 ] ) SEG3_ON( );
4089  0391 c10003        	cp	a,L7702_Led_Drive_PWM+3
4090  0394 2404          	jruge	L7712
4094  0396 7218500a      	bset	_PC_ODR,#4
4096  039a               L7712:
4097                     ; 366    if( Led_Drive_Cnt < Led_Drive_PWM[ 4 ] ) SEG4_ON( );
4099  039a c10004        	cp	a,L7702_Led_Drive_PWM+4
4100  039d 2404          	jruge	L1022
4104  039f 7216500a      	bset	_PC_ODR,#3
4106  03a3               L1022:
4107                     ; 367    if( Led_Drive_Cnt < Led_Drive_PWM[ 5 ] ) SEG5_ON( );
4109  03a3 c10005        	cp	a,L7702_Led_Drive_PWM+5
4110  03a6 2404          	jruge	L3022
4114  03a8 7214500a      	bset	_PC_ODR,#2
4116  03ac               L3022:
4117                     ; 368    if( Led_Drive_Cnt < Led_Drive_PWM[ 6 ] ) SEG6_ON( );
4119  03ac c10006        	cp	a,L7702_Led_Drive_PWM+6
4120  03af 2404          	jruge	L5022
4124  03b1 7212500a      	bset	_PC_ODR,#1
4126  03b5               L5022:
4127                     ; 369    if( Led_Drive_Cnt == 0 )  Led_Drive_Step++;
4129  03b5 c60001        	ld	a,L5702_Led_Drive_Cnt
4130  03b8 2604          	jrne	L7022
4133  03ba 725c0000      	inc	L3702_Led_Drive_Step
4134  03be               L7022:
4135                     ; 370 }
4138  03be 81            	ret	
4165                     ; 372 void Seg_Led_Out_E( void )
4165                     ; 373 {
4166                     	switch	.text
4167  03bf               _Seg_Led_Out_E:
4171                     ; 374    Led_Drive_Cnt--;
4173  03bf 725a0001      	dec	L5702_Led_Drive_Cnt
4174                     ; 375    if( Led_Drive_Cnt < Led_Drive_PWM[ 0 ] ) SEG0_ON( );
4176  03c3 c60001        	ld	a,L5702_Led_Drive_Cnt
4177  03c6 c10000        	cp	a,L7702_Led_Drive_PWM
4178  03c9 2404          	jruge	L1222
4182  03cb 721e500a      	bset	_PC_ODR,#7
4184  03cf               L1222:
4185                     ; 376    if( Led_Drive_Cnt < Led_Drive_PWM[ 1 ] ) SEG1_ON( );
4187  03cf c10001        	cp	a,L7702_Led_Drive_PWM+1
4188  03d2 2404          	jruge	L3222
4192  03d4 721c500a      	bset	_PC_ODR,#6
4194  03d8               L3222:
4195                     ; 377    if( Led_Drive_Cnt < Led_Drive_PWM[ 2 ] ) SEG2_ON( );
4197  03d8 c10002        	cp	a,L7702_Led_Drive_PWM+2
4198  03db 2404          	jruge	L5222
4202  03dd 721a500a      	bset	_PC_ODR,#5
4204  03e1               L5222:
4205                     ; 378    if( Led_Drive_Cnt < Led_Drive_PWM[ 3 ] ) SEG3_ON( );
4207  03e1 c10003        	cp	a,L7702_Led_Drive_PWM+3
4208  03e4 2404          	jruge	L7222
4212  03e6 7218500a      	bset	_PC_ODR,#4
4214  03ea               L7222:
4215                     ; 379    if( Led_Drive_Cnt < Led_Drive_PWM[ 4 ] ) SEG4_ON( );
4217  03ea c10004        	cp	a,L7702_Led_Drive_PWM+4
4218  03ed 2404          	jruge	L1322
4222  03ef 7216500a      	bset	_PC_ODR,#3
4224  03f3               L1322:
4225                     ; 380    if( Led_Drive_Cnt < Led_Drive_PWM[ 5 ] ) SEG5_ON( );
4227  03f3 c10005        	cp	a,L7702_Led_Drive_PWM+5
4228  03f6 2404          	jruge	L3322
4232  03f8 7214500a      	bset	_PC_ODR,#2
4234  03fc               L3322:
4235                     ; 381    if( Led_Drive_Cnt < Led_Drive_PWM[ 6 ] ) SEG6_ON( );
4237  03fc c10006        	cp	a,L7702_Led_Drive_PWM+6
4238  03ff 2404          	jruge	L5322
4242  0401 7212500a      	bset	_PC_ODR,#1
4244  0405               L5322:
4245                     ; 382    if( Led_Drive_Cnt == 0 )  Led_Drive_Step = 0;
4247  0405 c60001        	ld	a,L5702_Led_Drive_Cnt
4248  0408 2603          	jrne	L7322
4251  040a c70000        	ld	L3702_Led_Drive_Step,a
4252  040d               L7322:
4253                     ; 383 }
4256  040d 81            	ret	
4259                     .const:	section	.text
4260  0000               _LED_Drive_Tab:
4262  0000 024a          	dc.w	_Led_Drive_Clr
4264  0002 027f          	dc.w	_Com0_led_Init
4266  0004 036f          	dc.w	_Seg_Led_Out
4268  0006 024a          	dc.w	_Led_Drive_Clr
4270  0008 02af          	dc.w	_Com1_led_Init
4272  000a 036f          	dc.w	_Seg_Led_Out
4274  000c 024a          	dc.w	_Led_Drive_Clr
4276  000e 02df          	dc.w	_Com2_led_Init
4278  0010 036f          	dc.w	_Seg_Led_Out
4280  0012 024a          	dc.w	_Led_Drive_Clr
4282  0014 030f          	dc.w	_Com3_led_Init
4284  0016 036f          	dc.w	_Seg_Led_Out
4286  0018 024a          	dc.w	_Led_Drive_Clr
4288  001a 033f          	dc.w	_Com4_led_Init
4290  001c 03bf          	dc.w	_Seg_Led_Out_E
4292  001e 024a          	dc.w	_Led_Drive_Clr
4317                     ; 411 @far @nosvf @interrupt void SystemTimerISR( void ) // 20us
4317                     ; 412 //@far  @interrupt void SystemTimerISR( void ) // 20us
4317                     ; 413 
4317                     ; 414   {
4319                     	switch	.text
4320  040e               f_SystemTimerISR:
4325                     ; 416     TIM1_SR1 = 0X00;
4327  040e 725f5255      	clr	_TIM1_SR1
4328                     ; 417     sysTicks++;
4330  0412 725c0007      	inc	_sysTicks
4331                     ; 418     (*LED_Drive_Tab[ Led_Drive_Step & 0x0F ])( );
4333  0416 c60000        	ld	a,L3702_Led_Drive_Step
4334  0419 a40f          	and	a,#15
4335  041b 5f            	clrw	x
4336  041c 97            	ld	xl,a
4337  041d 58            	sllw	x
4338  041e de0000        	ldw	x,(_LED_Drive_Tab,x)
4339  0421 fd            	call	(x)
4341                     ; 420   }
4344  0422 80            	iret	
4391                     ; 422 U16  ADC_Convert( U8 chn )
4391                     ; 423   {
4393                     	switch	.text
4394  0423               _ADC_Convert:
4396  0423 89            	pushw	x
4397       00000002      OFST:	set	2
4400                     ; 425     ADC_HTRH = 0XFF;
4402  0424 35ff5408      	mov	_ADC_HTRH,#255
4403                     ; 426     ADC_HTRL = 0X03;
4405  0428 35035409      	mov	_ADC_HTRL,#3
4406                     ; 427     ADC_LTRH = 0X00;
4408  042c 725f540a      	clr	_ADC_LTRH
4409                     ; 428     ADC_LTRL = 0X00;
4411  0430 725f540b      	clr	_ADC_LTRL
4412                     ; 429     ADC_CSR  = chn;
4414  0434 c75400        	ld	_ADC_CSR,a
4415                     ; 430     ADC_CR3  = 0X00;
4417  0437 725f5403      	clr	_ADC_CR3
4418                     ; 431     ADC_CR2  = 0X08;
4420  043b 35085402      	mov	_ADC_CR2,#8
4421                     ; 432     ADC_CR1  = 0X71;
4423  043f 35715401      	mov	_ADC_CR1,#113
4424                     ; 433     buf = 1000;
4426  0443 ae03e8        	ldw	x,#1000
4427  0446 1f01          	ldw	(OFST-1,sp),x
4429  0448 200b          	jra	L3722
4430  044a               L7622:
4431                     ; 436         if ( ADC_CSR & 0X80 ) break;
4433  044a 720f540006    	btjf	_ADC_CSR,#7,L3722
4435  044f               L5722:
4436                     ; 438     return ADC_DR;
4438  044f ce5404        	ldw	x,_ADC_DR
4441  0452 5b02          	addw	sp,#2
4442  0454 81            	ret	
4443  0455               L3722:
4444                     ; 434     while ( buf-- )
4446  0455 1e01          	ldw	x,(OFST-1,sp)
4447  0457 5a            	decw	x
4448  0458 1f01          	ldw	(OFST-1,sp),x
4449  045a 5c            	incw	x
4450  045b 26ed          	jrne	L7622
4451  045d 20f0          	jra	L5722
4502                     	xdef	f_SystemTimerISR
4503                     	xdef	_LED_Drive_Tab
4504                     	xdef	_Seg_Led_Out_E
4505                     	xdef	_Seg_Led_Out
4506                     	xdef	_Com4_led_Init
4507                     	xdef	_Com3_led_Init
4508                     	xdef	_Com2_led_Init
4509                     	xdef	_Com1_led_Init
4510                     	xdef	_Com0_led_Init
4511                     	xdef	_Led_Drive_Clr
4512                     	switch	.bss
4513  0000               L7702_Led_Drive_PWM:
4514  0000 000000000000  	ds.b	7
4515                     	xref	_Led_Travel
4516                     	xref	_Led_Coffee
4517                     	xref	_Led_Tea
4518                     	xref	_Led_Keepwarm
4519                     	xref	_Led_Clean2
4520                     	xref	_Led_Delayset
4521                     	xref	_Led_Insertbasket
4522                     	xref	_Led_Addone
4523                     	xref	_Led_BAR1
4524                     	xref	_Led_Fullcarafe
4525                     	xref	_Led_Cup
4526                     	xref	_Led_Halfcarafe
4527                     	xref	_Led_Xlcup
4528                     	xref	_Led_Xltravel
4529                     	xref	_Led_Coldbrew
4530                     	xref	_Led_Oolong
4531                     	xref	_Led_Overice
4532                     	xref	_Led_Black
4533                     	xref	_Led_Rich
4534                     	xref	_Led_Herbal
4535                     	xref	_Led_Classic
4536                     	xref	_Led_Start
4537                     	xref	_Led_Style
4538                     	xref	_Led_Delicate
4539                     	xref	_Led_Immersion
4540                     	xref	_Led_Green
4541                     	xref	_Led_Speciality
4542                     	xref	_Led_White
4543                     	xref	_Led_BAR5
4544                     	xref	_Led_BAR4
4545                     	xref	_Led_BAR3
4546                     	xref	_Led_BAR2
4547                     	xref	_Led_Power
4548                     	xref	_Led_Clean
4549                     	xref	_Led_Type
4550                     	xdef	_ADC_Convert
4551                     	xdef	_EEP_WriteByte
4552                     	xdef	_MCU_Refresh
4553                     	xdef	_MCU_Init
4554  0007               _sysTicks:
4555  0007 00            	ds.b	1
4556                     	xdef	_sysTicks
4557  0008               _ledSegs:
4558  0008 0000000000    	ds.b	5
4559                     	xdef	_ledSegs
4560  000d               _hswTime:
4561  000d 0000          	ds.b	2
4562                     	xdef	_hswTime
4563  000f               _hswIDR:
4564  000f 00            	ds.b	1
4565                     	xdef	_hswIDR
4566  0010               _keyIDR:
4567  0010 00            	ds.b	1
4568                     	xdef	_keyIDR
4588                     	xref	c_lrzmp
4589                     	xref	c_lgsbc
4590                     	xref	c_ltor
4591                     	end

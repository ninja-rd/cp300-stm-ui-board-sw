   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   3                     ; Optimizer V4.2.4 - 18 Dec 2007
3800                     	switch	.data
3801  0000               _Delay_Brew_Min_Total:
3802  0000 0000          	dc.w	0
3803  0002               _Clock_Min_Total:
3804  0002 0000          	dc.w	0
3805  0004               _dimDelay10minute:
3806  0004 0000          	dc.w	0
3870                     ; 32 void Key_Init( void )
3870                     ; 33 {
3872                     	switch	.text
3873  0000               _Key_Init:
3877                     ; 35     memset( KEY_Buff, 0x00, sizeof( KEY_Buff ) );
3879  0000 ae0010        	ldw	x,#16
3880  0003               L6:
3881  0003 724f0004      	clr	(_KEY_Buff-1,x)
3882  0007 5a            	decw	x
3883  0008 26f9          	jrne	L6
3884                     ; 36     Knob0_count [ 1 ]  = fullcarafe;
3886  000a 35050055      	mov	_Knob0_count+1,#5
3887                     ; 37     Knob1_count [ 1 ]  = Classic;
3889  000e 725f0052      	clr	_Knob1_count+1
3890                     ; 38     Knob0_count [ 2 ]  = fullcarafe;
3892  0012 35050056      	mov	_Knob0_count+2,#5
3893                     ; 39     Knob1_count [ 2 ]  = Classic;
3895  0016 725f0053      	clr	_Knob1_count+2
3896                     ; 40     Knob2_count        = Herbal;
3898  001a 725f0050      	clr	_Knob2_count
3899                     ; 42     Clock_Set_Time  = 3;
3901  001e 35030033      	mov	_Clock_Set_Time,#3
3902                     ; 43     Clock.hour                  = 12;
3904  0022 350c004d      	mov	_Clock+2,#12
3905                     ; 44     Clock.hour_b                = 12;
3907  0026 350c004f      	mov	_Clock+4,#12
3908                     ; 45     Clock.min                   = 0;
3910  002a 725f004c      	clr	_Clock+1
3911                     ; 46     Clock.min_b                 = 0;
3913  002e 725f004e      	clr	_Clock+3
3914                     ; 47     Clock.second                = 0;
3916  0032 725f004b      	clr	_Clock
3917                     ; 49     Clock_DLY_BREW.hour         = 12;
3919  0036 350c0048      	mov	_Clock_DLY_BREW+2,#12
3920                     ; 50     Clock_DLY_BREW.hour_b       = 12;
3922  003a 350c004a      	mov	_Clock_DLY_BREW+4,#12
3923                     ; 51     Clock_DLY_BREW.min          = 0;
3925  003e 725f0047      	clr	_Clock_DLY_BREW+1
3926                     ; 52     Clock_DLY_BREW.min_b        = 0;
3928  0042 725f0049      	clr	_Clock_DLY_BREW+3
3929                     ; 53     Clock_DLY_BREW.second       = 0;
3931  0046 725f0046      	clr	_Clock_DLY_BREW
3932                     ; 55     Clock_Clean.hour            = 0;
3934  004a 725f0043      	clr	_Clock_Clean+2
3935                     ; 56     Clock_Clean.hour_b          = 0;
3937  004e 725f0045      	clr	_Clock_Clean+4
3938                     ; 57     Clock_Clean.min             = 59;
3940  0052 353b0042      	mov	_Clock_Clean+1,#59
3941                     ; 58     Clock_Clean.min_b           = 0;
3943  0056 725f0044      	clr	_Clock_Clean+3
3944                     ; 59     Clock_Clean.second          = 59;
3946  005a 353b0041      	mov	_Clock_Clean,#59
3947                     ; 61     Clock_Keep_Warm.hour        = 2;
3949  005e 3502003e      	mov	_Clock_Keep_Warm+2,#2
3950                     ; 62     Clock_Keep_Warm.hour_b      = 0;
3952  0062 725f0040      	clr	_Clock_Keep_Warm+4
3953                     ; 63     Clock_Keep_Warm.min         = 0;
3955  0066 725f003d      	clr	_Clock_Keep_Warm+1
3956                     ; 64     Clock_Keep_Warm.min_b       = 0;
3958  006a 725f003f      	clr	_Clock_Keep_Warm+3
3959                     ; 65     Clock_Keep_Warm.second      = 0;
3961  006e 725f003c      	clr	_Clock_Keep_Warm
3962                     ; 67     System_Status = ACTIVE;
3964  0072 3502003b      	mov	_System_Status,#2
3965                     ; 68     Standby_Timer = 300;// 上电5分钟无操作进行待机模式
3967  0076 ae012c        	ldw	x,#300
3968  0079 cf0030        	ldw	_Standby_Timer,x
3969                     ; 70     Start_Status  = OFF;// 关机模式，
3971  007c 725f003a      	clr	_Start_Status
3972                     ; 72     if( F_KEY_TEA_BASKET )
3974  0080 7207000006    	btjf	_Ctrl_Board_Status,#3,L1752
3975                     ; 73       Brew_Select   = TEA;
3977  0085 35020039      	mov	_Brew_Select,#2
3979  0089 200f          	jra	L3752
3980  008b               L1752:
3981                     ; 74     else if( F_KEY_COFFEE_BASKET )
3983  008b 7201000006    	btjf	_Ctrl_Board_Status,#0,L5752
3984                     ; 75       Brew_Select   = COFFEE;
3986  0090 35010039      	mov	_Brew_Select,#1
3988  0094 2004          	jra	L3752
3989  0096               L5752:
3990                     ; 77       Brew_Select   = NO_BASKET;
3992  0096 725f0039      	clr	_Brew_Select
3993  009a               L3752:
3994                     ; 78     Style_Type_Flag = 0;
3996  009a 725f0036      	clr	_Style_Type_Flag
3997                     ; 79     SET_Keep_Warm_Time    = 0;
3999  009e 725f002d      	clr	_SET_Keep_Warm_Time
4000                     ; 80     Delay_Keep_Warm_Time  = 0;
4002  00a2 725f002f      	clr	_Delay_Keep_Warm_Time
4003                     ; 81     Delay_Keep_Warm_Level = 0;
4005  00a6 725f002e      	clr	_Delay_Keep_Warm_Level
4006                     ; 83     CANCEL_DELAY_BREW_3S  = 0;
4008  00aa 725f002b      	clr	_CANCEL_DELAY_BREW_3S
4009                     ; 84     Delay_Brew_Time       = 0;
4011  00ae 725f002c      	clr	_Delay_Brew_Time
4012                     ; 86     Keep_Warm_Class       = WARM_HI;
4014  00b2 35020037      	mov	_Keep_Warm_Class,#2
4015                     ; 87     Complete_Sound_Delay  = 0;
4017  00b6 725f002a      	clr	_Complete_Sound_Delay
4018                     ; 88     Pause_Sound_Delay     = 0;
4020  00ba 725f0029      	clr	_Pause_Sound_Delay
4021                     ; 89     brewAdjust15S         = 0;
4023  00be 725f0028      	clr	_brewAdjust15S
4024                     ; 90     brewNoaction3S        = 0;
4026  00c2 725f0027      	clr	_brewNoaction3S
4027                     ; 92     delayBrewPause        = 0;
4029  00c6 5f            	clrw	x
4030  00c7 cf0024        	ldw	_delayBrewPause,x
4031                     ; 93     delayAdjust15S        = 0;
4033  00ca 725f0023      	clr	_delayAdjust15S
4034                     ; 94     delayNoaction3S       = 0;
4036  00ce 725f0022      	clr	_delayNoaction3S
4037                     ; 95     delayExitCnt          = 0;
4039  00d2 725f0021      	clr	_delayExitCnt
4040                     ; 96     cleanAlarm            = 0;
4042  00d6 725f0020      	clr	_cleanAlarm
4043                     ; 97     cleanStart            = 0;
4045  00da 725f001f      	clr	_cleanStart
4046                     ; 98     forcePump             = 0;
4048  00de 725f001e      	clr	_forcePump
4049                     ; 99     debugEnable           = 0;
4051  00e2 725f001d      	clr	_debugEnable
4052                     ; 101 }
4055  00e6 81            	ret	
4080                     ; 103 void Clock_Cnt( void )
4080                     ; 104 {
4081                     	switch	.text
4082  00e7               _Clock_Cnt:
4086                     ; 105     if( ++Clock.second> 59)
4088  00e7 725c004b      	inc	_Clock
4089  00eb c6004b        	ld	a,_Clock
4090  00ee a13c          	cp	a,#60
4091  00f0 2530          	jrult	L1162
4092                     ; 107       Clock.second = 0;
4094  00f2 725f004b      	clr	_Clock
4095                     ; 108       if(++Clock.min > 59)
4097  00f6 725c004c      	inc	_Clock+1
4098  00fa c6004c        	ld	a,_Clock+1
4099  00fd a13c          	cp	a,#60
4100  00ff 2521          	jrult	L1162
4101                     ; 110         Clock.min = 0;
4103  0101 725f004c      	clr	_Clock+1
4104                     ; 111         if( Clock.hour == 11 )
4106  0105 c6004d        	ld	a,_Clock+2
4107  0108 a10b          	cp	a,#11
4108  010a 2609          	jrne	L5162
4109                     ; 113           Clock.hour = 12;
4111  010c 350c004d      	mov	_Clock+2,#12
4112                     ; 114           AM_PM_FLAG ^= 1;
4114  0110 90100019      	bcpl	_Disp_Flag,#0
4117  0114 81            	ret	
4118  0115               L5162:
4119                     ; 116         else if( Clock.hour == 12)
4121  0115 a10c          	cp	a,#12
4122  0117 2605          	jrne	L1262
4123                     ; 117           Clock.hour = 1;
4125  0119 3501004d      	mov	_Clock+2,#1
4128  011d 81            	ret	
4129  011e               L1262:
4130                     ; 119           Clock.hour++;
4132  011e 725c004d      	inc	_Clock+2
4133  0122               L1162:
4134                     ; 122 }
4137  0122 81            	ret	
4168                     ; 123 void Delay_Brew_Sure( void )
4168                     ; 124 {
4169                     	switch	.text
4170  0123               _Delay_Brew_Sure:
4172  0123 89            	pushw	x
4173       00000002      OFST:	set	2
4176                     ; 125     DLY_BREW_FLAG = 0;
4178  0124 72190019      	bres	_Disp_Flag,#4
4179                     ; 126     Delay_Brew_Time = 0;
4181  0128 725f002c      	clr	_Delay_Brew_Time
4182                     ; 127     Style_Type_Flag = 0;
4184  012c 725f0036      	clr	_Style_Type_Flag
4185                     ; 128     BeepStart( Confirm_Sound );
4187  0130 ae0000        	ldw	x,#_Confirm_Sound
4188  0133 cd0000        	call	_BeepStart
4190                     ; 129     Clock_DLY_BREW.hour = Clock_DLY_BREW.hour_b;
4192  0136 55004a0048    	mov	_Clock_DLY_BREW+2,_Clock_DLY_BREW+4
4193                     ; 130     Clock_DLY_BREW.min  = Clock_DLY_BREW.min_b;
4195  013b c60049        	ld	a,_Clock_DLY_BREW+3
4196  013e c70047        	ld	_Clock_DLY_BREW+1,a
4197                     ; 131     Delay_Brew_Min_Total = (U16)(Clock_DLY_BREW.hour * 60) + (U16)Clock_DLY_BREW.min;
4199  0141 5f            	clrw	x
4200  0142 97            	ld	xl,a
4201  0143 1f01          	ldw	(OFST-1,sp),x
4202  0145 c60048        	ld	a,_Clock_DLY_BREW+2
4203  0148 97            	ld	xl,a
4204  0149 a63c          	ld	a,#60
4205  014b 42            	mul	x,a
4206  014c 72fb01        	addw	x,(OFST-1,sp)
4207  014f cf0000        	ldw	_Delay_Brew_Min_Total,x
4208                     ; 132     DLY_BREW_FUNC = 1;
4210  0152 721a0019      	bset	_Disp_Flag,#5
4211                     ; 133     delayAdjust15S = DELAY_ADJUST_TIME;
4213  0156 350f0023      	mov	_delayAdjust15S,#15
4214                     ; 134 }
4217  015a 85            	popw	x
4218  015b 81            	ret	
4221                     	switch	.data
4222  0006               L5362_t400ms:
4223  0006 00            	dc.b	0
4224  0007               L7362_t500ms:
4225  0007 00            	dc.b	0
4226  0008               L1462_time:
4227  0008 00            	dc.b	0
4228  0009               L3462_t600ms:
4229  0009 00            	dc.b	0
4230  000a               L5462_t750ms:
4231  000a 00            	dc.b	0
4333                     ; 136 void Time_Deal(void)
4333                     ; 137 {
4334                     	switch	.text
4335  015c               _Time_Deal:
4337  015c 89            	pushw	x
4338       00000002      OFST:	set	2
4341                     ; 143    DECEX(Clock_Stay_Time);
4343  015d c60032        	ld	a,_Clock_Stay_Time
4344  0160 2704          	jreq	L3762
4347  0162 725a0032      	dec	_Clock_Stay_Time
4348  0166               L3762:
4349                     ; 145    if( ++t400ms >= 40 )
4351  0166 725c0006      	inc	L5362_t400ms
4352  016a c60006        	ld	a,L5362_t400ms
4353  016d a128          	cp	a,#40
4354  016f 2508          	jrult	L5762
4355                     ; 147       t400ms = 0;
4357  0171 725f0006      	clr	L5362_t400ms
4358                     ; 148       _400ms_flag ^= 1;
4360  0175 9012001a      	bcpl	_Time_Flag,#1
4361  0179               L5762:
4362                     ; 151    if( ++t500ms >= 50 )
4364  0179 725c0007      	inc	L7362_t500ms
4365  017d c60007        	ld	a,L7362_t500ms
4366  0180 a132          	cp	a,#50
4367  0182 2508          	jrult	L7762
4368                     ; 153       t500ms = 0;
4370  0184 725f0007      	clr	L7362_t500ms
4371                     ; 154       _500ms_flag ^= 1;
4373  0188 9010001a      	bcpl	_Time_Flag,#0
4374  018c               L7762:
4375                     ; 157    if( ++time >= 100)
4377  018c 725c0008      	inc	L1462_time
4378  0190 c60008        	ld	a,L1462_time
4379  0193 a164          	cp	a,#100
4380  0195 2403cc051f    	jrult	L1072
4381                     ; 159       time = 0;
4383  019a 725f0008      	clr	L1462_time
4384                     ; 160       _1s_flag ^= 1;
4386  019e 9016001a      	bcpl	_Time_Flag,#3
4387                     ; 162       if( System_Status == ACTIVE  && Start_Status == OFF )
4389  01a2 c6003b        	ld	a,_System_Status
4390  01a5 a102          	cp	a,#2
4391  01a7 262a          	jrne	L3072
4393  01a9 725d003a      	tnz	_Start_Status
4394  01ad 2624          	jrne	L3072
4395                     ; 165          if( CLEAN_DISP_FLAG  ||  KEEP_WARM_FUNC )
4397  01af 7200001805    	btjt	_Disp_Flag1,#0,L7072
4399  01b4 7203001902    	btjf	_Disp_Flag,#1,L5072
4400  01b9               L7072:
4401                     ; 167            Standby_Timer = 300;
4403  01b9 2018          	jp	L3072
4404  01bb               L5072:
4405                     ; 171           if( Standby_Timer )
4407  01bb ce0030        	ldw	x,_Standby_Timer
4408  01be 2719          	jreq	L7172
4409                     ; 173             Standby_Timer--;
4411  01c0 5a            	decw	x
4412  01c1 cf0030        	ldw	_Standby_Timer,x
4413                     ; 174             if( Standby_Timer == 0 )
4415  01c4 2613          	jrne	L7172
4416                     ; 177               System_Status = DIM_STATUS;
4418  01c6 a601          	ld	a,#1
4419  01c8 c7003b        	ld	_System_Status,a
4420                     ; 178               dimDelay10minute = 600;
4422  01cb ae0258        	ldw	x,#600
4423  01ce cf0004        	ldw	_dimDelay10minute,x
4424  01d1 2006          	jra	L7172
4425  01d3               L3072:
4426                     ; 186         Standby_Timer = 300;
4429  01d3 ae012c        	ldw	x,#300
4430  01d6 cf0030        	ldw	_Standby_Timer,x
4431  01d9               L7172:
4432                     ; 189       if( System_Status == DIM_STATUS)
4434  01d9 4a            	dec	a
4435  01da 2612          	jrne	L1272
4436                     ; 191         if( dimDelay10minute ) dimDelay10minute--;
4438  01dc ce0004        	ldw	x,_dimDelay10minute
4439  01df 2704          	jreq	L3272
4442  01e1 5a            	decw	x
4443  01e2 cf0004        	ldw	_dimDelay10minute,x
4444  01e5               L3272:
4445                     ; 192         if( dimDelay10minute == 0) System_Status = STANDBY;
4447  01e5 ce0004        	ldw	x,_dimDelay10minute
4448  01e8 2604          	jrne	L1272
4451  01ea 725f003b      	clr	_System_Status
4452  01ee               L1272:
4453                     ; 195       if( Clock_Set_Time )
4455  01ee c60033        	ld	a,_Clock_Set_Time
4456  01f1 2716          	jreq	L7272
4457                     ; 197          Clock_Set_Time--;
4459  01f3 725a0033      	dec	_Clock_Set_Time
4460                     ; 198          if( Clock_Set_Time == 0 )
4462  01f7 2613          	jrne	L3372
4463                     ; 200            Clock.hour = Clock.hour_b;
4465  01f9 55004f004d    	mov	_Clock+2,_Clock+4
4466                     ; 201            Clock.min  = Clock.min_b;
4468  01fe 55004e004c    	mov	_Clock+1,_Clock+3
4469                     ; 202            Clock.second = 0;
4471  0203 725f004b      	clr	_Clock
4472  0207 2003          	jra	L3372
4473  0209               L7272:
4474                     ; 206       Clock_Cnt( );
4476  0209 cd00e7        	call	_Clock_Cnt
4478  020c               L3372:
4479                     ; 208       if ( Paused_Delay ) 
4481  020c ce0034        	ldw	x,_Paused_Delay
4482  020f 2768          	jreq	L5372
4483                     ; 210         Paused_Delay--;
4485  0211 5a            	decw	x
4486  0212 cf0034        	ldw	_Paused_Delay,x
4487                     ; 211         if( Paused_Delay <= PAUSE_TIME-20 )
4489  0215 a30119        	cpw	x,#281
4490  0218 240f          	jruge	L7372
4491                     ; 213           if( Pause_Sound_Delay )
4493  021a c60029        	ld	a,_Pause_Sound_Delay
4494  021d 270a          	jreq	L7372
4495                     ; 215            Pause_Sound_Delay--;
4497  021f 725a0029      	dec	_Pause_Sound_Delay
4498                     ; 216            BeepStart( Reminder_Sound );
4500  0223 ae0000        	ldw	x,#_Reminder_Sound
4501  0226 cd0000        	call	_BeepStart
4503  0229               L7372:
4504                     ; 219         if( 0 == Paused_Delay )
4506  0229 ce0034        	ldw	x,_Paused_Delay
4507  022c 2616          	jrne	L3472
4508                     ; 222           Start_Status = CANCEL;
4510  022e 3503003a      	mov	_Start_Status,#3
4511                     ; 223           KEEP_WARM_FUNC  = 0;
4513  0232 72130019      	bres	_Disp_Flag,#1
4514                     ; 224           WARM_LEVEL_FUNC = 0;
4516  0236 72170019      	bres	_Disp_Flag,#3
4517                     ; 225           CLEAN_FUNC_FLAG = 0;
4519  023a 72130018      	bres	_Disp_Flag1,#1
4520                     ; 226           BeepStart( Cancel_Sound );
4522  023e ae0000        	ldw	x,#_Cancel_Sound
4523  0241 cd0000        	call	_BeepStart
4525  0244               L3472:
4526                     ; 228         if( Brew_Select_B != NO_BASKET )
4528  0244 c60038        	ld	a,_Brew_Select_B
4529  0247 2754          	jreq	L5572
4530                     ; 231           if( Brew_Select_B == Brew_Select )
4532  0249 c10039        	cp	a,_Brew_Select
4533  024c 2613          	jrne	L7472
4534                     ; 233             if( NO_BASKET_PAUSE )
4536  024e 720100174a    	btjf	_Sys_Flag1,#0,L5572
4537                     ; 235              NO_BASKET_PAUSE = 0;
4539  0253 72110017      	bres	_Sys_Flag1,#0
4540                     ; 236              Paused_Delay = 0;
4542  0257 5f            	clrw	x
4543  0258 cf0034        	ldw	_Paused_Delay,x
4544                     ; 237              Start_Status = UNPAUSE;
4546  025b 3504003a      	mov	_Start_Status,#4
4547  025f 203c          	jra	L5572
4548  0261               L7472:
4549                     ; 242             Start_Status = CANCEL;
4551  0261 3503003a      	mov	_Start_Status,#3
4552                     ; 243             KEEP_WARM_FUNC  = 0;
4554  0265 72130019      	bres	_Disp_Flag,#1
4555                     ; 244             WARM_LEVEL_FUNC = 0;
4557  0269 72170019      	bres	_Disp_Flag,#3
4558                     ; 245             CLEAN_FUNC_FLAG = 0;
4560  026d 72130018      	bres	_Disp_Flag1,#1
4561                     ; 246             BeepStart( Cancel_Sound );
4563  0271 ae0000        	ldw	x,#_Cancel_Sound
4564  0274 cd0000        	call	_BeepStart
4566  0277 2024          	jra	L5572
4567  0279               L5372:
4568                     ; 253         if(( Start_Status == ON && Brew_Select_B == NO_BASKET )
4568                     ; 254           ||( Start_Status == UNPAUSE && Brew_Select_B == NO_BASKET ))
4570  0279 c6003a        	ld	a,_Start_Status
4571  027c a101          	cp	a,#1
4572  027e 2606          	jrne	L3672
4574  0280 725d0038      	tnz	_Brew_Select_B
4575  0284 2709          	jreq	L1672
4576  0286               L3672:
4578  0286 a104          	cp	a,#4
4579  0288 2613          	jrne	L5572
4581  028a c60038        	ld	a,_Brew_Select_B
4582  028d 260e          	jrne	L5572
4583  028f               L1672:
4584                     ; 256           NO_BASKET_PAUSE = 1;
4586  028f 72100017      	bset	_Sys_Flag1,#0
4587                     ; 257           Start_Status = PAUSE;
4589  0293 3502003a      	mov	_Start_Status,#2
4590                     ; 258           Paused_Delay = PAUSE_TIME;
4592  0297 ae012c        	ldw	x,#300
4593  029a cf0034        	ldw	_Paused_Delay,x
4594  029d               L5572:
4595                     ; 261       DECEX( brewAdjust15S );
4597  029d c60028        	ld	a,_brewAdjust15S
4598  02a0 2704          	jreq	L5672
4601  02a2 725a0028      	dec	_brewAdjust15S
4602  02a6               L5672:
4603                     ; 262       DECEX( brewNoaction3S );
4605  02a6 c60027        	ld	a,_brewNoaction3S
4606  02a9 2704          	jreq	L7672
4609  02ab 725a0027      	dec	_brewNoaction3S
4610  02af               L7672:
4611                     ; 263       if( brewAdjust15S == 0)
4613  02af c60028        	ld	a,_brewAdjust15S
4614  02b2 2613          	jrne	L1772
4615                     ; 265         if( brewNoaction3S )
4617  02b4 c60027        	ld	a,_brewNoaction3S
4618  02b7 270e          	jreq	L1772
4619                     ; 267           BeepStart( Cancel_Sound );
4621  02b9 ae0000        	ldw	x,#_Cancel_Sound
4622  02bc cd0000        	call	_BeepStart
4624                     ; 268           Start_Status = OFF;
4626  02bf 725f003a      	clr	_Start_Status
4627                     ; 269           KEEP_WARM_FUNC = 0;
4629  02c3 72130019      	bres	_Disp_Flag,#1
4630  02c7               L1772:
4631                     ; 272       if( Delay_Keep_Warm_Time )
4633  02c7 c6002f        	ld	a,_Delay_Keep_Warm_Time
4634  02ca 270c          	jreq	L5772
4635                     ; 274         Delay_Keep_Warm_Time--;
4637  02cc 725a002f      	dec	_Delay_Keep_Warm_Time
4638                     ; 275         if( Delay_Keep_Warm_Time == 0 )
4640  02d0 2606          	jrne	L5772
4641                     ; 278           BeepStart( Confirm_Sound );
4643  02d2 ae0000        	ldw	x,#_Confirm_Sound
4644  02d5 cd0000        	call	_BeepStart
4646  02d8               L5772:
4647                     ; 281       if( Delay_Keep_Warm_Level )
4649  02d8 c6002e        	ld	a,_Delay_Keep_Warm_Level
4650  02db 270c          	jreq	L1003
4651                     ; 283         Delay_Keep_Warm_Level--;
4653  02dd 725a002e      	dec	_Delay_Keep_Warm_Level
4654                     ; 284         if( Delay_Keep_Warm_Level == 0 )
4656  02e1 2606          	jrne	L1003
4657                     ; 286           BeepStart( Confirm_Sound );
4659  02e3 ae0000        	ldw	x,#_Confirm_Sound
4660  02e6 cd0000        	call	_BeepStart
4662  02e9               L1003:
4663                     ; 290       if((Halfcarafe == Knob0_count [ Brew_Select ]  || fullcarafe == Knob0_count [ Brew_Select ]  )
4663                     ; 291                    && (Classic == Knob1_count [ Brew_Select ]  || Rich == Knob1_count [ Brew_Select ] ))
4665  02e9 c60039        	ld	a,_Brew_Select
4666  02ec 5f            	clrw	x
4667  02ed 97            	ld	xl,a
4668  02ee d60054        	ld	a,(_Knob0_count,x)
4669  02f1 a104          	cp	a,#4
4670  02f3 270c          	jreq	L7003
4672  02f5 c60039        	ld	a,_Brew_Select
4673  02f8 5f            	clrw	x
4674  02f9 97            	ld	xl,a
4675  02fa d60054        	ld	a,(_Knob0_count,x)
4676  02fd a105          	cp	a,#5
4677  02ff 2627          	jrne	L5003
4678  0301               L7003:
4680  0301 c60039        	ld	a,_Brew_Select
4681  0304 5f            	clrw	x
4682  0305 97            	ld	xl,a
4683  0306 724d0051      	tnz	(_Knob1_count,x)
4684  030a 2708          	jreq	L1103
4686  030c 5f            	clrw	x
4687  030d 97            	ld	xl,a
4688  030e d60051        	ld	a,(_Knob1_count,x)
4689  0311 4a            	dec	a
4690  0312 2614          	jrne	L5003
4691  0314               L1103:
4692                     ; 293         if( Keep_Warm_Class == WARM_OFF )
4694  0314 c60037        	ld	a,_Keep_Warm_Class
4695  0317 2606          	jrne	L3103
4696                     ; 295           F_KEEP_WARM_START = 0;
4698  0319 72190000      	bres	_Ctrl_Board_Status,#4
4699  031d 2009          	jra	L5003
4700                     ; 296           if( F_KEEP_WARM_START )
4702                     ; 297             KEEP_WARM_FUNC = 1;
4704  031f               L3103:
4705                     ; 301          if( F_KEEP_WARM_START )
4707  031f 7209000004    	btjf	_Ctrl_Board_Status,#4,L5003
4708                     ; 302             KEEP_WARM_FUNC = 1;
4710  0324 72120019      	bset	_Disp_Flag,#1
4711  0328               L5003:
4712                     ; 305       if( KEEP_WARM_FUNC )
4714  0328 7203001951    	btjf	_Disp_Flag,#1,L3203
4715                     ; 307         if( ++Clock_Keep_Warm.second > 59)
4717  032d 725c003c      	inc	_Clock_Keep_Warm
4718  0331 c6003c        	ld	a,_Clock_Keep_Warm
4719  0334 a13c          	cp	a,#60
4720  0336 2522          	jrult	L5203
4721                     ; 309           Clock_Keep_Warm.second = 0;
4723  0338 725f003c      	clr	_Clock_Keep_Warm
4724                     ; 310           if( ++Clock_Keep_Warm.min_b > 59 )
4726  033c 725c003f      	inc	_Clock_Keep_Warm+3
4727  0340 c6003f        	ld	a,_Clock_Keep_Warm+3
4728  0343 a13c          	cp	a,#60
4729  0345 2513          	jrult	L5203
4730                     ; 312             Clock_Keep_Warm.min_b = 0;
4732  0347 725f003f      	clr	_Clock_Keep_Warm+3
4733                     ; 313             if( ++Clock_Keep_Warm.hour_b > 4 )
4735  034b 725c0040      	inc	_Clock_Keep_Warm+4
4736  034f c60040        	ld	a,_Clock_Keep_Warm+4
4737  0352 a105          	cp	a,#5
4738  0354 2504          	jrult	L5203
4739                     ; 314               Clock_Keep_Warm.hour_b = 0;
4741  0356 725f0040      	clr	_Clock_Keep_Warm+4
4742  035a               L5203:
4743                     ; 317         if( Clock_Keep_Warm.min == Clock_Keep_Warm.min_b
4743                     ; 318           && Clock_Keep_Warm.hour == Clock_Keep_Warm.hour_b )
4745  035a c6003d        	ld	a,_Clock_Keep_Warm+1
4746  035d c1003f        	cp	a,_Clock_Keep_Warm+3
4747  0360 261c          	jrne	L3203
4749  0362 c6003e        	ld	a,_Clock_Keep_Warm+2
4750  0365 c10040        	cp	a,_Clock_Keep_Warm+4
4751  0368 2614          	jrne	L3203
4752                     ; 320           KEEP_WARM_FUNC = 0; 
4754  036a 72130019      	bres	_Disp_Flag,#1
4755                     ; 321           F_KEEP_WARM_START = 0;
4757  036e 72190000      	bres	_Ctrl_Board_Status,#4
4758                     ; 322           Clock_Keep_Warm.second = 0;
4760  0372 725f003c      	clr	_Clock_Keep_Warm
4761                     ; 323           Clock_Keep_Warm.min_b  = 0;
4763  0376 725f003f      	clr	_Clock_Keep_Warm+3
4764                     ; 324           Clock_Keep_Warm.hour_b = 0;
4766  037a 725f0040      	clr	_Clock_Keep_Warm+4
4767  037e               L3203:
4768                     ; 327        if( DELAY_SET_TYPE && DELAY_SET_SIZE && DELAY_SET_STYLE )
4770  037e 720d001815    	btjf	_Disp_Flag1,#6,L5303
4772  0383 7209001810    	btjf	_Disp_Flag1,#4,L5303
4774  0388 720b00180b    	btjf	_Disp_Flag1,#5,L5303
4775                     ; 329          if( Delay_Brew_Time > 5 )
4777  038d c6002c        	ld	a,_Delay_Brew_Time
4778  0390 a106          	cp	a,#6
4779  0392 2504          	jrult	L5303
4780                     ; 330           Delay_Brew_Time = 5;
4782  0394 3505002c      	mov	_Delay_Brew_Time,#5
4783  0398               L5303:
4784                     ; 332        if( Delay_Brew_Time )
4786  0398 c6002c        	ld	a,_Delay_Brew_Time
4787  039b 272d          	jreq	L1403
4788                     ; 334           Delay_Brew_Time--;
4790  039d 725a002c      	dec	_Delay_Brew_Time
4791                     ; 336           if( Delay_Brew_Time == 0 )
4793  03a1 2627          	jrne	L1403
4794                     ; 338             if( DELAY_SET_TYPE && DELAY_SET_SIZE && DELAY_SET_STYLE )
4796  03a3 720d001812    	btjf	_Disp_Flag1,#6,L5403
4798  03a8 720900180d    	btjf	_Disp_Flag1,#4,L5403
4800  03ad 720b001808    	btjf	_Disp_Flag1,#5,L5403
4801                     ; 340               if( DLY_BREW_FLAG )
4803  03b2 7209001903    	btjf	_Disp_Flag,#4,L5403
4804                     ; 342                 Delay_Brew_Sure( );
4806  03b7 cd0123        	call	_Delay_Brew_Sure
4808  03ba               L5403:
4809                     ; 345             DLY_BREW_FLAG   = 0;
4811  03ba 72190019      	bres	_Disp_Flag,#4
4812                     ; 346             DELAY_SET_TYPE  = 0;
4814  03be 721d0018      	bres	_Disp_Flag1,#6
4815                     ; 347             DELAY_SET_SIZE  = 0;
4817  03c2 72190018      	bres	_Disp_Flag1,#4
4818                     ; 348             DELAY_SET_STYLE = 0;
4820  03c6 721b0018      	bres	_Disp_Flag1,#5
4821  03ca               L1403:
4822                     ; 352       if( DLY_BREW_FUNC )
4824  03ca 720a001903cc  	btjf	_Disp_Flag,#5,L1503
4825                     ; 354          Clock_Min_Total = (U16)(Clock.hour * 60) + (U16)Clock.min;
4827  03d2 c6004c        	ld	a,_Clock+1
4828  03d5 5f            	clrw	x
4829  03d6 97            	ld	xl,a
4830  03d7 1f01          	ldw	(OFST-1,sp),x
4831  03d9 c6004d        	ld	a,_Clock+2
4832  03dc 97            	ld	xl,a
4833  03dd a63c          	ld	a,#60
4834  03df 42            	mul	x,a
4835  03e0 72fb01        	addw	x,(OFST-1,sp)
4836  03e3 cf0002        	ldw	_Clock_Min_Total,x
4837                     ; 355          if( AM_PM_FLAG == DELAY_AM_PM_FLAG)
4839  03e6 7201001907    	btjf	_Disp_Flag,#0,L04
4840  03eb 720d00191c    	btjf	_Disp_Flag,#6,L3503
4841  03f0 2005          	jra	L24
4842  03f2               L04:
4843  03f2 720c001915    	btjt	_Disp_Flag,#6,L3503
4844  03f7               L24:
4845                     ; 357             if(( Clock_Min_Total == Delay_Brew_Min_Total)
4845                     ; 358               && ( Clock.second == Clock_DLY_BREW.second ))
4847  03f7 c30000        	cpw	x,_Delay_Brew_Min_Total
4848  03fa 2610          	jrne	L3503
4850  03fc c6004b        	ld	a,_Clock
4851  03ff c10046        	cp	a,_Clock_DLY_BREW
4852  0402 2608          	jrne	L3503
4853                     ; 360               DLY_BREW_FUNC = 0;
4855  0404 721b0019      	bres	_Disp_Flag,#5
4856                     ; 361               Start_Status = ON;
4858  0408 3501003a      	mov	_Start_Status,#1
4859  040c               L3503:
4860                     ; 364          if( Brew_Select_B == NO_BASKET )
4862  040c c60038        	ld	a,_Brew_Select_B
4863  040f 2611          	jrne	L7503
4864                     ; 366           if( delayBrewPause == 0)
4866  0411 ce0024        	ldw	x,_delayBrewPause
4867  0414 262c          	jrne	L3603
4868                     ; 368             delayBrewPause = DELAYPUASE;
4870  0416 ae012c        	ldw	x,#300
4871  0419 cf0024        	ldw	_delayBrewPause,x
4872                     ; 369             Pause_Sound_Delay = 10;
4874  041c 350a0029      	mov	_Pause_Sound_Delay,#10
4875  0420 2020          	jra	L3603
4876  0422               L7503:
4877                     ; 372          else if( Brew_Select_B == Brew_Select )
4879  0422 c10039        	cp	a,_Brew_Select
4880  0425 260a          	jrne	L5603
4881                     ; 374            delayBrewPause = 0;
4883  0427 5f            	clrw	x
4884  0428 cf0024        	ldw	_delayBrewPause,x
4885                     ; 375            Pause_Sound_Delay = 0;
4887  042b 725f0029      	clr	_Pause_Sound_Delay
4889  042f 2011          	jra	L3603
4890  0431               L5603:
4891                     ; 377          else if( Brew_Select_B != Brew_Select )
4893  0431 c10039        	cp	a,_Brew_Select
4894  0434 270c          	jreq	L3603
4895                     ; 379            delayBrewPause = 0;
4897  0436 5f            	clrw	x
4898  0437 cf0024        	ldw	_delayBrewPause,x
4899                     ; 380            Pause_Sound_Delay = 0;
4901  043a 725f0029      	clr	_Pause_Sound_Delay
4902                     ; 381            DLY_BREW_FUNC = 0;
4904  043e 721b0019      	bres	_Disp_Flag,#5
4905  0442               L3603:
4906                     ; 383         if( delayBrewPause )
4908  0442 ce0024        	ldw	x,_delayBrewPause
4909  0445 2721          	jreq	L3703
4910                     ; 385          delayBrewPause--;
4912  0447 5a            	decw	x
4913  0448 cf0024        	ldw	_delayBrewPause,x
4914                     ; 386          if( delayBrewPause <= DELAYPUASE - 20 )
4916  044b a30119        	cpw	x,#281
4917  044e 240f          	jruge	L5703
4918                     ; 388           if( Pause_Sound_Delay )
4920  0450 c60029        	ld	a,_Pause_Sound_Delay
4921  0453 270a          	jreq	L5703
4922                     ; 390            Pause_Sound_Delay--;
4924  0455 725a0029      	dec	_Pause_Sound_Delay
4925                     ; 391            BeepStart( Reminder_Sound );
4927  0459 ae0000        	ldw	x,#_Reminder_Sound
4928  045c cd0000        	call	_BeepStart
4930  045f               L5703:
4931                     ; 394          if( delayBrewPause == 0)
4933  045f ce0024        	ldw	x,_delayBrewPause
4934  0462 2604          	jrne	L3703
4935                     ; 395           DLY_BREW_FUNC = 0;
4937  0464 721b0019      	bres	_Disp_Flag,#5
4938  0468               L3703:
4939                     ; 398         DECEX( delayAdjust15S );
4941  0468 c60023        	ld	a,_delayAdjust15S
4942  046b 2704          	jreq	L3013
4945  046d 725a0023      	dec	_delayAdjust15S
4946  0471               L3013:
4947                     ; 399         DECEX( delayNoaction3S );
4949  0471 c60022        	ld	a,_delayNoaction3S
4950  0474 2704          	jreq	L5013
4953  0476 725a0022      	dec	_delayNoaction3S
4954  047a               L5013:
4955                     ; 400         if( delayAdjust15S == 0)
4957  047a c60023        	ld	a,_delayAdjust15S
4958  047d 260f          	jrne	L1503
4959                     ; 402           if( delayNoaction3S )
4961  047f c60022        	ld	a,_delayNoaction3S
4962  0482 270a          	jreq	L1503
4963                     ; 404             BeepStart( Cancel_Sound );
4965  0484 ae0000        	ldw	x,#_Cancel_Sound
4966  0487 cd0000        	call	_BeepStart
4968                     ; 405             DLY_BREW_FUNC = 0;
4970  048a 721b0019      	bres	_Disp_Flag,#5
4971  048e               L1503:
4972                     ; 410       DECEX( CANCEL_DELAY_BREW_3S );
4974  048e c6002b        	ld	a,_CANCEL_DELAY_BREW_3S
4975  0491 2704          	jreq	L3113
4978  0493 725a002b      	dec	_CANCEL_DELAY_BREW_3S
4979  0497               L3113:
4980                     ; 411       DECEX( CLean_Blink_10count );
4982  0497 c60000        	ld	a,_CLean_Blink_10count
4983  049a 2704          	jreq	L5113
4986  049c 725a0000      	dec	_CLean_Blink_10count
4987  04a0               L5113:
4988                     ; 412       if( cleanAlarm )
4990  04a0 c60020        	ld	a,_cleanAlarm
4991  04a3 2704          	jreq	L7113
4992                     ; 414         cleanAlarm--;
4994  04a5 725a0020      	dec	_cleanAlarm
4995  04a9               L7113:
4996                     ; 416       if( cleanStart )
4998  04a9 c6001f        	ld	a,_cleanStart
4999  04ac 270a          	jreq	L1213
5000                     ; 418         cleanStart--;
5002  04ae 725a001f      	dec	_cleanStart
5003                     ; 419         if( cleanStart == 0 )
5005  04b2 2604          	jrne	L1213
5006                     ; 420           CLEAN_DISP_FLAG = 0;
5008  04b4 72110018      	bres	_Disp_Flag1,#0
5009  04b8               L1213:
5010                     ; 422       if( CLEAN_FUNC_FLAG )
5012  04b8 7203001844    	btjf	_Disp_Flag1,#1,L5213
5013                     ; 424           if( Start_Status == ON || Start_Status == UNPAUSE )
5015  04bd c6003a        	ld	a,_Start_Status
5016  04c0 a101          	cp	a,#1
5017  04c2 2704          	jreq	L1313
5019  04c4 a104          	cp	a,#4
5020  04c6 2639          	jrne	L5213
5021  04c8               L1313:
5022                     ; 426             if( Clock_Clean.second )
5024  04c8 c60041        	ld	a,_Clock_Clean
5025  04cb 2706          	jreq	L3313
5026                     ; 427               Clock_Clean.second--;
5028  04cd 725a0041      	dec	_Clock_Clean
5030  04d1 2020          	jra	L5313
5031  04d3               L3313:
5032                     ; 428             else if( Clock_Clean.min )
5034  04d3 c60042        	ld	a,_Clock_Clean+1
5035  04d6 270a          	jreq	L7313
5036                     ; 430               Clock_Clean.second = 59;
5038  04d8 353b0041      	mov	_Clock_Clean,#59
5039                     ; 431               Clock_Clean.min--;
5041  04dc 725a0042      	dec	_Clock_Clean+1
5043  04e0 2011          	jra	L5313
5044  04e2               L7313:
5045                     ; 433             else if( Clock_Clean.hour ) 
5047  04e2 c60043        	ld	a,_Clock_Clean+2
5048  04e5 270c          	jreq	L5313
5049                     ; 435                Clock_Clean.second = 59;
5051  04e7 353b0041      	mov	_Clock_Clean,#59
5052                     ; 436                Clock_Clean.min    = 59;
5054  04eb 353b0042      	mov	_Clock_Clean+1,#59
5055                     ; 437                Clock_Clean.hour--;
5057  04ef 725a0043      	dec	_Clock_Clean+2
5058  04f3               L5313:
5059                     ; 439             if(( Clock_Clean.hour == 0) && ( Clock_Clean.min == 0))
5061  04f3 c60043        	ld	a,_Clock_Clean+2
5062  04f6 2609          	jrne	L5213
5064  04f8 c60042        	ld	a,_Clock_Clean+1
5065  04fb 2604          	jrne	L5213
5066                     ; 441                 CLEAN_FUNC_FLAG = 0;
5068  04fd 72130018      	bres	_Disp_Flag1,#1
5069  0501               L5213:
5070                     ; 445       if( Complete_Sound_Delay )
5072  0501 c6002a        	ld	a,_Complete_Sound_Delay
5073  0504 2719          	jreq	L1072
5074                     ; 447         Complete_Sound_Delay--;
5076  0506 725a002a      	dec	_Complete_Sound_Delay
5077                     ; 448         if( F_Brew_complete )
5079  050a 7203000005    	btjf	_Ctrl_Board_Status,#1,L1513
5080                     ; 450         BeepStart( Complete_Sound );
5082  050f ae0000        	ldw	x,#_Complete_Sound
5085  0512 2008          	jp	LC002
5086  0514               L1513:
5087                     ; 451         else if( F_Brew_abnormal_end )
5089  0514 7205000006    	btjf	_Ctrl_Board_Status,#2,L1072
5090                     ; 452         BeepStart( Reminder_Sound );
5092  0519 ae0000        	ldw	x,#_Reminder_Sound
5093  051c               LC002:
5094  051c cd0000        	call	_BeepStart
5096  051f               L1072:
5097                     ; 456 }
5100  051f 85            	popw	x
5101  0520 81            	ret	
5104                     	switch	.data
5105  000b               L7513_key_scan_old:
5106  000b 0000          	dc.w	0
5107                     .const:	section	.text
5108  0000               L3613_key_scan:
5109  0000 0000          	dc.w	0
5110                     	switch	.bss
5111  0000               L1613_key_time:
5112  0000 00            	ds.b	1
5398                     ; 458 void Key_Scan(void)
5398                     ; 459 {
5399                     	switch	.text
5400  0521               _Key_Scan:
5402  0521 5207          	subw	sp,#7
5403       00000007      OFST:	set	7
5406                     ; 463   WORD key_scan = { 0 };
5408  0523 c60000        	ld	a,L3613_key_scan
5409  0526 6b01          	ld	(OFST-6,sp),a
5410  0528 c60001        	ld	a,L3613_key_scan+1
5411  052b 6b02          	ld	(OFST-5,sp),a
5412                     ; 465   memset( key, 0x00, sizeof( key ) );
5414  052d 96            	ldw	x,sp
5415  052e 1c0003        	addw	x,#OFST-4
5416  0531 bf00          	ldw	c_x,x
5417  0533 ae0005        	ldw	x,#5
5418  0536               L65:
5419  0536 5a            	decw	x
5420  0537 926f00        	clr	([c_x.w],x)
5421  053a 5d            	tnzw	x
5422  053b 26f9          	jrne	L65
5423                     ; 466 	Pt696xRd(key,sizeof( key ));
5425  053d 4b05          	push	#5
5426  053f 96            	ldw	x,sp
5427  0540 1c0004        	addw	x,#OFST-3
5428  0543 cd0000        	call	_Pt696xRd
5430  0546 84            	pop	a
5431                     ; 467   if( key[0] & 0x01 ) key_scan.bits.b0  = 1;
5433  0547 7b03          	ld	a,(OFST-4,sp)
5434  0549 a501          	bcp	a,#1
5435  054b 2708          	jreq	L7433
5438  054d 7b01          	ld	a,(OFST-6,sp)
5439  054f aa01          	or	a,#1
5440  0551 6b01          	ld	(OFST-6,sp),a
5441  0553 7b03          	ld	a,(OFST-4,sp)
5442  0555               L7433:
5443                     ; 468 	if( key[0] & 0x02 ) key_scan.bits.b1  = 1;
5445  0555 a502          	bcp	a,#2
5446  0557 2708          	jreq	L1533
5449  0559 7b01          	ld	a,(OFST-6,sp)
5450  055b aa02          	or	a,#2
5451  055d 6b01          	ld	(OFST-6,sp),a
5452  055f 7b03          	ld	a,(OFST-4,sp)
5453  0561               L1533:
5454                     ; 469   if( key[0] & 0x08 ) key_scan.bits.b2  = 1;
5456  0561 a508          	bcp	a,#8
5457  0563 2708          	jreq	L3533
5460  0565 7b01          	ld	a,(OFST-6,sp)
5461  0567 aa04          	or	a,#4
5462  0569 6b01          	ld	(OFST-6,sp),a
5463  056b 7b03          	ld	a,(OFST-4,sp)
5464  056d               L3533:
5465                     ; 470 	if( key[0] & 0x10 ) key_scan.bits.b3  = 1;
5467  056d a510          	bcp	a,#16
5468  056f 2706          	jreq	L5533
5471  0571 7b01          	ld	a,(OFST-6,sp)
5472  0573 aa08          	or	a,#8
5473  0575 6b01          	ld	(OFST-6,sp),a
5474  0577               L5533:
5475                     ; 472   if( key[1] & 0x01 ) key_scan.bits.b4  = 1;
5477  0577 7b04          	ld	a,(OFST-3,sp)
5478  0579 a501          	bcp	a,#1
5479  057b 2708          	jreq	L7533
5482  057d 7b01          	ld	a,(OFST-6,sp)
5483  057f aa10          	or	a,#16
5484  0581 6b01          	ld	(OFST-6,sp),a
5485  0583 7b04          	ld	a,(OFST-3,sp)
5486  0585               L7533:
5487                     ; 473 	if( key[1] & 0x02 ) key_scan.bits.b5  = 1;
5489  0585 a502          	bcp	a,#2
5490  0587 2708          	jreq	L1633
5493  0589 7b01          	ld	a,(OFST-6,sp)
5494  058b aa20          	or	a,#32
5495  058d 6b01          	ld	(OFST-6,sp),a
5496  058f 7b04          	ld	a,(OFST-3,sp)
5497  0591               L1633:
5498                     ; 474   if( key[1] & 0x08 ) key_scan.bits.b6  = 1;
5500  0591 a508          	bcp	a,#8
5501  0593 2708          	jreq	L3633
5504  0595 7b01          	ld	a,(OFST-6,sp)
5505  0597 aa40          	or	a,#64
5506  0599 6b01          	ld	(OFST-6,sp),a
5507  059b 7b04          	ld	a,(OFST-3,sp)
5508  059d               L3633:
5509                     ; 475 	if( key[1] & 0x10 ) key_scan.bits.b7  = 1;
5511  059d a510          	bcp	a,#16
5512  059f 2706          	jreq	L5633
5515  05a1 7b01          	ld	a,(OFST-6,sp)
5516  05a3 aa80          	or	a,#128
5517  05a5 6b01          	ld	(OFST-6,sp),a
5518  05a7               L5633:
5519                     ; 477   if( key[2] & 0x01 ) key_scan.bits.b8  = 1;
5521  05a7 7b05          	ld	a,(OFST-2,sp)
5522  05a9 a501          	bcp	a,#1
5523  05ab 2708          	jreq	L7633
5526  05ad 7b02          	ld	a,(OFST-5,sp)
5527  05af aa01          	or	a,#1
5528  05b1 6b02          	ld	(OFST-5,sp),a
5529  05b3 7b05          	ld	a,(OFST-2,sp)
5530  05b5               L7633:
5531                     ; 478 	if( key[2] & 0x02 ) key_scan.bits.b9  = 1;
5533  05b5 a502          	bcp	a,#2
5534  05b7 2708          	jreq	L1733
5537  05b9 7b02          	ld	a,(OFST-5,sp)
5538  05bb aa02          	or	a,#2
5539  05bd 6b02          	ld	(OFST-5,sp),a
5540  05bf 7b05          	ld	a,(OFST-2,sp)
5541  05c1               L1733:
5542                     ; 479   if( key[2] & 0x08 ) key_scan.bits.b10 = 1;
5544  05c1 a508          	bcp	a,#8
5545  05c3 2708          	jreq	L3733
5548  05c5 7b02          	ld	a,(OFST-5,sp)
5549  05c7 aa04          	or	a,#4
5550  05c9 6b02          	ld	(OFST-5,sp),a
5551  05cb 7b05          	ld	a,(OFST-2,sp)
5552  05cd               L3733:
5553                     ; 480 	if( key[2] & 0x10 ) key_scan.bits.b11 = 1;
5555  05cd a510          	bcp	a,#16
5556  05cf 2706          	jreq	L5733
5559  05d1 7b02          	ld	a,(OFST-5,sp)
5560  05d3 aa08          	or	a,#8
5561  05d5 6b02          	ld	(OFST-5,sp),a
5562  05d7               L5733:
5563                     ; 482   if( key[3] & 0x01 ) key_scan.bits.b12 = 1;
5565  05d7 7b06          	ld	a,(OFST-1,sp)
5566  05d9 a501          	bcp	a,#1
5567  05db 2708          	jreq	L7733
5570  05dd 7b02          	ld	a,(OFST-5,sp)
5571  05df aa10          	or	a,#16
5572  05e1 6b02          	ld	(OFST-5,sp),a
5573  05e3 7b06          	ld	a,(OFST-1,sp)
5574  05e5               L7733:
5575                     ; 483 	if( key[3] & 0x02 ) key_scan.bits.b13 = 1;
5577  05e5 a502          	bcp	a,#2
5578  05e7 2708          	jreq	L1043
5581  05e9 7b02          	ld	a,(OFST-5,sp)
5582  05eb aa20          	or	a,#32
5583  05ed 6b02          	ld	(OFST-5,sp),a
5584  05ef 7b06          	ld	a,(OFST-1,sp)
5585  05f1               L1043:
5586                     ; 484   if( key[3] & 0x08 ) key_scan.bits.b14 = 1;
5588  05f1 a508          	bcp	a,#8
5589  05f3 2708          	jreq	L3043
5592  05f5 7b02          	ld	a,(OFST-5,sp)
5593  05f7 aa40          	or	a,#64
5594  05f9 6b02          	ld	(OFST-5,sp),a
5595  05fb 7b06          	ld	a,(OFST-1,sp)
5596  05fd               L3043:
5597                     ; 485 	if( key[3] & 0x10 ) key_scan.bits.b15 = 1;
5599  05fd a510          	bcp	a,#16
5600  05ff 2706          	jreq	L5043
5603  0601 7b02          	ld	a,(OFST-5,sp)
5604  0603 aa80          	or	a,#128
5605  0605 6b02          	ld	(OFST-5,sp),a
5606  0607               L5043:
5607                     ; 487   if( key_scan_old.word != key_scan.word )
5609  0607 ce000b        	ldw	x,L7513_key_scan_old
5610  060a 1301          	cpw	x,(OFST-6,sp)
5611  060c 270b          	jreq	L7043
5612                     ; 489     key_time = 0;
5614  060e 725f0000      	clr	L1613_key_time
5615                     ; 490     key_scan_old.word = key_scan.word;
5617  0612 1e01          	ldw	x,(OFST-6,sp)
5618  0614 cf000b        	ldw	L7513_key_scan_old,x
5620  0617 201a          	jra	L1143
5621  0619               L7043:
5622                     ; 494     INCEX( key_time );
5624  0619 725c0000      	inc	L1613_key_time
5625  061d 2604          	jrne	L3143
5628  061f 725a0000      	dec	L1613_key_time
5629  0623               L3143:
5630                     ; 495     if( key_time == 2 )
5632  0623 c60000        	ld	a,L1613_key_time
5633  0626 a102          	cp	a,#2
5634  0628 2609          	jrne	L1143
5635                     ; 497            keyScan = key_scan.word;
5637  062a 1e01          	ldw	x,(OFST-6,sp)
5638  062c bf05          	ldw	_keyScan,x
5639                     ; 498            if( keyScan == 0 )
5641  062e 2603          	jrne	L1143
5642                     ; 499              keyScan_old = 0;
5644  0630 5f            	clrw	x
5645  0631 bf03          	ldw	_keyScan_old,x
5646  0633               L1143:
5647                     ; 503 } 
5650  0633 5b07          	addw	sp,#7
5651  0635 81            	ret	
5682                     ; 506 void KeyGenerate( void )
5682                     ; 507   {
5683                     	switch	.text
5684  0636               _KeyGenerate:
5688                     ; 508     if ( keyData != keyScan )
5690  0636 be07          	ldw	x,_keyData
5691  0638 b305          	cpw	x,_keyScan
5692  063a 272b          	jreq	L5053
5693                     ; 511         if(keyScan_old == 0)
5695  063c be03          	ldw	x,_keyScan_old
5696  063e 2606          	jrne	L7053
5697                     ; 513             keyScan_old= keyScan;
5699  0640 be05          	ldw	x,_keyScan
5700  0642 bf03          	ldw	_keyScan_old,x
5701                     ; 514             keyData = keyScan;
5703  0644 2016          	jra	L5153
5704  0646               L7053:
5705                     ; 516         else if( keyScan == C_SC_HOUR_STYLE 
5705                     ; 517               || keyScan == C_SC_MINUTE_TYPE 
5705                     ; 518               || keyScan == C_SC_CLEAN_STYLE
5705                     ; 519               || keyScan == C_SC_MINUTE_HOUR)
5707  0646 be05          	ldw	x,_keyScan
5708  0648 a30300        	cpw	x,#768
5709  064b 270f          	jreq	L5153
5711  064d a34800        	cpw	x,#18432
5712  0650 270a          	jreq	L5153
5714  0652 a30500        	cpw	x,#1280
5715  0655 2705          	jreq	L5153
5717  0657 a30a00        	cpw	x,#2560
5718  065a 2608          	jrne	L3153
5719  065c               L5153:
5720                     ; 521           keyData = keyScan;
5724  065c bf07          	ldw	_keyData,x
5725                     ; 526         keyTime = C_KT_REPEAT;
5727  065e ae0014        	ldw	x,#20
5728  0661 bf09          	ldw	_keyTime,x
5731  0663 81            	ret	
5732  0664               L3153:
5733                     ; 524           keyData = 0;
5735  0664 5f            	clrw	x
5736  0665 20f5          	jra	L5153
5737  0667               L5053:
5738                     ; 530         INCEX( keyTime );
5740  0667 be09          	ldw	x,_keyTime
5741  0669 5c            	incw	x
5742  066a 2601          	jrne	L7253
5745  066c 5a            	decw	x
5746  066d               L7253:
5747  066d bf09          	ldw	_keyTime,x
5748                     ; 531         switch ( keyTime )
5751                     ; 580             default:
5751                     ; 581                 break;
5752  066f 1d0018        	subw	x,#24
5753  0672 2729          	jreq	L1243
5754  0674 1d0060        	subw	x,#96
5755  0677 2603cc076b    	jreq	L1643
5756  067c 1d0064        	subw	x,#100
5757  067f 2603cc0777    	jreq	L3643
5758  0684 1d012c        	subw	x,#300
5759  0687 2603cc077f    	jreq	L5643
5760  068c 1d01f4        	subw	x,#500
5761  068f 2603cc0787    	jreq	L7643
5762  0694 1d07d0        	subw	x,#2000
5763  0697 2603cc078f    	jreq	L1743
5765  069c 81            	ret	
5766  069d               L1243:
5767                     ; 535                   keyLast = keyCopy;
5769  069d 450001        	mov	_keyLast,_keyCopy
5770                     ; 536                   switch( keyData )
5772  06a0 be07          	ldw	x,_keyData
5774                     ; 555                     default:              keyCode = C_KC_UNDEF;         break;
5775  06a2 2751          	jreq	L3243
5776  06a4 5a            	decw	x
5777  06a5 2603cc0749    	jreq	L5443
5778  06aa 1d00ff        	subw	x,#255
5779  06ad 2772          	jreq	L5343
5780  06af 1d0100        	subw	x,#256
5781  06b2 2745          	jreq	L5243
5782  06b4 1d0100        	subw	x,#256
5783  06b7 2603cc0753    	jreq	L7443
5784  06bc 1d0100        	subw	x,#256
5785  06bf 276a          	jreq	L7343
5786  06c1 1d0100        	subw	x,#256
5787  06c4 2603cc075f    	jreq	L3543
5788  06c9 1d0300        	subw	x,#768
5789  06cc 2735          	jreq	L7243
5790  06ce 1d0200        	subw	x,#512
5791  06d1 2603cc0765    	jreq	L5543
5792  06d6 1d0600        	subw	x,#1536
5793  06d9 275a          	jreq	L1443
5794  06db 1d1000        	subw	x,#4096
5795  06de 2737          	jreq	L3343
5796  06e0 1d2000        	subw	x,#8192
5797  06e3 275a          	jreq	L3443
5798  06e5 1d0800        	subw	x,#2048
5799  06e8 276f          	jreq	L1543
5800  06ea 1d3800        	subw	x,#14336
5801  06ed 271e          	jreq	L1343
5804  06ef 35ff0002      	mov	_keyCode,#255
5807  06f3 207e          	jra	LC003
5808  06f5               L3243:
5809                     ; 538                     case C_SC_NONE:       keyCode = C_KC_NONE;                              break;
5811  06f5 3f02          	clr	_keyCode
5814  06f7 207a          	jra	LC003
5815  06f9               L5243:
5816                     ; 540                     case C_SC_HOUR:       keyCode = C_KC_HOUR;       KEY_Buff[ 0 ] = 0x01;  break;
5818  06f9 35010002      	mov	_keyCode,#1
5821  06fd 35010005      	mov	_KEY_Buff,#1
5824  0701 2070          	jra	LC003
5825  0703               L7243:
5826                     ; 541                     case C_SC_MINUTE:     keyCode = C_KC_MINUTE;     KEY_Buff[ 1 ] = 0x01;  break;
5828  0703 35020002      	mov	_keyCode,#2
5831  0707 35010006      	mov	_KEY_Buff+1,#1
5834  070b 2066          	jra	LC003
5835  070d               L1343:
5836                     ; 542                     case C_SC_KEEPWARM:   keyCode = C_KC_KEEPWARM;   KEY_Buff[ 2 ] = 0x01;  break;
5838  070d 35030002      	mov	_keyCode,#3
5841  0711 35010007      	mov	_KEY_Buff+2,#1
5844  0715 205c          	jra	LC003
5845  0717               L3343:
5846                     ; 543                     case C_SC_DELAYSET:   keyCode = C_KC_DELAYSET;   KEY_Buff[ 3 ] = 0x01;  break;
5848  0717 35040002      	mov	_keyCode,#4
5851  071b 35010008      	mov	_KEY_Buff+3,#1
5854  071f 2052          	jra	LC003
5855  0721               L5343:
5856                     ; 545                     case C_SC_STYLE:      keyCode = C_KC_STYLE;      KEY_Buff[ 4 ] = 0x01;  break;
5858  0721 35050002      	mov	_keyCode,#5
5861  0725 35010009      	mov	_KEY_Buff+4,#1
5864  0729 2048          	jra	LC003
5865  072b               L7343:
5866                     ; 546                     case C_SC_CLEAN:      keyCode = C_KC_CLEAN;      KEY_Buff[ 5 ] = 0x01;  break;
5868  072b 35080002      	mov	_keyCode,#8
5871  072f 3501000a      	mov	_KEY_Buff+5,#1
5874  0733 203e          	jra	LC003
5875  0735               L1443:
5876                     ; 547                     case C_SC_START:      keyCode = C_KC_START;      KEY_Buff[ 6 ] = 0x01;  break;
5878  0735 35090002      	mov	_keyCode,#9
5881  0739 3501000b      	mov	_KEY_Buff+6,#1
5884  073d 2034          	jra	LC003
5885  073f               L3443:
5886                     ; 548                     case C_SC_TYPE:       keyCode = C_KC_TYPE;       KEY_Buff[ 7 ] = 0x01;  break;
5888  073f 35060002      	mov	_keyCode,#6
5891  0743 3501000c      	mov	_KEY_Buff+7,#1
5894  0747 202a          	jra	LC003
5895  0749               L5443:
5896                     ; 550                     case C_SC_POWER:      keyCode = C_KC_POWER;      KEY_Buff[ 8 ] = 0x01;  break;
5898  0749 35070002      	mov	_keyCode,#7
5901  074d 3501000d      	mov	_KEY_Buff+8,#1
5904  0751 2020          	jra	LC003
5905  0753               L7443:
5906                     ; 551                     case C_SC_HOUR_STYLE: keyCode = C_KC_HOUR_STYLE;                        break;
5908  0753 350a0002      	mov	_keyCode,#10
5911  0757 201a          	jra	LC003
5912  0759               L1543:
5913                     ; 552                     case C_SC_MINUTE_TYPE: keyCode = C_KC_MINUTE_TYPE;                      break;
5915  0759 350b0002      	mov	_keyCode,#11
5918  075d 2014          	jra	LC003
5919  075f               L3543:
5920                     ; 553                     case C_SC_CLEAN_STYLE: keyCode = C_KC_CLEAN_STYLE;                      break;
5922  075f 350c0002      	mov	_keyCode,#12
5925  0763 200e          	jra	LC003
5926  0765               L5543:
5927                     ; 554                     case C_SC_MINUTE_HOUR: keyCode = C_KC_MINUTE_HOUR;                      break;
5929  0765 350d0002      	mov	_keyCode,#13
5932                     ; 558                keyCopy = keyCode;
5933                     ; 559                break;
5935  0769 2008          	jp	LC003
5936  076b               L1643:
5937                     ; 560             case C_KT_1SEC:
5937                     ; 561                 keyCode = ( keyCode & 0x1f ) | C_KS_1SEC;
5939  076b b602          	ld	a,_keyCode
5940  076d a41f          	and	a,#31
5941  076f aa20          	or	a,#32
5942  0771               LC004:
5943  0771 b702          	ld	_keyCode,a
5944                     ; 562                 keyCopy = keyCode;
5946  0773               LC003:
5952  0773 450200        	mov	_keyCopy,_keyCode
5953                     ; 563                 break;
5954                     ; 585 }
5957  0776 81            	ret	
5958  0777               L3643:
5959                     ; 564             case C_KT_2SEC:
5959                     ; 565                 keyCode = ( keyCode & 0x1f ) | C_KS_2SEC;
5961  0777 b602          	ld	a,_keyCode
5962  0779 a41f          	and	a,#31
5963  077b aa40          	or	a,#64
5964                     ; 566                 keyCopy = keyCode;
5965                     ; 567                 break;
5967  077d 20f2          	jp	LC004
5968  077f               L5643:
5969                     ; 568             case C_KT_5SEC:
5969                     ; 569                 keyCode = ( keyCode & 0x1f ) | C_KS_5SEC;
5971  077f b602          	ld	a,_keyCode
5972  0781 a41f          	and	a,#31
5973  0783 aa60          	or	a,#96
5974                     ; 570                 keyCopy = keyCode;
5975                     ; 571                 break;
5977  0785 20ea          	jp	LC004
5978  0787               L7643:
5979                     ; 572             case C_KT_10SEC:
5979                     ; 573                 keyCode = ( keyCode & 0x1f ) | C_KS_10SEC;
5981  0787 b602          	ld	a,_keyCode
5982  0789 a41f          	and	a,#31
5983  078b aa80          	or	a,#128
5984                     ; 574                 keyCopy = keyCode;
5985                     ; 575                 break;
5987  078d 20e2          	jp	LC004
5988  078f               L1743:
5989                     ; 576             case C_KT_30SEC:
5989                     ; 577                 keyCode = ( keyCode & 0x1f ) | C_KS_30SEC;
5991  078f b602          	ld	a,_keyCode
5992  0791 a41f          	and	a,#31
5993  0793 aaa0          	or	a,#160
5994                     ; 578                 keyCopy = keyCode;
5995                     ; 579                 break;
5997  0795 20da          	jp	LC004
5998                     ; 580             default:
5998                     ; 581                 break;
6023                     ; 586 void KeyRepeat( void )
6023                     ; 587   {
6024                     	switch	.text
6025  0797               _KeyRepeat:
6029                     ; 588     if ( keyTime >= C_KT_REPEAT )
6031  0797 be09          	ldw	x,_keyTime
6032  0799 a30014        	cpw	x,#20
6033  079c 2505          	jrult	L1553
6034                     ; 590         keyTime -= C_KT_REPEAT;
6036  079e 1d0014        	subw	x,#20
6037  07a1 bf09          	ldw	_keyTime,x
6038  07a3               L1553:
6039                     ; 592   }
6042  07a3 81            	ret	
6117                     ; 594 void KeyHandler( void )
6117                     ; 595 {
6118                     	switch	.text
6119  07a4               _KeyHandler:
6121  07a4 88            	push	a
6122       00000001      OFST:	set	1
6125                     ; 599   if ( C_KS_DEALED != ( keyCode & 0xe0 ) ) 
6127  07a5 b602          	ld	a,_keyCode
6128  07a7 a4e0          	and	a,#224
6129  07a9 a1e0          	cp	a,#224
6130  07ab 2603cc0f9d    	jreq	L3363
6131                     ; 601         key = keyCode;
6133  07b0 b602          	ld	a,_keyCode
6134  07b2 6b01          	ld	(OFST+0,sp),a
6135                     ; 602         keyCode |= C_KS_DEALED;
6137  07b4 aae0          	or	a,#224
6138  07b6 b702          	ld	_keyCode,a
6139                     ; 603         switch( key ) 
6141  07b8 7b01          	ld	a,(OFST+0,sp)
6143                     ; 1230           default: break;
6144  07ba 2767          	jreq	L3553
6145  07bc 4a            	dec	a
6146  07bd 2603cc09ee    	jreq	L1653
6147  07c2 4a            	dec	a
6148  07c3 2603cc0aea    	jreq	L5653
6149  07c8 a002          	sub	a,#2
6150  07ca 2603cc0c83    	jreq	L3753
6151  07cf 4a            	dec	a
6152  07d0 2603cc0d54    	jreq	L5753
6153  07d5 4a            	dec	a
6154  07d6 2603cc0dde    	jreq	L7753
6155  07db 4a            	dec	a
6156  07dc 2603cc097b    	jreq	L5553
6157  07e1 4a            	dec	a
6158  07e2 2603cc0e75    	jreq	L1063
6159  07e7 a039          	sub	a,#57
6160  07e9 2603cc09e6    	jreq	L7553
6161  07ee 4a            	dec	a
6162  07ef 2603cc0ae2    	jreq	L3653
6163  07f4 4a            	dec	a
6164  07f5 2603cc0bb7    	jreq	L7653
6165  07fa a006          	sub	a,#6
6166  07fc 2603cc0ef4    	jreq	L3063
6167  0801 4a            	dec	a
6168  0802 2603cc0f27    	jreq	L5063
6169  0807 4a            	dec	a
6170  0808 2603cc0f4e    	jreq	L7063
6171  080d 4a            	dec	a
6172  080e 2603cc0f75    	jreq	L1163
6173  0813 4a            	dec	a
6174  0814 2603cc0f86    	jreq	L3163
6175  0819 a016          	sub	a,#22
6176  081b 2603cc0c17    	jreq	L1753
6177  0820 cc0f9d        	jra	L3363
6178  0823               L3553:
6179                     ; 605           case C_KC_NONE:
6179                     ; 606                if( SET_WARM_LEVEL_FLAG )
6181  0823 7205001908    	btjf	_Disp_Flag,#2,L1463
6182                     ; 608                  Delay_Keep_Warm_Time = 5;
6184  0828 3505002f      	mov	_Delay_Keep_Warm_Time,#5
6185                     ; 609                  SET_WARM_LEVEL_FLAG =0;
6187  082c 72150019      	bres	_Disp_Flag,#2
6188  0830               L1463:
6189                     ; 611                if( keyLast == C_KC_KEEPWARM )
6191  0830 b601          	ld	a,_keyLast
6192  0832 a103          	cp	a,#3
6193  0834 2703cc08d2    	jrne	L3463
6194                     ; 614                  Standby_Timer = 300;
6196  0839 ae012c        	ldw	x,#300
6197  083c cf0030        	ldw	_Standby_Timer,x
6198                     ; 615                  if( System_Status == DIM_STATUS)
6200  083f c6003b        	ld	a,_System_Status
6201  0842 4a            	dec	a
6202  0843 2607          	jrne	L5463
6203                     ; 617                   System_Status = ACTIVE;
6205  0845 3502003b      	mov	_System_Status,#2
6206                     ; 618                   break;
6208  0849 cc0f9d        	jra	L3363
6209  084c               L5463:
6210                     ; 620                  if( DLY_BREW_FUNC )
6212  084c 720b001907    	btjf	_Disp_Flag,#5,L7463
6213                     ; 622                   DLY_BREW_FUNC = 0;
6215  0851 721b0019      	bres	_Disp_Flag,#5
6216                     ; 623                   break;
6218  0855 cc0f9d        	jra	L3363
6219  0858               L7463:
6220                     ; 630                  if( CLEAN_DISP_FLAG || CLEAN_FUNC_FLAG)
6222  0858 72000018f8    	btjt	_Disp_Flag1,#0,L3363
6224  085d 72020018f3    	btjt	_Disp_Flag1,#1,L3363
6225                     ; 632                  if((Halfcarafe == Knob0_count [ Brew_Select ]  || fullcarafe == Knob0_count [ Brew_Select ]  )
6225                     ; 633                    && (Classic == Knob1_count [ Brew_Select ]  || Rich == Knob1_count [ Brew_Select ] ))
6227  0862 c60039        	ld	a,_Brew_Select
6228  0865 5f            	clrw	x
6229  0866 97            	ld	xl,a
6230  0867 d60054        	ld	a,(_Knob0_count,x)
6231  086a a104          	cp	a,#4
6232  086c 270c          	jreq	L7563
6234  086e c60039        	ld	a,_Brew_Select
6235  0871 5f            	clrw	x
6236  0872 97            	ld	xl,a
6237  0873 d60054        	ld	a,(_Knob0_count,x)
6238  0876 a105          	cp	a,#5
6239  0878 26db          	jrne	L3363
6240  087a               L7563:
6242  087a c60039        	ld	a,_Brew_Select
6243  087d 5f            	clrw	x
6244  087e 97            	ld	xl,a
6245  087f 724d0051      	tnz	(_Knob1_count,x)
6246  0883 2708          	jreq	L1663
6248  0885 5f            	clrw	x
6249  0886 97            	ld	xl,a
6250  0887 d60051        	ld	a,(_Knob1_count,x)
6251  088a 4a            	dec	a
6252  088b 26c8          	jrne	L3363
6253  088d               L1663:
6254                     ; 635                      if( Delay_Keep_Warm_Time )
6256  088d c6002f        	ld	a,_Delay_Keep_Warm_Time
6257  0890 270d          	jreq	L3663
6258                     ; 637                       Delay_Keep_Warm_Time = 0;
6260  0892 725f002f      	clr	_Delay_Keep_Warm_Time
6261                     ; 638                       BeepStart( Confirm_Sound );
6263  0896 ae0000        	ldw	x,#_Confirm_Sound
6264  0899 cd0000        	call	_BeepStart
6266                     ; 639                       break;
6268  089c cc0f9d        	jra	L3363
6269  089f               L3663:
6270                     ; 641                      if( Delay_Keep_Warm_Level )
6272  089f 725d002e      	tnz	_Delay_Keep_Warm_Level
6273  08a3 270c          	jreq	L5663
6274                     ; 643                       Delay_Keep_Warm_Level = 0;
6276  08a5 c7002e        	ld	_Delay_Keep_Warm_Level,a
6277                     ; 644                       BeepStart( Confirm_Sound );
6279  08a8 ae0000        	ldw	x,#_Confirm_Sound
6280  08ab cd0000        	call	_BeepStart
6282                     ; 645                       break;
6284  08ae cc0f9d        	jra	L3363
6285  08b1               L5663:
6286                     ; 647                      KEEP_WARM_FUNC ^= 1;
6288  08b1 90120019      	bcpl	_Disp_Flag,#1
6289                     ; 648                      if( F_KEEP_WARM_START )
6291  08b5 7209000004    	btjf	_Ctrl_Board_Status,#4,L7663
6292                     ; 649                            F_KEEP_WARM_START = 0;// 自动保温后，再按时停止保温
6294  08ba 72190000      	bres	_Ctrl_Board_Status,#4
6295  08be               L7663:
6296                     ; 650                      if( KEEP_WARM_FUNC == 0 )
6298  08be 72020019eb    	btjt	_Disp_Flag,#1,L3363
6299                     ; 652                       Clock_Keep_Warm.second = 0;
6301  08c3 725f003c      	clr	_Clock_Keep_Warm
6302                     ; 653                       Clock_Keep_Warm.min_b  = 0;
6304  08c7 725f003f      	clr	_Clock_Keep_Warm+3
6305                     ; 654                       Clock_Keep_Warm.hour_b = 0;
6307  08cb 725f0040      	clr	_Clock_Keep_Warm+4
6308  08cf cc0f9d        	jra	L3363
6309  08d2               L3463:
6310                     ; 658                else if( keyLast == C_KC_START)
6312  08d2 a109          	cp	a,#9
6313  08d4 26f9          	jrne	L3363
6314                     ; 660                    Standby_Timer = 300;
6316  08d6 ae012c        	ldw	x,#300
6317  08d9 cf0030        	ldw	_Standby_Timer,x
6318                     ; 661                    Delay_Keep_Warm_Time = 0;
6320  08dc 725f002f      	clr	_Delay_Keep_Warm_Time
6321                     ; 662                    Delay_Keep_Warm_Level = 0;
6323  08e0 725f002e      	clr	_Delay_Keep_Warm_Level
6324                     ; 663                    if( System_Status == DIM_STATUS)
6326  08e4 c6003b        	ld	a,_System_Status
6327  08e7 a101          	cp	a,#1
6328  08e9 2607          	jrne	L7763
6329                     ; 665                     System_Status = ACTIVE;
6331  08eb 3502003b      	mov	_System_Status,#2
6332                     ; 666                     break;
6334  08ef cc0f9d        	jra	L3363
6335  08f2               L7763:
6336                     ; 668                    if( Brew_Select == NO_BASKET )
6338  08f2 725d0039      	tnz	_Brew_Select
6339  08f6 2602          	jrne	L1073
6340                     ; 669                     return;
6343  08f8 84            	pop	a
6344  08f9 81            	ret	
6345  08fa               L1073:
6346                     ; 672                    if( System_Status == ACTIVE )
6348  08fa a102          	cp	a,#2
6349  08fc 26f1          	jrne	L3363
6350                     ; 674                      if( OFF == Start_Status )
6352  08fe c6003a        	ld	a,_Start_Status
6353  0901 2629          	jrne	L5073
6354                     ; 676                        Start_Status    = ON;
6356  0903 3501003a      	mov	_Start_Status,#1
6357                     ; 677                        Style_Type_Flag = 0;
6359  0907 c70036        	ld	_Style_Type_Flag,a
6360                     ; 678                        Delay_Brew_Time       = 0;
6362  090a c7002c        	ld	_Delay_Brew_Time,a
6363                     ; 679                        brewAdjust15S    = BREW_ADJUST_TIME;
6365  090d 350f0028      	mov	_brewAdjust15S,#15
6366                     ; 680                        DLY_BREW_FLAG         = 0;
6368  0911 72190019      	bres	_Disp_Flag,#4
6369                     ; 681                        DLY_BREW_FUNC   = 0;
6371  0915 721b0019      	bres	_Disp_Flag,#5
6372                     ; 683                        if( CLEAN_DISP_FLAG )
6374  0919 72010018d1    	btjf	_Disp_Flag1,#0,L3363
6375                     ; 685                         CLEAN_DISP_FLAG = 0;
6377  091e 72110018      	bres	_Disp_Flag1,#0
6378                     ; 686                         cleanStart      = 0;
6380  0922 c7001f        	ld	_cleanStart,a
6381                     ; 687                         CLEAN_FUNC_FLAG = 1;
6383  0925 72120018      	bset	_Disp_Flag1,#1
6384  0929 cc0f9d        	jra	L3363
6385  092c               L5073:
6386                     ; 692                           if( F_Brew_complete ||  F_Brew_abnormal_end || errorCode0 )
6388  092c 720200000a    	btjt	_Ctrl_Board_Status,#1,L5173
6390  0931 7204000005    	btjt	_Ctrl_Board_Status,#2,L5173
6392  0936 c60000        	ld	a,_errorCode0
6393  0939 2713          	jreq	L3173
6394  093b               L5173:
6395                     ; 695                             F_Brew_complete     = 0;
6397  093b 72130000      	bres	_Ctrl_Board_Status,#1
6398                     ; 696                             F_Brew_abnormal_end = 0;
6400  093f 72150000      	bres	_Ctrl_Board_Status,#2
6401                     ; 697                             Complete_Sound_Delay = 0;
6403  0943 725f002a      	clr	_Complete_Sound_Delay
6404                     ; 698                             Start_Status        = OFF;
6406  0947 725f003a      	clr	_Start_Status
6408  094b cc0f9d        	jra	L3363
6409  094e               L3173:
6410                     ; 700                           else if( Paused_Delay )
6412  094e ce0034        	ldw	x,_Paused_Delay
6413  0951 2711          	jreq	L3273
6414                     ; 702                            Paused_Delay = 0;
6416  0953 5f            	clrw	x
6417  0954 cf0034        	ldw	_Paused_Delay,x
6418                     ; 703                            Start_Status = UNPAUSE;
6420  0957 3504003a      	mov	_Start_Status,#4
6421                     ; 704                            BeepStart( Confirm_Sound );
6423  095b ae0000        	ldw	x,#_Confirm_Sound
6424  095e cd0000        	call	_BeepStart
6427  0961 cc0f9d        	jra	L3363
6428  0964               L3273:
6429                     ; 708                            Paused_Delay = PAUSE_TIME;
6431  0964 ae012c        	ldw	x,#300
6432  0967 cf0034        	ldw	_Paused_Delay,x
6433                     ; 709                            Pause_Sound_Delay = 10;
6435  096a 350a0029      	mov	_Pause_Sound_Delay,#10
6436                     ; 710                            Start_Status = PAUSE;
6438  096e 3502003a      	mov	_Start_Status,#2
6439                     ; 711                            BeepStart( Confirm_Sound );
6441  0972 ae0000        	ldw	x,#_Confirm_Sound
6442  0975 cd0000        	call	_BeepStart
6444  0978 cc0f9d        	jra	L3363
6445  097b               L5553:
6446                     ; 717           case C_KC_POWER:
6446                     ; 718                Standby_Timer = 300;
6448  097b ae012c        	ldw	x,#300
6449  097e cf0030        	ldw	_Standby_Timer,x
6450                     ; 719                Delay_Keep_Warm_Time = 0;
6452  0981 c7002f        	ld	_Delay_Keep_Warm_Time,a
6453                     ; 720                Delay_Keep_Warm_Level = 0;
6455  0984 c7002e        	ld	_Delay_Keep_Warm_Level,a
6456                     ; 721                Delay_Brew_Time       = 0;
6458  0987 c7002c        	ld	_Delay_Brew_Time,a
6459                     ; 722                DLY_BREW_FLAG         = 0;
6461  098a 72190019      	bres	_Disp_Flag,#4
6462                     ; 723                if( System_Status == DIM_STATUS)
6464  098e c6003b        	ld	a,_System_Status
6465  0991 4a            	dec	a
6466  0992 2607          	jrne	L7273
6467                     ; 725                 System_Status = ACTIVE;
6469  0994 3502003b      	mov	_System_Status,#2
6470                     ; 726                 break;
6472  0998 cc0f9d        	jra	L3363
6473  099b               L7273:
6474                     ; 728                if( System_Status == STANDBY)
6476  099b c6003b        	ld	a,_System_Status
6477  099e 2607          	jrne	L1373
6478                     ; 730                   System_Status = ACTIVE;
6480  09a0 3502003b      	mov	_System_Status,#2
6482  09a4 cc0f9d        	jra	L3363
6483  09a7               L1373:
6484                     ; 734                     DLY_BREW_FUNC = 0;
6486  09a7 721b0019      	bres	_Disp_Flag,#5
6487                     ; 735                     KEEP_WARM_FUNC = 0;
6489  09ab 72130019      	bres	_Disp_Flag,#1
6490                     ; 736                     WARM_LEVEL_FUNC = 0;
6492  09af 72170019      	bres	_Disp_Flag,#3
6493                     ; 737                     System_Status = STANDBY;
6495  09b3 725f003b      	clr	_System_Status
6496                     ; 738                     Paused_Delay = 0;
6498  09b7 5f            	clrw	x
6499  09b8 cf0034        	ldw	_Paused_Delay,x
6500                     ; 739                     Start_Status = OFF;
6502  09bb 725f003a      	clr	_Start_Status
6503                     ; 740                     Style_Type_Flag = 0;
6505  09bf 725f0036      	clr	_Style_Type_Flag
6506                     ; 741                     CLEAN_FUNC_FLAG = 0;
6508  09c3 72130018      	bres	_Disp_Flag1,#1
6509                     ; 742                     CLEAN_DISP_FLAG = 0;
6511  09c7 72110018      	bres	_Disp_Flag1,#0
6512                     ; 743                     Clock_Clean.hour            = 0;
6514  09cb 725f0043      	clr	_Clock_Clean+2
6515                     ; 744                     Clock_Clean.min             = 59;
6517  09cf 353b0042      	mov	_Clock_Clean+1,#59
6518                     ; 745                     Clock_Clean.second          = 59;
6520  09d3 353b0041      	mov	_Clock_Clean,#59
6521                     ; 746                     F_Brew_complete     = 0;
6523  09d7 72130000      	bres	_Ctrl_Board_Status,#1
6524                     ; 747                     F_Brew_abnormal_end = 0;
6526  09db 72150000      	bres	_Ctrl_Board_Status,#2
6527                     ; 748                     Complete_Sound_Delay = 0;
6529  09df 725f002a      	clr	_Complete_Sound_Delay
6530  09e3 cc0f9d        	jra	L3363
6531  09e6               L7553:
6532                     ; 752           case C_KC_HOUR|C_KS_2SEC: // 连续加操作
6532                     ; 753                if(0 == Delay_Keep_Warm_Time ) // 保温设置时间不进行连加操作
6534  09e6 c6002f        	ld	a,_Delay_Keep_Warm_Time
6535  09e9 2603          	jrne	L1653
6536                     ; 754                  KeyRepeat( );   
6538  09eb cd0797        	call	_KeyRepeat
6540  09ee               L1653:
6541                     ; 755           case C_KC_HOUR:
6541                     ; 756                Standby_Timer = 300;
6543  09ee ae012c        	ldw	x,#300
6544  09f1 cf0030        	ldw	_Standby_Timer,x
6545                     ; 757                if( debugEnable )
6547  09f4 c6001d        	ld	a,_debugEnable
6548  09f7 2711          	jreq	L7373
6549                     ; 759                   if( debugIndex < DEBUG_MAX )
6551  09f9 c6001c        	ld	a,_debugIndex
6552  09fc a106          	cp	a,#6
6553  09fe 2503cc0f99    	jruge	LC005
6554                     ; 760                     debugIndex++;
6556  0a03 725c001c      	inc	_debugIndex
6557                     ; 761                   debugValueEnable = 0;
6558                     ; 762                   break;
6560  0a07 cc0f99        	jp	LC005
6561  0a0a               L7373:
6562                     ; 764                if( System_Status == DIM_STATUS)
6564  0a0a c6003b        	ld	a,_System_Status
6565  0a0d 4a            	dec	a
6566  0a0e 2607          	jrne	L3473
6567                     ; 766                 System_Status = ACTIVE;
6569  0a10 3502003b      	mov	_System_Status,#2
6570                     ; 767                 break;
6572  0a14 cc0f9d        	jra	L3363
6573  0a17               L3473:
6574                     ; 769                if( Delay_Brew_Time )
6576  0a17 c6002c        	ld	a,_Delay_Brew_Time
6577  0a1a 2731          	jreq	L5473
6578                     ; 771                   if( DLY_BREW_FLAG )
6580  0a1c 7209001904    	btjf	_Disp_Flag,#4,L7473
6581                     ; 772                   Delay_Brew_Time = 30;
6583  0a21 351e002c      	mov	_Delay_Brew_Time,#30
6584  0a25               L7473:
6585                     ; 773                   Clock_Stay_Time = 50;
6587  0a25 35320032      	mov	_Clock_Stay_Time,#50
6588                     ; 774                   if( Clock_DLY_BREW.hour_b == 11 )
6590  0a29 c6004a        	ld	a,_Clock_DLY_BREW+4
6591  0a2c a10b          	cp	a,#11
6592  0a2e 260b          	jrne	L1573
6593                     ; 776                      Clock_DLY_BREW.hour_b = 12;
6595  0a30 350c004a      	mov	_Clock_DLY_BREW+4,#12
6596                     ; 777                      DELAY_AM_PM_FLAG ^= 1;
6598  0a34 901c0019      	bcpl	_Disp_Flag,#6
6600  0a38 cc0f9d        	jra	L3363
6601  0a3b               L1573:
6602                     ; 779                    else if( Clock_DLY_BREW.hour_b == 12)
6604  0a3b a10c          	cp	a,#12
6605  0a3d 2607          	jrne	L5573
6606                     ; 780                      Clock_DLY_BREW.hour_b = 1;
6608  0a3f 3501004a      	mov	_Clock_DLY_BREW+4,#1
6610  0a43 cc0f9d        	jra	L3363
6611  0a46               L5573:
6612                     ; 782                       Clock_DLY_BREW.hour_b++;
6614  0a46 725c004a      	inc	_Clock_DLY_BREW+4
6615  0a4a cc0f9d        	jra	L3363
6616  0a4d               L5473:
6617                     ; 784                else if( Delay_Keep_Warm_Time )
6619  0a4d c6002f        	ld	a,_Delay_Keep_Warm_Time
6620  0a50 273c          	jreq	L3673
6621                     ; 786                  Clock_Stay_Time = 50;
6623  0a52 35320032      	mov	_Clock_Stay_Time,#50
6624                     ; 787                  Delay_Keep_Warm_Time = 5;
6626  0a56 3505002f      	mov	_Delay_Keep_Warm_Time,#5
6627                     ; 788                  if( Clock_Keep_Warm.min == 0 )
6629  0a5a c6003d        	ld	a,_Clock_Keep_Warm+1
6630  0a5d 2611          	jrne	L5673
6631                     ; 790                    if( ++Clock_Keep_Warm.hour > 4)
6633  0a5f 725c003e      	inc	_Clock_Keep_Warm+2
6634  0a63 c6003e        	ld	a,_Clock_Keep_Warm+2
6635  0a66 a105          	cp	a,#5
6636  0a68 2515          	jrult	L1773
6637                     ; 791                     Clock_Keep_Warm.hour = 1;
6639  0a6a 3501003e      	mov	_Clock_Keep_Warm+2,#1
6640  0a6e 200f          	jra	L1773
6641  0a70               L5673:
6642                     ; 795                    if( ++Clock_Keep_Warm.hour > 3)
6644  0a70 725c003e      	inc	_Clock_Keep_Warm+2
6645  0a74 c6003e        	ld	a,_Clock_Keep_Warm+2
6646  0a77 a104          	cp	a,#4
6647  0a79 2504          	jrult	L1773
6648                     ; 796                     Clock_Keep_Warm.hour = 0;
6650  0a7b 725f003e      	clr	_Clock_Keep_Warm+2
6651  0a7f               L1773:
6652                     ; 798                  Clock_Keep_Warm.second = 0;
6654  0a7f 725f003c      	clr	_Clock_Keep_Warm
6655                     ; 799                  Clock_Keep_Warm.min_b  = 0;
6657  0a83 725f003f      	clr	_Clock_Keep_Warm+3
6658                     ; 800                  Clock_Keep_Warm.hour_b = 0;
6660  0a87 725f0040      	clr	_Clock_Keep_Warm+4
6662  0a8b cc0f9d        	jra	L3363
6663  0a8e               L3673:
6664                     ; 802                else if( Delay_Keep_Warm_Level )
6666  0a8e c6002e        	ld	a,_Delay_Keep_Warm_Level
6667  0a91 2712          	jreq	L7773
6668                     ; 804                  Delay_Keep_Warm_Level = 5;
6670  0a93 3505002e      	mov	_Delay_Keep_Warm_Level,#5
6671                     ; 805                  if( Keep_Warm_Class < WARM_HI )
6673  0a97 c60037        	ld	a,_Keep_Warm_Class
6674  0a9a a102          	cp	a,#2
6675  0a9c 24ed          	jruge	L3363
6676                     ; 806                  Keep_Warm_Class++;
6678  0a9e 725c0037      	inc	_Keep_Warm_Class
6679  0aa2 cc0f9d        	jra	L3363
6680  0aa5               L7773:
6681                     ; 810                  if( Clock_Set_Time == 0)
6683  0aa5 c60033        	ld	a,_Clock_Set_Time
6684  0aa8 260c          	jrne	L5004
6685                     ; 812                   Clock_Set_Time = 3;
6687  0aaa 35030033      	mov	_Clock_Set_Time,#3
6688                     ; 813                   Clock.hour_b = Clock.hour;
6690  0aae 55004d004f    	mov	_Clock+4,_Clock+2
6691                     ; 814                   break;
6693  0ab3 cc0f9d        	jra	L3363
6694  0ab6               L5004:
6695                     ; 816                  Clock_Set_Time = 3;
6697  0ab6 35030033      	mov	_Clock_Set_Time,#3
6698                     ; 817                  Clock_Stay_Time = 50;
6700  0aba 35320032      	mov	_Clock_Stay_Time,#50
6701                     ; 818                  if( Clock.hour_b == 11 )
6703  0abe c6004f        	ld	a,_Clock+4
6704  0ac1 a10b          	cp	a,#11
6705  0ac3 260b          	jrne	L7004
6706                     ; 820                    Clock.hour_b = 12;
6708  0ac5 350c004f      	mov	_Clock+4,#12
6709                     ; 821                    AM_PM_FLAG ^= 1;
6711  0ac9 90100019      	bcpl	_Disp_Flag,#0
6713  0acd cc0f9d        	jra	L3363
6714  0ad0               L7004:
6715                     ; 823                  else if( Clock.hour_b == 12)
6717  0ad0 a10c          	cp	a,#12
6718  0ad2 2607          	jrne	L3104
6719                     ; 824                    Clock.hour_b = 1;
6721  0ad4 3501004f      	mov	_Clock+4,#1
6723  0ad8 cc0f9d        	jra	L3363
6724  0adb               L3104:
6725                     ; 826                     Clock.hour_b++;
6727  0adb 725c004f      	inc	_Clock+4
6728  0adf cc0f9d        	jra	L3363
6729  0ae2               L3653:
6730                     ; 829           case C_KC_MINUTE|C_KS_2SEC:
6730                     ; 830                 if(0 == Delay_Keep_Warm_Time ) // 保温设置时间不进行连加操作
6732  0ae2 c6002f        	ld	a,_Delay_Keep_Warm_Time
6733  0ae5 2603          	jrne	L5653
6734                     ; 831                   KeyRepeat( );  
6736  0ae7 cd0797        	call	_KeyRepeat
6738  0aea               L5653:
6739                     ; 832           case C_KC_MINUTE:
6739                     ; 833               Standby_Timer = 300;
6741  0aea ae012c        	ldw	x,#300
6742  0aed cf0030        	ldw	_Standby_Timer,x
6743                     ; 834               if( debugEnable )
6745  0af0 c6001d        	ld	a,_debugEnable
6746  0af3 270f          	jreq	L1204
6747                     ; 836                 if( debugIndex > 0 )
6749  0af5 c6001c        	ld	a,_debugIndex
6750  0af8 2603cc0f99    	jreq	LC005
6751                     ; 837                   debugIndex--;
6753  0afd 725a001c      	dec	_debugIndex
6754                     ; 838                 debugValueEnable = 0;
6755                     ; 839                 break;
6757  0b01 cc0f99        	jp	LC005
6758  0b04               L1204:
6759                     ; 841               if( System_Status == DIM_STATUS)
6761  0b04 c6003b        	ld	a,_System_Status
6762  0b07 4a            	dec	a
6763  0b08 2607          	jrne	L5204
6764                     ; 843                 System_Status = ACTIVE;
6766  0b0a 3502003b      	mov	_System_Status,#2
6767                     ; 844                 break;
6769  0b0e cc0f9d        	jra	L3363
6770  0b11               L5204:
6771                     ; 846               if( Delay_Brew_Time )
6773  0b11 c6002c        	ld	a,_Delay_Brew_Time
6774  0b14 271f          	jreq	L7204
6775                     ; 848                   if( DLY_BREW_FLAG )
6777  0b16 7209001904    	btjf	_Disp_Flag,#4,L1304
6778                     ; 849                   Delay_Brew_Time = 30;
6780  0b1b 351e002c      	mov	_Delay_Brew_Time,#30
6781  0b1f               L1304:
6782                     ; 850                   Clock_Stay_Time = 50;//连续按时不闪烁
6784  0b1f 35320032      	mov	_Clock_Stay_Time,#50
6785                     ; 851                   if( ++Clock_DLY_BREW.min_b > 59 )
6787  0b23 725c0049      	inc	_Clock_DLY_BREW+3
6788  0b27 c60049        	ld	a,_Clock_DLY_BREW+3
6789  0b2a a13c          	cp	a,#60
6790  0b2c 25e0          	jrult	L3363
6791                     ; 852                      Clock_DLY_BREW.min_b = 0;
6793  0b2e 725f0049      	clr	_Clock_DLY_BREW+3
6794  0b32 cc0f9d        	jra	L3363
6795  0b35               L7204:
6796                     ; 854                else if( Delay_Keep_Warm_Time )
6798  0b35 c6002f        	ld	a,_Delay_Keep_Warm_Time
6799  0b38 273d          	jreq	L7304
6800                     ; 856                  Delay_Keep_Warm_Time = 5;
6802  0b3a 3505002f      	mov	_Delay_Keep_Warm_Time,#5
6803                     ; 857                  Clock_Stay_Time = 50;
6805  0b3e 35320032      	mov	_Clock_Stay_Time,#50
6806                     ; 858                  if( Clock_Keep_Warm.hour == 4 )
6808  0b42 c6003e        	ld	a,_Clock_Keep_Warm+2
6809  0b45 a104          	cp	a,#4
6810  0b47 2604          	jrne	L1404
6811                     ; 860                    Clock_Keep_Warm.hour = 0;
6813  0b49 725f003e      	clr	_Clock_Keep_Warm+2
6814  0b4d               L1404:
6815                     ; 862                  Clock_Keep_Warm.min += 15;
6817  0b4d c6003d        	ld	a,_Clock_Keep_Warm+1
6818  0b50 ab0f          	add	a,#15
6819  0b52 c7003d        	ld	_Clock_Keep_Warm+1,a
6820                     ; 863                  if( Clock_Keep_Warm.min > 45 )
6822  0b55 a12e          	cp	a,#46
6823  0b57 250f          	jrult	L3404
6824                     ; 865                   if( Clock_Keep_Warm.hour )
6826  0b59 c6003e        	ld	a,_Clock_Keep_Warm+2
6827  0b5c 2706          	jreq	L5404
6828                     ; 866                     Clock_Keep_Warm.min = 0;
6830  0b5e 725f003d      	clr	_Clock_Keep_Warm+1
6832  0b62 2004          	jra	L3404
6833  0b64               L5404:
6834                     ; 868                     Clock_Keep_Warm.min = 15;
6836  0b64 350f003d      	mov	_Clock_Keep_Warm+1,#15
6837  0b68               L3404:
6838                     ; 871                  Clock_Keep_Warm.second = 0;
6840  0b68 725f003c      	clr	_Clock_Keep_Warm
6841                     ; 872                  Clock_Keep_Warm.min_b  = 0;
6843  0b6c 725f003f      	clr	_Clock_Keep_Warm+3
6844                     ; 873                  Clock_Keep_Warm.hour_b = 0;
6846  0b70 725f0040      	clr	_Clock_Keep_Warm+4
6848  0b74 cc0f9d        	jra	L3363
6849  0b77               L7304:
6850                     ; 875                else if( Delay_Keep_Warm_Level )
6852  0b77 c6002e        	ld	a,_Delay_Keep_Warm_Level
6853  0b7a 2710          	jreq	L3504
6854                     ; 877                  Delay_Keep_Warm_Level = 5;
6856  0b7c 3505002e      	mov	_Delay_Keep_Warm_Level,#5
6857                     ; 878                  if( Keep_Warm_Class > WARM_OFF )
6859  0b80 c60037        	ld	a,_Keep_Warm_Class
6860  0b83 27ef          	jreq	L3363
6861                     ; 879                  Keep_Warm_Class--;
6863  0b85 725a0037      	dec	_Keep_Warm_Class
6864  0b89 cc0f9d        	jra	L3363
6865  0b8c               L3504:
6866                     ; 883                  if( Clock_Set_Time == 0)
6868  0b8c c60033        	ld	a,_Clock_Set_Time
6869  0b8f 260c          	jrne	L1604
6870                     ; 885                   Clock_Set_Time = 3;
6872  0b91 35030033      	mov	_Clock_Set_Time,#3
6873                     ; 886                   Clock.min_b = Clock.min;
6875  0b95 55004c004e    	mov	_Clock+3,_Clock+1
6876                     ; 887                   break;
6878  0b9a cc0f9d        	jra	L3363
6879  0b9d               L1604:
6880                     ; 889                  Clock_Set_Time  = 3;
6882  0b9d 35030033      	mov	_Clock_Set_Time,#3
6883                     ; 890                  Clock_Stay_Time = 50;//连续按时不闪烁
6885  0ba1 35320032      	mov	_Clock_Stay_Time,#50
6886                     ; 891                  if( ++Clock.min_b > 59 )
6888  0ba5 725c004e      	inc	_Clock+3
6889  0ba9 c6004e        	ld	a,_Clock+3
6890  0bac a13c          	cp	a,#60
6891  0bae 25ea          	jrult	L3363
6892                     ; 892                   Clock.min_b = 0;
6894  0bb0 725f004e      	clr	_Clock+3
6895  0bb4 cc0f9d        	jra	L3363
6896  0bb7               L7653:
6897                     ; 897           case C_KC_KEEPWARM | C_KS_2SEC:
6897                     ; 898                Standby_Timer = 300;
6899  0bb7 ae012c        	ldw	x,#300
6900  0bba cf0030        	ldw	_Standby_Timer,x
6901                     ; 899                if( System_Status == DIM_STATUS)
6903  0bbd c6003b        	ld	a,_System_Status
6904  0bc0 4a            	dec	a
6905  0bc1 2607          	jrne	L5604
6906                     ; 901                 System_Status = ACTIVE;
6908  0bc3 3502003b      	mov	_System_Status,#2
6909                     ; 902                 break;
6911  0bc7 cc0f9d        	jra	L3363
6912  0bca               L5604:
6913                     ; 904                if( NO_BASKET == Brew_Select )
6915  0bca c60039        	ld	a,_Brew_Select
6916  0bcd 27f8          	jreq	L3363
6917                     ; 905                 break;
6919                     ; 908                if( Start_Status != OFF )
6921  0bcf 725d003a      	tnz	_Start_Status
6922  0bd3 26f2          	jrne	L3363
6923                     ; 909                 break;
6925                     ; 910                if( CLEAN_DISP_FLAG || CLEAN_FUNC_FLAG)
6927  0bd5 72000018ed    	btjt	_Disp_Flag1,#0,L3363
6929  0bda 72020018e8    	btjt	_Disp_Flag1,#1,L3363
6930                     ; 912                if( DLY_BREW_FUNC )
6932  0bdf 720a0019e3    	btjt	_Disp_Flag,#5,L3363
6933                     ; 913                   break;
6935                     ; 914                if((Halfcarafe == Knob0_count [ Brew_Select ]  || fullcarafe == Knob0_count [ Brew_Select ]  )
6935                     ; 915                    && (Classic == Knob1_count [ Brew_Select ] || Rich == Knob1_count [ Brew_Select ] ))
6937  0be4 5f            	clrw	x
6938  0be5 97            	ld	xl,a
6939  0be6 d60054        	ld	a,(_Knob0_count,x)
6940  0be9 a104          	cp	a,#4
6941  0beb 270c          	jreq	L3014
6943  0bed c60039        	ld	a,_Brew_Select
6944  0bf0 5f            	clrw	x
6945  0bf1 97            	ld	xl,a
6946  0bf2 d60054        	ld	a,(_Knob0_count,x)
6947  0bf5 a105          	cp	a,#5
6948  0bf7 26ce          	jrne	L3363
6949  0bf9               L3014:
6951  0bf9 c60039        	ld	a,_Brew_Select
6952  0bfc 5f            	clrw	x
6953  0bfd 97            	ld	xl,a
6954  0bfe 724d0051      	tnz	(_Knob1_count,x)
6955  0c02 2708          	jreq	L5014
6957  0c04 5f            	clrw	x
6958  0c05 97            	ld	xl,a
6959  0c06 d60051        	ld	a,(_Knob1_count,x)
6960  0c09 4a            	dec	a
6961  0c0a 26bb          	jrne	L3363
6962  0c0c               L5014:
6963                     ; 918                   Clock_Set_Time = 0;
6965  0c0c 725f0033      	clr	_Clock_Set_Time
6966                     ; 919                   SET_WARM_LEVEL_FLAG = 1;
6968  0c10 72140019      	bset	_Disp_Flag,#2
6969  0c14 cc0f9d        	jra	L3363
6970  0c17               L1753:
6971                     ; 922           case C_KC_KEEPWARM | C_KS_5SEC:
6971                     ; 923                Standby_Timer = 300;
6973  0c17 ae012c        	ldw	x,#300
6974  0c1a cf0030        	ldw	_Standby_Timer,x
6975                     ; 924                if( System_Status == DIM_STATUS)
6977  0c1d c6003b        	ld	a,_System_Status
6978  0c20 4a            	dec	a
6979  0c21 2607          	jrne	L7014
6980                     ; 926                 System_Status = ACTIVE;
6982  0c23 3502003b      	mov	_System_Status,#2
6983                     ; 927                 break;
6985  0c27 cc0f9d        	jra	L3363
6986  0c2a               L7014:
6987                     ; 935                if( CLEAN_DISP_FLAG || CLEAN_FUNC_FLAG)
6989  0c2a 72000018f8    	btjt	_Disp_Flag1,#0,L3363
6991  0c2f 72020018f3    	btjt	_Disp_Flag1,#1,L3363
6992                     ; 937                if( DLY_BREW_FUNC )
6994  0c34 720a0019ee    	btjt	_Disp_Flag,#5,L3363
6995                     ; 938                   break;
6997                     ; 939                if((Halfcarafe == Knob0_count [ Brew_Select ]  || fullcarafe == Knob0_count [ Brew_Select ]  )
6997                     ; 940                    && (Classic == Knob1_count [ Brew_Select ]  || Rich == Knob1_count [ Brew_Select ] ))
6999  0c39 c60039        	ld	a,_Brew_Select
7000  0c3c 5f            	clrw	x
7001  0c3d 97            	ld	xl,a
7002  0c3e d60054        	ld	a,(_Knob0_count,x)
7003  0c41 a104          	cp	a,#4
7004  0c43 270c          	jreq	L1214
7006  0c45 c60039        	ld	a,_Brew_Select
7007  0c48 5f            	clrw	x
7008  0c49 97            	ld	xl,a
7009  0c4a d60054        	ld	a,(_Knob0_count,x)
7010  0c4d a105          	cp	a,#5
7011  0c4f 26d6          	jrne	L3363
7012  0c51               L1214:
7014  0c51 c60039        	ld	a,_Brew_Select
7015  0c54 5f            	clrw	x
7016  0c55 97            	ld	xl,a
7017  0c56 724d0051      	tnz	(_Knob1_count,x)
7018  0c5a 2708          	jreq	L3214
7020  0c5c 5f            	clrw	x
7021  0c5d 97            	ld	xl,a
7022  0c5e d60051        	ld	a,(_Knob1_count,x)
7023  0c61 4a            	dec	a
7024  0c62 26c3          	jrne	L3363
7025  0c64               L3214:
7026                     ; 942                 Delay_Keep_Warm_Level = 5;
7028  0c64 3505002e      	mov	_Delay_Keep_Warm_Level,#5
7029                     ; 943                 Clock_Set_Time = 0;
7031  0c68 725f0033      	clr	_Clock_Set_Time
7032                     ; 944                 SET_WARM_LEVEL_FLAG = 0;// 超过5秒就清除设置保温时间.
7034  0c6c 72150019      	bres	_Disp_Flag,#2
7035                     ; 945                 if( WARM_LEVEL_FUNC )
7037  0c70 7207001907    	btjf	_Disp_Flag,#3,L5214
7038                     ; 946                   WARM_LEVEL_FUNC = 0;
7040  0c75 72170019      	bres	_Disp_Flag,#3
7042  0c79 cc0f9d        	jra	L3363
7043  0c7c               L5214:
7044                     ; 948                   WARM_LEVEL_FUNC = 1;
7046  0c7c 72160019      	bset	_Disp_Flag,#3
7047  0c80 cc0f9d        	jra	L3363
7048  0c83               L3753:
7049                     ; 951           case C_KC_DELAYSET:
7049                     ; 952                Standby_Timer = 300;
7051  0c83 ae012c        	ldw	x,#300
7052  0c86 cf0030        	ldw	_Standby_Timer,x
7053                     ; 953                Clock_Set_Time = 0;
7055  0c89 c70033        	ld	_Clock_Set_Time,a
7056                     ; 954                Delay_Keep_Warm_Time = 0;
7058  0c8c c7002f        	ld	_Delay_Keep_Warm_Time,a
7059                     ; 955                Delay_Keep_Warm_Level = 0;
7061  0c8f c7002e        	ld	_Delay_Keep_Warm_Level,a
7062                     ; 956                if( debugEnable )
7064  0c92 c6001d        	ld	a,_debugEnable
7065  0c95 2707          	jreq	L1314
7066                     ; 958                   debugValueEnable = 1;
7068  0c97 3501001b      	mov	_debugValueEnable,#1
7069                     ; 959                   break;
7071  0c9b cc0f9d        	jra	L3363
7072  0c9e               L1314:
7073                     ; 961                if( System_Status == DIM_STATUS)
7075  0c9e c6003b        	ld	a,_System_Status
7076  0ca1 4a            	dec	a
7077  0ca2 2607          	jrne	L3314
7078                     ; 963                 System_Status = ACTIVE;
7080  0ca4 3502003b      	mov	_System_Status,#2
7081                     ; 964                 break;
7083  0ca8 cc0f9d        	jra	L3363
7084  0cab               L3314:
7085                     ; 966                if( NO_BASKET == Brew_Select )
7087  0cab c60039        	ld	a,_Brew_Select
7088  0cae 27f8          	jreq	L3363
7089                     ; 967                 break;
7091                     ; 968                if( System_Status == STANDBY )
7093  0cb0 c6003b        	ld	a,_System_Status
7094  0cb3 27f3          	jreq	L3363
7095                     ; 969                 break;
7097                     ; 970                if( Start_Status != OFF )
7099  0cb5 725d003a      	tnz	_Start_Status
7100  0cb9 26ed          	jrne	L3363
7101                     ; 971                 break;
7103                     ; 972                if( CLEAN_DISP_FLAG || CLEAN_FUNC_FLAG)
7105  0cbb 72000018e8    	btjt	_Disp_Flag1,#0,L3363
7107  0cc0 72020018e3    	btjt	_Disp_Flag1,#1,L3363
7108                     ; 974                if( ACTIVE == System_Status && OFF == Start_Status )
7110  0cc5 a102          	cp	a,#2
7111  0cc7 26df          	jrne	L3363
7113  0cc9 c6003a        	ld	a,_Start_Status
7114  0ccc 26da          	jrne	L3363
7115                     ; 976                   if( DELAY_SET_TYPE && DELAY_SET_SIZE && DELAY_SET_STYLE )
7117  0cce 720d001828    	btjf	_Disp_Flag1,#6,L1514
7119  0cd3 7209001823    	btjf	_Disp_Flag1,#4,L1514
7121  0cd8 720b00181e    	btjf	_Disp_Flag1,#5,L1514
7122                     ; 978                     if( Delay_Brew_Time )
7124  0cdd c6002c        	ld	a,_Delay_Brew_Time
7125  0ce0 270b          	jreq	L3514
7126                     ; 980                       if( DLY_BREW_FLAG )
7128  0ce2 72090019c1    	btjf	_Disp_Flag,#4,L3363
7129                     ; 982                         Delay_Brew_Sure( );
7131  0ce7 cd0123        	call	_Delay_Brew_Sure
7133  0cea cc0f9d        	jra	L3363
7134  0ced               L3514:
7135                     ; 986                     DELAY_SET_TYPE = 0;
7137  0ced 721d0018      	bres	_Disp_Flag1,#6
7138                     ; 987                     DELAY_SET_TYPE = 0;
7140  0cf1 721d0018      	bres	_Disp_Flag1,#6
7141                     ; 988                     DELAY_SET_TYPE = 0;
7143  0cf5 721d0018      	bres	_Disp_Flag1,#6
7145  0cf9 2024          	jra	L7514
7146  0cfb               L1514:
7147                     ; 992                     if( DLY_BREW_FLAG )
7149  0cfb 720900191f    	btjf	_Disp_Flag,#4,L7514
7150                     ; 994                       Delay_Brew_Time = 0;
7152  0d00 725f002c      	clr	_Delay_Brew_Time
7153                     ; 995                       DLY_BREW_FLAG   = 0;
7155  0d04 72190019      	bres	_Disp_Flag,#4
7156                     ; 996                       Delay_Keep_Warm_Time = 0;
7158  0d08 725f002f      	clr	_Delay_Keep_Warm_Time
7159                     ; 997                       SET_WARM_LEVEL_FLAG = 0;
7161  0d0c 72150019      	bres	_Disp_Flag,#2
7162                     ; 998                       DELAY_SET_SIZE  = 0;
7164  0d10 72190018      	bres	_Disp_Flag1,#4
7165                     ; 999                       DELAY_SET_STYLE = 0;
7167  0d14 721b0018      	bres	_Disp_Flag1,#5
7168                     ; 1000                       DELAY_SET_TYPE  = 0;
7170  0d18 721d0018      	bres	_Disp_Flag1,#6
7171                     ; 1001                       break;
7173  0d1c cc0f9d        	jra	L3363
7174  0d1f               L7514:
7175                     ; 1005                  if( DLY_BREW_FUNC )
7177  0d1f 720b00190d    	btjf	_Disp_Flag,#5,L3614
7178                     ; 1007                   DLY_BREW_FUNC = 0;
7180  0d24 721b0019      	bres	_Disp_Flag,#5
7181                     ; 1008                   BeepStart( Confirm_Sound );
7183  0d28 ae0000        	ldw	x,#_Confirm_Sound
7184  0d2b cd0000        	call	_BeepStart
7187  0d2e cc0f9d        	jra	L3363
7188  0d31               L3614:
7189                     ; 1012                    Delay_Brew_Time = 30;
7191  0d31 351e002c      	mov	_Delay_Brew_Time,#30
7192                     ; 1013                    DLY_BREW_FLAG   = 1;
7194  0d35 72180019      	bset	_Disp_Flag,#4
7195                     ; 1014                    DELAY_SET_SIZE  = 0;
7197  0d39 72190018      	bres	_Disp_Flag1,#4
7198                     ; 1015                    DELAY_SET_STYLE = 0;
7200  0d3d 721b0018      	bres	_Disp_Flag1,#5
7201                     ; 1016                    DELAY_SET_TYPE  = 0;
7203  0d41 721d0018      	bres	_Disp_Flag1,#6
7204                     ; 1017                    Delay_Keep_Warm_Time = 0;
7206  0d45 725f002f      	clr	_Delay_Keep_Warm_Time
7207                     ; 1018                    SET_WARM_LEVEL_FLAG = 0;
7209  0d49 72150019      	bres	_Disp_Flag,#2
7210                     ; 1019                    Style_Type_Flag = 0;
7212  0d4d 725f0036      	clr	_Style_Type_Flag
7213  0d51 cc0f9d        	jra	L3363
7214  0d54               L5753:
7215                     ; 1023           case C_KC_STYLE:
7215                     ; 1024                Standby_Timer = 300;
7217  0d54 ae012c        	ldw	x,#300
7218  0d57 cf0030        	ldw	_Standby_Timer,x
7219                     ; 1025                Delay_Keep_Warm_Time = 0;
7221  0d5a c7002f        	ld	_Delay_Keep_Warm_Time,a
7222                     ; 1026                Delay_Keep_Warm_Level = 0;
7224  0d5d c7002e        	ld	_Delay_Keep_Warm_Level,a
7225                     ; 1027                if( System_Status == DIM_STATUS)
7227  0d60 c6003b        	ld	a,_System_Status
7228  0d63 4a            	dec	a
7229  0d64 2607          	jrne	L7614
7230                     ; 1029                 System_Status = ACTIVE;
7232  0d66 3502003b      	mov	_System_Status,#2
7233                     ; 1030                 break;
7235  0d6a cc0f9d        	jra	L3363
7236  0d6d               L7614:
7237                     ; 1032                if( System_Status == STANDBY )
7239  0d6d c6003b        	ld	a,_System_Status
7240  0d70 27f8          	jreq	L3363
7241                     ; 1033                 break;
7243                     ; 1034                if( NO_BASKET == Brew_Select || COFFEE == Brew_Select)
7245  0d72 c60039        	ld	a,_Brew_Select
7246  0d75 27f3          	jreq	L3363
7248  0d77 4a            	dec	a
7249  0d78 27f0          	jreq	L3363
7250                     ; 1036                if( DLY_BREW_FUNC )
7252  0d7a 720b001912    	btjf	_Disp_Flag,#5,L7714
7253                     ; 1038                   if( delayAdjust15S == 0)
7255  0d7f c60023        	ld	a,_delayAdjust15S
7256  0d82 260d          	jrne	L7714
7257                     ; 1040                     BeepStart( Cancel_Sound );
7259  0d84 ae0000        	ldw	x,#_Cancel_Sound
7260  0d87 cd0000        	call	_BeepStart
7262                     ; 1041                     delayExitCnt  = 3;
7264  0d8a 35030021      	mov	_delayExitCnt,#3
7265                     ; 1042                     break;
7267  0d8e cc0f9d        	jra	L3363
7268  0d91               L7714:
7269                     ; 1045                if( Start_Status != OFF )
7271  0d91 c6003a        	ld	a,_Start_Status
7272  0d94 2716          	jreq	L3024
7273                     ; 1047                  if( brewAdjust15S == 0)
7275  0d96 c60028        	ld	a,_brewAdjust15S
7276  0d99 2611          	jrne	L3024
7277                     ; 1049                     BeepStart( Cancel_Sound );
7279  0d9b ae0000        	ldw	x,#_Cancel_Sound
7280  0d9e cd0000        	call	_BeepStart
7282                     ; 1051                     KEEP_WARM_FUNC = 0;
7284  0da1 72130019      	bres	_Disp_Flag,#1
7285                     ; 1052                     brewExitCnt    = 3;
7287  0da5 35030026      	mov	_brewExitCnt,#3
7288                     ; 1053                     break;
7290  0da9 cc0f9d        	jra	L3363
7291  0dac               L3024:
7292                     ; 1056                Clock_Set_Time = 0;
7294  0dac 725f0033      	clr	_Clock_Set_Time
7295                     ; 1058                if( Delay_Brew_Time )
7297  0db0 c6002c        	ld	a,_Delay_Brew_Time
7298  0db3 2704          	jreq	L7024
7299                     ; 1059                 Delay_Brew_Time = 30;
7301  0db5 351e002c      	mov	_Delay_Brew_Time,#30
7302  0db9               L7024:
7303                     ; 1060                if( brewAdjust15S )
7305  0db9 c60028        	ld	a,_brewAdjust15S
7306  0dbc 2704          	jreq	L1124
7307                     ; 1061                  brewNoaction3S = BREW_NOACTION_TIME;
7309  0dbe 35030027      	mov	_brewNoaction3S,#3
7310  0dc2               L1124:
7311                     ; 1062                if( delayAdjust15S )
7314  0dc2 c60023        	ld	a,_delayAdjust15S
7315  0dc5 2704          	jreq	L3124
7316                     ; 1063                  delayNoaction3S = DELAY_NOACTION_TIME;
7318  0dc7 35030022      	mov	_delayNoaction3S,#3
7319  0dcb               L3124:
7320                     ; 1064                if( STYLE_S == Style_Type_Flag )
7323  0dcb c60036        	ld	a,_Style_Type_Flag
7324  0dce 4a            	dec	a
7325  0dcf 2606          	jrne	L5124
7326                     ; 1065                 Style_Type_Flag = 0;
7328  0dd1 c70036        	ld	_Style_Type_Flag,a
7330  0dd4 cc0f9d        	jra	L3363
7331  0dd7               L5124:
7332                     ; 1067                 Style_Type_Flag = STYLE_S;
7334  0dd7 35010036      	mov	_Style_Type_Flag,#1
7335  0ddb cc0f9d        	jra	L3363
7336  0dde               L7753:
7337                     ; 1069           case C_KC_TYPE:
7337                     ; 1070                Standby_Timer = 300;
7339  0dde ae012c        	ldw	x,#300
7340  0de1 cf0030        	ldw	_Standby_Timer,x
7341                     ; 1071                Delay_Keep_Warm_Time = 0;
7343  0de4 c7002f        	ld	_Delay_Keep_Warm_Time,a
7344                     ; 1072                Delay_Keep_Warm_Level = 0;
7346  0de7 c7002e        	ld	_Delay_Keep_Warm_Level,a
7347                     ; 1073                if( System_Status == DIM_STATUS)
7349  0dea c6003b        	ld	a,_System_Status
7350  0ded 4a            	dec	a
7351  0dee 2607          	jrne	L1224
7352                     ; 1075                 System_Status = ACTIVE;
7354  0df0 3502003b      	mov	_System_Status,#2
7355                     ; 1076                 break;
7357  0df4 cc0f9d        	jra	L3363
7358  0df7               L1224:
7359                     ; 1078                if( System_Status == STANDBY )
7361  0df7 c6003b        	ld	a,_System_Status
7362  0dfa 27f8          	jreq	L3363
7363                     ; 1079                 break;
7365                     ; 1080                if( NO_BASKET == Brew_Select || COFFEE == Brew_Select)
7367  0dfc c60039        	ld	a,_Brew_Select
7368  0dff 27f3          	jreq	L3363
7370  0e01 a101          	cp	a,#1
7371  0e03 27ef          	jreq	L3363
7372                     ; 1082                if( DLY_BREW_FUNC )
7374  0e05 720b001913    	btjf	_Disp_Flag,#5,L1324
7375                     ; 1084                   if( delayAdjust15S == 0)
7377  0e0a 725d0023      	tnz	_delayAdjust15S
7378  0e0e 260d          	jrne	L1324
7379                     ; 1086                     BeepStart( Cancel_Sound );
7381  0e10 ae0000        	ldw	x,#_Cancel_Sound
7382  0e13 cd0000        	call	_BeepStart
7384                     ; 1087                     delayExitCnt  = 3;
7386  0e16 35030021      	mov	_delayExitCnt,#3
7387                     ; 1088                     break;
7389  0e1a cc0f9d        	jra	L3363
7390  0e1d               L1324:
7391                     ; 1091                if( Start_Status != OFF )
7393  0e1d 725d003a      	tnz	_Start_Status
7394  0e21 2717          	jreq	L5324
7395                     ; 1093                  if( brewAdjust15S == 0)
7397  0e23 725d0028      	tnz	_brewAdjust15S
7398  0e27 2611          	jrne	L5324
7399                     ; 1095                     BeepStart( Cancel_Sound );
7401  0e29 ae0000        	ldw	x,#_Cancel_Sound
7402  0e2c cd0000        	call	_BeepStart
7404                     ; 1097                     KEEP_WARM_FUNC = 0;
7406  0e2f 72130019      	bres	_Disp_Flag,#1
7407                     ; 1098                     brewExitCnt    = 3;
7409  0e33 35030026      	mov	_brewExitCnt,#3
7410                     ; 1099                     break;
7412  0e37 cc0f9d        	jra	L3363
7413  0e3a               L5324:
7414                     ; 1102                Clock_Set_Time = 0;
7416  0e3a 725f0033      	clr	_Clock_Set_Time
7417                     ; 1103                if( Delay_Brew_Time )
7419  0e3e 725d002c      	tnz	_Delay_Brew_Time
7420  0e42 2704          	jreq	L1424
7421                     ; 1104                 Delay_Brew_Time = 30;
7423  0e44 351e002c      	mov	_Delay_Brew_Time,#30
7424  0e48               L1424:
7425                     ; 1105                if( brewAdjust15S )
7427  0e48 725d0028      	tnz	_brewAdjust15S
7428  0e4c 2704          	jreq	L3424
7429                     ; 1106                  brewNoaction3S = BREW_NOACTION_TIME;
7431  0e4e 35030027      	mov	_brewNoaction3S,#3
7432  0e52               L3424:
7433                     ; 1107                if( delayAdjust15S )
7436  0e52 725d0023      	tnz	_delayAdjust15S
7437  0e56 2704          	jreq	L5424
7438                     ; 1108                  delayNoaction3S = DELAY_NOACTION_TIME;
7440  0e58 35030022      	mov	_delayNoaction3S,#3
7441  0e5c               L5424:
7442                     ; 1109                if( Brew_Select == TEA )
7445  0e5c a102          	cp	a,#2
7446  0e5e 26d7          	jrne	L3363
7447                     ; 1111                  if( TYPE_S == Style_Type_Flag )
7449  0e60 c60036        	ld	a,_Style_Type_Flag
7450  0e63 a102          	cp	a,#2
7451  0e65 2607          	jrne	L1524
7452                     ; 1112                   Style_Type_Flag = 0;
7454  0e67 725f0036      	clr	_Style_Type_Flag
7456  0e6b cc0f9d        	jra	L3363
7457  0e6e               L1524:
7458                     ; 1114                   Style_Type_Flag = TYPE_S;
7460  0e6e 35020036      	mov	_Style_Type_Flag,#2
7461  0e72 cc0f9d        	jra	L3363
7462  0e75               L1063:
7463                     ; 1118           case C_KC_CLEAN:
7463                     ; 1119                Clock_Set_Time = 0;
7465  0e75 c70033        	ld	_Clock_Set_Time,a
7466                     ; 1121                Standby_Timer = 300;
7468  0e78 ae012c        	ldw	x,#300
7469  0e7b cf0030        	ldw	_Standby_Timer,x
7470                     ; 1122                Delay_Keep_Warm_Time = 0;
7472  0e7e c7002f        	ld	_Delay_Keep_Warm_Time,a
7473                     ; 1123                Delay_Keep_Warm_Level = 0;
7475  0e81 c7002e        	ld	_Delay_Keep_Warm_Level,a
7476                     ; 1124                if( System_Status == DIM_STATUS)
7478  0e84 c6003b        	ld	a,_System_Status
7479  0e87 4a            	dec	a
7480  0e88 2607          	jrne	L5524
7481                     ; 1126                 System_Status = ACTIVE;
7483  0e8a 3502003b      	mov	_System_Status,#2
7484                     ; 1127                 break;
7486  0e8e cc0f9d        	jra	L3363
7487  0e91               L5524:
7488                     ; 1129                if( NO_BASKET == Brew_Select )
7490  0e91 c60039        	ld	a,_Brew_Select
7491  0e94 27f8          	jreq	L3363
7492                     ; 1130                 break;
7494                     ; 1131                if( System_Status == STANDBY )
7496  0e96 725d003b      	tnz	_System_Status
7497  0e9a 27f2          	jreq	L3363
7498                     ; 1132                 break;
7500                     ; 1136                if( DLY_BREW_FUNC )
7502  0e9c 720b001907    	btjf	_Disp_Flag,#5,L3624
7503                     ; 1138                   DLY_BREW_FUNC = 0;
7505  0ea1 721b0019      	bres	_Disp_Flag,#5
7506                     ; 1139                   break;
7508  0ea5 cc0f9d        	jra	L3363
7509  0ea8               L3624:
7510                     ; 1141                if( OFF == Start_Status)
7512  0ea8 725d003a      	tnz	_Start_Status
7513  0eac 2620          	jrne	L5624
7514                     ; 1143                 if( Knob0_count [ Brew_Select ] !=  fullcarafe )
7516  0eae 5f            	clrw	x
7517  0eaf 97            	ld	xl,a
7518  0eb0 d60054        	ld	a,(_Knob0_count,x)
7519  0eb3 a105          	cp	a,#5
7520  0eb5 2707          	jreq	L7624
7521                     ; 1145                   cleanAlarm = 5;
7523  0eb7 35050020      	mov	_cleanAlarm,#5
7525  0ebb cc0f9d        	jra	L3363
7526  0ebe               L7624:
7527                     ; 1149                   CLEAN_DISP_FLAG ^= 1;
7529  0ebe 90100018      	bcpl	_Disp_Flag1,#0
7530                     ; 1150                   if( CLEAN_DISP_FLAG )
7532  0ec2 72010018f4    	btjf	_Disp_Flag1,#0,L3363
7533                     ; 1151                     cleanStart = 5;
7535  0ec7 3505001f      	mov	_cleanStart,#5
7536  0ecb cc0f9d        	jra	L3363
7537  0ece               L5624:
7538                     ; 1155                else if( CLEAN_FUNC_FLAG )
7540  0ece 72030018f8    	btjf	_Disp_Flag1,#1,L3363
7541                     ; 1157                 CLEAN_FUNC_FLAG = 0;
7543  0ed3 72130018      	bres	_Disp_Flag1,#1
7544                     ; 1159                 CLEAN_DISP_FLAG = 0;
7546  0ed7 72110018      	bres	_Disp_Flag1,#0
7547                     ; 1160                 Clock_Clean.hour            = 0;
7549  0edb 725f0043      	clr	_Clock_Clean+2
7550                     ; 1161                 Clock_Clean.min             = 59;
7552  0edf 353b0042      	mov	_Clock_Clean+1,#59
7553                     ; 1162                 Clock_Clean.second          = 59;
7555  0ee3 353b0041      	mov	_Clock_Clean,#59
7556                     ; 1163                 Start_Status = OFF;
7558  0ee7 725f003a      	clr	_Start_Status
7559                     ; 1164                 BeepStart( Cancel_Sound );
7561  0eeb ae0000        	ldw	x,#_Cancel_Sound
7562  0eee cd0000        	call	_BeepStart
7564  0ef1 cc0f9d        	jra	L3363
7565  0ef4               L3063:
7566                     ; 1168           case C_KC_START | C_KS_2SEC:
7566                     ; 1169                Standby_Timer = 300;
7568  0ef4 ae012c        	ldw	x,#300
7569  0ef7 cf0030        	ldw	_Standby_Timer,x
7570                     ; 1170                if( System_Status == DIM_STATUS)
7572  0efa c6003b        	ld	a,_System_Status
7573  0efd 4a            	dec	a
7574  0efe 2607          	jrne	L1034
7575                     ; 1172                 System_Status = ACTIVE;
7577  0f00 3502003b      	mov	_System_Status,#2
7578                     ; 1173                 break;
7580  0f04 cc0f9d        	jra	L3363
7581  0f07               L1034:
7582                     ; 1175                if( Brew_Select == NO_BASKET )
7584  0f07 c60039        	ld	a,_Brew_Select
7585  0f0a 2602          	jrne	L3034
7586                     ; 1176                   return;
7589  0f0c 84            	pop	a
7590  0f0d 81            	ret	
7591  0f0e               L3034:
7592                     ; 1177                if( Start_Status == PAUSE )
7594  0f0e c6003a        	ld	a,_Start_Status
7595  0f11 a102          	cp	a,#2
7596  0f13 26ef          	jrne	L3363
7597                     ; 1179                  Start_Status  = OFF;
7599  0f15 725f003a      	clr	_Start_Status
7600                     ; 1180                  Paused_Delay  = 0;
7602  0f19 5f            	clrw	x
7603  0f1a cf0034        	ldw	_Paused_Delay,x
7604                     ; 1181                  KEEP_WARM_FUNC = 0;
7606  0f1d 72130019      	bres	_Disp_Flag,#1
7607                     ; 1182                  CLEAN_FUNC_FLAG = 0;
7609  0f21 72130018      	bres	_Disp_Flag1,#1
7610  0f25 2076          	jra	L3363
7611  0f27               L5063:
7612                     ; 1186           case C_KC_HOUR_STYLE | C_KS_2SEC:
7612                     ; 1187                Standby_Timer = 300;
7614  0f27 ae012c        	ldw	x,#300
7615  0f2a cf0030        	ldw	_Standby_Timer,x
7616                     ; 1188                if( System_Status == DIM_STATUS)
7618  0f2d c6003b        	ld	a,_System_Status
7619  0f30 4a            	dec	a
7620  0f31 2606          	jrne	L7034
7621                     ; 1190                 System_Status = ACTIVE;
7623  0f33 3502003b      	mov	_System_Status,#2
7624                     ; 1191                 break;
7626  0f37 2064          	jra	L3363
7627  0f39               L7034:
7628                     ; 1193                if( COFFEE_KEY_FLAG )
7630  0f39 7205001806    	btjf	_Disp_Flag1,#2,L1134
7631                     ; 1195                 COFFEE_KEY_FLAG = 0;
7633  0f3e 72150018      	bres	_Disp_Flag1,#2
7635  0f42 2004          	jra	L3134
7636  0f44               L1134:
7637                     ; 1198                 COFFEE_KEY_FLAG = 1;
7639  0f44 72140018      	bset	_Disp_Flag1,#2
7640  0f48               L3134:
7641                     ; 1199                TEA_KEY_FLAG    = 0;
7643  0f48 72170018      	bres	_Disp_Flag1,#3
7644                     ; 1200                break;
7646  0f4c 204f          	jra	L3363
7647  0f4e               L7063:
7648                     ; 1201           case C_KC_MINUTE_TYPE | C_KS_2SEC:
7648                     ; 1202                Standby_Timer = 300;
7650  0f4e ae012c        	ldw	x,#300
7651  0f51 cf0030        	ldw	_Standby_Timer,x
7652                     ; 1203                if( System_Status == DIM_STATUS)
7654  0f54 c6003b        	ld	a,_System_Status
7655  0f57 4a            	dec	a
7656  0f58 2606          	jrne	L5134
7657                     ; 1205                 System_Status = ACTIVE;
7659  0f5a 3502003b      	mov	_System_Status,#2
7660                     ; 1206                 break;
7662  0f5e 203d          	jra	L3363
7663  0f60               L5134:
7664                     ; 1208                if( TEA_KEY_FLAG )
7666  0f60 7207001806    	btjf	_Disp_Flag1,#3,L7134
7667                     ; 1210                 TEA_KEY_FLAG = 0;
7669  0f65 72170018      	bres	_Disp_Flag1,#3
7671  0f69 2004          	jra	L1234
7672  0f6b               L7134:
7673                     ; 1213                 TEA_KEY_FLAG = 1;
7675  0f6b 72160018      	bset	_Disp_Flag1,#3
7676  0f6f               L1234:
7677                     ; 1214                COFFEE_KEY_FLAG    = 0;
7679  0f6f 72150018      	bres	_Disp_Flag1,#2
7680                     ; 1215                break;
7682  0f73 2028          	jra	L3363
7683  0f75               L1163:
7684                     ; 1216           case C_KC_CLEAN_STYLE | C_KS_2SEC:
7684                     ; 1217                if( forcePump == 0 )
7686  0f75 c6001e        	ld	a,_forcePump
7687  0f78 2606          	jrne	L3234
7688                     ; 1218                  forcePump = 1;
7690  0f7a 3501001e      	mov	_forcePump,#1
7692  0f7e 201d          	jra	L3363
7693  0f80               L3234:
7694                     ; 1220                  forcePump = 0;
7696  0f80 725f001e      	clr	_forcePump
7697  0f84 2017          	jra	L3363
7698  0f86               L3163:
7699                     ; 1222           case C_KC_MINUTE_HOUR | C_KS_2SEC:
7699                     ; 1223             if( debugEnable == 0 )
7701  0f86 c6001d        	ld	a,_debugEnable
7702  0f89 2606          	jrne	L7234
7703                     ; 1224                debugEnable = 1;
7705  0f8b 3501001d      	mov	_debugEnable,#1
7707  0f8f 2004          	jra	L1334
7708  0f91               L7234:
7709                     ; 1226                debugEnable = 0;
7711  0f91 725f001d      	clr	_debugEnable
7712  0f95               L1334:
7713                     ; 1227              debugIndex = 0;
7715  0f95 725f001c      	clr	_debugIndex
7716                     ; 1228              debugValueEnable = 0;
7718  0f99               LC005:
7721  0f99 725f001b      	clr	_debugValueEnable
7722                     ; 1229                break;
7724                     ; 1230           default: break;
7726  0f9d               L3363:
7727                     ; 1233 }
7730  0f9d 84            	pop	a
7731  0f9e 81            	ret	
7757                     ; 1235 void Key_Deal( void )
7757                     ; 1236 {
7758                     	switch	.text
7759  0f9f               _Key_Deal:
7763                     ; 1237  Key_Scan( );
7765  0f9f cd0521        	call	_Key_Scan
7767                     ; 1238  KeyGenerate( );
7769  0fa2 cd0636        	call	_KeyGenerate
7771                     ; 1239  KeyHandler( );
7774                     ; 1240 }
7777  0fa5 cc07a4        	jp	_KeyHandler
7780                     	switch	.bss
7781  0001               L3434_Size_Value:
7782  0001 00            	ds.b	1
7848                     ; 1244 void Size_Rotary_Code( void )
7848                     ; 1245 {
7849                     	switch	.text
7850  0fa8               _Size_Rotary_Code:
7852  0fa8 88            	push	a
7853       00000001      OFST:	set	1
7856                     ; 1247          U8 Size_Value_temp  = 0;
7858                     ; 1248          U8 PB_DATA = PB_IDR;
7860  0fa9 c65006        	ld	a,_PB_IDR
7861                     ; 1249   Size_Value_temp = ( PB_DATA & 0X30 ) >> 4 ;
7863  0fac 4e            	swap	a
7864  0fad a403          	and	a,#3
7865  0faf 6b01          	ld	(OFST+0,sp),a
7866                     ; 1250   if(( Size_Value ^ Size_Value_temp) & 0x03)
7868  0fb1 c80001        	xor	a,L3434_Size_Value
7869  0fb4 a503          	bcp	a,#3
7870  0fb6 2603cc1127    	jreq	L3734
7871                     ; 1252     Size_Value <<= 2;
7873  0fbb 72580001      	sll	L3434_Size_Value
7874  0fbf 72580001      	sll	L3434_Size_Value
7875                     ; 1253     Size_Value |= Size_Value_temp;
7877  0fc3 c60001        	ld	a,L3434_Size_Value
7878  0fc6 1a01          	or	a,(OFST+0,sp)
7879  0fc8 c70001        	ld	L3434_Size_Value,a
7880                     ; 1254     TEST_SIZE = Size_Value & 0X3F;
7882  0fcb a43f          	and	a,#63
7883  0fcd c70004        	ld	_TEST_SIZE,a
7884                     ; 1255     switch( TEST_SIZE )
7887                     ; 1358       default:break;
7888  0fd0 a007          	sub	a,#7
7889  0fd2 2603cc1086    	jreq	L7434
7890  0fd7 a004          	sub	a,#4
7891  0fd9 270b          	jreq	L5434
7892  0fdb a029          	sub	a,#41
7893  0fdd 2707          	jreq	L5434
7894  0fdf a004          	sub	a,#4
7895  0fe1 27f1          	jreq	L7434
7896  0fe3 cc1127        	jra	L3734
7897  0fe6               L5434:
7898                     ; 1257       case 0x0B:
7898                     ; 1258       case 0x34:
7898                     ; 1259               
7898                     ; 1260                Clock_Set_Time = 0;
7900  0fe6 725f0033      	clr	_Clock_Set_Time
7901                     ; 1262                Standby_Timer = 300;
7903  0fea ae012c        	ldw	x,#300
7904  0fed cf0030        	ldw	_Standby_Timer,x
7905                     ; 1263                Delay_Keep_Warm_Time = 0;
7907  0ff0 725f002f      	clr	_Delay_Keep_Warm_Time
7908                     ; 1264                Delay_Keep_Warm_Level = 0;
7910  0ff4 725f002e      	clr	_Delay_Keep_Warm_Level
7911                     ; 1266                if( System_Status == DIM_STATUS)
7913  0ff8 c6003b        	ld	a,_System_Status
7914  0ffb 4a            	dec	a
7915  0ffc 2607          	jrne	L1044
7916                     ; 1268                 System_Status = ACTIVE;
7918  0ffe 3502003b      	mov	_System_Status,#2
7919                     ; 1269                 break;
7921  1002 cc1127        	jra	L3734
7922  1005               L1044:
7923                     ; 1271                if( Delay_Brew_Time )
7925  1005 c6002c        	ld	a,_Delay_Brew_Time
7926  1008 2704          	jreq	L3044
7927                     ; 1272                 Delay_Brew_Time = 30;
7929  100a 351e002c      	mov	_Delay_Brew_Time,#30
7930  100e               L3044:
7931                     ; 1273                if( NO_BASKET == Brew_Select )
7933  100e c60039        	ld	a,_Brew_Select
7934  1011 27ef          	jreq	L3734
7935                     ; 1274                 break;
7937                     ; 1275                if( System_Status == STANDBY )
7939  1013 725d003b      	tnz	_System_Status
7940  1017 27e9          	jreq	L3734
7941                     ; 1276                 break;
7943                     ; 1278                if( Start_Status != OFF )
7945  1019 725d003a      	tnz	_Start_Status
7946  101d 2717          	jreq	L1144
7947                     ; 1280                 if( brewAdjust15S == 0)
7949  101f 725d0028      	tnz	_brewAdjust15S
7950  1023 2611          	jrne	L1144
7951                     ; 1282                     BeepStart( Cancel_Sound );
7953  1025 ae0000        	ldw	x,#_Cancel_Sound
7954  1028 cd0000        	call	_BeepStart
7956                     ; 1284                     KEEP_WARM_FUNC = 0;
7958  102b 72130019      	bres	_Disp_Flag,#1
7959                     ; 1285                     brewExitCnt    = 3;
7961  102f 35030026      	mov	_brewExitCnt,#3
7962                     ; 1286                     break;
7964  1033 cc1127        	jra	L3734
7965  1036               L1144:
7966                     ; 1290                DELAY_SET_SIZE = 1;
7968  1036 72180018      	bset	_Disp_Flag1,#4
7969                     ; 1291                if( Speciality == Knob1_count [ Brew_Select ]  || Immersion == Knob1_count [ Brew_Select ]  )
7971  103a 5f            	clrw	x
7972  103b 97            	ld	xl,a
7973  103c d60051        	ld	a,(_Knob1_count,x)
7974  103f a104          	cp	a,#4
7975  1041 27f0          	jreq	L3734
7977  1043 c60039        	ld	a,_Brew_Select
7978  1046 5f            	clrw	x
7979  1047 97            	ld	xl,a
7980  1048 d60051        	ld	a,(_Knob1_count,x)
7981  104b a105          	cp	a,#5
7982  104d 27e4          	jreq	L3734
7983                     ; 1293                if( DLY_BREW_FUNC )
7985  104f 720b001908    	btjf	_Disp_Flag,#5,L1244
7986                     ; 1295                   if( delayAdjust15S == 0)
7988  1054 c60023        	ld	a,_delayAdjust15S
7989                     ; 1297                     BeepStart( Cancel_Sound );
7991                     ; 1298                     delayExitCnt  = 3;
7992                     ; 1299                     break;
7994  1057 2603cc10f4    	jreq	LC006
7995  105c               L1244:
7996                     ; 1302                if( brewAdjust15S )
7998  105c c60028        	ld	a,_brewAdjust15S
7999  105f 2704          	jreq	L5244
8000                     ; 1303                  brewNoaction3S = BREW_NOACTION_TIME;
8002  1061 35030027      	mov	_brewNoaction3S,#3
8003  1065               L5244:
8004                     ; 1304                if( delayAdjust15S )
8007  1065 c60023        	ld	a,_delayAdjust15S
8008  1068 2704          	jreq	L7244
8009                     ; 1305                  delayNoaction3S = DELAY_NOACTION_TIME;
8011  106a 35030022      	mov	_delayNoaction3S,#3
8012  106e               L7244:
8013                     ; 1306                if( Knob0_count [ Brew_Select ] < fullcarafe )
8016  106e c60039        	ld	a,_Brew_Select
8017  1071 5f            	clrw	x
8018  1072 97            	ld	xl,a
8019  1073 d60054        	ld	a,(_Knob0_count,x)
8020  1076 a105          	cp	a,#5
8021  1078 24b9          	jruge	L3734
8022                     ; 1307                 Knob0_count [ Brew_Select ]++;
8024  107a c60039        	ld	a,_Brew_Select
8025  107d 5f            	clrw	x
8026  107e 97            	ld	xl,a
8027  107f 724c0054      	inc	(_Knob0_count,x)
8028  1083 cc1127        	jra	L3734
8029  1086               L7434:
8030                     ; 1310       case 0x07:
8030                     ; 1311       case 0x38:
8030                     ; 1312                Clock_Set_Time = 0;
8032  1086 725f0033      	clr	_Clock_Set_Time
8033                     ; 1314                Standby_Timer = 300;
8035  108a ae012c        	ldw	x,#300
8036  108d cf0030        	ldw	_Standby_Timer,x
8037                     ; 1315                Delay_Keep_Warm_Time = 0;
8039  1090 725f002f      	clr	_Delay_Keep_Warm_Time
8040                     ; 1316                Delay_Keep_Warm_Level = 0;
8042  1094 725f002e      	clr	_Delay_Keep_Warm_Level
8043                     ; 1317                if( System_Status == DIM_STATUS)
8045  1098 c6003b        	ld	a,_System_Status
8046  109b 4a            	dec	a
8047  109c 2607          	jrne	L3344
8048                     ; 1319                 System_Status = ACTIVE;
8050  109e 3502003b      	mov	_System_Status,#2
8051                     ; 1320                 break;
8053  10a2 cc1127        	jra	L3734
8054  10a5               L3344:
8055                     ; 1322                if( Delay_Brew_Time )
8057  10a5 c6002c        	ld	a,_Delay_Brew_Time
8058  10a8 2704          	jreq	L5344
8059                     ; 1323                 Delay_Brew_Time = 30;
8061  10aa 351e002c      	mov	_Delay_Brew_Time,#30
8062  10ae               L5344:
8063                     ; 1324                if( NO_BASKET == Brew_Select )
8065  10ae c60039        	ld	a,_Brew_Select
8066  10b1 2774          	jreq	L3734
8067                     ; 1325                 break;
8069                     ; 1326                if( System_Status == STANDBY )
8071  10b3 725d003b      	tnz	_System_Status
8072  10b7 276e          	jreq	L3734
8073                     ; 1327                 break;
8075                     ; 1328                if( Start_Status != OFF )
8077  10b9 725d003a      	tnz	_Start_Status
8078  10bd 2716          	jreq	L3444
8079                     ; 1330                 if( brewAdjust15S == 0)
8081  10bf 725d0028      	tnz	_brewAdjust15S
8082  10c3 2610          	jrne	L3444
8083                     ; 1332                     BeepStart( Cancel_Sound );
8085  10c5 ae0000        	ldw	x,#_Cancel_Sound
8086  10c8 cd0000        	call	_BeepStart
8088                     ; 1334                     KEEP_WARM_FUNC = 0;
8090  10cb 72130019      	bres	_Disp_Flag,#1
8091                     ; 1335                     brewExitCnt    = 3;
8093  10cf 35030026      	mov	_brewExitCnt,#3
8094                     ; 1336                     break;
8096  10d3 2052          	jra	L3734
8097  10d5               L3444:
8098                     ; 1339                if( Speciality == Knob1_count [ Brew_Select ]  || Immersion == Knob1_count [ Brew_Select ]  )
8100  10d5 5f            	clrw	x
8101  10d6 97            	ld	xl,a
8102  10d7 d60051        	ld	a,(_Knob1_count,x)
8103  10da a104          	cp	a,#4
8104  10dc 2749          	jreq	L3734
8106  10de c60039        	ld	a,_Brew_Select
8107  10e1 5f            	clrw	x
8108  10e2 97            	ld	xl,a
8109  10e3 d60051        	ld	a,(_Knob1_count,x)
8110  10e6 a105          	cp	a,#5
8111  10e8 273d          	jreq	L3734
8112                     ; 1341                if( DLY_BREW_FUNC )
8114  10ea 720b001911    	btjf	_Disp_Flag,#5,L3544
8115                     ; 1343                   if( delayAdjust15S == 0)
8117  10ef c60023        	ld	a,_delayAdjust15S
8118  10f2 260c          	jrne	L3544
8119                     ; 1345                     BeepStart( Cancel_Sound );
8122                     ; 1346                     delayExitCnt  = 3;
8124  10f4               LC006:
8126  10f4 ae0000        	ldw	x,#_Cancel_Sound
8127  10f7 cd0000        	call	_BeepStart
8129  10fa 35030021      	mov	_delayExitCnt,#3
8130                     ; 1347                     break;
8132  10fe 2027          	jra	L3734
8133  1100               L3544:
8134                     ; 1350                DELAY_SET_SIZE = 1;
8136  1100 72180018      	bset	_Disp_Flag1,#4
8137                     ; 1351                if( brewAdjust15S )
8139  1104 c60028        	ld	a,_brewAdjust15S
8140  1107 2704          	jreq	L7544
8141                     ; 1352                  brewNoaction3S = BREW_NOACTION_TIME;
8143  1109 35030027      	mov	_brewNoaction3S,#3
8144  110d               L7544:
8145                     ; 1353                if( delayAdjust15S )
8148  110d c60023        	ld	a,_delayAdjust15S
8149  1110 2704          	jreq	L1644
8150                     ; 1354                  delayNoaction3S = DELAY_NOACTION_TIME;
8152  1112 35030022      	mov	_delayNoaction3S,#3
8153  1116               L1644:
8154                     ; 1355                if( Knob0_count [ Brew_Select ]  > Cup )
8157  1116 c60039        	ld	a,_Brew_Select
8158  1119 5f            	clrw	x
8159  111a 97            	ld	xl,a
8160  111b 724d0054      	tnz	(_Knob0_count,x)
8161  111f 2706          	jreq	L3734
8162                     ; 1356                 Knob0_count [ Brew_Select ]--;
8164  1121 5f            	clrw	x
8165  1122 97            	ld	xl,a
8166  1123 724a0054      	dec	(_Knob0_count,x)
8167                     ; 1358       default:break;
8169  1127               L3734:
8170                     ; 1361 }
8173  1127 84            	pop	a
8174  1128 81            	ret	
8177                     	switch	.bss
8178  0002               L5644_Style_Type:
8179  0002 00            	ds.b	1
8247                     ; 1365 void Style_Type_Rotary_Code( void )
8247                     ; 1366 {
8248                     	switch	.text
8249  1129               _Style_Type_Rotary_Code:
8251  1129 88            	push	a
8252       00000001      OFST:	set	1
8255                     ; 1368          U8 Style_Type_temp  = 0;
8257                     ; 1369          U8 PB_DATA = PB_IDR;
8259  112a c65006        	ld	a,_PB_IDR
8260                     ; 1370   Style_Type_temp = ( PB_DATA & 0X0C ) >> 2 ;
8262  112d a40c          	and	a,#12
8263  112f 44            	srl	a
8264  1130 44            	srl	a
8265  1131 6b01          	ld	(OFST+0,sp),a
8266                     ; 1371   if(( Style_Type ^ Style_Type_temp) & 0x03)
8268  1133 c80002        	xor	a,L5644_Style_Type
8269  1136 a503          	bcp	a,#3
8270  1138 2603cc132b    	jreq	L5154
8271                     ; 1373     Style_Type <<= 2;
8273  113d 72580002      	sll	L5644_Style_Type
8274  1141 72580002      	sll	L5644_Style_Type
8275                     ; 1374     Style_Type |= Style_Type_temp;
8277  1145 c60002        	ld	a,L5644_Style_Type
8278  1148 1a01          	or	a,(OFST+0,sp)
8279  114a c70002        	ld	L5644_Style_Type,a
8280                     ; 1375     TEST_STYLE = Style_Type & 0X3F;
8282  114d a43f          	and	a,#63
8283  114f c70003        	ld	_TEST_STYLE,a
8284                     ; 1376     switch(  TEST_STYLE )
8287                     ; 1530       default:break;
8288  1152 a007          	sub	a,#7
8289  1154 270f          	jreq	L7644
8290  1156 a004          	sub	a,#4
8291  1158 2603cc1257    	jreq	L1744
8292  115d a029          	sub	a,#41
8293  115f 27f9          	jreq	L1744
8294  1161 a004          	sub	a,#4
8295  1163 26d5          	jrne	L5154
8296  1165               L7644:
8297                     ; 1378       case 0x07:
8297                     ; 1379       case 0x38:
8297                     ; 1380                Clock_Set_Time = 0;
8299  1165 725f0033      	clr	_Clock_Set_Time
8300                     ; 1382                Standby_Timer = 300;
8302  1169 ae012c        	ldw	x,#300
8303  116c cf0030        	ldw	_Standby_Timer,x
8304                     ; 1383                Delay_Keep_Warm_Time = 0;
8306  116f 725f002f      	clr	_Delay_Keep_Warm_Time
8307                     ; 1384                Delay_Keep_Warm_Level = 0;
8309  1173 725f002e      	clr	_Delay_Keep_Warm_Level
8310                     ; 1385                if( System_Status == DIM_STATUS)
8312  1177 c6003b        	ld	a,_System_Status
8313  117a 4a            	dec	a
8314  117b 2607          	jrne	L3254
8315                     ; 1387                 System_Status = ACTIVE;
8317  117d 3502003b      	mov	_System_Status,#2
8318                     ; 1388                 break;
8320  1181 cc132b        	jra	L5154
8321  1184               L3254:
8322                     ; 1390                if( Delay_Brew_Time )
8324  1184 c6002c        	ld	a,_Delay_Brew_Time
8325  1187 2704          	jreq	L5254
8326                     ; 1391                 Delay_Brew_Time = 30;
8328  1189 351e002c      	mov	_Delay_Brew_Time,#30
8329  118d               L5254:
8330                     ; 1392                if( NO_BASKET == Brew_Select )
8332  118d c60039        	ld	a,_Brew_Select
8333  1190 27ef          	jreq	L5154
8334                     ; 1393                 break;
8336                     ; 1394                if( System_Status == STANDBY )
8338  1192 725d003b      	tnz	_System_Status
8339  1196 27e9          	jreq	L5154
8340                     ; 1395                 break;
8342                     ; 1396                if( Start_Status != OFF )
8344  1198 725d003a      	tnz	_Start_Status
8345  119c 2717          	jreq	L3354
8346                     ; 1398                 if( brewAdjust15S == 0)
8348  119e 725d0028      	tnz	_brewAdjust15S
8349  11a2 2611          	jrne	L3354
8350                     ; 1400                     BeepStart( Cancel_Sound );
8352  11a4 ae0000        	ldw	x,#_Cancel_Sound
8353  11a7 cd0000        	call	_BeepStart
8355                     ; 1402                     KEEP_WARM_FUNC = 0;
8357  11aa 72130019      	bres	_Disp_Flag,#1
8358                     ; 1403                     brewExitCnt    = 3;
8360  11ae 35030026      	mov	_brewExitCnt,#3
8361                     ; 1404                     break;
8363  11b2 cc132b        	jra	L5154
8364  11b5               L3354:
8365                     ; 1407                if( DLY_BREW_FUNC )
8367  11b5 720b001913    	btjf	_Disp_Flag,#5,L7354
8368                     ; 1409                   if( delayAdjust15S == 0)
8370  11ba 725d0023      	tnz	_delayAdjust15S
8371  11be 260d          	jrne	L7354
8372                     ; 1411                     BeepStart( Cancel_Sound );
8374  11c0 ae0000        	ldw	x,#_Cancel_Sound
8375  11c3 cd0000        	call	_BeepStart
8377                     ; 1412                     delayExitCnt  = 3;
8379  11c6 35030021      	mov	_delayExitCnt,#3
8380                     ; 1413                     break;
8382  11ca cc132b        	jra	L5154
8383  11cd               L7354:
8384                     ; 1416                if( Brew_Select == TEA )
8386  11cd a102          	cp	a,#2
8387  11cf 260a          	jrne	L3454
8388                     ; 1418                  if( Style_Type_Flag == 0 )
8390  11d1 725d0036      	tnz	_Style_Type_Flag
8391  11d5 2604          	jrne	L3454
8392                     ; 1420                   Style_Type_Flag = STYLE_S;
8394  11d7 35010036      	mov	_Style_Type_Flag,#1
8395  11db               L3454:
8396                     ; 1424                if ( Brew_Select == COFFEE )
8398  11db a101          	cp	a,#1
8399  11dd 262d          	jrne	L7454
8400                     ; 1426                 DELAY_SET_STYLE = 1;
8402  11df 721a0018      	bset	_Disp_Flag1,#5
8403                     ; 1427                 if( brewAdjust15S )
8405  11e3 725d0028      	tnz	_brewAdjust15S
8406  11e7 2704          	jreq	L1554
8407                     ; 1428                  brewNoaction3S = BREW_NOACTION_TIME;
8409  11e9 35030027      	mov	_brewNoaction3S,#3
8410  11ed               L1554:
8411                     ; 1429                 if( delayAdjust15S )
8414  11ed 725d0023      	tnz	_delayAdjust15S
8415  11f1 2704          	jreq	L3554
8416                     ; 1430                  delayNoaction3S = DELAY_NOACTION_TIME;
8418  11f3 35030022      	mov	_delayNoaction3S,#3
8419  11f7               L3554:
8420                     ; 1431                 if( Knob1_count [ Brew_Select ]  < Immersion )
8423  11f7 5f            	clrw	x
8424  11f8 97            	ld	xl,a
8425  11f9 d60051        	ld	a,(_Knob1_count,x)
8426  11fc a105          	cp	a,#5
8427  11fe 24ca          	jruge	L5154
8428                     ; 1432                      Knob1_count [ Brew_Select ] ++;
8430  1200 c60039        	ld	a,_Brew_Select
8431  1203 5f            	clrw	x
8432  1204 97            	ld	xl,a
8433  1205 724c0051      	inc	(_Knob1_count,x)
8434  1209 cc132b        	jra	L5154
8435  120c               L7454:
8436                     ; 1436                  if( brewAdjust15S )
8438  120c c60028        	ld	a,_brewAdjust15S
8439  120f 2704          	jreq	L1654
8440                     ; 1437                  brewNoaction3S = BREW_NOACTION_TIME;
8442  1211 35030027      	mov	_brewNoaction3S,#3
8443  1215               L1654:
8444                     ; 1438                  if( delayAdjust15S )
8447  1215 c60023        	ld	a,_delayAdjust15S
8448  1218 2704          	jreq	L3654
8449                     ; 1439                  delayNoaction3S = DELAY_NOACTION_TIME;
8451  121a 35030022      	mov	_delayNoaction3S,#3
8452  121e               L3654:
8453                     ; 1440                  if( Style_Type_Flag == STYLE_S )
8456  121e c60036        	ld	a,_Style_Type_Flag
8457  1221 a101          	cp	a,#1
8458  1223 261c          	jrne	L5654
8459                     ; 1442                    DELAY_SET_STYLE = 1;
8461  1225 721a0018      	bset	_Disp_Flag1,#5
8462                     ; 1443                    if( Knob1_count [ Brew_Select ]  < Speciality )
8464  1229 c60039        	ld	a,_Brew_Select
8465  122c 5f            	clrw	x
8466  122d 97            	ld	xl,a
8467  122e d60051        	ld	a,(_Knob1_count,x)
8468  1231 a104          	cp	a,#4
8469  1233 24d4          	jruge	L5154
8470                     ; 1444                      Knob1_count [ Brew_Select ]++;
8472  1235 c60039        	ld	a,_Brew_Select
8473  1238 5f            	clrw	x
8474  1239 97            	ld	xl,a
8475  123a 724c0051      	inc	(_Knob1_count,x)
8476  123e cc132b        	jra	L5154
8477  1241               L5654:
8478                     ; 1446                  else if( Style_Type_Flag == TYPE_S )
8480  1241 a102          	cp	a,#2
8481  1243 26f9          	jrne	L5154
8482                     ; 1448                    DELAY_SET_TYPE = 1;
8484  1245 721c0018      	bset	_Disp_Flag1,#6
8485                     ; 1449                    if( Knob2_count < Delicate )
8487  1249 c60050        	ld	a,_Knob2_count
8488  124c a105          	cp	a,#5
8489  124e 24ee          	jruge	L5154
8490                     ; 1450                      Knob2_count++;
8492  1250 725c0050      	inc	_Knob2_count
8493  1254 cc132b        	jra	L5154
8494  1257               L1744:
8495                     ; 1454       case 0x0B:
8495                     ; 1455       case 0x34:
8495                     ; 1456                Clock_Set_Time = 0;
8497  1257 725f0033      	clr	_Clock_Set_Time
8498                     ; 1458                Standby_Timer = 300;
8500  125b ae012c        	ldw	x,#300
8501  125e cf0030        	ldw	_Standby_Timer,x
8502                     ; 1459                Delay_Keep_Warm_Time = 0;
8504  1261 725f002f      	clr	_Delay_Keep_Warm_Time
8505                     ; 1460                Delay_Keep_Warm_Level = 0;
8507  1265 725f002e      	clr	_Delay_Keep_Warm_Level
8508                     ; 1461                if( System_Status == DIM_STATUS)
8510  1269 c6003b        	ld	a,_System_Status
8511  126c 4a            	dec	a
8512  126d 2607          	jrne	L7754
8513                     ; 1463                 System_Status = ACTIVE;
8515  126f 3502003b      	mov	_System_Status,#2
8516                     ; 1464                 break;
8518  1273 cc132b        	jra	L5154
8519  1276               L7754:
8520                     ; 1466                if( Delay_Brew_Time )
8522  1276 c6002c        	ld	a,_Delay_Brew_Time
8523  1279 2704          	jreq	L1064
8524                     ; 1467                 Delay_Brew_Time = 30;
8526  127b 351e002c      	mov	_Delay_Brew_Time,#30
8527  127f               L1064:
8528                     ; 1468                if( NO_BASKET == Brew_Select )
8530  127f c60039        	ld	a,_Brew_Select
8531  1282 27ef          	jreq	L5154
8532                     ; 1469                 break;
8534                     ; 1470                if( System_Status == STANDBY )
8536  1284 725d003b      	tnz	_System_Status
8537  1288 27e9          	jreq	L5154
8538                     ; 1471                 break;
8540                     ; 1472                if( Start_Status != OFF )
8542  128a 725d003a      	tnz	_Start_Status
8543  128e 2717          	jreq	L7064
8544                     ; 1474                  if( brewAdjust15S == 0)
8546  1290 725d0028      	tnz	_brewAdjust15S
8547  1294 2611          	jrne	L7064
8548                     ; 1476                     BeepStart( Cancel_Sound );
8550  1296 ae0000        	ldw	x,#_Cancel_Sound
8551  1299 cd0000        	call	_BeepStart
8553                     ; 1478                     KEEP_WARM_FUNC = 0;
8555  129c 72130019      	bres	_Disp_Flag,#1
8556                     ; 1479                     brewExitCnt    = 3;
8558  12a0 35030026      	mov	_brewExitCnt,#3
8559                     ; 1480                     break;
8561  12a4 cc132b        	jra	L5154
8562  12a7               L7064:
8563                     ; 1483                if( DLY_BREW_FUNC )
8565  12a7 720b001912    	btjf	_Disp_Flag,#5,L3164
8566                     ; 1485                   if( delayAdjust15S == 0)
8568  12ac 725d0023      	tnz	_delayAdjust15S
8569  12b0 260c          	jrne	L3164
8570                     ; 1487                     BeepStart( Cancel_Sound );
8572  12b2 ae0000        	ldw	x,#_Cancel_Sound
8573  12b5 cd0000        	call	_BeepStart
8575                     ; 1488                     delayExitCnt  = 3;
8577  12b8 35030021      	mov	_delayExitCnt,#3
8578                     ; 1489                     break;
8580  12bc 206d          	jra	L5154
8581  12be               L3164:
8582                     ; 1492                if( Brew_Select == TEA )
8584  12be a102          	cp	a,#2
8585  12c0 260a          	jrne	L7164
8586                     ; 1494                  if( Style_Type_Flag == 0 )
8588  12c2 725d0036      	tnz	_Style_Type_Flag
8589  12c6 2604          	jrne	L7164
8590                     ; 1496                   Style_Type_Flag = STYLE_S;
8592  12c8 35010036      	mov	_Style_Type_Flag,#1
8593  12cc               L7164:
8594                     ; 1500                if ( Brew_Select == COFFEE )
8596  12cc a101          	cp	a,#1
8597  12ce 261a          	jrne	L3264
8598                     ; 1502                 DELAY_SET_STYLE = 1;
8600  12d0 721a0018      	bset	_Disp_Flag1,#5
8601                     ; 1503                 if( brewAdjust15S )
8603  12d4 725d0028      	tnz	_brewAdjust15S
8604  12d8 2704          	jreq	L5264
8605                     ; 1504                  brewNoaction3S = BREW_NOACTION_TIME;
8607  12da 35030027      	mov	_brewNoaction3S,#3
8608  12de               L5264:
8609                     ; 1505                 if( delayAdjust15S )
8612  12de 725d0023      	tnz	_delayAdjust15S
8613  12e2 2726          	jreq	LC007
8614                     ; 1506                  delayNoaction3S = DELAY_NOACTION_TIME;
8616  12e4 35030022      	mov	_delayNoaction3S,#3
8617                     ; 1507                 if( Knob1_count [ Brew_Select ]  > Classic )
8619                     ; 1508                    Knob1_count [ Brew_Select ] --;
8620  12e8 2020          	jp	LC007
8621  12ea               L3264:
8622                     ; 1512                  if( brewAdjust15S )
8624  12ea c60028        	ld	a,_brewAdjust15S
8625  12ed 2704          	jreq	L5364
8626                     ; 1513                  brewNoaction3S = BREW_NOACTION_TIME;
8628  12ef 35030027      	mov	_brewNoaction3S,#3
8629  12f3               L5364:
8630                     ; 1514                  if( delayAdjust15S )
8633  12f3 c60023        	ld	a,_delayAdjust15S
8634  12f6 2704          	jreq	L7364
8635                     ; 1515                  delayNoaction3S = DELAY_NOACTION_TIME;
8637  12f8 35030022      	mov	_delayNoaction3S,#3
8638  12fc               L7364:
8639                     ; 1516                  if( Style_Type_Flag == STYLE_S )
8642  12fc c60036        	ld	a,_Style_Type_Flag
8643  12ff a101          	cp	a,#1
8644  1301 2617          	jrne	L1464
8645                     ; 1518                    DELAY_SET_STYLE = 1;
8647  1303 721a0018      	bset	_Disp_Flag1,#5
8648                     ; 1519                    if( Knob1_count [ Brew_Select ]  > Classic )
8650  1307 c60039        	ld	a,_Brew_Select
8651                     ; 1520                      Knob1_count [ Brew_Select ]--;
8653  130a               LC007:
8655  130a 5f            	clrw	x
8656  130b 97            	ld	xl,a
8657  130c 724d0051      	tnz	(_Knob1_count,x)
8658  1310 2719          	jreq	L5154
8660  1312 5f            	clrw	x
8661  1313 97            	ld	xl,a
8662  1314 724a0051      	dec	(_Knob1_count,x)
8663  1318 2011          	jra	L5154
8664  131a               L1464:
8665                     ; 1522                  else if( Style_Type_Flag == TYPE_S )
8667  131a a102          	cp	a,#2
8668  131c 260d          	jrne	L5154
8669                     ; 1524                    DELAY_SET_TYPE = 1;
8671  131e 721c0018      	bset	_Disp_Flag1,#6
8672                     ; 1525                    if( Knob2_count > Herbal)
8674  1322 c60050        	ld	a,_Knob2_count
8675  1325 2704          	jreq	L5154
8676                     ; 1526                      Knob2_count--;
8678  1327 725a0050      	dec	_Knob2_count
8679                     ; 1530       default:break;
8681  132b               L5154:
8682                     ; 1533 }
8685  132b 84            	pop	a
8686  132c 81            	ret	
8800                     	switch	.bss
8801  0003               _TEST_STYLE:
8802  0003 00            	ds.b	1
8803                     	xdef	_TEST_STYLE
8804  0004               _TEST_SIZE:
8805  0004 00            	ds.b	1
8806                     	xdef	_TEST_SIZE
8807                     	xdef	_KeyHandler
8808                     	xdef	_KeyRepeat
8809                     	xdef	_KeyGenerate
8810                     	xdef	_Delay_Brew_Sure
8811                     	xdef	_Clock_Cnt
8812                     	xdef	_dimDelay10minute
8813                     	xdef	_Clock_Min_Total
8814                     	xdef	_Delay_Brew_Min_Total
8815                     	switch	.ubsct
8816  0000               _keyCopy:
8817  0000 00            	ds.b	1
8818                     	xdef	_keyCopy
8819  0001               _keyLast:
8820  0001 00            	ds.b	1
8821                     	xdef	_keyLast
8822  0002               _keyCode:
8823  0002 00            	ds.b	1
8824                     	xdef	_keyCode
8825  0003               _keyScan_old:
8826  0003 0000          	ds.b	2
8827                     	xdef	_keyScan_old
8828  0005               _keyScan:
8829  0005 0000          	ds.b	2
8830                     	xdef	_keyScan
8831  0007               _keyData:
8832  0007 0000          	ds.b	2
8833                     	xdef	_keyData
8834  0009               _keyTime:
8835  0009 0000          	ds.b	2
8836                     	xdef	_keyTime
8837                     	xref	_BeepStart
8838                     	xref	_Cancel_Sound
8839                     	xref	_Complete_Sound
8840                     	xref	_Reminder_Sound
8841                     	xref	_Confirm_Sound
8842                     	xref	_errorCode0
8843                     	xref	_CLean_Blink_10count
8844                     	xref	_Ctrl_Board_Status
8845                     	xref	_Pt696xRd
8846                     	xdef	_Style_Type_Rotary_Code
8847                     	xdef	_Size_Rotary_Code
8848                     	xdef	_Key_Deal
8849                     	xdef	_Key_Scan
8850                     	xdef	_Key_Init
8851                     	xdef	_Time_Deal
8852                     	switch	.bss
8853  0005               _KEY_Buff:
8854  0005 000000000000  	ds.b	16
8855                     	xdef	_KEY_Buff
8856  0015               _Key_Value:
8857  0015 0000          	ds.b	2
8858                     	xdef	_Key_Value
8859  0017               _Sys_Flag1:
8860  0017 00            	ds.b	1
8861                     	xdef	_Sys_Flag1
8862  0018               _Disp_Flag1:
8863  0018 00            	ds.b	1
8864                     	xdef	_Disp_Flag1
8865  0019               _Disp_Flag:
8866  0019 00            	ds.b	1
8867                     	xdef	_Disp_Flag
8868  001a               _Time_Flag:
8869  001a 00            	ds.b	1
8870                     	xdef	_Time_Flag
8871  001b               _debugValueEnable:
8872  001b 00            	ds.b	1
8873                     	xdef	_debugValueEnable
8874  001c               _debugIndex:
8875  001c 00            	ds.b	1
8876                     	xdef	_debugIndex
8877  001d               _debugEnable:
8878  001d 00            	ds.b	1
8879                     	xdef	_debugEnable
8880  001e               _forcePump:
8881  001e 00            	ds.b	1
8882                     	xdef	_forcePump
8883  001f               _cleanStart:
8884  001f 00            	ds.b	1
8885                     	xdef	_cleanStart
8886  0020               _cleanAlarm:
8887  0020 00            	ds.b	1
8888                     	xdef	_cleanAlarm
8889  0021               _delayExitCnt:
8890  0021 00            	ds.b	1
8891                     	xdef	_delayExitCnt
8892  0022               _delayNoaction3S:
8893  0022 00            	ds.b	1
8894                     	xdef	_delayNoaction3S
8895  0023               _delayAdjust15S:
8896  0023 00            	ds.b	1
8897                     	xdef	_delayAdjust15S
8898  0024               _delayBrewPause:
8899  0024 0000          	ds.b	2
8900                     	xdef	_delayBrewPause
8901  0026               _brewExitCnt:
8902  0026 00            	ds.b	1
8903                     	xdef	_brewExitCnt
8904  0027               _brewNoaction3S:
8905  0027 00            	ds.b	1
8906                     	xdef	_brewNoaction3S
8907  0028               _brewAdjust15S:
8908  0028 00            	ds.b	1
8909                     	xdef	_brewAdjust15S
8910  0029               _Pause_Sound_Delay:
8911  0029 00            	ds.b	1
8912                     	xdef	_Pause_Sound_Delay
8913  002a               _Complete_Sound_Delay:
8914  002a 00            	ds.b	1
8915                     	xdef	_Complete_Sound_Delay
8916  002b               _CANCEL_DELAY_BREW_3S:
8917  002b 00            	ds.b	1
8918                     	xdef	_CANCEL_DELAY_BREW_3S
8919  002c               _Delay_Brew_Time:
8920  002c 00            	ds.b	1
8921                     	xdef	_Delay_Brew_Time
8922  002d               _SET_Keep_Warm_Time:
8923  002d 00            	ds.b	1
8924                     	xdef	_SET_Keep_Warm_Time
8925  002e               _Delay_Keep_Warm_Level:
8926  002e 00            	ds.b	1
8927                     	xdef	_Delay_Keep_Warm_Level
8928  002f               _Delay_Keep_Warm_Time:
8929  002f 00            	ds.b	1
8930                     	xdef	_Delay_Keep_Warm_Time
8931  0030               _Standby_Timer:
8932  0030 0000          	ds.b	2
8933                     	xdef	_Standby_Timer
8934  0032               _Clock_Stay_Time:
8935  0032 00            	ds.b	1
8936                     	xdef	_Clock_Stay_Time
8937  0033               _Clock_Set_Time:
8938  0033 00            	ds.b	1
8939                     	xdef	_Clock_Set_Time
8940  0034               _Paused_Delay:
8941  0034 0000          	ds.b	2
8942                     	xdef	_Paused_Delay
8943  0036               _Style_Type_Flag:
8944  0036 00            	ds.b	1
8945                     	xdef	_Style_Type_Flag
8946  0037               _Keep_Warm_Class:
8947  0037 00            	ds.b	1
8948                     	xdef	_Keep_Warm_Class
8949  0038               _Brew_Select_B:
8950  0038 00            	ds.b	1
8951                     	xdef	_Brew_Select_B
8952  0039               _Brew_Select:
8953  0039 00            	ds.b	1
8954                     	xdef	_Brew_Select
8955  003a               _Start_Status:
8956  003a 00            	ds.b	1
8957                     	xdef	_Start_Status
8958  003b               _System_Status:
8959  003b 00            	ds.b	1
8960                     	xdef	_System_Status
8961  003c               _Clock_Keep_Warm:
8962  003c 0000000000    	ds.b	5
8963                     	xdef	_Clock_Keep_Warm
8964  0041               _Clock_Clean:
8965  0041 0000000000    	ds.b	5
8966                     	xdef	_Clock_Clean
8967  0046               _Clock_DLY_BREW:
8968  0046 0000000000    	ds.b	5
8969                     	xdef	_Clock_DLY_BREW
8970  004b               _Clock:
8971  004b 0000000000    	ds.b	5
8972                     	xdef	_Clock
8973  0050               _Knob2_count:
8974  0050 00            	ds.b	1
8975                     	xdef	_Knob2_count
8976  0051               _Knob1_count:
8977  0051 000000        	ds.b	3
8978                     	xdef	_Knob1_count
8979  0054               _Knob0_count:
8980  0054 000000        	ds.b	3
8981                     	xdef	_Knob0_count
8982                     	xref	_memset
8983                     	xref.b	c_x
9003                     	end

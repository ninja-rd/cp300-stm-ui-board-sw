   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   3                     ; Optimizer V4.2.4 - 18 Dec 2007
3051                     	bsct
3052  0000               L5012_ledBreathe:
3053  0000 00            	dc.b	0
3054                     .const:	section	.text
3055  0000               _digCode:
3056  0000 3f            	dc.b	63
3057  0001 06            	dc.b	6
3058  0002 5b            	dc.b	91
3059  0003 4f            	dc.b	79
3060  0004 66            	dc.b	102
3061  0005 6d            	dc.b	109
3062  0006 7d            	dc.b	125
3063  0007 07            	dc.b	7
3064  0008 7f            	dc.b	127
3065  0009 6f            	dc.b	111
3066  000a 77            	dc.b	119
3067  000b 7c            	dc.b	124
3068  000c 39            	dc.b	57
3069  000d 5e            	dc.b	94
3070  000e 79            	dc.b	121
3071  000f 71            	dc.b	113
3105                     ; 28 void DisplayDriver( void )
3105                     ; 29   {
3107                     	switch	.text
3108  0000               _DisplayDriver:
3112                     ; 30       Pt696xCmd( C_PT_DIG4);
3114  0000 4f            	clr	a
3115  0001 cd0000        	call	_Pt696xCmd
3117                     ; 31       Pt696xWr( 0, ledMap, sizeof( ledMap ) );
3119  0004 4b08          	push	#8
3120  0006 ae0023        	ldw	x,#_ledMap
3121  0009 89            	pushw	x
3122  000a 4f            	clr	a
3123  000b cd0000        	call	_Pt696xWr
3125  000e 5b03          	addw	sp,#3
3126                     ; 32       if( System_Status == ACTIVE )
3128  0010 c60000        	ld	a,_System_Status
3129  0013 a102          	cp	a,#2
3130  0015 2604          	jrne	L5212
3131                     ; 33         Pt696xCmd( C_PT_ON7);
3133  0017 a68f          	ld	a,#143
3136  0019 2002          	jra	L7212
3137  001b               L5212:
3138                     ; 35         Pt696xCmd( C_PT_ON1);
3140  001b a689          	ld	a,#137
3142  001d               L7212:
3143                     ; 36   }
3146  001d cc0000        	jp	_Pt696xCmd
3187                     ; 41 void hour_min( U8 hour,U8 min )
3187                     ; 42   {
3188                     	switch	.text
3189  0020               _hour_min:
3191  0020 89            	pushw	x
3192       00000000      OFST:	set	0
3195                     ; 43     if( hour < 10)
3197  0021 9e            	ld	a,xh
3198  0022 a10a          	cp	a,#10
3199  0024 2404          	jruge	L7412
3200                     ; 44       digImage[ 3 ] = 0;
3202  0026 3f03          	clr	_digImage+3
3204  0028 200e          	jra	L1512
3205  002a               L7412:
3206                     ; 46       digImage[ 3 ] = digCode[ hour / 10 ];
3208  002a 7b01          	ld	a,(OFST+1,sp)
3209  002c 5f            	clrw	x
3210  002d 97            	ld	xl,a
3211  002e a60a          	ld	a,#10
3212  0030 cd0000        	call	c_sdivx
3214  0033 d60000        	ld	a,(_digCode,x)
3215  0036 b703          	ld	_digImage+3,a
3216  0038               L1512:
3217                     ; 47     digImage[ 2 ] = digCode[ hour % 10 ];
3219  0038 7b01          	ld	a,(OFST+1,sp)
3220  003a 5f            	clrw	x
3221  003b 97            	ld	xl,a
3222  003c a60a          	ld	a,#10
3223  003e cd0000        	call	c_smodx
3225  0041 d60000        	ld	a,(_digCode,x)
3226  0044 b702          	ld	_digImage+2,a
3227                     ; 49     digImage[ 1 ] = digCode[ min / 10 ];
3229  0046 7b02          	ld	a,(OFST+2,sp)
3230  0048 5f            	clrw	x
3231  0049 97            	ld	xl,a
3232  004a a60a          	ld	a,#10
3233  004c cd0000        	call	c_sdivx
3235  004f d60000        	ld	a,(_digCode,x)
3236  0052 b701          	ld	_digImage+1,a
3237                     ; 50     min = min % 10;
3239  0054 7b02          	ld	a,(OFST+2,sp)
3240  0056 ae000a        	ldw	x,#10
3241  0059 9093          	ldw	y,x
3242  005b 5f            	clrw	x
3243  005c 97            	ld	xl,a
3244  005d 65            	divw	x,y
3245  005e 909f          	ld	a,yl
3246                     ; 51     digImage[ 0 ] = digCode[ min ];
3248  0060 5f            	clrw	x
3249  0061 97            	ld	xl,a
3250  0062 d60000        	ld	a,(_digCode,x)
3251  0065 b700          	ld	_digImage,a
3252                     ; 52   }
3255  0067 85            	popw	x
3256  0068 81            	ret	
3286                     ; 54 void Clock_Disp(void)
3286                     ; 55 {
3287                     	switch	.text
3288  0069               _Clock_Disp:
3292                     ; 56   if( Clock_Set_Time )
3294  0069 c60000        	ld	a,_Clock_Set_Time
3295  006c 2721          	jreq	L3612
3296                     ; 58     if( _500ms_flag  || Clock_Stay_Time)
3298  006e 7200000005    	btjt	_Time_Flag,#0,L7612
3300  0073 c60000        	ld	a,_Clock_Stay_Time
3301  0076 2738          	jreq	L5712
3302  0078               L7612:
3303                     ; 60       hour_min(Clock.hour_b,Clock.min_b);
3305  0078 c60003        	ld	a,_Clock+3
3306  007b 97            	ld	xl,a
3307  007c c60004        	ld	a,_Clock+4
3308  007f 95            	ld	xh,a
3309  0080 ad9e          	call	_hour_min
3311                     ; 61       BSETEX( ledMap, LM_2DP);
3313  0082 721a0027      	bset	_ledMap+4,#5
3314                     ; 62       if( AM_PM_FLAG )
3316  0086 7201000002    	btjf	_Disp_Flag,#0,L1712
3317                     ; 63        BSETEX( ledMap, LM_PM);
3319  008b 201a          	jp	LC002
3320  008d               L1712:
3321                     ; 65        BSETEX( ledMap, LM_AM);
3322  008d 201d          	jp	L1022
3323  008f               L3612:
3324                     ; 70       hour_min(Clock.hour,Clock.min);
3326  008f c60001        	ld	a,_Clock+1
3327  0092 97            	ld	xl,a
3328  0093 c60002        	ld	a,_Clock+2
3329  0096 95            	ld	xh,a
3330  0097 ad87          	call	_hour_min
3332                     ; 71       if( _500ms_flag )
3334  0099 7201000004    	btjf	_Time_Flag,#0,L7712
3335                     ; 73         BSETEX( ledMap, LM_2DP);
3337  009e 721a0027      	bset	_ledMap+4,#5
3338  00a2               L7712:
3339                     ; 75       if( AM_PM_FLAG )
3341  00a2 7201000005    	btjf	_Disp_Flag,#0,L1022
3342                     ; 76        BSETEX( ledMap, LM_PM);
3344  00a7               LC002:
3346  00a7 721a0025      	bset	_ledMap+2,#5
3349  00ab 81            	ret	
3350  00ac               L1022:
3351                     ; 78        BSETEX( ledMap, LM_AM);
3354  00ac 721a0023      	bset	_ledMap,#5
3355  00b0               L5712:
3356                     ; 80 }
3359  00b0 81            	ret	
3384                     ; 307 void Digit_Figure( void )
3384                     ; 308 {
3385                     	switch	.text
3386  00b1               _Digit_Figure:
3390                     ; 309       if ( BTST( digImage[ 2 ], 0 ) )  BSETEX( ledMap, LM_3A );
3392  00b1 7201000204    	btjf	_digImage+2,#0,L5122
3395  00b6 721a0026      	bset	_ledMap+3,#5
3396  00ba               L5122:
3397                     ; 310       if ( BTST( digImage[ 2 ], 1 ) )  BSETEX( ledMap, LM_3B );
3399  00ba 7203000204    	btjf	_digImage+2,#1,L7122
3402  00bf 72180026      	bset	_ledMap+3,#4
3403  00c3               L7122:
3404                     ; 311       if ( BTST( digImage[ 2 ], 2 ) )  BSETEX( ledMap, LM_3C );
3406  00c3 7205000204    	btjf	_digImage+2,#2,L1222
3409  00c8 72160026      	bset	_ledMap+3,#3
3410  00cc               L1222:
3411                     ; 312       if ( BTST( digImage[ 2 ], 3 ) )  BSETEX( ledMap, LM_3D );
3413  00cc 7207000204    	btjf	_digImage+2,#3,L3222
3416  00d1 72120026      	bset	_ledMap+3,#1
3417  00d5               L3222:
3418                     ; 313       if ( BTST( digImage[ 2 ], 4 ) )  BSETEX( ledMap, LM_3E );
3420  00d5 7209000204    	btjf	_digImage+2,#4,L5222
3423  00da 72100026      	bset	_ledMap+3,#0
3424  00de               L5222:
3425                     ; 314       if ( BTST( digImage[ 2 ], 5 ) )  BSETEX( ledMap, LM_3F );  
3427  00de 720b000204    	btjf	_digImage+2,#5,L7222
3430  00e3 721e0025      	bset	_ledMap+2,#7
3431  00e7               L7222:
3432                     ; 315       if ( BTST( digImage[ 2 ], 6 ) )  BSETEX( ledMap, LM_3G );
3434  00e7 720d000204    	btjf	_digImage+2,#6,L1322
3437  00ec 721c0025      	bset	_ledMap+2,#6
3438  00f0               L1322:
3439                     ; 316       if ( BTST( digImage[ 1 ], 0 ) )  BSETEX( ledMap, LM_2A );
3441  00f0 7201000104    	btjf	_digImage+1,#0,L3322
3444  00f5 721a0028      	bset	_ledMap+5,#5
3445  00f9               L3322:
3446                     ; 317       if ( BTST( digImage[ 1 ], 1 ) )  BSETEX( ledMap, LM_2B );
3448  00f9 7203000104    	btjf	_digImage+1,#1,L5322
3451  00fe 72180028      	bset	_ledMap+5,#4
3452  0102               L5322:
3453                     ; 318       if ( BTST( digImage[ 1 ], 2 ) )  BSETEX( ledMap, LM_2C );
3455  0102 7205000104    	btjf	_digImage+1,#2,L7322
3458  0107 72160028      	bset	_ledMap+5,#3
3459  010b               L7322:
3460                     ; 319       if ( BTST( digImage[ 1 ], 3 ) )  BSETEX( ledMap, LM_2D );
3462  010b 7207000104    	btjf	_digImage+1,#3,L1422
3465  0110 72120028      	bset	_ledMap+5,#1
3466  0114               L1422:
3467                     ; 320       if ( BTST( digImage[ 1 ], 4 ) )  BSETEX( ledMap, LM_2E );
3469  0114 7209000104    	btjf	_digImage+1,#4,L3422
3472  0119 72100028      	bset	_ledMap+5,#0
3473  011d               L3422:
3474                     ; 321       if ( BTST( digImage[ 1 ], 5 ) )  BSETEX( ledMap, LM_2F );  
3476  011d 720b000104    	btjf	_digImage+1,#5,L5422
3479  0122 721e0027      	bset	_ledMap+4,#7
3480  0126               L5422:
3481                     ; 322       if ( BTST( digImage[ 1 ], 6 ) )  BSETEX( ledMap, LM_2G );
3483  0126 720d000104    	btjf	_digImage+1,#6,L7422
3486  012b 721c0027      	bset	_ledMap+4,#6
3487  012f               L7422:
3488                     ; 323       if ( BTST( digImage[ 0 ], 0 ) )  BSETEX( ledMap, LM_1A );
3490  012f 7201000004    	btjf	_digImage,#0,L1522
3493  0134 721a002a      	bset	_ledMap+7,#5
3494  0138               L1522:
3495                     ; 324       if ( BTST( digImage[ 0 ], 1 ) )  BSETEX( ledMap, LM_1B );
3497  0138 7203000004    	btjf	_digImage,#1,L3522
3500  013d 7218002a      	bset	_ledMap+7,#4
3501  0141               L3522:
3502                     ; 325       if ( BTST( digImage[ 0 ], 2 ) )  BSETEX( ledMap, LM_1C );
3504  0141 7205000004    	btjf	_digImage,#2,L5522
3507  0146 7216002a      	bset	_ledMap+7,#3
3508  014a               L5522:
3509                     ; 326       if ( BTST( digImage[ 0 ], 3 ) )  BSETEX( ledMap, LM_1D );
3511  014a 7207000004    	btjf	_digImage,#3,L7522
3514  014f 7212002a      	bset	_ledMap+7,#1
3515  0153               L7522:
3516                     ; 327       if ( BTST( digImage[ 0 ], 4 ) )  BSETEX( ledMap, LM_1E );
3518  0153 7209000004    	btjf	_digImage,#4,L1622
3521  0158 7210002a      	bset	_ledMap+7,#0
3522  015c               L1622:
3523                     ; 328       if ( BTST( digImage[ 0 ], 5 ) )  BSETEX( ledMap, LM_1F );
3525  015c 720b000004    	btjf	_digImage,#5,L3622
3528  0161 721e0029      	bset	_ledMap+6,#7
3529  0165               L3622:
3530                     ; 329       if ( BTST( digImage[ 0 ], 6 ) )  BSETEX( ledMap, LM_1G );
3532  0165 720d000004    	btjf	_digImage,#6,L5622
3535  016a 721c0029      	bset	_ledMap+6,#6
3536  016e               L5622:
3537                     ; 330       if ( BTST( digImage[ 3 ], 0 ) )  BSETEX( ledMap, LM_4A );
3539  016e 7201000304    	btjf	_digImage+3,#0,L7622
3542  0173 721a0024      	bset	_ledMap+1,#5
3543  0177               L7622:
3544                     ; 331       if ( BTST( digImage[ 3 ], 1 ) )  BSETEX( ledMap, LM_4B );
3546  0177 7203000304    	btjf	_digImage+3,#1,L1722
3549  017c 72180024      	bset	_ledMap+1,#4
3550  0180               L1722:
3551                     ; 332       if ( BTST( digImage[ 3 ], 2 ) )  BSETEX( ledMap, LM_4C );
3553  0180 7205000304    	btjf	_digImage+3,#2,L3722
3556  0185 72160024      	bset	_ledMap+1,#3
3557  0189               L3722:
3558                     ; 333       if ( BTST( digImage[ 3 ], 3 ) )  BSETEX( ledMap, LM_4D );
3560  0189 7207000304    	btjf	_digImage+3,#3,L5722
3563  018e 72120024      	bset	_ledMap+1,#1
3564  0192               L5722:
3565                     ; 334       if ( BTST( digImage[ 3 ], 4 ) )  BSETEX( ledMap, LM_4E );
3567  0192 7209000304    	btjf	_digImage+3,#4,L7722
3570  0197 72100024      	bset	_ledMap+1,#0
3571  019b               L7722:
3572                     ; 335       if ( BTST( digImage[ 3 ], 5 ) )  BSETEX( ledMap, LM_4F );
3574  019b 720b000304    	btjf	_digImage+3,#5,L1032
3577  01a0 721e0023      	bset	_ledMap,#7
3578  01a4               L1032:
3579                     ; 336       if ( BTST( digImage[ 3 ], 6 ) )  BSETEX( ledMap, LM_4G );
3581  01a4 720d000304    	btjf	_digImage+3,#6,L3032
3584  01a9 721c0023      	bset	_ledMap,#6
3585  01ad               L3032:
3586                     ; 337 }
3589  01ad 81            	ret	
3628                     ; 467 void Tea_Coffee_Dim_Disp( U8 dim )
3628                     ; 468 {
3629                     	switch	.text
3630  01ae               _Tea_Coffee_Dim_Disp:
3634                     ; 470   Led_Cup        = dim;
3636  01ae c7000a        	ld	_Led_Cup,a
3637                     ; 471   Led_Xlcup      = dim;
3639  01b1 c7000c        	ld	_Led_Xlcup,a
3640                     ; 472   Led_Travel     = dim;
3642  01b4 c70000        	ld	_Led_Travel,a
3643                     ; 473   Led_Xltravel   = dim;
3645  01b7 c7000d        	ld	_Led_Xltravel,a
3646                     ; 474   Led_Halfcarafe = dim;
3648  01ba c7000b        	ld	_Led_Halfcarafe,a
3649                     ; 475   Led_Fullcarafe = dim;
3651  01bd c70009        	ld	_Led_Fullcarafe,a
3652                     ; 476 }
3655  01c0 81            	ret	
3694                     ; 478 void Coffee_Style_Dim_Disp( U8 dim )
3694                     ; 479 {
3695                     	switch	.text
3696  01c1               _Coffee_Style_Dim_Disp:
3700                     ; 480   Led_Classic    = dim;
3702  01c1 c70014        	ld	_Led_Classic,a
3703                     ; 481   Led_Rich       = dim;
3705  01c4 c70012        	ld	_Led_Rich,a
3706                     ; 482   Led_Overice    = dim;
3708  01c7 c70010        	ld	_Led_Overice,a
3709                     ; 483   Led_Coldbrew   = dim;
3711  01ca c7000e        	ld	_Led_Coldbrew,a
3712                     ; 484   Led_Speciality = dim;
3714  01cd c7001a        	ld	_Led_Speciality,a
3715                     ; 485   Led_Immersion  = dim;
3717  01d0 c70018        	ld	_Led_Immersion,a
3718                     ; 486 }
3721  01d3 81            	ret	
3768                     ; 488 void Tea_Style_Type_Dim_Disp( U8 dim )
3768                     ; 489 {
3769                     	switch	.text
3770  01d4               _Tea_Style_Type_Dim_Disp:
3772  01d4 88            	push	a
3773       00000000      OFST:	set	0
3776                     ; 492   Led_Style       = dim;
3778  01d5 c70016        	ld	_Led_Style,a
3779                     ; 493   Led_Type        = dim;
3781  01d8 c70022        	ld	_Led_Type,a
3782                     ; 494   if( Style_Type_Flag == 1 )
3784  01db c60000        	ld	a,_Style_Type_Flag
3785  01de a101          	cp	a,#1
3786  01e0 2625          	jrne	L1532
3787                     ; 496     if( dim == DIM10)
3789  01e2 7b01          	ld	a,(OFST+1,sp)
3790  01e4 a10a          	cp	a,#10
3791  01e6 2606          	jrne	L3532
3792                     ; 497       Led_Style = LED_ON;
3794  01e8 35640016      	mov	_Led_Style,#100
3796  01ec 2008          	jra	L5532
3797  01ee               L3532:
3798                     ; 498     else if( dim == DIM)
3800  01ee a105          	cp	a,#5
3801  01f0 2604          	jrne	L5532
3802                     ; 499       Led_Style = DIM10;
3804  01f2 350a0016      	mov	_Led_Style,#10
3805  01f6               L5532:
3806                     ; 500     Led_Classic    = dim;
3808  01f6 c70014        	ld	_Led_Classic,a
3809                     ; 501     Led_Rich       = dim;
3811  01f9 c70012        	ld	_Led_Rich,a
3812                     ; 502     Led_Overice    = dim;
3814  01fc c70010        	ld	_Led_Overice,a
3815                     ; 503     Led_Coldbrew   = dim;
3817  01ff c7000e        	ld	_Led_Coldbrew,a
3818                     ; 504     Led_Speciality = dim;
3820  0202 c7001a        	ld	_Led_Speciality,a
3822  0205 202a          	jra	L1632
3823  0207               L1532:
3824                     ; 507   else if( 2 == Style_Type_Flag )
3826  0207 a102          	cp	a,#2
3827  0209 2626          	jrne	L1632
3828                     ; 510     if( dim == DIM10)
3830  020b 7b01          	ld	a,(OFST+1,sp)
3831  020d a10a          	cp	a,#10
3832  020f 2606          	jrne	L5632
3833                     ; 511       Led_Type  = LED_ON;
3835  0211 35640022      	mov	_Led_Type,#100
3837  0215 2008          	jra	L7632
3838  0217               L5632:
3839                     ; 512     else if( dim == DIM)
3841  0217 a105          	cp	a,#5
3842  0219 2604          	jrne	L7632
3843                     ; 513       Led_Type  = DIM10;
3845  021b 350a0022      	mov	_Led_Type,#10
3846  021f               L7632:
3847                     ; 514     Led_Herbal      = dim;
3849  021f c70013        	ld	_Led_Herbal,a
3850                     ; 515     Led_Black       = dim;
3852  0222 c70011        	ld	_Led_Black,a
3853                     ; 516     Led_Oolong      = dim;
3855  0225 c7000f        	ld	_Led_Oolong,a
3856                     ; 517     Led_White       = dim;
3858  0228 c7001b        	ld	_Led_White,a
3859                     ; 518     Led_Green       = dim;
3861  022b c70019        	ld	_Led_Green,a
3862                     ; 519     Led_Delicate    = dim;
3864  022e c70017        	ld	_Led_Delicate,a
3865  0231               L1632:
3866                     ; 521 }
3869  0231 84            	pop	a
3870  0232 81            	ret	
3917                     ; 523 void DelayBrew_Tea_Dim_Disp( U8 dim )
3917                     ; 524 {
3918                     	switch	.text
3919  0233               _DelayBrew_Tea_Dim_Disp:
3921  0233 88            	push	a
3922       00000000      OFST:	set	0
3925                     ; 525   Led_Style       = dim;
3927  0234 c70016        	ld	_Led_Style,a
3928                     ; 526   Led_Type        = dim;
3930  0237 c70022        	ld	_Led_Type,a
3931                     ; 527   Led_Classic     = dim;
3933  023a c70014        	ld	_Led_Classic,a
3934                     ; 528   Led_Rich        = dim;
3936  023d c70012        	ld	_Led_Rich,a
3937                     ; 529   Led_Overice     = dim;
3939  0240 c70010        	ld	_Led_Overice,a
3940                     ; 530   Led_Coldbrew    = dim;
3942  0243 c7000e        	ld	_Led_Coldbrew,a
3943                     ; 531   Led_Speciality  = dim;
3945  0246 c7001a        	ld	_Led_Speciality,a
3946                     ; 533   Led_Herbal      = dim;
3948  0249 c70013        	ld	_Led_Herbal,a
3949                     ; 534   Led_Black       = dim;
3951  024c c70011        	ld	_Led_Black,a
3952                     ; 535   Led_Oolong      = dim;
3954  024f c7000f        	ld	_Led_Oolong,a
3955                     ; 536   Led_White       = dim;
3957  0252 c7001b        	ld	_Led_White,a
3958                     ; 537   Led_Green       = dim;
3960  0255 c70019        	ld	_Led_Green,a
3961                     ; 538   Led_Delicate    = dim;
3963  0258 c70017        	ld	_Led_Delicate,a
3964                     ; 539   if( Style_Type_Flag == 1 )
3966  025b c60000        	ld	a,_Style_Type_Flag
3967  025e a101          	cp	a,#1
3968  0260 2616          	jrne	L7042
3969                     ; 541     if( dim == DIM10)
3971  0262 7b01          	ld	a,(OFST+1,sp)
3972  0264 a10a          	cp	a,#10
3973  0266 2606          	jrne	L1142
3974                     ; 542       Led_Style = LED_ON;
3976  0268 35640016      	mov	_Led_Style,#100
3978  026c 2022          	jra	L7142
3979  026e               L1142:
3980                     ; 543     else if( dim == DIM)
3982  026e a105          	cp	a,#5
3983  0270 261e          	jrne	L7142
3984                     ; 544       Led_Style = DIM10;
3986  0272 350a0016      	mov	_Led_Style,#10
3987  0276 2018          	jra	L7142
3988  0278               L7042:
3989                     ; 546   else if( 2 == Style_Type_Flag )
3991  0278 a102          	cp	a,#2
3992  027a 2614          	jrne	L7142
3993                     ; 548     if( dim == DIM10)
3995  027c 7b01          	ld	a,(OFST+1,sp)
3996  027e a10a          	cp	a,#10
3997  0280 2606          	jrne	L3242
3998                     ; 549       Led_Type  = LED_ON;
4000  0282 35640022      	mov	_Led_Type,#100
4002  0286 2008          	jra	L7142
4003  0288               L3242:
4004                     ; 550     else if( dim == DIM)
4006  0288 a105          	cp	a,#5
4007  028a 2604          	jrne	L7142
4008                     ; 551       Led_Type  = DIM10;
4010  028c 350a0022      	mov	_Led_Type,#10
4011  0290               L7142:
4012                     ; 553 }
4015  0290 84            	pop	a
4016  0291 81            	ret	
4064                     ; 555 void Tea_Coffee_Same_Disp( U8 dim )
4064                     ; 556 {
4065                     	switch	.text
4066  0292               _Tea_Coffee_Same_Disp:
4068  0292 88            	push	a
4069       00000000      OFST:	set	0
4072                     ; 557   switch ( Knob1_count [ Brew_Select ]  )
4074  0293 c60000        	ld	a,_Brew_Select
4075  0296 5f            	clrw	x
4076  0297 97            	ld	xl,a
4077  0298 d60000        	ld	a,(_Knob1_count,x)
4079                     ; 565      default:break;
4080  029b 2711          	jreq	L1342
4081  029d 4a            	dec	a
4082  029e 2715          	jreq	L3342
4083  02a0 4a            	dec	a
4084  02a1 2719          	jreq	L5342
4085  02a3 4a            	dec	a
4086  02a4 271d          	jreq	L7342
4087  02a6 4a            	dec	a
4088  02a7 2721          	jreq	L1442
4089  02a9 4a            	dec	a
4090  02aa 2725          	jreq	L3442
4091  02ac 2028          	jra	L3052
4092  02ae               L1342:
4093                     ; 559      case Classic:    Led_Classic     = dim;break;
4095  02ae 7b01          	ld	a,(OFST+1,sp)
4096  02b0 c70014        	ld	_Led_Classic,a
4099  02b3 2021          	jra	L3052
4100  02b5               L3342:
4101                     ; 560      case Rich:       Led_Rich        = dim;break;
4103  02b5 7b01          	ld	a,(OFST+1,sp)
4104  02b7 c70012        	ld	_Led_Rich,a
4107  02ba 201a          	jra	L3052
4108  02bc               L5342:
4109                     ; 561      case Overice:    Led_Overice     = dim;break;
4111  02bc 7b01          	ld	a,(OFST+1,sp)
4112  02be c70010        	ld	_Led_Overice,a
4115  02c1 2013          	jra	L3052
4116  02c3               L7342:
4117                     ; 562      case Coldbrew:   Led_Coldbrew    = dim;break;
4119  02c3 7b01          	ld	a,(OFST+1,sp)
4120  02c5 c7000e        	ld	_Led_Coldbrew,a
4123  02c8 200c          	jra	L3052
4124  02ca               L1442:
4125                     ; 563      case Speciality: Led_Speciality  = dim;break;
4127  02ca 7b01          	ld	a,(OFST+1,sp)
4128  02cc c7001a        	ld	_Led_Speciality,a
4131  02cf 2005          	jra	L3052
4132  02d1               L3442:
4133                     ; 564      case Immersion:  Led_Immersion   = dim;break;
4135  02d1 7b01          	ld	a,(OFST+1,sp)
4136  02d3 c70018        	ld	_Led_Immersion,a
4139                     ; 565      default:break;
4141  02d6               L3052:
4142                     ; 567   if( Speciality == Knob1_count [ Brew_Select ]  
4142                     ; 568     || Immersion == Knob1_count [ Brew_Select ]  )
4144  02d6 c60000        	ld	a,_Brew_Select
4145  02d9 5f            	clrw	x
4146  02da 97            	ld	xl,a
4147  02db d60000        	ld	a,(_Knob1_count,x)
4148  02de a104          	cp	a,#4
4149  02e0 270c          	jreq	L7052
4151  02e2 c60000        	ld	a,_Brew_Select
4152  02e5 5f            	clrw	x
4153  02e6 97            	ld	xl,a
4154  02e7 d60000        	ld	a,(_Knob1_count,x)
4155  02ea a105          	cp	a,#5
4156  02ec 261a          	jrne	L5052
4157  02ee               L7052:
4158                     ; 572     Led_Cup        = LED_OFF;
4160  02ee 725f000a      	clr	_Led_Cup
4161                     ; 573     Led_Xlcup      = LED_OFF;
4163  02f2 725f000c      	clr	_Led_Xlcup
4164                     ; 574     Led_Travel     = LED_OFF;
4166  02f6 725f0000      	clr	_Led_Travel
4167                     ; 575     Led_Xltravel   = LED_OFF;
4169  02fa 725f000d      	clr	_Led_Xltravel
4170                     ; 576     Led_Halfcarafe = LED_OFF;
4172  02fe 725f000b      	clr	_Led_Halfcarafe
4173                     ; 577     Led_Fullcarafe = LED_OFF;
4175  0302 725f0009      	clr	_Led_Fullcarafe
4177  0306               L1152:
4178                     ; 592 }
4181  0306 84            	pop	a
4182  0307 81            	ret	
4183  0308               L5052:
4184                     ; 581     switch ( Knob0_count [ Brew_Select ]  )
4186  0308 c60000        	ld	a,_Brew_Select
4187  030b 5f            	clrw	x
4188  030c 97            	ld	xl,a
4189  030d d60000        	ld	a,(_Knob0_count,x)
4191                     ; 589        default:break;
4192  0310 2711          	jreq	L7442
4193  0312 4a            	dec	a
4194  0313 2715          	jreq	L1542
4195  0315 4a            	dec	a
4196  0316 2719          	jreq	L3542
4197  0318 4a            	dec	a
4198  0319 271d          	jreq	L5542
4199  031b 4a            	dec	a
4200  031c 2721          	jreq	L7542
4201  031e 4a            	dec	a
4202  031f 2725          	jreq	L1642
4203  0321 20e3          	jra	L1152
4204  0323               L7442:
4205                     ; 583        case Cup:        Led_Cup = dim;break;
4207  0323 7b01          	ld	a,(OFST+1,sp)
4208  0325 c7000a        	ld	_Led_Cup,a
4211  0328 20dc          	jra	L1152
4212  032a               L1542:
4213                     ; 584        case Xlcup:      Led_Xlcup = dim;break;
4215  032a 7b01          	ld	a,(OFST+1,sp)
4216  032c c7000c        	ld	_Led_Xlcup,a
4219  032f 20d5          	jra	L1152
4220  0331               L3542:
4221                     ; 585        case Travel:     Led_Travel = dim;break;
4223  0331 7b01          	ld	a,(OFST+1,sp)
4224  0333 c70000        	ld	_Led_Travel,a
4227  0336 20ce          	jra	L1152
4228  0338               L5542:
4229                     ; 586        case Xltrael:    Led_Xltravel = dim;break;
4231  0338 7b01          	ld	a,(OFST+1,sp)
4232  033a c7000d        	ld	_Led_Xltravel,a
4235  033d 20c7          	jra	L1152
4236  033f               L7542:
4237                     ; 587        case Halfcarafe: Led_Halfcarafe = dim;break;
4239  033f 7b01          	ld	a,(OFST+1,sp)
4240  0341 c7000b        	ld	_Led_Halfcarafe,a
4243  0344 20c0          	jra	L1152
4244  0346               L1642:
4245                     ; 588        case fullcarafe: Led_Fullcarafe = dim;break;
4247  0346 7b01          	ld	a,(OFST+1,sp)
4248  0348 c70009        	ld	_Led_Fullcarafe,a
4251  034b 20b9          	jra	L1152
4252                     ; 589        default:break;
4286                     ; 593 void Coffee_Disp( U8 dim )
4286                     ; 594 {
4287                     	switch	.text
4288  034d               _Coffee_Disp:
4292                     ; 595   Led_Coffee = dim;
4294  034d c70001        	ld	_Led_Coffee,a
4295                     ; 599 }
4298  0350 81            	ret	
4338                     ; 601 void Tea_Disp( U8 dim )
4338                     ; 602 { 
4339                     	switch	.text
4340  0351               _Tea_Disp:
4342  0351 88            	push	a
4343       00000000      OFST:	set	0
4346                     ; 603    Led_Tea = dim;
4348  0352 c70002        	ld	_Led_Tea,a
4349                     ; 604    switch ( Knob2_count )
4351  0355 c60000        	ld	a,_Knob2_count
4353                     ; 612        default:break;
4354  0358 2711          	jreq	L3352
4355  035a 4a            	dec	a
4356  035b 2715          	jreq	L5352
4357  035d 4a            	dec	a
4358  035e 2719          	jreq	L7352
4359  0360 4a            	dec	a
4360  0361 271d          	jreq	L1452
4361  0363 4a            	dec	a
4362  0364 2721          	jreq	L3452
4363  0366 4a            	dec	a
4364  0367 2725          	jreq	L5452
4365  0369 2028          	jra	L7652
4366  036b               L3352:
4367                     ; 606        case Herbal:   Led_Herbal   = dim;break;
4369  036b 7b01          	ld	a,(OFST+1,sp)
4370  036d c70013        	ld	_Led_Herbal,a
4373  0370 2021          	jra	L7652
4374  0372               L5352:
4375                     ; 607        case Black:    Led_Black    = dim;break;
4377  0372 7b01          	ld	a,(OFST+1,sp)
4378  0374 c70011        	ld	_Led_Black,a
4381  0377 201a          	jra	L7652
4382  0379               L7352:
4383                     ; 608        case Oolong:   Led_Oolong   = dim;break;
4385  0379 7b01          	ld	a,(OFST+1,sp)
4386  037b c7000f        	ld	_Led_Oolong,a
4389  037e 2013          	jra	L7652
4390  0380               L1452:
4391                     ; 609        case White:    Led_White    = dim;break;
4393  0380 7b01          	ld	a,(OFST+1,sp)
4394  0382 c7001b        	ld	_Led_White,a
4397  0385 200c          	jra	L7652
4398  0387               L3452:
4399                     ; 610        case Green:    Led_Green    = dim;break;
4401  0387 7b01          	ld	a,(OFST+1,sp)
4402  0389 c70019        	ld	_Led_Green,a
4405  038c 2005          	jra	L7652
4406  038e               L5452:
4407                     ; 611        case Delicate: Led_Delicate = dim;break;
4409  038e 7b01          	ld	a,(OFST+1,sp)
4410  0390 c70017        	ld	_Led_Delicate,a
4413                     ; 612        default:break;
4415  0393               L7652:
4416                     ; 614 }
4419  0393 84            	pop	a
4420  0394 81            	ret	
4459                     ; 616 void Tea_Type_Disp( U8 dim)
4459                     ; 617 {
4460                     	switch	.text
4461  0395               _Tea_Type_Disp:
4463  0395 88            	push	a
4464       00000000      OFST:	set	0
4467                     ; 618  switch ( Knob2_count )
4469  0396 c60000        	ld	a,_Knob2_count
4471                     ; 626      default:break;
4472  0399 2711          	jreq	L1752
4473  039b 4a            	dec	a
4474  039c 2715          	jreq	L3752
4475  039e 4a            	dec	a
4476  039f 2719          	jreq	L5752
4477  03a1 4a            	dec	a
4478  03a2 271d          	jreq	L7752
4479  03a4 4a            	dec	a
4480  03a5 2721          	jreq	L1062
4481  03a7 4a            	dec	a
4482  03a8 2725          	jreq	L3062
4483  03aa 2028          	jra	L5262
4484  03ac               L1752:
4485                     ; 620      case Herbal:   Led_Herbal   = dim;break;
4487  03ac 7b01          	ld	a,(OFST+1,sp)
4488  03ae c70013        	ld	_Led_Herbal,a
4491  03b1 2021          	jra	L5262
4492  03b3               L3752:
4493                     ; 621      case Black:    Led_Black    = dim;break;
4495  03b3 7b01          	ld	a,(OFST+1,sp)
4496  03b5 c70011        	ld	_Led_Black,a
4499  03b8 201a          	jra	L5262
4500  03ba               L5752:
4501                     ; 622      case Oolong:   Led_Oolong   = dim;break;
4503  03ba 7b01          	ld	a,(OFST+1,sp)
4504  03bc c7000f        	ld	_Led_Oolong,a
4507  03bf 2013          	jra	L5262
4508  03c1               L7752:
4509                     ; 623      case White:    Led_White    = dim;break;
4511  03c1 7b01          	ld	a,(OFST+1,sp)
4512  03c3 c7001b        	ld	_Led_White,a
4515  03c6 200c          	jra	L5262
4516  03c8               L1062:
4517                     ; 624      case Green:    Led_Green    = dim;break;
4519  03c8 7b01          	ld	a,(OFST+1,sp)
4520  03ca c70019        	ld	_Led_Green,a
4523  03cd 2005          	jra	L5262
4524  03cf               L3062:
4525                     ; 625      case Delicate: Led_Delicate = dim;break;
4527  03cf 7b01          	ld	a,(OFST+1,sp)
4528  03d1 c70017        	ld	_Led_Delicate,a
4531                     ; 626      default:break;
4533  03d4               L5262:
4534                     ; 628 }
4537  03d4 84            	pop	a
4538  03d5 81            	ret	
4562                     ; 629 void Coffee_DIM_Disp( void )
4562                     ; 630 {
4563                     	switch	.text
4564  03d6               _Coffee_DIM_Disp:
4568                     ; 631   Led_Coffee = DIM;
4570  03d6 35050001      	mov	_Led_Coffee,#5
4571                     ; 635 }
4574  03da 81            	ret	
4605                     ; 637 void Tea_DIM_Disp( void )
4605                     ; 638 { 
4606                     	switch	.text
4607  03db               _Tea_DIM_Disp:
4611                     ; 639    Led_Tea = DIM;
4613  03db 35050002      	mov	_Led_Tea,#5
4614                     ; 640    switch ( Knob2_count )
4616  03df c60000        	ld	a,_Knob2_count
4618                     ; 648        default:break;
4619  03e2 2710          	jreq	L7362
4620  03e4 4a            	dec	a
4621  03e5 2712          	jreq	L1462
4622  03e7 4a            	dec	a
4623  03e8 2714          	jreq	L3462
4624  03ea 4a            	dec	a
4625  03eb 2716          	jreq	L5462
4626  03ed 4a            	dec	a
4627  03ee 2718          	jreq	L7462
4628  03f0 4a            	dec	a
4629  03f1 271a          	jreq	L1562
4631  03f3 81            	ret	
4632  03f4               L7362:
4633                     ; 642        case Herbal:   Led_Herbal   = DIM10;break;
4635  03f4 350a0013      	mov	_Led_Herbal,#10
4639  03f8 81            	ret	
4640  03f9               L1462:
4641                     ; 643        case Black:    Led_Black    = DIM10;break;
4643  03f9 350a0011      	mov	_Led_Black,#10
4647  03fd 81            	ret	
4648  03fe               L3462:
4649                     ; 644        case Oolong:   Led_Oolong   = DIM10;break;
4651  03fe 350a000f      	mov	_Led_Oolong,#10
4655  0402 81            	ret	
4656  0403               L5462:
4657                     ; 645        case White:    Led_White    = DIM10;break;
4659  0403 350a001b      	mov	_Led_White,#10
4663  0407 81            	ret	
4664  0408               L7462:
4665                     ; 646        case Green:    Led_Green    = DIM10;break;
4667  0408 350a0019      	mov	_Led_Green,#10
4671  040c 81            	ret	
4672  040d               L1562:
4673                     ; 647        case Delicate: Led_Delicate = DIM10;break;
4675  040d 350a0017      	mov	_Led_Delicate,#10
4678                     ; 648        default:break;
4680                     ; 650 }
4683  0411 81            	ret	
4724                     ; 652 void CLean_Disp_Standby( void )
4724                     ; 653 {
4725                     	switch	.text
4726  0412               _CLean_Disp_Standby:
4730                     ; 654      if( CLEAN_DISP_FLAG )
4732  0412 720100002e    	btjf	_Disp_Flag1,#0,L5172
4733                     ; 656        if( _500ms_flag )
4735  0417 7201000029    	btjf	_Time_Flag,#0,L5172
4736                     ; 658           hour_min(Clock_Clean.min,Clock_Clean.second);
4738  041c c60000        	ld	a,_Clock_Clean
4739  041f 97            	ld	xl,a
4740  0420 c60001        	ld	a,_Clock_Clean+1
4741  0423 95            	ld	xh,a
4742  0424 cd0020        	call	_hour_min
4744                     ; 659           BSETEX( ledMap, LM_2DP); //
4746  0427 721a0027      	bset	_ledMap+4,#5
4747                     ; 660           Led_Fullcarafe = LED_ON;
4749  042b 35640009      	mov	_Led_Fullcarafe,#100
4750                     ; 661           Led_Start      = LED_ON;
4752  042f 35640015      	mov	_Led_Start,#100
4753                     ; 662           if( CLean_Class == 0 || CLean_Class == 1 )
4755  0433 c60000        	ld	a,_CLean_Class
4756  0436 2703          	jreq	L3272
4758  0438 4a            	dec	a
4759  0439 2606          	jrne	L1272
4760  043b               L3272:
4761                     ; 663             Led_Clean2  = LED_ON;
4763  043b 35640004      	mov	_Led_Clean2,#100
4765  043f 2004          	jra	L5172
4766  0441               L1272:
4767                     ; 665             Led_Clean = LED_ON;
4769  0441 35640021      	mov	_Led_Clean,#100
4770  0445               L5172:
4771                     ; 668      if( cleanAlarm )
4773  0445 c60000        	ld	a,_cleanAlarm
4774  0448 2603cc04d2    	jreq	L7272
4775                     ; 671        if( _500ms_flag )
4777  044d 7201000016    	btjf	_Time_Flag,#0,L1372
4778                     ; 673           Led_Fullcarafe = LED_ON;
4780  0452 35640009      	mov	_Led_Fullcarafe,#100
4781                     ; 674           if( CLean_Class == 0 || CLean_Class == 1 )
4783  0456 c60000        	ld	a,_CLean_Class
4784  0459 2703          	jreq	L5372
4786  045b 4a            	dec	a
4787  045c 2606          	jrne	L3372
4788  045e               L5372:
4789                     ; 675             Led_Clean2  = LED_ON;
4791  045e 35640004      	mov	_Led_Clean2,#100
4793  0462 2004          	jra	L1372
4794  0464               L3372:
4795                     ; 677             Led_Clean = LED_ON;
4797  0464 35640021      	mov	_Led_Clean,#100
4798  0468               L1372:
4799                     ; 679        switch ( Knob0_count [ Brew_Select ]  )
4801  0468 c60000        	ld	a,_Brew_Select
4802  046b 5f            	clrw	x
4803  046c 97            	ld	xl,a
4804  046d d60000        	ld	a,(_Knob0_count,x)
4806                     ; 711            default:break;
4807  0470 270d          	jreq	L1762
4808  0472 4a            	dec	a
4809  0473 2714          	jreq	L3762
4810  0475 4a            	dec	a
4811  0476 271f          	jreq	L5762
4812  0478 4a            	dec	a
4813  0479 272e          	jreq	L7762
4814  047b 4a            	dec	a
4815  047c 2740          	jreq	L1072
4817  047e 81            	ret	
4818  047f               L1762:
4819                     ; 681            case Cup:        Led_Cup = LED_ON;
4821  047f 3564000a      	mov	_Led_Cup,#100
4822                     ; 682                             Led_Xlcup = DIM10;
4824  0483 350a000c      	mov	_Led_Xlcup,#10
4825                     ; 683                             Led_Travel = DIM10;
4826                     ; 684                             Led_Xltravel = DIM10;
4827                     ; 685                             Led_Halfcarafe = DIM10;
4828                     ; 686                             break;
4830  0487 2008          	jp	LC005
4831  0489               L3762:
4832                     ; 687            case Xlcup:      Led_Cup = DIM10;
4834  0489 350a000a      	mov	_Led_Cup,#10
4835                     ; 688                             Led_Xlcup = LED_ON;
4837  048d 3564000c      	mov	_Led_Xlcup,#100
4838                     ; 689                             Led_Travel = DIM10;
4840  0491               LC005:
4842  0491 350a0000      	mov	_Led_Travel,#10
4843                     ; 690                             Led_Xltravel = DIM10;
4844                     ; 691                             Led_Halfcarafe = DIM10;
4845                     ; 692                             break;
4847  0495 200c          	jp	LC004
4848  0497               L5762:
4849                     ; 693            case Travel:     Led_Cup = DIM10;
4851  0497 350a000a      	mov	_Led_Cup,#10
4852                     ; 694                             Led_Xlcup = DIM10;
4854  049b 350a000c      	mov	_Led_Xlcup,#10
4855                     ; 695                             Led_Travel = LED_ON;
4857  049f 35640000      	mov	_Led_Travel,#100
4858                     ; 696                             Led_Xltravel = DIM10;
4860  04a3               LC004:
4863  04a3 350a000d      	mov	_Led_Xltravel,#10
4864                     ; 697                             Led_Halfcarafe = DIM10;
4865                     ; 698                             break;
4867  04a7 2010          	jp	LC003
4868  04a9               L7762:
4869                     ; 699            case Xltrael:    Led_Cup = DIM10;
4871  04a9 350a000a      	mov	_Led_Cup,#10
4872                     ; 700                             Led_Xlcup = DIM10;
4874  04ad 350a000c      	mov	_Led_Xlcup,#10
4875                     ; 701                             Led_Travel = DIM10;
4877  04b1 350a0000      	mov	_Led_Travel,#10
4878                     ; 702                             Led_Xltravel = LED_ON;
4880  04b5 3564000d      	mov	_Led_Xltravel,#100
4881                     ; 703                             Led_Halfcarafe = DIM10;
4883  04b9               LC003:
4887  04b9 350a000b      	mov	_Led_Halfcarafe,#10
4888                     ; 704                             break;
4891  04bd 81            	ret	
4892  04be               L1072:
4893                     ; 705            case Halfcarafe: Led_Cup = DIM10;
4895  04be 350a000a      	mov	_Led_Cup,#10
4896                     ; 706                             Led_Xlcup = DIM10;
4898  04c2 350a000c      	mov	_Led_Xlcup,#10
4899                     ; 707                             Led_Travel = DIM10;
4901  04c6 350a0000      	mov	_Led_Travel,#10
4902                     ; 708                             Led_Xltravel = DIM10;
4904  04ca 350a000d      	mov	_Led_Xltravel,#10
4905                     ; 709                             Led_Halfcarafe = LED_ON;
4907  04ce 3564000b      	mov	_Led_Halfcarafe,#100
4908                     ; 710                             break;
4910                     ; 711            default:break;
4912  04d2               L7272:
4913                     ; 715 }
4916  04d2 81            	ret	
4964                     ; 716 void Pause_Clock_Disp( U16 pauseclock )
4964                     ; 717 {
4965                     	switch	.text
4966  04d3               _Pause_Clock_Disp:
4968  04d3 89            	pushw	x
4969  04d4 89            	pushw	x
4970       00000002      OFST:	set	2
4973                     ; 718       U8 tempM = (U8 )(pauseclock/60);
4975  04d5 a63c          	ld	a,#60
4976  04d7 62            	div	x,a
4977  04d8 9f            	ld	a,xl
4978  04d9 6b01          	ld	(OFST-1,sp),a
4979                     ; 719       U8 tempS = (U8 )(pauseclock%60);
4981  04db 1e03          	ldw	x,(OFST+1,sp)
4982  04dd a63c          	ld	a,#60
4983  04df 62            	div	x,a
4984  04e0 6b02          	ld	(OFST+0,sp),a
4985                     ; 720       hour_min(tempM,tempS);
4987  04e2 97            	ld	xl,a
4988  04e3 7b01          	ld	a,(OFST-1,sp)
4989  04e5 95            	ld	xh,a
4990  04e6 cd0020        	call	_hour_min
4992                     ; 721       BSETEX( ledMap, LM_2DP); //
4994  04e9 721a0027      	bset	_ledMap+4,#5
4995                     ; 722 }
4998  04ed 5b04          	addw	sp,#4
4999  04ef 81            	ret	
5031                     ; 724 void CLean_Disp_Run( void )
5031                     ; 725 {
5032                     	switch	.text
5033  04f0               _CLean_Disp_Run:
5037                     ; 727       if( Paused_Delay )
5039  04f0 ce0000        	ldw	x,_Paused_Delay
5040  04f3 2704          	jreq	L5772
5041                     ; 729         Pause_Clock_Disp( Paused_Delay );
5043  04f5 addc          	call	_Pause_Clock_Disp
5046  04f7 200f          	jra	L7772
5047  04f9               L5772:
5048                     ; 733         hour_min(Clock_Clean.min,Clock_Clean.second);
5050  04f9 c60000        	ld	a,_Clock_Clean
5051  04fc 97            	ld	xl,a
5052  04fd c60001        	ld	a,_Clock_Clean+1
5053  0500 95            	ld	xh,a
5054  0501 cd0020        	call	_hour_min
5056                     ; 734         BSETEX( ledMap, LM_2DP); //
5058  0504 721a0027      	bset	_ledMap+4,#5
5059  0508               L7772:
5060                     ; 736       Led_Fullcarafe = LED_ON;
5062  0508 35640009      	mov	_Led_Fullcarafe,#100
5063                     ; 737       if( CLean_Class == 0 || CLean_Class == 1 )
5065  050c c60000        	ld	a,_CLean_Class
5066  050f 2703          	jreq	L3003
5068  0511 4a            	dec	a
5069  0512 2605          	jrne	L1003
5070  0514               L3003:
5071                     ; 738           Led_Clean2  = LED_ON;
5073  0514 35640004      	mov	_Led_Clean2,#100
5075                     ; 741 }
5078  0518 81            	ret	
5079  0519               L1003:
5080                     ; 740           Led_Clean = LED_ON;
5082  0519 35640021      	mov	_Led_Clean,#100
5084  051d 81            	ret	
5113                     ; 743 void Stay_Warm_Disp( void )
5113                     ; 744 {
5114                     	switch	.text
5115  051e               _Stay_Warm_Disp:
5119                     ; 745   if( Brew_Select == NO_BASKET )
5121  051e c60000        	ld	a,_Brew_Select
5122  0521 260f          	jrne	L7103
5123                     ; 747     if( KEEP_WARM_FUNC )
5125  0523 7203000002    	btjf	_Disp_Flag,#1,L1203
5126                     ; 748           Led_Keepwarm = LED_ON;
5128  0528 2035          	jp	LC008
5129  052a               L1203:
5130                     ; 751          if( System_Status == DIM_STATUS )
5132  052a c60000        	ld	a,_System_Status
5133  052d 4a            	dec	a
5134  052e 263f          	jrne	L5403
5135                     ; 752           Led_Keepwarm = DIM;
5137  0530 2038          	jp	LC007
5138                     ; 754           Led_Keepwarm = DIM10;
5139  0532               L7103:
5140                     ; 759       if((Halfcarafe == Knob0_count [ Brew_Select ]  || fullcarafe == Knob0_count [ Brew_Select ]  )
5140                     ; 760                        && (Classic == Knob1_count [ Brew_Select ]  || Rich == Knob1_count [ Brew_Select ] ))
5142  0532 5f            	clrw	x
5143  0533 97            	ld	xl,a
5144  0534 d60000        	ld	a,(_Knob0_count,x)
5145  0537 a104          	cp	a,#4
5146  0539 270c          	jreq	L5303
5148  053b c60000        	ld	a,_Brew_Select
5149  053e 5f            	clrw	x
5150  053f 97            	ld	xl,a
5151  0540 d60000        	ld	a,(_Knob0_count,x)
5152  0543 a105          	cp	a,#5
5153  0545 262d          	jrne	L3303
5154  0547               L5303:
5156  0547 c60000        	ld	a,_Brew_Select
5157  054a 5f            	clrw	x
5158  054b 97            	ld	xl,a
5159  054c 724d0000      	tnz	(_Knob1_count,x)
5160  0550 2708          	jreq	L7303
5162  0552 5f            	clrw	x
5163  0553 97            	ld	xl,a
5164  0554 d60000        	ld	a,(_Knob1_count,x)
5165  0557 4a            	dec	a
5166  0558 261a          	jrne	L3303
5167  055a               L7303:
5168                     ; 763           if( KEEP_WARM_FUNC )
5170  055a 7203000005    	btjf	_Disp_Flag,#1,L1403
5171                     ; 764               Led_Keepwarm = LED_ON;
5173  055f               LC008:
5175  055f 35640003      	mov	_Led_Keepwarm,#100
5178  0563 81            	ret	
5179  0564               L1403:
5180                     ; 767              if( System_Status == DIM_STATUS )
5182  0564 c60000        	ld	a,_System_Status
5183  0567 4a            	dec	a
5184  0568 2605          	jrne	L5403
5185                     ; 768               Led_Keepwarm = DIM;
5187  056a               LC007:
5189  056a 35050003      	mov	_Led_Keepwarm,#5
5192  056e 81            	ret	
5193  056f               L5403:
5194                     ; 770               Led_Keepwarm = DIM10;
5197  056f 350a0003      	mov	_Led_Keepwarm,#10
5198                     ; 778 }
5201  0573 81            	ret	
5202  0574               L3303:
5203                     ; 775         KEEP_WARM_FUNC = 0;// any change to setting which disable stay warm will switch stay warm off
5205  0574 72130000      	bres	_Disp_Flag,#1
5207  0578 81            	ret	
5237                     ; 779 void Keep_Warm_Clock_Disp(void)
5237                     ; 780 {
5238                     	switch	.text
5239  0579               _Keep_Warm_Clock_Disp:
5243                     ; 781     if( _500ms_flag  || Clock_Stay_Time)
5245  0579 7200000005    	btjt	_Time_Flag,#0,L5603
5247  057e c60000        	ld	a,_Clock_Stay_Time
5248  0581 270f          	jreq	L3603
5249  0583               L5603:
5250                     ; 783       hour_min(Clock_Keep_Warm.hour,Clock_Keep_Warm.min);
5252  0583 c60001        	ld	a,_Clock_Keep_Warm+1
5253  0586 97            	ld	xl,a
5254  0587 c60002        	ld	a,_Clock_Keep_Warm+2
5255  058a 95            	ld	xh,a
5256  058b cd0020        	call	_hour_min
5258                     ; 784       BSETEX( ledMap, LM_2DP); //
5260  058e 721a0027      	bset	_ledMap+4,#5
5261  0592               L3603:
5262                     ; 786     if( _500ms_flag )// ��˸
5264  0592 7201000004    	btjf	_Time_Flag,#0,L7603
5265                     ; 787      Led_Keepwarm = LED_ON;// STAY WARM_LED
5267  0597 35640003      	mov	_Led_Keepwarm,#100
5268  059b               L7603:
5269                     ; 788 }
5272  059b 81            	ret	
5300                     ; 789 void Keep_Warm_Level_Disp(void)
5300                     ; 790 {
5301                     	switch	.text
5302  059c               _Keep_Warm_Level_Disp:
5306                     ; 791      if( _500ms_flag )
5308  059c 7201000032    	btjf	_Time_Flag,#0,L1013
5309                     ; 793         Led_Keepwarm = LED_ON;
5311  05a1 35640003      	mov	_Led_Keepwarm,#100
5312                     ; 794         if( Keep_Warm_Class == WARM_HI)
5314  05a5 c60000        	ld	a,_Keep_Warm_Class
5315  05a8 a102          	cp	a,#2
5316  05aa 260a          	jrne	L3013
5317                     ; 796           digImage[ 3 ] = C_CODE_h;
5319  05ac 35740003      	mov	_digImage+3,#116
5320                     ; 797           digImage[ 2 ] = SEGC;
5322  05b0 35040002      	mov	_digImage+2,#4
5323                     ; 798           digImage[ 1 ] = 0;
5324                     ; 799           digImage[ 0 ] = 0;
5326  05b4 200b          	jp	LC010
5327  05b6               L3013:
5328                     ; 801         else if(  Keep_Warm_Class == WARM_LO )
5330  05b6 4a            	dec	a
5331  05b7 260c          	jrne	L7013
5332                     ; 803           digImage[ 3 ] = C_CODE_L;
5334  05b9 35380003      	mov	_digImage+3,#56
5335                     ; 804           digImage[ 2 ] = C_CODE_o;
5337  05bd 355c0002      	mov	_digImage+2,#92
5338                     ; 805           digImage[ 1 ] = 0;
5340  05c1               LC010:
5342  05c1 3f01          	clr	_digImage+1
5343                     ; 806           digImage[ 0 ] = 0;
5345  05c3 200c          	jp	LC009
5346  05c5               L7013:
5347                     ; 810           digImage[ 3 ] = C_CODE_o;
5349  05c5 355c0003      	mov	_digImage+3,#92
5350                     ; 811           digImage[ 2 ] = C_CODE_F;
5352  05c9 35710002      	mov	_digImage+2,#113
5353                     ; 812           digImage[ 1 ] = C_CODE_F;
5355  05cd 35710001      	mov	_digImage+1,#113
5356                     ; 813           digImage[ 0 ] = 0;
5358  05d1               LC009:
5361  05d1 3f00          	clr	_digImage
5362  05d3               L1013:
5363                     ; 816 }
5366  05d3 81            	ret	
5428                     ; 818 void Delay_Brew_Clock_Disp( void )
5428                     ; 819 {
5429                     	switch	.text
5430  05d4               _Delay_Brew_Clock_Disp:
5434                     ; 820     Tea_Coffee_Dim_Disp( DIM10 );
5436  05d4 a60a          	ld	a,#10
5437  05d6 cd01ae        	call	_Tea_Coffee_Dim_Disp
5439                     ; 821     if( COFFEE == Brew_Select )
5441  05d9 c60000        	ld	a,_Brew_Select
5442  05dc a101          	cp	a,#1
5443  05de 260a          	jrne	L1233
5444                     ; 823       Led_Coffee = DIM10;
5446  05e0 a60a          	ld	a,#10
5447  05e2 c70001        	ld	_Led_Coffee,a
5448                     ; 824       Coffee_Style_Dim_Disp( DIM10 );
5450  05e5 cd01c1        	call	_Coffee_Style_Dim_Disp
5453  05e8 200c          	jra	L3233
5454  05ea               L1233:
5455                     ; 826     else if( TEA == Brew_Select )
5457  05ea a102          	cp	a,#2
5458  05ec 2608          	jrne	L3233
5459                     ; 828       Led_Tea = DIM10;
5461  05ee a60a          	ld	a,#10
5462  05f0 c70002        	ld	_Led_Tea,a
5463                     ; 830       DelayBrew_Tea_Dim_Disp( DIM10);
5465  05f3 cd0233        	call	_DelayBrew_Tea_Dim_Disp
5467  05f6               L3233:
5468                     ; 832     if( _500ms_flag  || Clock_Stay_Time)
5470  05f6 7200000005    	btjt	_Time_Flag,#0,L1333
5472  05fb c60000        	ld	a,_Clock_Stay_Time
5473  05fe 271e          	jreq	L7233
5474  0600               L1333:
5475                     ; 834       hour_min(Clock_DLY_BREW.hour_b,Clock_DLY_BREW.min_b);
5477  0600 c60003        	ld	a,_Clock_DLY_BREW+3
5478  0603 97            	ld	xl,a
5479  0604 c60004        	ld	a,_Clock_DLY_BREW+4
5480  0607 95            	ld	xh,a
5481  0608 cd0020        	call	_hour_min
5483                     ; 835       BSETEX( ledMap, LM_2DP); //
5485  060b 721a0027      	bset	_ledMap+4,#5
5486                     ; 836       if( DELAY_AM_PM_FLAG )
5488  060f 720d000006    	btjf	_Disp_Flag,#6,L3333
5489                     ; 837         BSETEX( ledMap, LM_PM);
5491  0614 721a0025      	bset	_ledMap+2,#5
5493  0618 2004          	jra	L7233
5494  061a               L3333:
5495                     ; 839        BSETEX( ledMap, LM_AM);
5497  061a 721a0023      	bset	_ledMap,#5
5498  061e               L7233:
5499                     ; 841     if( _500ms_flag )
5501  061e 7201000004    	btjf	_Time_Flag,#0,L7333
5502                     ; 842       Led_Delayset = LED_ON;
5504  0623 35640005      	mov	_Led_Delayset,#100
5505  0627               L7333:
5506                     ; 843     if( DELAY_SET_STYLE == 0 )
5508  0627 c60000        	ld	a,_Brew_Select
5509  062a 5f            	clrw	x
5510  062b 97            	ld	xl,a
5511  062c d60000        	ld	a,(_Knob1_count,x)
5512  062f 720a000064    	btjt	_Disp_Flag1,#5,L1433
5513                     ; 845       if( _500ms_flag )
5515  0634 720100002e    	btjf	_Time_Flag,#0,L3433
5516                     ; 847           switch ( Knob1_count [ Brew_Select ]  )
5519                     ; 855              default:break;
5520  0639 2770          	jreq	L7413
5521  063b 4a            	dec	a
5522  063c 2711          	jreq	L5113
5523  063e 4a            	dec	a
5524  063f 2714          	jreq	L7113
5525  0641 4a            	dec	a
5526  0642 2717          	jreq	L1213
5527  0644 4a            	dec	a
5528  0645 2603cc06e3    	jreq	L7513
5529  064a 4a            	dec	a
5530  064b 2714          	jreq	L5213
5531  064d 2060          	jra	L7533
5532                     ; 849              case Classic:    Led_Classic     = LED_ON;break;
5535  064f               L5113:
5536                     ; 850              case Rich:       Led_Rich        = LED_ON;break;
5538  064f 35640012      	mov	_Led_Rich,#100
5541  0653 205a          	jra	L7533
5542  0655               L7113:
5543                     ; 851              case Overice:    Led_Overice     = LED_ON;break;
5545  0655 35640010      	mov	_Led_Overice,#100
5548  0659 2054          	jra	L7533
5549  065b               L1213:
5550                     ; 852              case Coldbrew:   Led_Coldbrew    = LED_ON;break;
5552  065b 3564000e      	mov	_Led_Coldbrew,#100
5555  065f 204e          	jra	L7533
5556                     ; 853              case Speciality: Led_Speciality  = LED_ON;break;
5559  0661               L5213:
5560                     ; 854              case Immersion:  Led_Immersion   = LED_ON;break;
5562  0661 35640018      	mov	_Led_Immersion,#100
5565  0665 2048          	jra	L7533
5566                     ; 855              default:break;
5569  0667               L3433:
5570                     ; 860           switch ( Knob1_count [ Brew_Select ]  )
5573                     ; 868              default:break;
5574  0667 2711          	jreq	L1313
5575  0669 4a            	dec	a
5576  066a 2713          	jreq	L3313
5577  066c 4a            	dec	a
5578  066d 2715          	jreq	L5313
5579  066f 4a            	dec	a
5580  0670 2717          	jreq	L7313
5581  0672 4a            	dec	a
5582  0673 2719          	jreq	L1413
5583  0675 4a            	dec	a
5584  0676 271b          	jreq	L3413
5585  0678 2035          	jra	L7533
5586  067a               L1313:
5587                     ; 862              case Classic:    Led_Classic     = LED_OFF;break;
5589  067a c70014        	ld	_Led_Classic,a
5592  067d 2030          	jra	L7533
5593  067f               L3313:
5594                     ; 863              case Rich:       Led_Rich        = LED_OFF;break;
5596  067f c70012        	ld	_Led_Rich,a
5599  0682 202b          	jra	L7533
5600  0684               L5313:
5601                     ; 864              case Overice:    Led_Overice     = LED_OFF;break;
5603  0684 c70010        	ld	_Led_Overice,a
5606  0687 2026          	jra	L7533
5607  0689               L7313:
5608                     ; 865              case Coldbrew:   Led_Coldbrew    = LED_OFF;break;
5610  0689 c7000e        	ld	_Led_Coldbrew,a
5613  068c 2021          	jra	L7533
5614  068e               L1413:
5615                     ; 866              case Speciality: Led_Speciality  = LED_OFF;break;
5617  068e c7001a        	ld	_Led_Speciality,a
5620  0691 201c          	jra	L7533
5621  0693               L3413:
5622                     ; 867              case Immersion:  Led_Immersion   = LED_OFF;break;
5624  0693 c70018        	ld	_Led_Immersion,a
5627  0696 2017          	jra	L7533
5628                     ; 868              default:break;
5630  0698               L1433:
5631                     ; 874           switch ( Knob1_count [ Brew_Select ]  )
5634                     ; 882              default:break;
5635  0698 2711          	jreq	L7413
5636  069a 4a            	dec	a
5637  069b 2734          	jreq	L1513
5638  069d 4a            	dec	a
5639  069e 2737          	jreq	L3513
5640  06a0 4a            	dec	a
5641  06a1 273a          	jreq	L5513
5642  06a3 4a            	dec	a
5643  06a4 273d          	jreq	L7513
5644  06a6 4a            	dec	a
5645  06a7 2740          	jreq	L1613
5646  06a9 2004          	jra	L7533
5647  06ab               L7413:
5648                     ; 876              case Classic:    Led_Classic= LED_ON;break;
5651  06ab 35640014      	mov	_Led_Classic,#100
5653  06af               L7533:
5654                     ; 886     if( DELAY_SET_SIZE == 0 )
5656  06af c60000        	ld	a,_Brew_Select
5657  06b2 5f            	clrw	x
5658  06b3 97            	ld	xl,a
5659  06b4 d60000        	ld	a,(_Knob1_count,x)
5660  06b7 a104          	cp	a,#4
5661  06b9 7209000003cc  	btjt	_Disp_Flag1,#4,L5633
5662                     ; 888       if( Speciality == Knob1_count [ Brew_Select ]  
5662                     ; 889         || Immersion == Knob1_count [ Brew_Select ]  )
5664  06c1 272c          	jreq	L1733
5666  06c3 c60000        	ld	a,_Brew_Select
5667  06c6 5f            	clrw	x
5668  06c7 97            	ld	xl,a
5669  06c8 d60000        	ld	a,(_Knob1_count,x)
5670  06cb a105          	cp	a,#5
5671  06cd 262d          	jrne	L7633
5672  06cf 201e          	jra	L1733
5673  06d1               L1513:
5674                     ; 877              case Rich:       Led_Rich= LED_ON;break;
5676  06d1 35640012      	mov	_Led_Rich,#100
5679  06d5 20d8          	jra	L7533
5680  06d7               L3513:
5681                     ; 878              case Overice:    Led_Overice= LED_ON;break;
5683  06d7 35640010      	mov	_Led_Overice,#100
5686  06db 20d2          	jra	L7533
5687  06dd               L5513:
5688                     ; 879              case Coldbrew:   Led_Coldbrew= LED_ON;break;
5690  06dd 3564000e      	mov	_Led_Coldbrew,#100
5693  06e1 20cc          	jra	L7533
5694  06e3               L7513:
5695                     ; 880              case Speciality: Led_Speciality= LED_ON;break;
5698  06e3 3564001a      	mov	_Led_Speciality,#100
5701  06e7 20c6          	jra	L7533
5702  06e9               L1613:
5703                     ; 881              case Immersion:  Led_Immersion= LED_ON;break;
5705  06e9 35640018      	mov	_Led_Immersion,#100
5708  06ed 20c0          	jra	L7533
5709                     ; 882              default:break;
5711  06ef               L1733:
5712                     ; 891         if( _500ms_flag )
5714  06ef 7201000002    	btjf	_Time_Flag,#0,L3733
5715                     ; 892           Led_Cup = LED_ON;
5717  06f4 207d          	jp	L1243
5718  06f6               L3733:
5719                     ; 894           Led_Cup = LED_OFF;
5721  06f6 725f000a      	clr	_Led_Cup
5722  06fa 207b          	jra	L5143
5723  06fc               L7633:
5724                     ; 898           if( _500ms_flag )
5726  06fc c60000        	ld	a,_Brew_Select
5727  06ff 5f            	clrw	x
5728  0700 97            	ld	xl,a
5729  0701 d60000        	ld	a,(_Knob0_count,x)
5730  0704 720100002b    	btjf	_Time_Flag,#0,L1043
5731                     ; 900             switch ( Knob0_count [ Brew_Select ]  )
5734                     ; 908                default:break;
5735  0709 2768          	jreq	L1243
5736  070b 4a            	dec	a
5737  070c 2714          	jreq	L7613
5738  070e 4a            	dec	a
5739  070f 2717          	jreq	L1713
5740  0711 4a            	dec	a
5741  0712 271a          	jreq	L3713
5742  0714 4a            	dec	a
5743  0715 2603cc07bb    	jreq	L1323
5744  071a 4a            	dec	a
5745  071b 2603cc07c1    	jreq	L3323
5746  0720 2055          	jra	L5143
5747                     ; 902                case Cup:        Led_Cup        = LED_ON;break;
5750  0722               L7613:
5751                     ; 903                case Xlcup:      Led_Xlcup      = LED_ON;break;
5753  0722 3564000c      	mov	_Led_Xlcup,#100
5756  0726 204f          	jra	L5143
5757  0728               L1713:
5758                     ; 904                case Travel:     Led_Travel     = LED_ON;break;
5760  0728 35640000      	mov	_Led_Travel,#100
5763  072c 2049          	jra	L5143
5764  072e               L3713:
5765                     ; 905                case Xltrael:    Led_Xltravel   = LED_ON;break;
5767  072e 3564000d      	mov	_Led_Xltravel,#100
5770  0732 2043          	jra	L5143
5771                     ; 906                case Halfcarafe: Led_Halfcarafe = LED_ON;break;
5774                     ; 907                case fullcarafe: Led_Fullcarafe = LED_ON;break;
5777                     ; 908                default:break;
5780  0734               L1043:
5781                     ; 913             switch ( Knob0_count [ Brew_Select ]  )
5784                     ; 921                default:break;
5785  0734 2711          	jreq	L3023
5786  0736 4a            	dec	a
5787  0737 2713          	jreq	L5023
5788  0739 4a            	dec	a
5789  073a 2715          	jreq	L7023
5790  073c 4a            	dec	a
5791  073d 2717          	jreq	L1123
5792  073f 4a            	dec	a
5793  0740 2719          	jreq	L3123
5794  0742 4a            	dec	a
5795  0743 271b          	jreq	L5123
5796  0745 2030          	jra	L5143
5797  0747               L3023:
5798                     ; 915                case Cup:        Led_Cup        = LED_OFF;break;
5800  0747 c7000a        	ld	_Led_Cup,a
5803  074a 202b          	jra	L5143
5804  074c               L5023:
5805                     ; 916                case Xlcup:      Led_Xlcup      = LED_OFF;break;
5807  074c c7000c        	ld	_Led_Xlcup,a
5810  074f 2026          	jra	L5143
5811  0751               L7023:
5812                     ; 917                case Travel:     Led_Travel     = LED_OFF;break;
5814  0751 c70000        	ld	_Led_Travel,a
5817  0754 2021          	jra	L5143
5818  0756               L1123:
5819                     ; 918                case Xltrael:    Led_Xltravel   = LED_OFF;break;
5821  0756 c7000d        	ld	_Led_Xltravel,a
5824  0759 201c          	jra	L5143
5825  075b               L3123:
5826                     ; 919                case Halfcarafe: Led_Halfcarafe = LED_OFF;break;
5828  075b c7000b        	ld	_Led_Halfcarafe,a
5831  075e 2017          	jra	L5143
5832  0760               L5123:
5833                     ; 920                case fullcarafe: Led_Fullcarafe = LED_OFF;break;
5835  0760 c70009        	ld	_Led_Fullcarafe,a
5838  0763 2012          	jra	L5143
5839                     ; 921                default:break;
5841  0765               L5633:
5842                     ; 928         if( Speciality == Knob1_count [ Brew_Select ]  
5842                     ; 929         || Immersion == Knob1_count [ Brew_Select ]  )
5844  0765 270c          	jreq	L1243
5846  0767 c60000        	ld	a,_Brew_Select
5847  076a 5f            	clrw	x
5848  076b 97            	ld	xl,a
5849  076c d60000        	ld	a,(_Knob1_count,x)
5850  076f a105          	cp	a,#5
5851  0771 261b          	jrne	L7143
5852  0773               L1243:
5853                     ; 931             Led_Cup = LED_ON;
5858  0773 3564000a      	mov	_Led_Cup,#100
5860  0777               L5143:
5861                     ; 948     if( Brew_Select == TEA )
5863  0777 c60000        	ld	a,_Brew_Select
5864  077a a102          	cp	a,#2
5865  077c 2703cc0857    	jrne	L1343
5866                     ; 950       if( Style_Type_Flag == 1 )
5868  0781 c60000        	ld	a,_Style_Type_Flag
5869  0784 a101          	cp	a,#1
5870  0786 263f          	jrne	L3343
5871                     ; 951         Led_Style = LED_ON;
5873  0788 35640016      	mov	_Led_Style,#100
5875  078c 2041          	jra	L5343
5876  078e               L7143:
5877                     ; 935             switch ( Knob0_count [ Brew_Select ]  )
5879  078e c60000        	ld	a,_Brew_Select
5880  0791 5f            	clrw	x
5881  0792 97            	ld	xl,a
5882  0793 d60000        	ld	a,(_Knob0_count,x)
5884                     ; 943                default:break;
5885  0796 27db          	jreq	L1243
5886  0798 4a            	dec	a
5887  0799 270e          	jreq	L3223
5888  079b 4a            	dec	a
5889  079c 2711          	jreq	L5223
5890  079e 4a            	dec	a
5891  079f 2714          	jreq	L7223
5892  07a1 4a            	dec	a
5893  07a2 2717          	jreq	L1323
5894  07a4 4a            	dec	a
5895  07a5 271a          	jreq	L3323
5896  07a7 20ce          	jra	L5143
5897                     ; 937                case Cup:        Led_Cup        = LED_ON;break;
5900  07a9               L3223:
5901                     ; 938                case Xlcup:      Led_Xlcup      = LED_ON;break;
5903  07a9 3564000c      	mov	_Led_Xlcup,#100
5906  07ad 20c8          	jra	L5143
5907  07af               L5223:
5908                     ; 939                case Travel:     Led_Travel     = LED_ON;break;
5910  07af 35640000      	mov	_Led_Travel,#100
5913  07b3 20c2          	jra	L5143
5914  07b5               L7223:
5915                     ; 940                case Xltrael:    Led_Xltravel   = LED_ON;break;
5917  07b5 3564000d      	mov	_Led_Xltravel,#100
5920  07b9 20bc          	jra	L5143
5921  07bb               L1323:
5922                     ; 941                case Halfcarafe: Led_Halfcarafe = LED_ON;break;
5925  07bb 3564000b      	mov	_Led_Halfcarafe,#100
5928  07bf 20b6          	jra	L5143
5929  07c1               L3323:
5930                     ; 942                case fullcarafe: Led_Fullcarafe = LED_ON;break;
5933  07c1 35640009      	mov	_Led_Fullcarafe,#100
5936  07c5 20b0          	jra	L5143
5937                     ; 943                default:break;
5939  07c7               L3343:
5940                     ; 952       else if( 2 == Style_Type_Flag )
5942  07c7 a102          	cp	a,#2
5943  07c9 2604          	jrne	L5343
5944                     ; 953         Led_Type = LED_ON;
5946  07cb 35640022      	mov	_Led_Type,#100
5947  07cf               L5343:
5948                     ; 954       if( DELAY_SET_TYPE == 0 )
5950  07cf c60000        	ld	a,_Knob2_count
5951  07d2 720c000050    	btjt	_Disp_Flag1,#6,L1443
5952                     ; 956         if( _500ms_flag )
5954  07d7 7201000021    	btjf	_Time_Flag,#0,L3443
5955                     ; 958           switch ( Knob2_count )
5958                     ; 966              default:break;
5959  07dc 275b          	jreq	L3723
5960  07de 4a            	dec	a
5961  07df 275d          	jreq	L5723
5962  07e1 4a            	dec	a
5963  07e2 270a          	jreq	L3423
5964  07e4 4a            	dec	a
5965  07e5 270c          	jreq	L5423
5966  07e7 4a            	dec	a
5967  07e8 2763          	jreq	L3033
5968  07ea 4a            	dec	a
5969  07eb 270b          	jreq	L1523
5971  07ed 81            	ret	
5972                     ; 960              case Herbal:   Led_Herbal   = LED_ON;break;
5975                     ; 961              case Black:    Led_Black    = LED_ON;break;
5978  07ee               L3423:
5979                     ; 962              case Oolong:   Led_Oolong   = LED_ON;break;
5981  07ee 3564000f      	mov	_Led_Oolong,#100
5985  07f2 81            	ret	
5986  07f3               L5423:
5987                     ; 963              case White:    Led_White    = LED_ON;break;
5989  07f3 3564001b      	mov	_Led_White,#100
5993  07f7 81            	ret	
5994                     ; 964              case Green:    Led_Green    = LED_ON;break;
5997  07f8               L1523:
5998                     ; 965              case Delicate: Led_Delicate = LED_ON;break;
6000  07f8 35640017      	mov	_Led_Delicate,#100
6004  07fc 81            	ret	
6005                     ; 966              default:break;
6008  07fd               L3443:
6009                     ; 971           switch ( Knob2_count )
6012                     ; 979              default:break;
6013  07fd 2710          	jreq	L5523
6014  07ff 4a            	dec	a
6015  0800 2711          	jreq	L7523
6016  0802 4a            	dec	a
6017  0803 2712          	jreq	L1623
6018  0805 4a            	dec	a
6019  0806 2713          	jreq	L3623
6020  0808 4a            	dec	a
6021  0809 2714          	jreq	L5623
6022  080b 4a            	dec	a
6023  080c 2715          	jreq	L7623
6025  080e 81            	ret	
6026  080f               L5523:
6027                     ; 973              case Herbal:   Led_Herbal   = LED_OFF;break;
6029  080f c70013        	ld	_Led_Herbal,a
6033  0812 81            	ret	
6034  0813               L7523:
6035                     ; 974              case Black:    Led_Black    = LED_OFF;break;
6037  0813 c70011        	ld	_Led_Black,a
6041  0816 81            	ret	
6042  0817               L1623:
6043                     ; 975              case Oolong:   Led_Oolong   = LED_OFF;break;
6045  0817 c7000f        	ld	_Led_Oolong,a
6049  081a 81            	ret	
6050  081b               L3623:
6051                     ; 976              case White:    Led_White    = LED_OFF;break;
6053  081b c7001b        	ld	_Led_White,a
6057  081e 81            	ret	
6058  081f               L5623:
6059                     ; 977              case Green:    Led_Green    = LED_OFF;break;
6061  081f c70019        	ld	_Led_Green,a
6065  0822 81            	ret	
6066  0823               L7623:
6067                     ; 978              case Delicate: Led_Delicate = LED_OFF;break;
6069  0823 c70017        	ld	_Led_Delicate,a
6073  0826 81            	ret	
6074                     ; 979              default:break;
6076  0827               L1443:
6077                     ; 985           switch ( Knob2_count )
6080                     ; 993              default:break;
6081  0827 2710          	jreq	L3723
6082  0829 4a            	dec	a
6083  082a 2712          	jreq	L5723
6084  082c 4a            	dec	a
6085  082d 2714          	jreq	L7723
6086  082f 4a            	dec	a
6087  0830 2716          	jreq	L1033
6088  0832 4a            	dec	a
6089  0833 2718          	jreq	L3033
6090  0835 4a            	dec	a
6091  0836 271a          	jreq	L5033
6093  0838 81            	ret	
6094  0839               L3723:
6095                     ; 987              case Herbal:   Led_Herbal = LED_ON;  break;
6098  0839 35640013      	mov	_Led_Herbal,#100
6102  083d 81            	ret	
6103  083e               L5723:
6104                     ; 988              case Black:    Led_Black= LED_ON;    break;
6107  083e 35640011      	mov	_Led_Black,#100
6111  0842 81            	ret	
6112  0843               L7723:
6113                     ; 989              case Oolong:   Led_Oolong= LED_ON;   break;
6115  0843 3564000f      	mov	_Led_Oolong,#100
6119  0847 81            	ret	
6120  0848               L1033:
6121                     ; 990              case White:    Led_White= LED_ON;    break;
6123  0848 3564001b      	mov	_Led_White,#100
6127  084c 81            	ret	
6128  084d               L3033:
6129                     ; 991              case Green:    Led_Green= LED_ON;    break;
6132  084d 35640019      	mov	_Led_Green,#100
6136  0851 81            	ret	
6137  0852               L5033:
6138                     ; 992              case Delicate: Led_Delicate = LED_ON;break;
6140  0852 35640017      	mov	_Led_Delicate,#100
6144  0856 81            	ret	
6145                     ; 993              default:break;
6147  0857               L1343:
6148                     ; 999      DELAY_SET_TYPE = 1;
6150  0857 721c0000      	bset	_Disp_Flag1,#6
6151                     ; 1002 }
6154  085b 81            	ret	
6157                     	switch	.data
6158  0000               L7643_delayAlarm:
6159  0000 00            	dc.b	0
6206                     ; 1003 void Delay_Brew_Disp( void )
6206                     ; 1004 {
6207                     	switch	.text
6208  085c               _Delay_Brew_Disp:
6212                     ; 1006   if( DLY_BREW_FUNC )
6214  085c 720a000003cc  	btjf	_Disp_Flag,#5,L5053
6215                     ; 1008     if( delayBrewPause )
6217  0864 ce0000        	ldw	x,_delayBrewPause
6218  0867 2705          	jreq	L7053
6219                     ; 1010       if( _500ms_flag )
6221  0869 7201000004    	btjf	_Time_Flag,#0,L3153
6222                     ; 1011         Led_Delayset = LED_ON;
6223  086e               L7053:
6224                     ; 1014       Led_Delayset = LED_ON;
6227  086e 35640005      	mov	_Led_Delayset,#100
6228  0872               L3153:
6229                     ; 1017       if( delayAdjust15S )
6231  0872 c60000        	ld	a,_delayAdjust15S
6232  0875 2753          	jreq	L5153
6233                     ; 1019         if( delayNoaction3S )
6235  0877 c60000        	ld	a,_delayNoaction3S
6236  087a 272e          	jreq	L7153
6237                     ; 1021            if( COFFEE == Brew_Select )
6239  087c c60000        	ld	a,_Brew_Select
6240  087f 4a            	dec	a
6241  0880 2614          	jrne	L1253
6242                     ; 1023              Tea_Coffee_Dim_Disp( DIM10 );
6244  0882 a60a          	ld	a,#10
6245  0884 cd01ae        	call	_Tea_Coffee_Dim_Disp
6247                     ; 1024              Coffee_Style_Dim_Disp( DIM10 );
6249  0887 a60a          	ld	a,#10
6250  0889 cd01c1        	call	_Coffee_Style_Dim_Disp
6252                     ; 1025              Tea_Coffee_Same_Disp( LED_ON);
6254  088c a664          	ld	a,#100
6255  088e cd0292        	call	_Tea_Coffee_Same_Disp
6257                     ; 1026              Coffee_Disp( LED_ON );
6259  0891 a664          	ld	a,#100
6263  0893 cc034d        	jp	_Coffee_Disp
6264  0896               L1253:
6265                     ; 1030              Tea_Coffee_Dim_Disp( DIM10 );
6267  0896 a60a          	ld	a,#10
6268  0898 cd01ae        	call	_Tea_Coffee_Dim_Disp
6270                     ; 1031              Tea_Style_Type_Dim_Disp( DIM10 );
6272  089b a60a          	ld	a,#10
6273  089d cd01d4        	call	_Tea_Style_Type_Dim_Disp
6275                     ; 1032              Tea_Coffee_Same_Disp( LED_ON );
6277  08a0 a664          	ld	a,#100
6278  08a2 cd0292        	call	_Tea_Coffee_Same_Disp
6280                     ; 1033              Tea_Disp( LED_ON );
6282  08a5 a664          	ld	a,#100
6285  08a7 cc0351        	jp	_Tea_Disp
6286  08aa               L7153:
6287                     ; 1036         else if( System_Status == ACTIVE )
6289  08aa c60000        	ld	a,_System_Status
6290  08ad a102          	cp	a,#2
6291  08af 2703cc0947    	jrne	L7653
6292                     ; 1038           Tea_Coffee_Same_Disp( LED_ON );
6294  08b4 a664          	ld	a,#100
6295  08b6 cd0292        	call	_Tea_Coffee_Same_Disp
6297                     ; 1039           if( Brew_Select == TEA )
6299  08b9 c60000        	ld	a,_Brew_Select
6300  08bc a102          	cp	a,#2
6301  08be 2605          	jrne	L1353
6302                     ; 1041              Tea_Disp( LED_ON );
6304  08c0 a664          	ld	a,#100
6308  08c2 cc0351        	jp	_Tea_Disp
6309  08c5               L1353:
6310                     ; 1044             Coffee_Disp( LED_ON );
6312  08c5 a664          	ld	a,#100
6315  08c7 cc034d        	jp	_Coffee_Disp
6316  08ca               L5153:
6317                     ; 1051         if( delayExitCnt )
6319  08ca c60000        	ld	a,_delayExitCnt
6320  08cd 273e          	jreq	L7353
6321                     ; 1053            delayAlarm++;
6323  08cf 725c0000      	inc	L7643_delayAlarm
6324                     ; 1054            if( delayAlarm <= 20)
6326  08d3 c60000        	ld	a,L7643_delayAlarm
6327  08d6 a115          	cp	a,#21
6328  08d8 240f          	jruge	L1453
6329                     ; 1056             Tea_Coffee_Same_Disp( LED_OFF);
6331  08da 4f            	clr	a
6332  08db cd0292        	call	_Tea_Coffee_Same_Disp
6334                     ; 1057             if( Brew_Select == TEA )
6336  08de c60000        	ld	a,_Brew_Select
6337  08e1 a102          	cp	a,#2
6338  08e3 2662          	jrne	L7653
6339                     ; 1058               Tea_Type_Disp( LED_OFF);
6341  08e5 4f            	clr	a
6344  08e6 cc0395        	jp	_Tea_Type_Disp
6345  08e9               L1453:
6346                     ; 1060            else if( delayAlarm <= 60 )
6348  08e9 a13d          	cp	a,#61
6349  08eb 2411          	jruge	L7453
6350                     ; 1062             Tea_Coffee_Same_Disp( LED_ON);
6352  08ed a664          	ld	a,#100
6353  08ef cd0292        	call	_Tea_Coffee_Same_Disp
6355                     ; 1063             if( Brew_Select == TEA )
6357  08f2 c60000        	ld	a,_Brew_Select
6358  08f5 a102          	cp	a,#2
6359  08f7 264e          	jrne	L7653
6360                     ; 1064               Tea_Type_Disp( LED_ON);
6362  08f9 a664          	ld	a,#100
6365  08fb cc0395        	jp	_Tea_Type_Disp
6366  08fe               L7453:
6367                     ; 1068             delayAlarm = 0;
6369  08fe 725f0000      	clr	L7643_delayAlarm
6370                     ; 1069             delayExitCnt--;
6372  0902 725a0000      	dec	_delayExitCnt
6373                     ; 1070             if( delayExitCnt == 0 )
6375  0906 263f          	jrne	L7653
6376                     ; 1071               DLY_BREW_FUNC = 0;
6378  0908 721b0000      	bres	_Disp_Flag,#5
6380  090c 81            	ret	
6381  090d               L7353:
6382                     ; 1074         else if( System_Status == ACTIVE )
6384  090d c60000        	ld	a,_System_Status
6385  0910 a102          	cp	a,#2
6386  0912 2633          	jrne	L7653
6387                     ; 1076           Tea_Coffee_Same_Disp( LED_ON );
6389  0914 a664          	ld	a,#100
6390  0916 cd0292        	call	_Tea_Coffee_Same_Disp
6392                     ; 1077           if( Brew_Select == TEA )
6394  0919 c60000        	ld	a,_Brew_Select
6395  091c a102          	cp	a,#2
6396  091e 2605          	jrne	L3653
6397                     ; 1079              Tea_Disp( LED_ON );
6399  0920 a664          	ld	a,#100
6403  0922 cc0351        	jp	_Tea_Disp
6404  0925               L3653:
6405                     ; 1082             Coffee_Disp( LED_ON );
6407  0925 a664          	ld	a,#100
6410  0927 cc034d        	jp	_Coffee_Disp
6411  092a               L5053:
6412                     ; 1088     if( Brew_Select != NO_BASKET && Start_Status == OFF )
6414  092a c60000        	ld	a,_Brew_Select
6415  092d 2718          	jreq	L7653
6417  092f c60000        	ld	a,_Start_Status
6418  0932 2613          	jrne	L7653
6419                     ; 1090       if( System_Status == ACTIVE)
6421  0934 c60000        	ld	a,_System_Status
6422  0937 a102          	cp	a,#2
6423  0939 2605          	jrne	L3753
6424                     ; 1091         Led_Delayset = DIM10;
6426  093b 350a0005      	mov	_Led_Delayset,#10
6429  093f 81            	ret	
6430  0940               L3753:
6431                     ; 1092       else if( System_Status == DIM_STATUS )
6433  0940 4a            	dec	a
6434  0941 2604          	jrne	L7653
6435                     ; 1093         Led_Delayset = DIM;
6437  0943 35050005      	mov	_Led_Delayset,#5
6438  0947               L7653:
6439                     ; 1096 }
6442  0947 81            	ret	
6483                     ; 1098 void Disp_Temperature( S16 Temp )
6483                     ; 1099 {
6484                     	switch	.text
6485  0948               _Disp_Temperature:
6487  0948 89            	pushw	x
6488  0949 88            	push	a
6489       00000001      OFST:	set	1
6492                     ; 1100   U8 num = Temp / 1000;
6494  094a 90ae03e8      	ldw	y,#1000
6495  094e cd0000        	call	c_idiv
6497  0951 01            	rrwa	x,a
6498  0952 6b01          	ld	(OFST+0,sp),a
6499  0954 02            	rlwa	x,a
6500                     ; 1101   digImage[ 3 ] = 0;
6502  0955 3f03          	clr	_digImage+3
6503                     ; 1102   if( num == 0 )
6505  0957 0d01          	tnz	(OFST+0,sp)
6506  0959 2604          	jrne	L7163
6507                     ; 1103   digImage[ 2 ] = 0;
6509  095b 3f02          	clr	_digImage+2
6511  095d 200e          	jra	L1263
6512  095f               L7163:
6513                     ; 1105   digImage[ 2 ] = digCode[ Temp / 1000 ];
6515  095f 1e02          	ldw	x,(OFST+1,sp)
6516  0961 90ae03e8      	ldw	y,#1000
6517  0965 cd0000        	call	c_idiv
6519  0968 d60000        	ld	a,(_digCode,x)
6520  096b b702          	ld	_digImage+2,a
6521  096d               L1263:
6522                     ; 1106   Temp %= 1000;
6524  096d 1e02          	ldw	x,(OFST+1,sp)
6525  096f 90ae03e8      	ldw	y,#1000
6526  0973 cd0000        	call	c_idiv
6528  0976 93            	ldw	x,y
6529  0977 1f02          	ldw	(OFST+1,sp),x
6530                     ; 1107   digImage[ 1 ] = digCode[ Temp / 100 ];
6532  0979 a664          	ld	a,#100
6533  097b cd0000        	call	c_sdivx
6535  097e d60000        	ld	a,(_digCode,x)
6536  0981 b701          	ld	_digImage+1,a
6537                     ; 1108   Temp %= 100;
6539  0983 1e02          	ldw	x,(OFST+1,sp)
6540  0985 a664          	ld	a,#100
6541  0987 cd0000        	call	c_smodx
6543                     ; 1109   digImage[ 0 ] = digCode[ Temp / 10 ];
6545  098a a60a          	ld	a,#10
6546  098c cd0000        	call	c_sdivx
6548  098f d60000        	ld	a,(_digCode,x)
6549  0992 b700          	ld	_digImage,a
6550                     ; 1111 }
6553  0994 5b03          	addw	sp,#3
6554  0996 81            	ret	
6595                     ; 1112 void FlowRate_Disp( S16 flow)
6595                     ; 1113 {
6596                     	switch	.text
6597  0997               _FlowRate_Disp:
6599  0997 89            	pushw	x
6600  0998 88            	push	a
6601       00000001      OFST:	set	1
6604                     ; 1114   U8 num = flow / 100;
6606  0999 a664          	ld	a,#100
6607  099b cd0000        	call	c_sdivx
6609  099e 01            	rrwa	x,a
6610  099f 6b01          	ld	(OFST+0,sp),a
6611                     ; 1115   digImage[ 3 ] = 0;
6613  09a1 3f03          	clr	_digImage+3
6614                     ; 1116   if( num == 0 )
6616  09a3 7b01          	ld	a,(OFST+0,sp)
6617                     ; 1117   digImage[ 2 ] = 0;
6619  09a5 270a          	jreq	L3463
6620                     ; 1119   digImage[ 2 ] = digCode[ flow / 100 ];
6622  09a7 1e02          	ldw	x,(OFST+1,sp)
6623  09a9 a664          	ld	a,#100
6624  09ab cd0000        	call	c_sdivx
6626  09ae d60000        	ld	a,(_digCode,x)
6627  09b1               L3463:
6629  09b1 b702          	ld	_digImage+2,a
6630                     ; 1120   flow %= 100;
6632  09b3 1e02          	ldw	x,(OFST+1,sp)
6633  09b5 a664          	ld	a,#100
6634  09b7 cd0000        	call	c_smodx
6636  09ba 1f02          	ldw	(OFST+1,sp),x
6637                     ; 1121   digImage[ 1 ] = digCode[ flow / 10 ];
6639  09bc a60a          	ld	a,#10
6640  09be cd0000        	call	c_sdivx
6642  09c1 d60000        	ld	a,(_digCode,x)
6643  09c4 b701          	ld	_digImage+1,a
6644                     ; 1122   digImage[ 0 ] = digCode[ flow % 10 ];
6646  09c6 1e02          	ldw	x,(OFST+1,sp)
6647  09c8 a60a          	ld	a,#10
6648  09ca cd0000        	call	c_smodx
6650  09cd d60000        	ld	a,(_digCode,x)
6651  09d0 b700          	ld	_digImage,a
6652                     ; 1123 }
6655  09d2 5b03          	addw	sp,#3
6656  09d4 81            	ret	
6681                     ; 1124 void Error_Code_Disp( void )
6681                     ; 1125 {
6682                     	switch	.text
6683  09d5               _Error_Code_Disp:
6687                     ; 1126   if( errorCode0 & 0x01)
6689  09d5 7201000011    	btjf	_errorCode0,#0,L5563
6690                     ; 1128      digImage[ 3 ] = C_CODE_E;
6692  09da 35790003      	mov	_digImage+3,#121
6693                     ; 1129      digImage[ 2 ] = C_CODE_r;
6695  09de 35500002      	mov	_digImage+2,#80
6696                     ; 1130      digImage[ 1 ] = C_CODE_r;
6698  09e2 35500001      	mov	_digImage+1,#80
6699                     ; 1131      digImage[ 0 ] = C_CODE_1;
6701  09e6 35060000      	mov	_digImage,#6
6704  09ea 81            	ret	
6705  09eb               L5563:
6706                     ; 1133   else if( errorCode0 & 0x02)
6708  09eb 7203000011    	btjf	_errorCode0,#1,L1663
6709                     ; 1135      digImage[ 3 ] = C_CODE_E;
6711  09f0 35790003      	mov	_digImage+3,#121
6712                     ; 1136      digImage[ 2 ] = C_CODE_r;
6714  09f4 35500002      	mov	_digImage+2,#80
6715                     ; 1137      digImage[ 1 ] = C_CODE_r;
6717  09f8 35500001      	mov	_digImage+1,#80
6718                     ; 1138      digImage[ 0 ] = C_CODE_2;
6720  09fc 355b0000      	mov	_digImage,#91
6723  0a00 81            	ret	
6724  0a01               L1663:
6725                     ; 1140   else if( errorCode0 & 0x04)
6727  0a01 7205000011    	btjf	_errorCode0,#2,L5663
6728                     ; 1142      digImage[ 3 ] = C_CODE_E;
6730  0a06 35790003      	mov	_digImage+3,#121
6731                     ; 1143      digImage[ 2 ] = C_CODE_r;
6733  0a0a 35500002      	mov	_digImage+2,#80
6734                     ; 1144      digImage[ 1 ] = C_CODE_r;
6736  0a0e 35500001      	mov	_digImage+1,#80
6737                     ; 1145      digImage[ 0 ] = C_CODE_3;
6739  0a12 354f0000      	mov	_digImage,#79
6742  0a16 81            	ret	
6743  0a17               L5663:
6744                     ; 1147   else if( errorCode0 & 0x08)
6746  0a17 7207000011    	btjf	_errorCode0,#3,L1763
6747                     ; 1149      digImage[ 3 ] = C_CODE_E;
6749  0a1c 35790003      	mov	_digImage+3,#121
6750                     ; 1150      digImage[ 2 ] = C_CODE_r;
6752  0a20 35500002      	mov	_digImage+2,#80
6753                     ; 1151      digImage[ 1 ] = C_CODE_r;
6755  0a24 35500001      	mov	_digImage+1,#80
6756                     ; 1152      digImage[ 0 ] = C_CODE_4;
6758  0a28 35660000      	mov	_digImage,#102
6761  0a2c 81            	ret	
6762  0a2d               L1763:
6763                     ; 1154   else if( errorCode0 & 0x10)
6765  0a2d 7209000011    	btjf	_errorCode0,#4,L5763
6766                     ; 1156      digImage[ 3 ] = C_CODE_E;
6768  0a32 35790003      	mov	_digImage+3,#121
6769                     ; 1157      digImage[ 2 ] = C_CODE_r;
6771  0a36 35500002      	mov	_digImage+2,#80
6772                     ; 1158      digImage[ 1 ] = C_CODE_r;
6774  0a3a 35500001      	mov	_digImage+1,#80
6775                     ; 1159      digImage[ 0 ] = C_CODE_5;
6777  0a3e 356d0000      	mov	_digImage,#109
6780  0a42 81            	ret	
6781  0a43               L5763:
6782                     ; 1161   else if( errorCode0 & 0x20)
6784  0a43 720b000011    	btjf	_errorCode0,#5,L1073
6785                     ; 1163      digImage[ 3 ] = C_CODE_E;
6787  0a48 35790003      	mov	_digImage+3,#121
6788                     ; 1164      digImage[ 2 ] = C_CODE_r;
6790  0a4c 35500002      	mov	_digImage+2,#80
6791                     ; 1165      digImage[ 1 ] = C_CODE_r;
6793  0a50 35500001      	mov	_digImage+1,#80
6794                     ; 1166      digImage[ 0 ] = C_CODE_6;
6796  0a54 357d0000      	mov	_digImage,#125
6799  0a58 81            	ret	
6800  0a59               L1073:
6801                     ; 1168   else if( errorCode0 & 0x40)
6803  0a59 720d000011    	btjf	_errorCode0,#6,L5073
6804                     ; 1170      digImage[ 3 ] = C_CODE_E;
6806  0a5e 35790003      	mov	_digImage+3,#121
6807                     ; 1171      digImage[ 2 ] = C_CODE_r;
6809  0a62 35500002      	mov	_digImage+2,#80
6810                     ; 1172      digImage[ 1 ] = C_CODE_r;
6812  0a66 35500001      	mov	_digImage+1,#80
6813                     ; 1173      digImage[ 0 ] = C_CODE_7;
6815  0a6a 35070000      	mov	_digImage,#7
6818  0a6e 81            	ret	
6819  0a6f               L5073:
6820                     ; 1175   else if( errorCode0 & 0x80)
6822  0a6f 720f000010    	btjf	_errorCode0,#7,L7563
6823                     ; 1177      digImage[ 3 ] = C_CODE_E;
6825  0a74 35790003      	mov	_digImage+3,#121
6826                     ; 1178      digImage[ 2 ] = C_CODE_r;
6828  0a78 35500002      	mov	_digImage+2,#80
6829                     ; 1179      digImage[ 1 ] = C_CODE_r;
6831  0a7c 35500001      	mov	_digImage+1,#80
6832                     ; 1180      digImage[ 0 ] = C_CODE_8;
6834  0a80 357f0000      	mov	_digImage,#127
6835  0a84               L7563:
6836                     ; 1182 }
6839  0a84 81            	ret	
6871                     ; 1184 void DebugMessage_Disp( void )
6871                     ; 1185 {
6872                     	switch	.text
6873  0a85               _DebugMessage_Disp:
6877                     ; 1186    if( debugEnable )
6879  0a85 c60000        	ld	a,_debugEnable
6880  0a88 2603cc0b1c    	jreq	L1473
6881                     ; 1188       BCLREX( ledMap, LM_2DP);
6883  0a8d 721b0027      	bres	_ledMap+4,#5
6884                     ; 1189       BCLREX( ledMap, LM_PM);
6886  0a91 721b0025      	bres	_ledMap+2,#5
6887                     ; 1190       BCLREX( ledMap, LM_AM);
6889  0a95 721b0023      	bres	_ledMap,#5
6890                     ; 1191       if( debugValueEnable )
6892  0a99 c60000        	ld	a,_debugValueEnable
6893  0a9c 2768          	jreq	L3473
6894                     ; 1193          switch( debugIndex )
6896  0a9e c60000        	ld	a,_debugIndex
6898                     ; 1221                 break;
6899  0aa1 2713          	jreq	L3173
6900  0aa3 4a            	dec	a
6901  0aa4 2723          	jreq	L5173
6902  0aa6 4a            	dec	a
6903  0aa7 2733          	jreq	L7173
6904  0aa9 4a            	dec	a
6905  0aaa 2739          	jreq	L1273
6906  0aac 4a            	dec	a
6907  0aad 273f          	jreq	L3273
6908  0aaf 4a            	dec	a
6909  0ab0 2745          	jreq	L5273
6910  0ab2 4a            	dec	a
6911  0ab3 274b          	jreq	L7273
6913  0ab5 81            	ret	
6914  0ab6               L3173:
6915                     ; 1195            case 0:
6915                     ; 1196                  digImage[ 3 ] = 0;
6917  0ab6 b703          	ld	_digImage+3,a
6918                     ; 1197                  digImage[ 2 ] = 0;
6920  0ab8 b702          	ld	_digImage+2,a
6921                     ; 1198                  digImage[ 1 ] = digCode[ 0 ];
6923  0aba 353f0001      	mov	_digImage+1,#63
6924                     ; 1199                  digImage[ 0 ] = digCode[ debugMessage.commSoftVersion ];
6926  0abe c60024        	ld	a,_debugMessage+36
6927  0ac1 5f            	clrw	x
6928  0ac2 97            	ld	xl,a
6929  0ac3 d60000        	ld	a,(_digCode,x)
6930  0ac6 b700          	ld	_digImage,a
6931                     ; 1200                 break;
6934  0ac8 81            	ret	
6935  0ac9               L5173:
6936                     ; 1201            case 1:
6936                     ; 1202                  digImage[ 3 ] = 0;
6938  0ac9 b703          	ld	_digImage+3,a
6939                     ; 1203                  digImage[ 2 ] = 0;
6941  0acb b702          	ld	_digImage+2,a
6942                     ; 1204                  digImage[ 1 ] = C_CODE_b;
6944  0acd 357c0001      	mov	_digImage+1,#124
6945                     ; 1205                  digImage[ 0 ] = digCode[ debugMessage.commBlock + 1 ];
6947  0ad1 c60001        	ld	a,_debugMessage+1
6948  0ad4 5f            	clrw	x
6949  0ad5 97            	ld	xl,a
6950  0ad6 d60001        	ld	a,(_digCode+1,x)
6951  0ad9 b700          	ld	_digImage,a
6952                     ; 1206                 break;
6955  0adb 81            	ret	
6956  0adc               L7173:
6957                     ; 1207            case 2:
6957                     ; 1208                 Disp_Temperature( debugMessage.commTempBoiler + 5);
6959  0adc ce000c        	ldw	x,_debugMessage+12
6960  0adf 1c0005        	addw	x,#5
6962                     ; 1209                 break;
6965  0ae2 cc0948        	jp	_Disp_Temperature
6966  0ae5               L1273:
6967                     ; 1210            case 3:
6967                     ; 1211                 Disp_Temperature( debugMessage.commTempWarmPlate +5 );
6969  0ae5 ce0025        	ldw	x,_debugMessage+37
6970  0ae8 1c0005        	addw	x,#5
6972                     ; 1212                 break;
6975  0aeb cc0948        	jp	_Disp_Temperature
6976  0aee               L3273:
6977                     ; 1213            case 4:
6977                     ; 1214                 Disp_Temperature( debugMessage.commTempInlet + 5 );
6979  0aee ce0027        	ldw	x,_debugMessage+39
6980  0af1 1c0005        	addw	x,#5
6982                     ; 1215                 break;
6985  0af4 cc0948        	jp	_Disp_Temperature
6986  0af7               L5273:
6987                     ; 1216            case 5:
6987                     ; 1217                 Disp_Temperature( debugMessage.commTempWaterout + 5);
6989  0af7 ce0004        	ldw	x,_debugMessage+4
6990  0afa 1c0005        	addw	x,#5
6992                     ; 1218                 break;
6995  0afd cc0948        	jp	_Disp_Temperature
6996  0b00               L7273:
6997                     ; 1219            case 6:
6997                     ; 1220                 FlowRate_Disp( debugMessage.commflowRate );
6999  0b00 ce0018        	ldw	x,_debugMessage+24
7001                     ; 1221                 break;
7004  0b03 cc0997        	jp	_FlowRate_Disp
7006  0b06               L3473:
7007                     ; 1226         digImage[ 3 ] = C_SEGG;
7009  0b06 35400003      	mov	_digImage+3,#64
7010                     ; 1227         digImage[ 2 ] = C_CODE_0;
7012  0b0a 353f0002      	mov	_digImage+2,#63
7013                     ; 1228         digImage[ 0 ] = C_SEGG;
7015  0b0e 35400000      	mov	_digImage,#64
7016                     ; 1229         digImage[ 1 ] = digCode[ debugIndex ];
7018  0b12 c60000        	ld	a,_debugIndex
7019  0b15 5f            	clrw	x
7020  0b16 97            	ld	xl,a
7021  0b17 d60000        	ld	a,(_digCode,x)
7022  0b1a b701          	ld	_digImage+1,a
7023  0b1c               L1473:
7024                     ; 1232 }
7027  0b1c 81            	ret	
7058                     ; 1233 void Pause_Disp( void )
7058                     ; 1234 {
7059                     	switch	.text
7060  0b1d               _Pause_Disp:
7064                     ; 1235    if( CLEAN_FUNC_FLAG == 1)
7066  0b1d 720300000a    	btjf	_Disp_Flag1,#1,L3673
7067                     ; 1237      if( _500ms_flag )
7069  0b22 7201000022    	btjf	_Time_Flag,#0,L7673
7070                     ; 1238       Led_Start = LED_ON;
7072  0b27 35640015      	mov	_Led_Start,#100
7074  0b2b 81            	ret	
7075  0b2c               L3673:
7076                     ; 1240    else if( _500ms_flag )
7078  0b2c 7201000018    	btjf	_Time_Flag,#0,L7673
7079                     ; 1242        Led_Start = LED_ON;
7081  0b31 35640015      	mov	_Led_Start,#100
7082                     ; 1243        Led_BAR1 = LED_ON;
7084  0b35 35640008      	mov	_Led_BAR1,#100
7085                     ; 1244        Led_BAR2 = LED_ON;
7087  0b39 3564001f      	mov	_Led_BAR2,#100
7088                     ; 1245        Led_BAR3 = LED_ON;
7090  0b3d 3564001e      	mov	_Led_BAR3,#100
7091                     ; 1246        Led_BAR4 = LED_ON;
7093  0b41 3564001d      	mov	_Led_BAR4,#100
7094                     ; 1247        Led_BAR5 = LED_ON;
7096  0b45 3564001c      	mov	_Led_BAR5,#100
7097  0b49               L7673:
7098                     ; 1249 }
7101  0b49 81            	ret	
7143                     ; 1251 void Run_Disp(void)
7143                     ; 1252 {
7144                     	switch	.text
7145  0b4a               _Run_Disp:
7149                     ; 1253      Led_Start = LED_ON;
7151  0b4a 35640015      	mov	_Led_Start,#100
7152                     ; 1254      if( CLEAN_FUNC_FLAG == 1)
7154  0b4e 7203000003cc  	btjt	_Disp_Flag1,#1,L1204
7156                     ; 1256      else if( F_Brew_complete == 0 && F_Brew_abnormal_end == 0 )
7158  0b56 72020000f8    	btjt	_Ctrl_Board_Status,#1,L1204
7160  0b5b 72040000f3    	btjt	_Ctrl_Board_Status,#2,L1204
7161                     ; 1259          switch( Brew_Bar )
7163  0b60 c60000        	ld	a,_Brew_Bar
7165                     ; 1300                    break;
7166  0b63 2711          	jreq	L3773
7167  0b65 4a            	dec	a
7168  0b66 2719          	jreq	L5773
7169  0b68 4a            	dec	a
7170  0b69 2725          	jreq	L7773
7171  0b6b 4a            	dec	a
7172  0b6c 2735          	jreq	L1004
7173  0b6e 4a            	dec	a
7174  0b6f 2749          	jreq	L3004
7175  0b71 4a            	dec	a
7176  0b72 2758          	jreq	L5004
7177  0b74 206f          	jra	L1204
7178  0b76               L3773:
7179                     ; 1261            case 0: Led_BAR1 = ledBreathe;
7181  0b76 5500000008    	mov	_Led_BAR1,L5012_ledBreathe
7182                     ; 1262                    Led_BAR2 = DIM;
7184  0b7b 3505001f      	mov	_Led_BAR2,#5
7185                     ; 1263                    Led_BAR3 = DIM;
7186                     ; 1264                    Led_BAR4 = DIM;
7187                     ; 1265                    Led_BAR5 = DIM;
7188                     ; 1266                    break;
7190  0b7f 2009          	jp	LC023
7191  0b81               L5773:
7192                     ; 1267            case 1:   
7192                     ; 1268                    Led_BAR1 = LED_ON;
7194  0b81 35640008      	mov	_Led_BAR1,#100
7195                     ; 1269                    Led_BAR2 = ledBreathe;
7197  0b85 550000001f    	mov	_Led_BAR2,L5012_ledBreathe
7198                     ; 1270                    Led_BAR3 = DIM;
7200  0b8a               LC023:
7202  0b8a 3505001e      	mov	_Led_BAR3,#5
7203                     ; 1271                    Led_BAR4 = DIM;
7204                     ; 1272                    Led_BAR5 = DIM;
7205                     ; 1273                    break;
7207  0b8e 200d          	jp	LC022
7208  0b90               L7773:
7209                     ; 1274            case 2: 
7209                     ; 1275                    Led_BAR1 = LED_ON;
7211  0b90 35640008      	mov	_Led_BAR1,#100
7212                     ; 1276                    Led_BAR2 = LED_ON;
7214  0b94 3564001f      	mov	_Led_BAR2,#100
7215                     ; 1277                    Led_BAR3 = ledBreathe;
7217  0b98 550000001e    	mov	_Led_BAR3,L5012_ledBreathe
7218                     ; 1278                    Led_BAR4 = DIM;
7220  0b9d               LC022:
7223  0b9d 3505001d      	mov	_Led_BAR4,#5
7224                     ; 1279                    Led_BAR5 = DIM;
7225                     ; 1280                    break;
7227  0ba1 2011          	jp	LC021
7228  0ba3               L1004:
7229                     ; 1281            case 3: 
7229                     ; 1282                    Led_BAR1 = LED_ON;
7231  0ba3 35640008      	mov	_Led_BAR1,#100
7232                     ; 1283                    Led_BAR2 = LED_ON;
7234  0ba7 3564001f      	mov	_Led_BAR2,#100
7235                     ; 1284                    Led_BAR3 = LED_ON;
7237  0bab 3564001e      	mov	_Led_BAR3,#100
7238                     ; 1285                    Led_BAR4 = ledBreathe;
7240  0baf 550000001d    	mov	_Led_BAR4,L5012_ledBreathe
7241                     ; 1286                    Led_BAR5 = DIM;
7243  0bb4               LC021:
7247  0bb4 3505001c      	mov	_Led_BAR5,#5
7248                     ; 1287                    break;
7250  0bb8 202b          	jra	L1204
7251  0bba               L3004:
7252                     ; 1288            case 4: 
7252                     ; 1289                    Led_BAR1 = LED_ON;
7254  0bba 35640008      	mov	_Led_BAR1,#100
7255                     ; 1290                    Led_BAR2 = LED_ON;
7257  0bbe 3564001f      	mov	_Led_BAR2,#100
7258                     ; 1291                    Led_BAR3 = LED_ON;
7260  0bc2 3564001e      	mov	_Led_BAR3,#100
7261                     ; 1292                    Led_BAR4 = LED_ON;
7263  0bc6 3564001d      	mov	_Led_BAR4,#100
7264                     ; 1293                    Led_BAR5 = ledBreathe;
7265                     ; 1294                    break;
7267  0bca 2014          	jp	LC020
7268  0bcc               L5004:
7269                     ; 1295            case 5: Led_BAR1 = ledBreathe;
7271  0bcc 5500000008    	mov	_Led_BAR1,L5012_ledBreathe
7272                     ; 1296                    Led_BAR2 = ledBreathe;
7274  0bd1 550000001f    	mov	_Led_BAR2,L5012_ledBreathe
7275                     ; 1297                    Led_BAR3 = ledBreathe;
7277  0bd6 550000001e    	mov	_Led_BAR3,L5012_ledBreathe
7278                     ; 1298                    Led_BAR4 = ledBreathe;
7280  0bdb 550000001d    	mov	_Led_BAR4,L5012_ledBreathe
7281                     ; 1299                    Led_BAR5 = ledBreathe;
7283  0be0               LC020:
7285  0be0 550000001c    	mov	_Led_BAR5,L5012_ledBreathe
7286                     ; 1300                    break;
7288  0be5               L1204:
7289                     ; 1303     if( brewAdjust15S )
7291  0be5 c60000        	ld	a,_brewAdjust15S
7292  0be8 2738          	jreq	L1304
7293                     ; 1305       if( brewNoaction3S )
7295  0bea c60000        	ld	a,_brewNoaction3S
7296  0bed 2733          	jreq	L1304
7297                     ; 1307          if( COFFEE == Brew_Select )
7299  0bef c60000        	ld	a,_Brew_Select
7300  0bf2 a101          	cp	a,#1
7301  0bf4 2614          	jrne	L5304
7302                     ; 1309            Tea_Coffee_Dim_Disp( DIM10 );
7304  0bf6 a60a          	ld	a,#10
7305  0bf8 cd01ae        	call	_Tea_Coffee_Dim_Disp
7307                     ; 1310            Coffee_Style_Dim_Disp( DIM10 );
7309  0bfb a60a          	ld	a,#10
7310  0bfd cd01c1        	call	_Coffee_Style_Dim_Disp
7312                     ; 1311            Tea_Coffee_Same_Disp( LED_ON);
7314  0c00 a664          	ld	a,#100
7315  0c02 cd0292        	call	_Tea_Coffee_Same_Disp
7317                     ; 1312            Coffee_Disp( LED_ON );
7319  0c05 a664          	ld	a,#100
7323  0c07 cc034d        	jp	_Coffee_Disp
7324  0c0a               L5304:
7325                     ; 1314          else if( TEA == Brew_Select)
7327  0c0a a102          	cp	a,#2
7328  0c0c 2614          	jrne	L1304
7329                     ; 1316            Tea_Coffee_Dim_Disp( DIM10 );
7331  0c0e a60a          	ld	a,#10
7332  0c10 cd01ae        	call	_Tea_Coffee_Dim_Disp
7334                     ; 1317            Tea_Style_Type_Dim_Disp( DIM10 );
7336  0c13 a60a          	ld	a,#10
7337  0c15 cd01d4        	call	_Tea_Style_Type_Dim_Disp
7339                     ; 1318            Tea_Coffee_Same_Disp( LED_ON );
7341  0c18 a664          	ld	a,#100
7342  0c1a cd0292        	call	_Tea_Coffee_Same_Disp
7344                     ; 1319            Tea_Disp( LED_ON );
7346  0c1d a664          	ld	a,#100
7347  0c1f cd0351        	call	_Tea_Disp
7349  0c22               L1304:
7350                     ; 1323 }
7353  0c22 81            	ret	
7356                     	bsct
7357  0001               L3404_timBreathe:
7358  0001 00            	dc.b	0
7359                     	switch	.data
7360  0001               L5404_blinkAlarm:
7361  0001 00            	dc.b	0
7480                     ; 1326 void LED_DISP( void )
7480                     ; 1327 {
7481                     	switch	.text
7482  0c23               _LED_DISP:
7486                     ; 1331    if ( ++timBreathe > 200 ) timBreathe = 0;
7488  0c23 3c01          	inc	L3404_timBreathe
7489  0c25 b601          	ld	a,L3404_timBreathe
7490  0c27 a1c9          	cp	a,#201
7491  0c29 2503          	jrult	L7014
7494  0c2b 4f            	clr	a
7495  0c2c b701          	ld	L3404_timBreathe,a
7496  0c2e               L7014:
7497                     ; 1332    if( timBreathe <= DIM )
7499  0c2e a106          	cp	a,#6
7500                     ; 1333     ledBreathe = DIM;
7502  0c30 2515          	jrult	L1214
7503                     ; 1334    else if( timBreathe <= 100 )
7505  0c32 a165          	cp	a,#101
7506  0c34 2405          	jruge	L5114
7507                     ; 1335     ledBreathe = timBreathe;
7509  0c36 450100        	mov	L5012_ledBreathe,L3404_timBreathe
7511  0c39 2010          	jra	L3114
7512  0c3b               L5114:
7513                     ; 1336    else if( timBreathe <= 200-DIM)
7515  0c3b a1c4          	cp	a,#196
7516  0c3d 2408          	jruge	L1214
7517                     ; 1337     ledBreathe = 200 - timBreathe;
7519  0c3f a6c8          	ld	a,#200
7520  0c41 b001          	sub	a,L3404_timBreathe
7521  0c43 b700          	ld	L5012_ledBreathe,a
7523  0c45 2004          	jra	L3114
7524  0c47               L1214:
7525                     ; 1339     ledBreathe = DIM;
7528  0c47 35050000      	mov	L5012_ledBreathe,#5
7529  0c4b               L3114:
7530                     ; 1341     Led_Keepwarm         = LED_OFF;
7532  0c4b 725f0003      	clr	_Led_Keepwarm
7533                     ; 1342     Led_Type             = LED_OFF;
7535  0c4f 725f0022      	clr	_Led_Type
7536                     ; 1343     Led_Clean            = LED_OFF;
7538  0c53 725f0021      	clr	_Led_Clean
7539                     ; 1344     Led_Power            = LED_OFF;
7541  0c57 725f0020      	clr	_Led_Power
7542                     ; 1345     Led_BAR2             = LED_OFF;
7544  0c5b 725f001f      	clr	_Led_BAR2
7545                     ; 1346     Led_BAR3             = LED_OFF;
7547  0c5f 725f001e      	clr	_Led_BAR3
7548                     ; 1347     Led_BAR4             = LED_OFF;
7550  0c63 725f001d      	clr	_Led_BAR4
7551                     ; 1348     Led_BAR5             = LED_OFF;
7553  0c67 725f001c      	clr	_Led_BAR5
7554                     ; 1350     Led_White            = LED_OFF;
7556  0c6b 725f001b      	clr	_Led_White
7557                     ; 1351     Led_Speciality       = LED_OFF;
7559  0c6f 725f001a      	clr	_Led_Speciality
7560                     ; 1352     Led_Green            = LED_OFF;
7562  0c73 725f0019      	clr	_Led_Green
7563                     ; 1353     Led_Immersion        = LED_OFF;
7565  0c77 725f0018      	clr	_Led_Immersion
7566                     ; 1354     Led_Delicate         = LED_OFF;
7568  0c7b 725f0017      	clr	_Led_Delicate
7569                     ; 1355     Led_Style            = LED_OFF;
7571  0c7f 725f0016      	clr	_Led_Style
7572                     ; 1356     Led_Start            = LED_OFF;
7574  0c83 725f0015      	clr	_Led_Start
7575                     ; 1358     Led_Classic          = LED_OFF;
7577  0c87 725f0014      	clr	_Led_Classic
7578                     ; 1359     Led_Herbal           = LED_OFF;
7580  0c8b 725f0013      	clr	_Led_Herbal
7581                     ; 1360     Led_Rich             = LED_OFF;
7583  0c8f 725f0012      	clr	_Led_Rich
7584                     ; 1361     Led_Black            = LED_OFF;
7586  0c93 725f0011      	clr	_Led_Black
7587                     ; 1362     Led_Overice          = LED_OFF;
7589  0c97 725f0010      	clr	_Led_Overice
7590                     ; 1363     Led_Oolong           = LED_OFF;
7592  0c9b 725f000f      	clr	_Led_Oolong
7593                     ; 1364     Led_Coldbrew         = LED_OFF;
7595  0c9f 725f000e      	clr	_Led_Coldbrew
7596                     ; 1366     Led_Xltravel         = LED_OFF;
7598  0ca3 725f000d      	clr	_Led_Xltravel
7599                     ; 1367     Led_Xlcup            = LED_OFF;
7601  0ca7 725f000c      	clr	_Led_Xlcup
7602                     ; 1368     Led_Halfcarafe       = LED_OFF;
7604  0cab 725f000b      	clr	_Led_Halfcarafe
7605                     ; 1369     Led_Cup              = LED_OFF;
7607  0caf 725f000a      	clr	_Led_Cup
7608                     ; 1370     Led_Fullcarafe       = LED_OFF;
7610  0cb3 725f0009      	clr	_Led_Fullcarafe
7611                     ; 1371     Led_BAR1             = LED_OFF;
7613  0cb7 725f0008      	clr	_Led_BAR1
7614                     ; 1372     Led_Addone           = LED_OFF;
7616  0cbb 725f0007      	clr	_Led_Addone
7617                     ; 1375     Led_Insertbasket     = LED_OFF;
7619  0cbf 725f0006      	clr	_Led_Insertbasket
7620                     ; 1376     Led_Delayset         = LED_OFF;
7622  0cc3 725f0005      	clr	_Led_Delayset
7623                     ; 1377     Led_Clean2           = LED_OFF;
7625  0cc7 725f0004      	clr	_Led_Clean2
7626                     ; 1379     Led_Tea              = LED_OFF;
7628  0ccb 725f0002      	clr	_Led_Tea
7629                     ; 1380     Led_Coffee           = LED_OFF;
7631  0ccf 725f0001      	clr	_Led_Coffee
7632                     ; 1381     Led_Travel           = LED_OFF;
7634  0cd3 725f0000      	clr	_Led_Travel
7635                     ; 1383    memset( ledMap, 0x00, sizeof( ledMap ) );
7637  0cd7 ae0008        	ldw	x,#8
7638  0cda               L232:
7639  0cda 724f0022      	clr	(_ledMap-1,x)
7640  0cde 5a            	decw	x
7641  0cdf 26f9          	jrne	L232
7642                     ; 1384    memset( digImage, 0x00, sizeof( digImage ) );
7644  0ce1 ae0004        	ldw	x,#4
7645  0ce4               L432:
7646  0ce4 6fff          	clr	(_digImage-1,x)
7647  0ce6 5a            	decw	x
7648  0ce7 26fb          	jrne	L432
7649                     ; 1385    if( CLEAN_DISP_FLAG || CLEAN_FUNC_FLAG || F_Brew_complete || F_Brew_abnormal_end )
7651  0ce9 720000000f    	btjt	_Disp_Flag1,#0,L7214
7653  0cee 720200000a    	btjt	_Disp_Flag1,#1,L7214
7655  0cf3 7202000005    	btjt	_Ctrl_Board_Status,#1,L7214
7657  0cf8 720500001e    	btjf	_Ctrl_Board_Status,#2,L5214
7658  0cfd               L7214:
7659                     ; 1387       Error_Code_Disp( );
7661  0cfd cd09d5        	call	_Error_Code_Disp
7664  0d00               L5314:
7665                     ; 1415    if( ACTIVE == System_Status )
7667  0d00 c60000        	ld	a,_System_Status
7668  0d03 a102          	cp	a,#2
7669  0d05 2703cc0f4c    	jrne	L7614
7670                     ; 1417      Led_Power = LED_ON;
7672  0d0a 35640020      	mov	_Led_Power,#100
7673                     ; 1418      if(( NO_BASKET == Brew_Select )&& ( OFF == Start_Status)) // ����ʱ��˸ָʾ��
7675  0d0e c60000        	ld	a,_Brew_Select
7676  0d11 264a          	jrne	L1714
7678  0d13 c60000        	ld	a,_Start_Status
7679  0d16 2645          	jrne	L1714
7681  0d18 cc0f91        	jra	L7634
7682  0d1b               L5214:
7683                     ; 1389    else if( Delay_Keep_Warm_Time )
7685  0d1b c60000        	ld	a,_Delay_Keep_Warm_Time
7686  0d1e 2705          	jreq	L7314
7687                     ; 1391       Keep_Warm_Clock_Disp( );
7689  0d20 cd0579        	call	_Keep_Warm_Clock_Disp
7692  0d23 20db          	jra	L5314
7693  0d25               L7314:
7694                     ; 1393    else if( Delay_Keep_Warm_Level )
7696  0d25 c60000        	ld	a,_Delay_Keep_Warm_Level
7697  0d28 2705          	jreq	L3414
7698                     ; 1395       Keep_Warm_Level_Disp( );
7700  0d2a cd059c        	call	_Keep_Warm_Level_Disp
7703  0d2d 20d1          	jra	L5314
7704  0d2f               L3414:
7705                     ; 1397    else if( Delay_Brew_Time )
7707  0d2f c60000        	ld	a,_Delay_Brew_Time
7708  0d32 2705          	jreq	L7414
7709                     ; 1399      Delay_Brew_Clock_Disp( );
7711  0d34 cd05d4        	call	_Delay_Brew_Clock_Disp
7714  0d37 20c7          	jra	L5314
7715  0d39               L7414:
7716                     ; 1403          if( errorCode0 )
7718  0d39 c60000        	ld	a,_errorCode0
7719  0d3c 2705          	jreq	L3514
7720                     ; 1404          Error_Code_Disp();
7722  0d3e cd09d5        	call	_Error_Code_Disp
7725  0d41 2012          	jra	L5514
7726  0d43               L3514:
7727                     ; 1405          else if( Paused_Delay )
7729  0d43 ce0000        	ldw	x,_Paused_Delay
7730                     ; 1406           Pause_Clock_Disp( Paused_Delay );
7733  0d46 2605          	jrne	LC025
7734                     ; 1407          else if( delayBrewPause )
7736  0d48 ce0000        	ldw	x,_delayBrewPause
7737  0d4b 2705          	jreq	L3614
7738                     ; 1408           Pause_Clock_Disp( delayBrewPause );
7740  0d4d               LC025:
7742  0d4d cd04d3        	call	_Pause_Clock_Disp
7745  0d50 2003          	jra	L5514
7746  0d52               L3614:
7747                     ; 1410           Clock_Disp( );
7749  0d52 cd0069        	call	_Clock_Disp
7751  0d55               L5514:
7752                     ; 1411          Stay_Warm_Disp( );
7754  0d55 cd051e        	call	_Stay_Warm_Disp
7756                     ; 1412          Delay_Brew_Disp( );
7758  0d58 cd085c        	call	_Delay_Brew_Disp
7760  0d5b 20a3          	jra	L5314
7761  0d5d               L1714:
7762                     ; 1425        if( ON == Start_Status || PAUSE == Start_Status || UNPAUSE== Start_Status )
7764  0d5d c60000        	ld	a,_Start_Status
7765  0d60 a101          	cp	a,#1
7766  0d62 270b          	jreq	L7714
7768  0d64 a102          	cp	a,#2
7769  0d66 2707          	jreq	L7714
7771  0d68 a104          	cp	a,#4
7772  0d6a 2703cc0eca    	jrne	L5714
7773  0d6f               L7714:
7774                     ; 1427          switch( Start_Status )
7777                     ; 1431              case UNPAUSE: Run_Disp();break;
7778  0d6f 4a            	dec	a
7779  0d70 270e          	jreq	L1504
7780  0d72 4a            	dec	a
7781  0d73 2706          	jreq	L7404
7782  0d75 a002          	sub	a,#2
7783  0d77 2707          	jreq	L1504
7784  0d79 2008          	jra	L5024
7785  0d7b               L7404:
7786                     ; 1429              case PAUSE:  Pause_Disp();break;
7788  0d7b cd0b1d        	call	_Pause_Disp
7792  0d7e 2003          	jra	L5024
7793  0d80               L1504:
7794                     ; 1430              case ON:
7794                     ; 1431              case UNPAUSE: Run_Disp();break;
7796  0d80 cd0b4a        	call	_Run_Disp
7800  0d83               L5024:
7801                     ; 1433          if( F_Brew_complete || F_Brew_abnormal_end )
7803  0d83 7202000008    	btjt	_Ctrl_Board_Status,#1,L1124
7805  0d88 7204000003cc  	btjf	_Ctrl_Board_Status,#2,L7024
7806  0d90               L1124:
7807                     ; 1435              if( _500ms_flag )
7809  0d90 72010000f8    	btjf	_Time_Flag,#0,L7024
7810                     ; 1437                  switch( Brew_Bar )
7812  0d95 c60000        	ld	a,_Brew_Bar
7814                     ; 1459                            break;
7815  0d98 4a            	dec	a
7816  0d99 270e          	jreq	L5504
7817  0d9b 4a            	dec	a
7818  0d9c 2711          	jreq	L7504
7819  0d9e 4a            	dec	a
7820  0d9f 2718          	jreq	L1604
7821  0da1 4a            	dec	a
7822  0da2 2723          	jreq	L3604
7823  0da4 4a            	dec	a
7824  0da5 2732          	jreq	L5604
7825  0da7 2044          	jra	L7124
7826                     ; 1439                    case 0: break;
7828  0da9               L5504:
7829                     ; 1440                    case 1: Led_BAR1 = LED_ON;                   
7831  0da9 35640008      	mov	_Led_BAR1,#100
7832                     ; 1441                            break;
7834  0dad 203e          	jra	L7124
7835  0daf               L7504:
7836                     ; 1442                    case 2: Led_BAR1 = LED_ON;
7838  0daf 35640008      	mov	_Led_BAR1,#100
7839                     ; 1443                            Led_BAR2 = LED_ON;
7841  0db3 3564001f      	mov	_Led_BAR2,#100
7842                     ; 1444                            break;
7844  0db7 2034          	jra	L7124
7845  0db9               L1604:
7846                     ; 1445                    case 3: Led_BAR1 = LED_ON;
7848  0db9 35640008      	mov	_Led_BAR1,#100
7849                     ; 1446                            Led_BAR2 = LED_ON;
7851  0dbd 3564001f      	mov	_Led_BAR2,#100
7852                     ; 1447                            Led_BAR3 = LED_ON;
7854  0dc1 3564001e      	mov	_Led_BAR3,#100
7855                     ; 1448                            break;
7857  0dc5 2026          	jra	L7124
7858  0dc7               L3604:
7859                     ; 1449                    case 4: Led_BAR1 = LED_ON;
7861  0dc7 35640008      	mov	_Led_BAR1,#100
7862                     ; 1450                            Led_BAR2 = LED_ON;
7864  0dcb 3564001f      	mov	_Led_BAR2,#100
7865                     ; 1451                            Led_BAR3 = LED_ON;
7867  0dcf 3564001e      	mov	_Led_BAR3,#100
7868                     ; 1452                            Led_BAR4 = LED_ON;
7870  0dd3 3564001d      	mov	_Led_BAR4,#100
7871                     ; 1453                            break;
7873  0dd7 2014          	jra	L7124
7874  0dd9               L5604:
7875                     ; 1454                    case 5: Led_BAR1 = LED_ON;
7877  0dd9 35640008      	mov	_Led_BAR1,#100
7878                     ; 1455                            Led_BAR2 = LED_ON;
7880  0ddd 3564001f      	mov	_Led_BAR2,#100
7881                     ; 1456                            Led_BAR3 = LED_ON;
7883  0de1 3564001e      	mov	_Led_BAR3,#100
7884                     ; 1457                            Led_BAR4 = LED_ON;
7886  0de5 3564001d      	mov	_Led_BAR4,#100
7887                     ; 1458                            Led_BAR5 = LED_ON;
7889  0de9 3564001c      	mov	_Led_BAR5,#100
7890                     ; 1459                            break;
7892  0ded               L7124:
7893                     ; 1461                  digImage[ 3 ] = C_CODE_E;
7895  0ded 35790003      	mov	_digImage+3,#121
7896                     ; 1462                  digImage[ 2 ] = C_CODE_n;
7898  0df1 35540002      	mov	_digImage+2,#84
7899                     ; 1463                  digImage[ 1 ] = C_CODE_d;
7901  0df5 355e0001      	mov	_digImage+1,#94
7902                     ; 1464                  digImage[ 0 ] = 0;
7904  0df9 3f00          	clr	_digImage
7905                     ; 1466                  if( CLEAN_FUNC_FLAG )
7907  0dfb 7203000016    	btjf	_Disp_Flag1,#1,L7024
7908                     ; 1468                     Led_Fullcarafe = LED_ON;
7910  0e00 35640009      	mov	_Led_Fullcarafe,#100
7911                     ; 1469                    if( CLean_Class == 0 || CLean_Class == 1 )
7913  0e04 c60000        	ld	a,_CLean_Class
7914  0e07 2703          	jreq	L5224
7916  0e09 4a            	dec	a
7917  0e0a 2606          	jrne	L3224
7918  0e0c               L5224:
7919                     ; 1470                     Led_Clean2  = LED_ON;
7921  0e0c 35640004      	mov	_Led_Clean2,#100
7923  0e10 2004          	jra	L7024
7924  0e12               L3224:
7925                     ; 1472                     Led_Clean = LED_ON;
7927  0e12 35640021      	mov	_Led_Clean,#100
7928  0e16               L7024:
7929                     ; 1477          if( Brew_Select_B == NO_BASKET )
7931  0e16 c60000        	ld	a,_Brew_Select_B
7932  0e19 2603cc0f91    	jreq	L7634
7934                     ; 1479          else if( CLEAN_FUNC_FLAG )
7936  0e1e 7203000010    	btjf	_Disp_Flag1,#1,L5324
7937                     ; 1481             if( F_Brew_complete || F_Brew_abnormal_end )
7939  0e23 72020000f3    	btjt	_Ctrl_Board_Status,#1,L7634
7941  0e28 72040000ee    	btjt	_Ctrl_Board_Status,#2,L7634
7942                     ; 1485            CLean_Disp_Run( );
7944  0e2d cd04f0        	call	_CLean_Disp_Run
7946  0e30 cc0f91        	jra	L7634
7947  0e33               L5324:
7948                     ; 1487          else if( COFFEE == Brew_Select )
7950  0e33 c60000        	ld	a,_Brew_Select
7951  0e36 4a            	dec	a
7952  0e37 2638          	jrne	L7424
7953                     ; 1491            if( brewExitCnt )
7955  0e39 c60000        	ld	a,_brewExitCnt
7956  0e3c 2726          	jreq	L1524
7957                     ; 1493              blinkAlarm++;
7959  0e3e 725c0001      	inc	L5404_blinkAlarm
7960                     ; 1494              if( blinkAlarm <= 20)
7962  0e42 c60001        	ld	a,L5404_blinkAlarm
7963  0e45 a115          	cp	a,#21
7964  0e47 2403          	jruge	L3524
7965                     ; 1495               Tea_Coffee_Same_Disp( LED_OFF);
7967  0e49 4f            	clr	a
7970  0e4a 201a          	jp	LC026
7971  0e4c               L3524:
7972                     ; 1496              else if( blinkAlarm <= 60 )
7974  0e4c a13d          	cp	a,#61
7975                     ; 1497               Tea_Coffee_Same_Disp( LED_ON);
7978  0e4e 2514          	jrult	L1524
7979                     ; 1500               blinkAlarm = 0;
7981  0e50 725f0001      	clr	L5404_blinkAlarm
7982                     ; 1501               brewExitCnt--;
7984  0e54 725a0000      	dec	_brewExitCnt
7985                     ; 1502               if( brewExitCnt == 0 )
7987  0e58 2604          	jrne	L3624
7988                     ; 1503                 Start_Status = OFF;
7990  0e5a 725f0000      	clr	_Start_Status
7991  0e5e               L3624:
7992                     ; 1504               blinkAlarm = 0;
7994  0e5e 725f0001      	clr	L5404_blinkAlarm
7995  0e62 2005          	jra	L5624
7996  0e64               L1524:
7997                     ; 1510             Tea_Coffee_Same_Disp( LED_ON);
8000  0e64 a664          	ld	a,#100
8001  0e66               LC026:
8002  0e66 cd0292        	call	_Tea_Coffee_Same_Disp
8004  0e69               L5624:
8005                     ; 1512            Coffee_Disp( LED_ON);
8007  0e69 a664          	ld	a,#100
8008  0e6b cd034d        	call	_Coffee_Disp
8011  0e6e cc0f91        	jra	L7634
8012  0e71               L7424:
8013                     ; 1518            if( brewExitCnt )
8015  0e71 c60000        	ld	a,_brewExitCnt
8016  0e74 2747          	jreq	L1724
8017                     ; 1520              blinkAlarm++;
8019  0e76 725c0001      	inc	L5404_blinkAlarm
8020                     ; 1521              if( blinkAlarm <= 20)
8022  0e7a c60001        	ld	a,L5404_blinkAlarm
8023  0e7d a115          	cp	a,#21
8024  0e7f 2411          	jruge	L3724
8025                     ; 1523               Tea_Coffee_Same_Disp( LED_OFF);
8027  0e81 4f            	clr	a
8028  0e82 cd0292        	call	_Tea_Coffee_Same_Disp
8030                     ; 1524               if( Brew_Select == TEA )
8032  0e85 c60000        	ld	a,_Brew_Select
8033  0e88 a102          	cp	a,#2
8034  0e8a 2636          	jrne	L1134
8035                     ; 1525               Tea_Type_Disp( LED_OFF);
8037  0e8c 4f            	clr	a
8038  0e8d cd0395        	call	_Tea_Type_Disp
8040  0e90 2030          	jra	L1134
8041  0e92               L3724:
8042                     ; 1527              else if( blinkAlarm <= 60 )
8044  0e92 a13d          	cp	a,#61
8045  0e94 2413          	jruge	L1034
8046                     ; 1529               Tea_Coffee_Same_Disp( LED_ON);
8048  0e96 a664          	ld	a,#100
8049  0e98 cd0292        	call	_Tea_Coffee_Same_Disp
8051                     ; 1530               if( Brew_Select == TEA )
8053  0e9b c60000        	ld	a,_Brew_Select
8054  0e9e a102          	cp	a,#2
8055  0ea0 2620          	jrne	L1134
8056                     ; 1531               Tea_Type_Disp( LED_ON);
8058  0ea2 a664          	ld	a,#100
8059  0ea4 cd0395        	call	_Tea_Type_Disp
8061  0ea7 2019          	jra	L1134
8062  0ea9               L1034:
8063                     ; 1535               blinkAlarm = 0;
8065  0ea9 725f0001      	clr	L5404_blinkAlarm
8066                     ; 1536               brewExitCnt--;
8068  0ead 725a0000      	dec	_brewExitCnt
8069                     ; 1537               if( brewExitCnt == 0 )
8071  0eb1 2604          	jrne	L7034
8072                     ; 1538                 Start_Status = OFF;
8074  0eb3 725f0000      	clr	_Start_Status
8075  0eb7               L7034:
8076                     ; 1539               blinkAlarm = 0;
8078  0eb7 725f0001      	clr	L5404_blinkAlarm
8079  0ebb 2005          	jra	L1134
8080  0ebd               L1724:
8081                     ; 1545             Tea_Coffee_Same_Disp( LED_ON);
8083  0ebd a664          	ld	a,#100
8084  0ebf cd0292        	call	_Tea_Coffee_Same_Disp
8086  0ec2               L1134:
8087                     ; 1547            Tea_Disp( LED_ON );
8089  0ec2 a664          	ld	a,#100
8090  0ec4 cd0351        	call	_Tea_Disp
8092  0ec7 cc0f91        	jra	L7634
8093  0eca               L5714:
8094                     ; 1552          if( DLY_BREW_FUNC || DLY_BREW_FLAG )
8096  0eca 720a0000f8    	btjt	_Disp_Flag,#5,L7634
8098  0ecf 72080000f3    	btjt	_Disp_Flag,#4,L7634
8099                     ; 1555          else if( CLEAN_DISP_FLAG || cleanAlarm )
8101  0ed4 7200000005    	btjt	_Disp_Flag1,#0,L5234
8103  0ed9 c60000        	ld	a,_cleanAlarm
8104  0edc 2706          	jreq	L3234
8105  0ede               L5234:
8106                     ; 1557            CLean_Disp_Standby( );
8108  0ede cd0412        	call	_CLean_Disp_Standby
8111  0ee1 cc0f91        	jra	L7634
8112  0ee4               L3234:
8113                     ; 1561              if( Brew_Select == NO_BASKET )
8115  0ee4 c60000        	ld	a,_Brew_Select
8116  0ee7 27f8          	jreq	L7634
8118                     ; 1566                  if( COFFEE == Brew_Select )
8120  0ee9 4a            	dec	a
8121  0eea 2616          	jrne	L5334
8122                     ; 1568                    Tea_Coffee_Dim_Disp( DIM10 );
8124  0eec a60a          	ld	a,#10
8125  0eee cd01ae        	call	_Tea_Coffee_Dim_Disp
8127                     ; 1569                    Coffee_Style_Dim_Disp( DIM10 );
8129  0ef1 a60a          	ld	a,#10
8130  0ef3 cd01c1        	call	_Coffee_Style_Dim_Disp
8132                     ; 1570                    Tea_Coffee_Same_Disp( LED_ON);
8134  0ef6 a664          	ld	a,#100
8135  0ef8 cd0292        	call	_Tea_Coffee_Same_Disp
8137                     ; 1571                    Coffee_Disp( LED_ON );
8139  0efb a664          	ld	a,#100
8140  0efd cd034d        	call	_Coffee_Disp
8143  0f00 2014          	jra	L7334
8144  0f02               L5334:
8145                     ; 1575                    Tea_Coffee_Dim_Disp( DIM10 );
8147  0f02 a60a          	ld	a,#10
8148  0f04 cd01ae        	call	_Tea_Coffee_Dim_Disp
8150                     ; 1576                    Tea_Style_Type_Dim_Disp( DIM10 );
8152  0f07 a60a          	ld	a,#10
8153  0f09 cd01d4        	call	_Tea_Style_Type_Dim_Disp
8155                     ; 1577                    Tea_Coffee_Same_Disp( LED_ON );
8157  0f0c a664          	ld	a,#100
8158  0f0e cd0292        	call	_Tea_Coffee_Same_Disp
8160                     ; 1578                    Tea_Disp( LED_ON );
8162  0f11 a664          	ld	a,#100
8163  0f13 cd0351        	call	_Tea_Disp
8165  0f16               L7334:
8166                     ; 1580                  if( CLean_Blink_10count )
8168  0f16 c60000        	ld	a,_CLean_Blink_10count
8169  0f19 2712          	jreq	L1434
8170                     ; 1582                    if( _400ms_flag )
8172  0f1b 7203000071    	btjf	_Time_Flag,#1,L7634
8173                     ; 1584                      if( CLean_Class == 1 )
8175  0f20 c60000        	ld	a,_CLean_Class
8176  0f23 a101          	cp	a,#1
8177  0f25 261b          	jrne	L1634
8178                     ; 1585                       Led_Clean2  = LED_ON;
8180  0f27 35640004      	mov	_Led_Clean2,#100
8182  0f2b 2064          	jra	L7634
8183                     ; 1586                      else if( CLean_Class == 2 )
8184                     ; 1587                       Led_Clean   = LED_ON;
8185  0f2d               L1434:
8186                     ; 1592                      if( CLean_Class == 0 )
8188  0f2d c60000        	ld	a,_CLean_Class
8189  0f30 2606          	jrne	L5534
8190                     ; 1593                       Led_Clean2  = DIM;
8192  0f32 35050004      	mov	_Led_Clean2,#5
8194  0f36 2059          	jra	L7634
8195  0f38               L5534:
8196                     ; 1594                      else if( CLean_Class == 1 )
8198  0f38 a101          	cp	a,#1
8199  0f3a 2606          	jrne	L1634
8200                     ; 1595                       Led_Clean2  = LED_ON;
8202  0f3c 35640004      	mov	_Led_Clean2,#100
8204  0f40 204f          	jra	L7634
8205  0f42               L1634:
8206                     ; 1596                      else if( CLean_Class == 2 )
8208                     ; 1597                       Led_Clean   = LED_ON;
8211  0f42 a102          	cp	a,#2
8212  0f44 264b          	jrne	L7634
8214  0f46 35640021      	mov	_Led_Clean,#100
8215  0f4a 2045          	jra	L7634
8216  0f4c               L7614:
8217                     ; 1604    else if( DIM_STATUS ==System_Status )
8219  0f4c 4a            	dec	a
8220  0f4d 2642          	jrne	L7634
8221                     ; 1606           if( Brew_Select_B == NO_BASKET )
8223  0f4f c60000        	ld	a,_Brew_Select_B
8224  0f52 273d          	jreq	L7634
8226                     ; 1610               if( DLY_BREW_FUNC )
8228  0f54 720a000034    	btjt	_Disp_Flag,#5,L1044
8230                     ; 1615                 if( COFFEE == Brew_Select )
8232  0f59 c60000        	ld	a,_Brew_Select
8233  0f5c 4a            	dec	a
8234  0f5d 2616          	jrne	L3044
8235                     ; 1617                    Tea_Coffee_Dim_Disp( DIM );
8237  0f5f a605          	ld	a,#5
8238  0f61 cd01ae        	call	_Tea_Coffee_Dim_Disp
8240                     ; 1618                    Coffee_Style_Dim_Disp( DIM );
8242  0f64 a605          	ld	a,#5
8243  0f66 cd01c1        	call	_Coffee_Style_Dim_Disp
8245                     ; 1619                    Tea_Coffee_Same_Disp( DIM10);
8247  0f69 a60a          	ld	a,#10
8248  0f6b cd0292        	call	_Tea_Coffee_Same_Disp
8250                     ; 1620                    Coffee_Disp( DIM10 );
8252  0f6e a60a          	ld	a,#10
8253  0f70 cd034d        	call	_Coffee_Disp
8256  0f73 2014          	jra	L5044
8257  0f75               L3044:
8258                     ; 1624                    Tea_Coffee_Dim_Disp( DIM );
8260  0f75 a605          	ld	a,#5
8261  0f77 cd01ae        	call	_Tea_Coffee_Dim_Disp
8263                     ; 1625                    Tea_Style_Type_Dim_Disp( DIM );
8265  0f7a a605          	ld	a,#5
8266  0f7c cd01d4        	call	_Tea_Style_Type_Dim_Disp
8268                     ; 1626                    Tea_Coffee_Same_Disp( DIM10 );
8270  0f7f a60a          	ld	a,#10
8271  0f81 cd0292        	call	_Tea_Coffee_Same_Disp
8273                     ; 1627                    Tea_Disp( DIM10 );
8275  0f84 a60a          	ld	a,#10
8276  0f86 cd0351        	call	_Tea_Disp
8278  0f89               L5044:
8279                     ; 1629                  Led_Clean2  = DIM;
8281  0f89 35050004      	mov	_Led_Clean2,#5
8282  0f8d               L1044:
8283                     ; 1631               Led_Power = DIM10;
8285  0f8d 350a0020      	mov	_Led_Power,#10
8286  0f91               L7634:
8287                     ; 1636    DebugMessage_Disp( );
8289  0f91 cd0a85        	call	_DebugMessage_Disp
8291                     ; 1637    Digit_Figure( );
8293  0f94 cd00b1        	call	_Digit_Figure
8295                     ; 1638    DisplayDriver( );
8298                     ; 1639 }
8301  0f97 cc0000        	jp	_DisplayDriver
8345                     	xdef	_Run_Disp
8346                     	xdef	_Pause_Disp
8347                     	xdef	_DebugMessage_Disp
8348                     	xdef	_Error_Code_Disp
8349                     	xdef	_FlowRate_Disp
8350                     	xdef	_Disp_Temperature
8351                     	xdef	_Delay_Brew_Disp
8352                     	xdef	_Delay_Brew_Clock_Disp
8353                     	xdef	_Keep_Warm_Level_Disp
8354                     	xdef	_Keep_Warm_Clock_Disp
8355                     	xdef	_Stay_Warm_Disp
8356                     	xdef	_CLean_Disp_Run
8357                     	xdef	_Pause_Clock_Disp
8358                     	xdef	_CLean_Disp_Standby
8359                     	xdef	_Tea_DIM_Disp
8360                     	xdef	_Coffee_DIM_Disp
8361                     	xdef	_Tea_Type_Disp
8362                     	xdef	_Tea_Disp
8363                     	xdef	_Coffee_Disp
8364                     	xdef	_Tea_Coffee_Same_Disp
8365                     	xdef	_DelayBrew_Tea_Dim_Disp
8366                     	xdef	_Tea_Style_Type_Dim_Disp
8367                     	xdef	_Coffee_Style_Dim_Disp
8368                     	xdef	_Tea_Coffee_Dim_Disp
8369                     	xdef	_Digit_Figure
8370                     	xdef	_Clock_Disp
8371                     	xdef	_hour_min
8372                     	xdef	_DisplayDriver
8373                     	switch	.ubsct
8374  0000               _digImage:
8375  0000 00000000      	ds.b	4
8376                     	xdef	_digImage
8377                     	xdef	_digCode
8378                     	xref	_Disp_Flag1
8379                     	xref	_Disp_Flag
8380                     	xref	_Time_Flag
8381                     	xref	_debugValueEnable
8382                     	xref	_debugIndex
8383                     	xref	_debugEnable
8384                     	xref	_cleanAlarm
8385                     	xref	_delayExitCnt
8386                     	xref	_delayNoaction3S
8387                     	xref	_delayAdjust15S
8388                     	xref	_delayBrewPause
8389                     	xref	_brewExitCnt
8390                     	xref	_brewNoaction3S
8391                     	xref	_brewAdjust15S
8392                     	xref	_Delay_Brew_Time
8393                     	xref	_Delay_Keep_Warm_Level
8394                     	xref	_Delay_Keep_Warm_Time
8395                     	xref	_Clock_Stay_Time
8396                     	xref	_Clock_Set_Time
8397                     	xref	_Paused_Delay
8398                     	xref	_Style_Type_Flag
8399                     	xref	_Keep_Warm_Class
8400                     	xref	_Brew_Select_B
8401                     	xref	_Brew_Select
8402                     	xref	_Start_Status
8403                     	xref	_System_Status
8404                     	xref	_Clock_Keep_Warm
8405                     	xref	_Clock_Clean
8406                     	xref	_Clock_DLY_BREW
8407                     	xref	_Clock
8408                     	xref	_Knob2_count
8409                     	xref	_Knob1_count
8410                     	xref	_Knob0_count
8411                     	xref	_errorCode0
8412                     	xref	_CLean_Blink_10count
8413                     	xref	_CLean_Class
8414                     	xref	_Brew_Bar
8415                     	xref	_debugMessage
8416                     	xref	_Ctrl_Board_Status
8417                     	xref	_Pt696xWr
8418                     	xref	_Pt696xCmd
8419                     	xdef	_LED_DISP
8420                     	switch	.bss
8421  0000               _Led_Travel:
8422  0000 00            	ds.b	1
8423                     	xdef	_Led_Travel
8424  0001               _Led_Coffee:
8425  0001 00            	ds.b	1
8426                     	xdef	_Led_Coffee
8427  0002               _Led_Tea:
8428  0002 00            	ds.b	1
8429                     	xdef	_Led_Tea
8430  0003               _Led_Keepwarm:
8431  0003 00            	ds.b	1
8432                     	xdef	_Led_Keepwarm
8433  0004               _Led_Clean2:
8434  0004 00            	ds.b	1
8435                     	xdef	_Led_Clean2
8436  0005               _Led_Delayset:
8437  0005 00            	ds.b	1
8438                     	xdef	_Led_Delayset
8439  0006               _Led_Insertbasket:
8440  0006 00            	ds.b	1
8441                     	xdef	_Led_Insertbasket
8442  0007               _Led_Addone:
8443  0007 00            	ds.b	1
8444                     	xdef	_Led_Addone
8445  0008               _Led_BAR1:
8446  0008 00            	ds.b	1
8447                     	xdef	_Led_BAR1
8448  0009               _Led_Fullcarafe:
8449  0009 00            	ds.b	1
8450                     	xdef	_Led_Fullcarafe
8451  000a               _Led_Cup:
8452  000a 00            	ds.b	1
8453                     	xdef	_Led_Cup
8454  000b               _Led_Halfcarafe:
8455  000b 00            	ds.b	1
8456                     	xdef	_Led_Halfcarafe
8457  000c               _Led_Xlcup:
8458  000c 00            	ds.b	1
8459                     	xdef	_Led_Xlcup
8460  000d               _Led_Xltravel:
8461  000d 00            	ds.b	1
8462                     	xdef	_Led_Xltravel
8463  000e               _Led_Coldbrew:
8464  000e 00            	ds.b	1
8465                     	xdef	_Led_Coldbrew
8466  000f               _Led_Oolong:
8467  000f 00            	ds.b	1
8468                     	xdef	_Led_Oolong
8469  0010               _Led_Overice:
8470  0010 00            	ds.b	1
8471                     	xdef	_Led_Overice
8472  0011               _Led_Black:
8473  0011 00            	ds.b	1
8474                     	xdef	_Led_Black
8475  0012               _Led_Rich:
8476  0012 00            	ds.b	1
8477                     	xdef	_Led_Rich
8478  0013               _Led_Herbal:
8479  0013 00            	ds.b	1
8480                     	xdef	_Led_Herbal
8481  0014               _Led_Classic:
8482  0014 00            	ds.b	1
8483                     	xdef	_Led_Classic
8484  0015               _Led_Start:
8485  0015 00            	ds.b	1
8486                     	xdef	_Led_Start
8487  0016               _Led_Style:
8488  0016 00            	ds.b	1
8489                     	xdef	_Led_Style
8490  0017               _Led_Delicate:
8491  0017 00            	ds.b	1
8492                     	xdef	_Led_Delicate
8493  0018               _Led_Immersion:
8494  0018 00            	ds.b	1
8495                     	xdef	_Led_Immersion
8496  0019               _Led_Green:
8497  0019 00            	ds.b	1
8498                     	xdef	_Led_Green
8499  001a               _Led_Speciality:
8500  001a 00            	ds.b	1
8501                     	xdef	_Led_Speciality
8502  001b               _Led_White:
8503  001b 00            	ds.b	1
8504                     	xdef	_Led_White
8505  001c               _Led_BAR5:
8506  001c 00            	ds.b	1
8507                     	xdef	_Led_BAR5
8508  001d               _Led_BAR4:
8509  001d 00            	ds.b	1
8510                     	xdef	_Led_BAR4
8511  001e               _Led_BAR3:
8512  001e 00            	ds.b	1
8513                     	xdef	_Led_BAR3
8514  001f               _Led_BAR2:
8515  001f 00            	ds.b	1
8516                     	xdef	_Led_BAR2
8517  0020               _Led_Power:
8518  0020 00            	ds.b	1
8519                     	xdef	_Led_Power
8520  0021               _Led_Clean:
8521  0021 00            	ds.b	1
8522                     	xdef	_Led_Clean
8523  0022               _Led_Type:
8524  0022 00            	ds.b	1
8525                     	xdef	_Led_Type
8526  0023               _ledMap:
8527  0023 000000000000  	ds.b	8
8528                     	xdef	_ledMap
8529                     	xref	_memset
8549                     	xref	c_idiv
8550                     	xref	c_smodx
8551                     	xref	c_sdivx
8552                     	end

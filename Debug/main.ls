   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   3                     ; Optimizer V4.2.4 - 18 Dec 2007
2777                     	switch	.data
2778  0000               L3671_sysTicks10ms:
2779  0000 00            	dc.b	0
2832                     ; 19 void main( void )
2832                     ; 20   {
2834                     	switch	.text
2835  0000               _main:
2839                     ; 22     MCU_Init( );
2841  0000 cd0000        	call	_MCU_Init
2843                     ; 23     COM_ODU_Init( );
2845  0003 cd0000        	call	_COM_ODU_Init
2847                     ; 24     Key_Init( );
2849  0006 cd0000        	call	_Key_Init
2851                     ; 26     sysTicks = 0;
2853  0009 725f0000      	clr	_sysTicks
2854  000d               L7002:
2855                     ; 29         __EI( );
2858  000d 9a            	rim	
2860                     ; 30         if ( sysTicks >= 50 )
2862  000e c60000        	ld	a,_sysTicks
2863  0011 a132          	cp	a,#50
2864  0013 25f8          	jrult	L7002
2865                     ; 32             __DI( );
2868  0015 9b            	sim	
2870                     ; 33             sysTicks -= 50;
2872  0016 a032          	sub	a,#50
2873  0018 c70000        	ld	_sysTicks,a
2874                     ; 34             __EI( );
2877  001b 9a            	rim	
2879                     ; 36             if( ++sysTicks10ms >= 10 )
2881  001c 725c0000      	inc	L3671_sysTicks10ms
2882  0020 c60000        	ld	a,L3671_sysTicks10ms
2883  0023 a10a          	cp	a,#10
2884  0025 251a          	jrult	L5102
2885                     ; 38               __WDTC( );
2887  0027 35aa50e0      	mov	_IWDG_KR,#170
2888                     ; 39               sysTicks10ms = 0;
2890  002b 725f0000      	clr	L3671_sysTicks10ms
2891                     ; 40              MCU_Refresh( );
2893  002f cd0000        	call	_MCU_Refresh
2895                     ; 41              Time_Deal( );
2897  0032 cd0000        	call	_Time_Deal
2899                     ; 42              Key_Deal( );
2901  0035 cd0000        	call	_Key_Deal
2903                     ; 43              SYS_Ctrl( );
2905  0038 cd0000        	call	_SYS_Ctrl
2907                     ; 44              LED_DISP( );
2909  003b cd0000        	call	_LED_DISP
2911                     ; 45              BeepCtrl( );
2913  003e cd0000        	call	_BeepCtrl
2915  0041               L5102:
2916                     ; 47             Size_Rotary_Code( );
2918  0041 cd0000        	call	_Size_Rotary_Code
2920                     ; 48             Style_Type_Rotary_Code( );
2922  0044 cd0000        	call	_Style_Type_Rotary_Code
2924                     ; 49             COM_ODU_Ctrl( );
2926  0047 cd0000        	call	_COM_ODU_Ctrl
2928  004a 20c1          	jra	L7002
2941                     	xdef	_main
2942                     	xref	_BeepCtrl
2943                     	xref	_LED_DISP
2944                     	xref	_Style_Type_Rotary_Code
2945                     	xref	_Size_Rotary_Code
2946                     	xref	_Key_Deal
2947                     	xref	_Key_Init
2948                     	xref	_Time_Deal
2949                     	xref	_COM_ODU_Ctrl
2950                     	xref	_COM_ODU_Init
2951                     	xref	_SYS_Ctrl
2952                     	xref	_MCU_Refresh
2953                     	xref	_MCU_Init
2954                     	xref	_sysTicks
2973                     	end

   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   3                     ; Optimizer V4.2.4 - 18 Dec 2007
2834                     .const:	section	.text
2835  0000               _adNTC_3480_10K:
2836  0000 0274          	dc.w	628
2837  0002 0294          	dc.w	660
2838  0004 02b5          	dc.w	693
2839  0006 02d7          	dc.w	727
2840  0008 02fb          	dc.w	763
2841  000a 031f          	dc.w	799
2842  000c 0345          	dc.w	837
2843  000e 036d          	dc.w	877
2844  0010 0396          	dc.w	918
2845  0012 03c0          	dc.w	960
2846  0014 03eb          	dc.w	1003
2847  0016 0418          	dc.w	1048
2848  0018 0446          	dc.w	1094
2849  001a 0476          	dc.w	1142
2850  001c 04a7          	dc.w	1191
2851  001e 04d9          	dc.w	1241
2852  0020 050d          	dc.w	1293
2853  0022 0542          	dc.w	1346
2854  0024 0579          	dc.w	1401
2855  0026 05b1          	dc.w	1457
2856  0028 05eb          	dc.w	1515
2857  002a 0625          	dc.w	1573
2858  002c 0661          	dc.w	1633
2859  002e 069f          	dc.w	1695
2860  0030 06de          	dc.w	1758
2861  0032 071e          	dc.w	1822
2862  0034 075f          	dc.w	1887
2863  0036 07a1          	dc.w	1953
2864  0038 07e5          	dc.w	2021
2865  003a 082a          	dc.w	2090
2866  003c 086f          	dc.w	2159
2867  003e 08b6          	dc.w	2230
2868  0040 08fe          	dc.w	2302
2869  0042 0947          	dc.w	2375
2870  0044 0990          	dc.w	2448
2871  0046 09db          	dc.w	2523
2872  0048 0a26          	dc.w	2598
2873  004a 0a73          	dc.w	2675
2874  004c 0abf          	dc.w	2751
2875  004e 0b0d          	dc.w	2829
2876  0050 0b5a          	dc.w	2906
2877  0052 0ba8          	dc.w	2984
2878  0054 0bf8          	dc.w	3064
2879  0056 0c46          	dc.w	3142
2880  0058 0c95          	dc.w	3221
2881  005a 0ce6          	dc.w	3302
2882  005c 0d35          	dc.w	3381
2883  005e 0d85          	dc.w	3461
2884  0060 0dd6          	dc.w	3542
2885  0062 0e26          	dc.w	3622
2886  0064 0e74          	dc.w	3700
2887  0066 0ec4          	dc.w	3780
2888  0068 0f15          	dc.w	3861
2889  006a 0f62          	dc.w	3938
2890  006c 0fb2          	dc.w	4018
2891  006e 1000          	dc.w	4096
2892  0070 104e          	dc.w	4174
2893  0072 109b          	dc.w	4251
2894  0074 10e8          	dc.w	4328
2895  0076 1135          	dc.w	4405
2896  0078 1180          	dc.w	4480
2897  007a 11cb          	dc.w	4555
2898  007c 1216          	dc.w	4630
2899  007e 125f          	dc.w	4703
2900  0080 12a8          	dc.w	4776
2901  0082 12f0          	dc.w	4848
2902  0084 1337          	dc.w	4919
2903  0086 137d          	dc.w	4989
2904  0088 13c2          	dc.w	5058
2905  008a 1406          	dc.w	5126
2906  008c 1453          	dc.w	5203
2907  008e 148c          	dc.w	5260
2908  0090 14cc          	dc.w	5324
2909  0092 150d          	dc.w	5389
2910  0094 154c          	dc.w	5452
2911  0096 158a          	dc.w	5514
2912  0098 15c7          	dc.w	5575
2913  009a 1603          	dc.w	5635
2914  009c 163e          	dc.w	5694
2915  009e 1678          	dc.w	5752
2916  00a0 16b1          	dc.w	5809
2917  00a2 16e8          	dc.w	5864
2918  00a4 171f          	dc.w	5919
2919  00a6 1755          	dc.w	5973
2920  00a8 1789          	dc.w	6025
2921  00aa 17bc          	dc.w	6076
2922  00ac 17ef          	dc.w	6127
2923  00ae 1820          	dc.w	6176
2924  00b0 1850          	dc.w	6224
2925  00b2 187f          	dc.w	6271
2926  00b4 18ae          	dc.w	6318
2927  00b6 18db          	dc.w	6363
2928  00b8 1907          	dc.w	6407
2929  00ba 1932          	dc.w	6450
2930  00bc 195c          	dc.w	6492
2931  00be 1985          	dc.w	6533
2932  00c0 19ad          	dc.w	6573
2933  00c2 19d4          	dc.w	6612
2934  00c4 19fb          	dc.w	6651
2935  00c6 1a20          	dc.w	6688
2936  00c8 1a45          	dc.w	6725
2937  00ca 1a69          	dc.w	6761
2938  00cc 1a8c          	dc.w	6796
2939  00ce 1aae          	dc.w	6830
2940  00d0 1ace          	dc.w	6862
2941  00d2 1aee          	dc.w	6894
3010                     ; 123 S16  TPR_3480_10K( U16 ad )
3010                     ; 124   {
3012                     	switch	.text
3013  0000               _TPR_3480_10K:
3015  0000 89            	pushw	x
3016  0001 5206          	subw	sp,#6
3017       00000006      OFST:	set	6
3020                     ; 126     if ( ad < adNTC_3480_10K[ 0 ] )
3022  0003 a30274        	cpw	x,#628
3023  0006 2405          	jruge	L1502
3024                     ; 128         return TPRREF( -30 );
3026  0008 aee200        	ldw	x,#57856
3028  000b 200a          	jra	L6
3029  000d               L1502:
3030                     ; 130     if ( ad >= adNTC_3480_10K[ LENOF( adNTC_3480_10K ) - 1 ] )
3032  000d 1e07          	ldw	x,(OFST+1,sp)
3033  000f a31aee        	cpw	x,#6894
3034  0012 2506          	jrult	L3502
3035                     ; 132         return TPRREF( -30 + LENOF( adNTC_3480_10K ) - 1 );
3037  0014 ae4b00        	ldw	x,#19200
3039  0017               L6:
3041  0017 5b08          	addw	sp,#8
3042  0019 81            	ret	
3043  001a               L3502:
3044                     ; 134     for ( a = 0, b = 0x80; b != 0; b >>= 1 )
3046  001a 5f            	clrw	x
3047  001b 1f03          	ldw	(OFST-3,sp),x
3048  001d ae0080        	ldw	x,#128
3049  0020 1f05          	ldw	(OFST-1,sp),x
3050  0022               L5502:
3051                     ; 136         if ( a + b >= LENOF( adNTC_3480_10K ) ) continue;
3053  0022 1e03          	ldw	x,(OFST-3,sp)
3054  0024 72fb05        	addw	x,(OFST-1,sp)
3055  0027 a3006a        	cpw	x,#106
3056  002a 240f          	jruge	L7502
3059                     ; 137         if ( ad >= adNTC_3480_10K[ a + b ] ) a += b;
3061  002c 58            	sllw	x
3062  002d de0000        	ldw	x,(_adNTC_3480_10K,x)
3063  0030 1307          	cpw	x,(OFST+1,sp)
3064  0032 2207          	jrugt	L7502
3067  0034 1e03          	ldw	x,(OFST-3,sp)
3068  0036 72fb05        	addw	x,(OFST-1,sp)
3069  0039 1f03          	ldw	(OFST-3,sp),x
3070  003b               L7502:
3071                     ; 134     for ( a = 0, b = 0x80; b != 0; b >>= 1 )
3073  003b 0405          	srl	(OFST-1,sp)
3074  003d 0606          	rrc	(OFST+0,sp)
3077  003f 1e05          	ldw	x,(OFST-1,sp)
3078  0041 26df          	jrne	L5502
3079                     ; 139     c = ad - adNTC_3480_10K[ a ];
3081  0043 1e07          	ldw	x,(OFST+1,sp)
3082  0045 1603          	ldw	y,(OFST-3,sp)
3083  0047 9058          	sllw	y
3084  0049 90de0000      	ldw	y,(_adNTC_3480_10K,y)
3085  004d 90bf00        	ldw	c_x,y
3086  0050 72b00000      	subw	x,c_x
3087  0054 1f01          	ldw	(OFST-5,sp),x
3088                     ; 140     b = adNTC_3480_10K[ a + 1 ] - adNTC_3480_10K[ a ];
3090  0056 1e03          	ldw	x,(OFST-3,sp)
3091  0058 58            	sllw	x
3092  0059 de0002        	ldw	x,(_adNTC_3480_10K+2,x)
3093  005c 1603          	ldw	y,(OFST-3,sp)
3094  005e 9058          	sllw	y
3095  0060 90de0000      	ldw	y,(_adNTC_3480_10K,y)
3096  0064 90bf00        	ldw	c_x,y
3097  0067 72b00000      	subw	x,c_x
3098  006b 1f05          	ldw	(OFST-1,sp),x
3100  006d 2008          	jra	L3702
3101  006f               L7602:
3102                     ; 143         c >>= 1;
3104  006f 0401          	srl	(OFST-5,sp)
3105  0071 0602          	rrc	(OFST-4,sp)
3106                     ; 144         b >>= 1;
3108  0073 0405          	srl	(OFST-1,sp)
3109  0075 0606          	rrc	(OFST+0,sp)
3110  0077               L3702:
3111                     ; 141     while ( c >= 256 )
3113  0077 1e01          	ldw	x,(OFST-5,sp)
3114  0079 a30100        	cpw	x,#256
3115  007c 24f1          	jruge	L7602
3116                     ; 146     b = ( c << 8 ) / b;// (c/b)  *8 小数点也放大256倍
3118  007e 4f            	clr	a
3119  007f 02            	rlwa	x,a
3120  0080 1605          	ldw	y,(OFST-1,sp)
3121  0082 65            	divw	x,y
3122  0083 1f05          	ldw	(OFST-1,sp),x
3123                     ; 147     return ( S16 )( ( a - 30 << 8 ) + b );// 求出来的(温度值+小数点)都已放大256倍
3125  0085 1e03          	ldw	x,(OFST-3,sp)
3126  0087 4f            	clr	a
3127  0088 02            	rlwa	x,a
3128  0089 1d1e00        	subw	x,#7680
3129  008c 72fb05        	addw	x,(OFST-1,sp)
3131  008f 2086          	jra	L6
3134                     	bsct
3135  0000               L7702_adCnt:
3136  0000 00            	dc.b	0
3137  0001               L1012_adErr:
3138  0001 00            	dc.b	0
3139                     	switch	.data
3140  0000               L3012_adMin:
3141  0000 0000          	dc.w	0
3142  0002               L5012_adMax:
3143  0002 0000          	dc.w	0
3144  0004               L7012_adSum:
3145  0004 0000          	dc.w	0
3201                     ; 156 U8   ADC_Collect( U8 chn )
3201                     ; 157   {
3202                     	switch	.text
3203  0091               _ADC_Collect:
3205  0091 5203          	subw	sp,#3
3206       00000003      OFST:	set	3
3209                     ; 158     U16 ad = ADC_Convert( chn );
3211  0093 cd0000        	call	_ADC_Convert
3213  0096 1f02          	ldw	(OFST-1,sp),x
3214                     ; 159     U8  N = adCnt % 10;
3216  0098 b600          	ld	a,L7702_adCnt
3217  009a ae000a        	ldw	x,#10
3218  009d 9093          	ldw	y,x
3219  009f 5f            	clrw	x
3220  00a0 97            	ld	xl,a
3221  00a1 65            	divw	x,y
3222  00a2 909f          	ld	a,yl
3223  00a4 6b01          	ld	(OFST-2,sp),a
3224                     ; 160     if ( N == 0 )
3226  00a6 2614          	jrne	L7312
3227                     ; 162         adMin = ad;
3229  00a8 1e02          	ldw	x,(OFST-1,sp)
3230  00aa cf0000        	ldw	L3012_adMin,x
3231                     ; 163         adMax = ad;
3233  00ad cf0002        	ldw	L5012_adMax,x
3234                     ; 164         adSum = ad;
3236  00b0 cf0004        	ldw	L7012_adSum,x
3237                     ; 165         adErr = 0;
3239  00b3 b701          	ld	L1012_adErr,a
3240                     ; 166         if ( ad < 20U || ad >= 1000U ) adErr++;
3242  00b5 a30014        	cpw	x,#20
3243  00b8 252d          	jrult	L5512
3246  00ba 2026          	jp	LC002
3247  00bc               L7312:
3248                     ; 170         adSum += ad;
3250  00bc ce0004        	ldw	x,L7012_adSum
3251  00bf 72fb02        	addw	x,(OFST-1,sp)
3252  00c2 cf0004        	ldw	L7012_adSum,x
3253                     ; 171         if ( ad < adMin ) adMin = ad;
3255  00c5 1e02          	ldw	x,(OFST-1,sp)
3256  00c7 c30000        	cpw	x,L3012_adMin
3257  00ca 2403          	jruge	L7412
3260  00cc cf0000        	ldw	L3012_adMin,x
3261  00cf               L7412:
3262                     ; 172         if ( adMax < ad ) adMax = ad;
3264  00cf ce0002        	ldw	x,L5012_adMax
3265  00d2 1302          	cpw	x,(OFST-1,sp)
3266  00d4 2405          	jruge	L1512
3269  00d6 1e02          	ldw	x,(OFST-1,sp)
3270  00d8 cf0002        	ldw	L5012_adMax,x
3271  00db               L1512:
3272                     ; 173         if ( ad < 20U || ad >= 1000U ) adErr++;
3274  00db 1e02          	ldw	x,(OFST-1,sp)
3275  00dd a30014        	cpw	x,#20
3276  00e0 2505          	jrult	L5512
3278  00e2               LC002:
3279  00e2 a303e8        	cpw	x,#1000
3280  00e5 2502          	jrult	L5412
3281  00e7               L5512:
3285  00e7 3c01          	inc	L1012_adErr
3286  00e9               L5412:
3287                     ; 175     return ( N == 9 ? 1 : 0 );
3289  00e9 7b01          	ld	a,(OFST-2,sp)
3290  00eb a109          	cp	a,#9
3291  00ed 2604          	jrne	L41
3292  00ef a601          	ld	a,#1
3293  00f1 2001          	jra	L61
3294  00f3               L41:
3295  00f3 4f            	clr	a
3296  00f4               L61:
3299  00f4 5b03          	addw	sp,#3
3300  00f6 81            	ret	
3370                     ; 178 void ADC_2_TPR( TPR * p )
3370                     ; 179   {
3371                     	switch	.text
3372  00f7               _ADC_2_TPR:
3374  00f7 89            	pushw	x
3375       00000000      OFST:	set	0
3378                     ; 180     if ( adErr == 0 )
3380  00f8 b601          	ld	a,L1012_adErr
3381  00fa 2620          	jrne	L1122
3382                     ; 182         if ( p->cnt == 0 )
3384  00fc 6d03          	tnz	(3,x)
3385  00fe 2616          	jrne	L3122
3386                     ; 184             p->err = 0;
3388  0100 e702          	ld	(2,x),a
3389                     ; 185             p->val = TPR_3480_10K( adSum - adMin - adMax );
3391  0102 ce0004        	ldw	x,L7012_adSum
3392  0105 72b00000      	subw	x,L3012_adMin
3393  0109 72b00002      	subw	x,L5012_adMax
3394  010d cd0000        	call	_TPR_3480_10K
3396  0110 1601          	ldw	y,(OFST+1,sp)
3397  0112 90ff          	ldw	(y),x
3399  0114 2014          	jra	L7122
3400  0116               L3122:
3401                     ; 189             p->cnt = 0;
3403  0116 1e01          	ldw	x,(OFST+1,sp)
3404  0118 6f03          	clr	(3,x)
3405  011a 200e          	jra	L7122
3406  011c               L1122:
3407                     ; 194         if ( ++p->cnt >= 20 )
3409  011c 1e01          	ldw	x,(OFST+1,sp)
3410  011e 6c03          	inc	(3,x)
3411  0120 e603          	ld	a,(3,x)
3412  0122 a114          	cp	a,#20
3413  0124 2504          	jrult	L7122
3414                     ; 196             p->err = 1;
3416  0126 a601          	ld	a,#1
3417  0128 e702          	ld	(2,x),a
3418  012a               L7122:
3419                     ; 199   }
3422  012a 85            	popw	x
3423  012b 81            	ret	
3447                     ; 201 void NTC_Init( void )
3447                     ; 202   {
3448                     	switch	.text
3449  012c               _NTC_Init:
3453                     ; 203     tprRoom.val = 0;
3455  012c 5f            	clrw	x
3456  012d bf00          	ldw	_tprRoom,x
3457                     ; 204     tprRoom.err = 0;
3459  012f 3f02          	clr	_tprRoom+2
3460                     ; 205     tprRoom.cnt = 0;
3462  0131 3f03          	clr	_tprRoom+3
3463                     ; 206   }
3466  0133 81            	ret	
3493                     ; 208 void NTC_Check( void )
3493                     ; 209   {
3494                     	switch	.text
3495  0134               _NTC_Check:
3499                     ; 212     if ( adCnt < 10 )
3501  0134 b600          	ld	a,L7702_adCnt
3502  0136 a10a          	cp	a,#10
3503  0138 240d          	jruge	L3422
3504                     ; 215         if ( ADC_Collect( AIN_TPR_ROOM ) )
3506  013a a605          	ld	a,#5
3507  013c cd0091        	call	_ADC_Collect
3509  013f 4d            	tnz	a
3510  0140 2705          	jreq	L3422
3511                     ; 217             ADC_2_TPR( &tprRoom );
3513  0142 ae0000        	ldw	x,#_tprRoom
3514  0145 adb0          	call	_ADC_2_TPR
3516  0147               L3422:
3517                     ; 221     if ( ++adCnt >= 50 ) adCnt = 0;
3519  0147 3c00          	inc	L7702_adCnt
3520  0149 b600          	ld	a,L7702_adCnt
3521  014b a132          	cp	a,#50
3522  014d 2502          	jrult	L7422
3525  014f 3f00          	clr	L7702_adCnt
3526  0151               L7422:
3527                     ; 222   }
3530  0151 81            	ret	
3600                     	xdef	_ADC_2_TPR
3601                     	xdef	_ADC_Collect
3602                     	xdef	_TPR_3480_10K
3603                     	xdef	_adNTC_3480_10K
3604                     	xdef	_NTC_Check
3605                     	xdef	_NTC_Init
3606                     	switch	.ubsct
3607  0000               _tprRoom:
3608  0000 00000000      	ds.b	4
3609                     	xdef	_tprRoom
3610                     	xref	_ADC_Convert
3611                     	xref.b	c_x
3631                     	end

   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   3                     ; Optimizer V4.2.4 - 18 Dec 2007
2808                     ; 21 void Pt696xStart( void )
2808                     ; 22   {
2810                     	switch	.text
2811  0000               _Pt696xStart:
2815                     ; 23     PT_TB_OUT( );
2818  0000 721e5011      	bset	_PD_DDR,#7
2820                     ; 24     PT_CK_OUT( );
2823  0004 7218501b      	bset	_PF_DDR,#4
2825                     ; 25     PIN_PT_TB_SET( );
2828  0008 721e500f      	bset	_PD_ODR,#7
2830                     ; 26     Pt696xDelay( );
2833  000c 9d            	nop	
2838  000d 9d            	nop	
2843  000e 9d            	nop	
2848  000f 9d            	nop	
2850                     ; 27     PIN_PT_TB_CLR( );
2853  0010 721f500f      	bres	_PD_ODR,#7
2855                     ; 28   }
2858  0014 81            	ret	
2899                     ; 32 void Pt696xSend( U8 dat )
2899                     ; 33   {
2900                     	switch	.text
2901  0015               _Pt696xSend:
2903  0015 88            	push	a
2904  0016 88            	push	a
2905       00000001      OFST:	set	1
2908                     ; 34     U8  bit = 0x01;
2910  0017 a601          	ld	a,#1
2911  0019 6b01          	ld	(OFST+0,sp),a
2912                     ; 35     PT_DI_OUT( );
2915  001b 721a5016      	bset	_PE_DDR,#5
2917  001f               L7302:
2918                     ; 37         PIN_PT_CK_CLR( );
2921  001f 72195019      	bres	_PF_ODR,#4
2923                     ; 38         if ( dat & bit )
2925  0023 7b02          	ld	a,(OFST+1,sp)
2926  0025 1501          	bcp	a,(OFST+0,sp)
2927  0027 2706          	jreq	L5402
2928                     ; 40             PIN_PT_DI_SET( );
2931  0029 721a5014      	bset	_PE_ODR,#5
2934  002d 2004          	jra	L1502
2935  002f               L5402:
2936                     ; 44             PIN_PT_DI_CLR( );
2939  002f 721b5014      	bres	_PE_ODR,#5
2941  0033               L1502:
2942                     ; 46         Pt696xDelay( );
2945  0033 9d            	nop	
2950  0034 9d            	nop	
2955  0035 9d            	nop	
2960  0036 9d            	nop	
2962                     ; 47         PIN_PT_CK_SET( );
2965  0037 72185019      	bset	_PF_ODR,#4
2967                     ; 48         Pt696xDelay( );
2970  003b 9d            	nop	
2975  003c 9d            	nop	
2980  003d 9d            	nop	
2985  003e 9d            	nop	
2987                     ; 49       } while ( bit<<=1 );
2989  003f 0801          	sll	(OFST+0,sp)
2990  0041 26dc          	jrne	L7302
2991                     ; 50   }
2994  0043 85            	popw	x
2995  0044 81            	ret	
3030                     ; 52 void Pt696xCmd( U8 cmd )
3030                     ; 53   {
3031                     	switch	.text
3032  0045               _Pt696xCmd:
3034  0045 88            	push	a
3035       00000000      OFST:	set	0
3038                     ; 54     Pt696xStart( );
3040  0046 adb8          	call	_Pt696xStart
3042                     ; 55     Pt696xSend( cmd );
3044  0048 7b01          	ld	a,(OFST+1,sp)
3045  004a adc9          	call	_Pt696xSend
3047                     ; 56     Pt696xStop( );
3050  004c 721e500f      	bset	_PD_ODR,#7
3052                     ; 57   }
3055  0050 84            	pop	a
3056  0051 81            	ret	
3108                     ; 59 void Pt696xWr( U8 addr, U8 *pImage, U8 size )
3108                     ; 60   {
3109                     	switch	.text
3110  0052               _Pt696xWr:
3112  0052 88            	push	a
3113       00000000      OFST:	set	0
3116                     ; 61     Pt696xStart( );
3118  0053 adab          	call	_Pt696xStart
3120                     ; 62     Pt696xSend( 0x40 );
3122  0055 a640          	ld	a,#64
3123  0057 adbc          	call	_Pt696xSend
3125                     ; 63     Pt696xStop( );
3128  0059 721e500f      	bset	_PD_ODR,#7
3130                     ; 64     Pt696xStart( );
3132  005d ada1          	call	_Pt696xStart
3134                     ; 65     Pt696xSend( 0xf0 | addr );
3136  005f 7b01          	ld	a,(OFST+1,sp)
3137  0061 aaf0          	or	a,#240
3138  0063 adb0          	call	_Pt696xSend
3141  0065 200e          	jra	L1212
3142  0067               L7112:
3143                     ; 68         __WDTC( );
3145  0067 35aa50e0      	mov	_IWDG_KR,#170
3146                     ; 69         Pt696xSend( *pImage );
3148  006b 1e04          	ldw	x,(OFST+4,sp)
3149  006d f6            	ld	a,(x)
3150  006e ada5          	call	_Pt696xSend
3152                     ; 70         pImage++;
3154  0070 1e04          	ldw	x,(OFST+4,sp)
3155  0072 5c            	incw	x
3156  0073 1f04          	ldw	(OFST+4,sp),x
3157  0075               L1212:
3158                     ; 66     while ( size-- )
3160  0075 7b06          	ld	a,(OFST+6,sp)
3161  0077 0a06          	dec	(OFST+6,sp)
3162  0079 4d            	tnz	a
3163  007a 26eb          	jrne	L7112
3164                     ; 72     Pt696xStop( );
3167  007c 721e500f      	bset	_PD_ODR,#7
3169                     ; 73   }
3172  0080 84            	pop	a
3173  0081 81            	ret	
3216                     ; 74 U8   Pt696xRecieve( void )
3216                     ; 75   {
3217                     	switch	.text
3218  0082               _Pt696xRecieve:
3220  0082 89            	pushw	x
3221       00000002      OFST:	set	2
3224                     ; 76     U8  dat = 0;
3226  0083 0f01          	clr	(OFST-1,sp)
3227                     ; 77     U8  bit = 0x01;
3229  0085 a601          	ld	a,#1
3230  0087 6b02          	ld	(OFST+0,sp),a
3231                     ; 78     PT_DO_IN( );
3234  0089 721b5016      	bres	_PE_DDR,#5
3236                     ; 79     Pt696xDelay( );
3239  008d 9d            	nop	
3244  008e 9d            	nop	
3249  008f 9d            	nop	
3254  0090 9d            	nop	
3256  0091               L3512:
3257                     ; 81         PIN_PT_CK_CLR( );
3260  0091 72195019      	bres	_PF_ODR,#4
3262                     ; 82         Pt696xDelay( );
3265  0095 9d            	nop	
3270  0096 9d            	nop	
3275  0097 9d            	nop	
3280  0098 9d            	nop	
3282                     ; 83         PIN_PT_CK_SET( );
3285  0099 72185019      	bset	_PF_ODR,#4
3287                     ; 84         Pt696xDelay( );
3290  009d 9d            	nop	
3295  009e 9d            	nop	
3300  009f 9d            	nop	
3305  00a0 9d            	nop	
3307                     ; 85         if ( PIN_PT_DO() ) dat |= bit;
3309  00a1 720b501506    	btjf	_PE_IDR,#5,L5512
3312  00a6 7b01          	ld	a,(OFST-1,sp)
3313  00a8 1a02          	or	a,(OFST+0,sp)
3314  00aa 6b01          	ld	(OFST-1,sp),a
3315  00ac               L5512:
3316                     ; 86       } while ( bit<<=1 );
3318  00ac 0802          	sll	(OFST+0,sp)
3319  00ae 26e1          	jrne	L3512
3320                     ; 87     return ( dat );
3322  00b0 7b01          	ld	a,(OFST-1,sp)
3325  00b2 85            	popw	x
3326  00b3 81            	ret	
3373                     ; 89 void Pt696xRd( U8 *pImage, U8 size )
3373                     ; 90   {
3374                     	switch	.text
3375  00b4               _Pt696xRd:
3377  00b4 89            	pushw	x
3378       00000000      OFST:	set	0
3381                     ; 91     Pt696xStart( );
3383  00b5 cd0000        	call	_Pt696xStart
3385                     ; 92     Pt696xSend( 0x42 );
3387  00b8 a642          	ld	a,#66
3388  00ba cd0015        	call	_Pt696xSend
3391  00bd 200c          	jra	L5122
3392  00bf               L3122:
3393                     ; 95         __WDTC( );
3395  00bf 35aa50e0      	mov	_IWDG_KR,#170
3396                     ; 96         *pImage = Pt696xRecieve( );
3398  00c3 adbd          	call	_Pt696xRecieve
3400  00c5 1e01          	ldw	x,(OFST+1,sp)
3401  00c7 f7            	ld	(x),a
3402                     ; 97         pImage++;
3404  00c8 5c            	incw	x
3405  00c9 1f01          	ldw	(OFST+1,sp),x
3406  00cb               L5122:
3407                     ; 93     while ( size-- )
3409  00cb 7b05          	ld	a,(OFST+5,sp)
3410  00cd 0a05          	dec	(OFST+5,sp)
3411  00cf 4d            	tnz	a
3412  00d0 26ed          	jrne	L3122
3413                     ; 99     Pt696xStop( );
3416  00d2 721e500f      	bset	_PD_ODR,#7
3418                     ; 100   }  
3421  00d6 85            	popw	x
3422  00d7 81            	ret	
3435                     	xdef	_Pt696xRecieve
3436                     	xdef	_Pt696xSend
3437                     	xdef	_Pt696xStart
3438                     	xdef	_Pt696xRd
3439                     	xdef	_Pt696xWr
3440                     	xdef	_Pt696xCmd
3459                     	end

   1                     
   2                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   3                     ;           File Name : SoftSerial_STM8.s
   4                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   6                                 CLIST       ON
   7                                 DLIST       ON
   9                                 BASE        10
  10                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  11                                 XDEF        _FILT
  12                                 XDEF       f_SoftSerialISR
  13                                 XDEF        _SRL_Init
  14                                 XDEF        _SRL_Check
  15                                 XDEF        _SRL_RX_Getc
  16                                 XDEF        _SRL_RX_Gets
  17                                 XDEF        _SRL_TX_Putc
  18                                 XDEF        _SRL_TX_Puts
  19                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  20                                 XREF        _PA_IDR
  21                                 XREF        _PB_IDR
  22                                 XREF        _PC_IDR
  23                                 XREF        _PD_IDR
  24                                 XREF        _PE_IDR
  25                                ;XREF        _PF_IDR
  26                                 XREF        _PG_IDR
  27                                 XREF        _PA_ODR
  28                                 XREF        _PB_ODR
  29                                 XREF        _PC_ODR
  30                                 XREF        _PD_ODR
  31                                 XREF        _PE_ODR
  32                                ;XREF        _PF_ODR
  33                                 XREF        _PG_ODR
  34                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  35                                 XREF        _TIM4_CR1
  36                                 XREF        _TIM4_IER
  37                                 XREF        _TIM4_SR
  38                                 XREF        _TIM4_EGR
  39                                 XREF        _TIM4_CNTR
  40                                 XREF        _TIM4_PSCR
  41                                 XREF        _TIM4_ARR
  42                     TMR_SET_UP: MACRO
  43                                 MOV         _TIM4_CR1,  #0X04
  44                                 MOV         _TIM4_PSCR, #1
  45                                 MOV         _TIM4_ARR,  #166
  46                                 MOV         _TIM4_SR,   #0X00
  47                                 MOV         _TIM4_IER,  #0X01
  48                                 MOV         _TIM4_CR1,  #0X05
  49                                 ENDM
  50                     TMR_CLR_IF: MACRO
  51                                 MOV         _TIM4_SR,   #0XFE
  52                                 ENDM
  53                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  54       00002580                  BAUD_RATE:  EQU         9600
  55       00000000                  PARI_NONE:  EQU         0X00
  56       00000080                  PARI_EVEN:  EQU         0X80
  57       00000040                  PARI_ODD:   EQU         0X40
  58       000000c0                  PARI_ENB:   EQU         0XC0
  59       00000020                  TE_ENB:     EQU         0X20
  60       00000010                  TE_INV:     EQU         0X10
  61       00000008                  TX_ENB:     EQU         0X08
  62       00000004                  TX_INV:     EQU         0X04
  63       00000002                  RX_ENB:     EQU         0X02
  64       00000001                  RX_INV:     EQU         0X01
  65                                 ;--------------------------------------------------------
  66       0000000a                  SU0_MODE:   EQU         PARI_NONE+TX_ENB+RX_ENB
  67       00000020                  SU0_TX_LEN: EQU         32
  68       00000020                  SU0_RX_LEN: EQU         32
  69                                 SU0_TE_ODR: EQU         _PA_ODR
  70       00000000                  SU0_TE_PIN: EQU         0
  71                                 SU0_TX_ODR: EQU         _PD_ODR
  72       00000000                  SU0_TX_PIN: EQU         0
  73                                 SU0_RX_IDR: EQU         _PE_IDR
  74       00000000                  SU0_RX_PIN: EQU         0
  75                                 ;--------------------------------------------------------
  76       0000002a                  SU1_MODE:   EQU         PARI_NONE+TE_ENB+TX_ENB+RX_ENB
  77       00000020                  SU1_TX_LEN: EQU         32
  78       00000020                  SU1_RX_LEN: EQU         32
  79                                 SU1_TE_ODR: EQU         _PD_ODR
  80       00000007                  SU1_TE_PIN: EQU         7
  81                                 SU1_TX_ODR: EQU         _PD_ODR
  82       00000005                  SU1_TX_PIN: EQU         5
  83                                 SU1_RX_IDR: EQU         _PD_IDR
  84       00000006                  SU1_RX_PIN: EQU         6
  85                                 ;--------------------------------------------------------
  86       0000000a                  SU2_MODE:   EQU         PARI_NONE+TX_ENB+RX_ENB
  87       00000020                  SU2_TX_LEN: EQU         32
  88       00000020                  SU2_RX_LEN: EQU         32
  89                                 SU2_TE_ODR: EQU         _PA_ODR
  90       00000000                  SU2_TE_PIN: EQU         0
  91                                 SU2_TX_ODR: EQU         _PD_ODR
  92       00000003                  SU2_TX_PIN: EQU         3
  93                                 SU2_RX_IDR: EQU         _PD_IDR
  94       00000004                  SU2_RX_PIN: EQU         4
  95                                 ;--------------------------------------------------------
  96       00000000                  SU3_MODE:   EQU         0
  97       00000020                  SU3_TX_LEN: EQU         32
  98       00000020                  SU3_RX_LEN: EQU         32
  99                                 SU3_TE_ODR: EQU         _PA_ODR
 100       00000000                  SU3_TE_PIN: EQU         0
 101                                 SU3_TX_ODR: EQU         _PA_ODR
 102       00000001                  SU3_TX_PIN: EQU         1
 103                                 SU3_RX_IDR: EQU         _PA_IDR
 104       00000000                  SU3_RX_PIN: EQU         0
 105                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 106       00000000                  STA:        SET         0
 107       00000001                  IOR:        SET         1
 108       00000002                  CNT:        SET         2
 109       00000003                  REG:        SET         3
 110       00000004                  nBT:        SET         4
 111       00000005                  iWR:        SET         5
 112       00000006                  iRD:        SET         6
 113                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 114                                 SWITCH      .bss
 115  0000 00                        PRSCL:      DS.B        1
 116  0001 00                        TMR_WDT:    DS.B        1
 117  0002 0000                      TMR_CNT:    DS.B        2
 118                                 ;--------------------------------------------------------
 119                                 IF          ( SU0_MODE & RX_ENB ) && ( SU0_RX_LEN != 0 )
 120  0004 000000000000              SU0_RX:     DS.B        8
 121  000c 000000000000              SU0_RX_BUF: DS.B        SU0_RX_LEN
 122                                 ENDIF
 123                                 IF          ( SU0_MODE & TX_ENB ) && ( SU0_TX_LEN != 0 )
 124  002c 000000000000              SU0_TX:     DS.B        8
 125  0034 000000000000              SU0_TX_BUF: DS.B        SU0_TX_LEN
 126                                 ENDIF
 127                                 ;--------------------------------------------------------
 128                                 IF          ( SU1_MODE & RX_ENB ) && ( SU1_RX_LEN != 0 )
 129  0054 000000000000              SU1_RX:     DS.B        8
 130  005c 000000000000              SU1_RX_BUF: DS.B        SU1_RX_LEN
 131                                 ENDIF
 132                                 IF          ( SU1_MODE & TX_ENB ) && ( SU1_TX_LEN != 0 )
 133  007c 000000000000              SU1_TX:     DS.B        8
 134  0084 000000000000              SU1_TX_BUF: DS.B        SU1_TX_LEN
 135                                 ENDIF
 136                                 ;--------------------------------------------------------
 137                                 IF          ( SU2_MODE & RX_ENB ) && ( SU2_RX_LEN != 0 )
 138  00a4 000000000000              SU2_RX:     DS.B        8
 139  00ac 000000000000              SU2_RX_BUF: DS.B        SU2_RX_LEN
 140                                 ENDIF
 141                                 IF          ( SU2_MODE & TX_ENB ) && ( SU2_TX_LEN != 0 )
 142  00cc 000000000000              SU2_TX:     DS.B        8
 143  00d4 000000000000              SU2_TX_BUF: DS.B        SU2_TX_LEN
 144                                 ENDIF
 145                                 ;--------------------------------------------------------
 146                                 IF          ( SU3_MODE & RX_ENB ) && ( SU3_RX_LEN != 0 )
 147                                 SU3_RX:     DS.B        8
 148                                 SU3_RX_BUF: DS.B        SU3_RX_LEN
 149                                 ENDIF
 150                                 IF          ( SU3_MODE & TX_ENB ) && ( SU3_TX_LEN != 0 )
 151                                 SU3_TX:     DS.B        8
 152                                 SU3_TX_BUF: DS.B        SU3_TX_LEN
 153                                 ENDIF
 154                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 155                     CLR_RAM:    MACRO       \RAM
 156                                 CLR         \RAM + STA
 157                                 CLR         \RAM + IOR
 158                                 CLR         \RAM + CNT
 159                                 CLR         \RAM + REG
 160                                 CLR         \RAM + nBT
 161                                 CLR         \RAM + iWR
 162                                 CLR         \RAM + iRD
 163                                 ENDM
 164                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 165                     TE_PORT:    MACRO       \TX,        \ODR,       \PIN,       \MODE
 166                                 IF          \MODE & TE_ENB
 167                                 LD          A,          \TX + IOR
 168                                 RLC         A
 169                                 BCCM        \ODR,       #\PIN
 170                                 ENDIF
 171                                 ENDM
 172                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 173                     TX_PORT:    MACRO       \TX,        \ODR,       \PIN,       \MODE
 174                                 IF          \MODE & TX_ENB
 175                                 LD          A,          \TX + IOR
 176                                 RRC         A
 177                                 BCCM        \ODR,       #\PIN
 178                                 ENDIF
 179                                 ENDM
 180                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 181                     RX_PORT:    MACRO       \RX,        \IDR,       \PIN,       \MODE
 182                                 IF          \MODE & RX_ENB
 183                                 LD          A,          \IDR
 184                                 IF          \MODE & RX_INV
 185                                 AND         A,          #(1<<\PIN)
 186                                 SUB         A,          #1
 187                                 ELSE
 188                                 OR          A,          #(1<<\PIN)^0XFF
 189                                 ADD         A,          #1
 190                                 ENDIF
 191                                 BCCM        \RX + IOR,  #0
 192                                 ENDIF
 193                                 ENDM
 194                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 195                     TE_0_TX_1:  MACRO       \TX,        \MODE
 196                                 IF          !( \MODE & TE_INV ) && !( \MODE & TX_INV )
 197                                 MOV         \TX + IOR,  #0X01
 198                                 ENDIF
 199                                 IF          !( \MODE & TE_INV ) &&  ( \MODE & TX_INV )
 200                                 MOV         \TX + IOR,  #0X00
 201                                 ENDIF
 202                                 IF           ( \MODE & TE_INV ) && !( \MODE & TX_INV )
 203                                 MOV         \TX + IOR,  #0X81
 204                                 ENDIF
 205                                 IF           ( \MODE & TE_INV ) &&  ( \MODE & TX_INV )
 206                                 MOV         \TX + IOR,  #0X80
 207                                 ENDIF
 208                                 ENDM
 209                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 210                     TE_1_TX_0:  MACRO       \TX,        \MODE
 211                                 IF          !( \MODE & TE_INV ) && !( \MODE & TX_INV )
 212                                 MOV         \TX + IOR,  #0X80
 213                                 ENDIF
 214                                 IF          !( \MODE & TE_INV ) &&  ( \MODE & TX_INV )
 215                                 MOV         \TX + IOR,  #0X81
 216                                 ENDIF
 217                                 IF           ( \MODE & TE_INV ) && !( \MODE & TX_INV )
 218                                 MOV         \TX + IOR,  #0X00
 219                                 ENDIF
 220                                 IF           ( \MODE & TE_INV ) &&  ( \MODE & TX_INV )
 221                                 MOV         \TX + IOR,  #0X01
 222                                 ENDIF
 223                                 ENDM
 224                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 225                     TX_1:       MACRO       \TX,        \MODE
 226                                 IF          \MODE & TX_INV
 227                                 BRES        \TX + IOR,  #0
 228                                 ELSE
 229                                 BSET        \TX + IOR,  #0
 230                                 ENDIF
 231                                 ENDM
 232                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 233                     TX_0:       MACRO       \TX,        \MODE
 234                                 IF          \MODE & TX_INV
 235                                 BSET        \TX + IOR,  #0
 236                                 ELSE
 237                                 BRES        \TX + IOR,  #0
 238                                 ENDIF
 239                                 ENDM
 240                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 241                     TX_PROC:    MACRO       \TX,        \BUF,       \LEN,       \MODE
 242                                 IF          ( \MODE & TX_ENB ) && ( \LEN != 0 )
 243                                 ;--------------------------------------------------------
 244                                 \@START:
 245                                 BTJT        \TX + STA,  #0,         \@BITS
 246                                 TNZ         \TX + nBT
 247                                 JRNE        \@START_1
 248                                 TE_0_TX_1   \TX,        \MODE
 249                                 TNZ         \TX + CNT
 250                                 JRMI        \@EXIT
 251                                 INC         \TX + CNT
 252                                 IRET
 253                                 \@START_1:
 254                                 LD          A,          \TX + iRD
 255                                 AND         A,          #\LEN-1
 256                                 CLRW        X
 257                                 LD          XL,         A
 258                                 LD          A,          (\BUF,X)
 259                                 LD          \TX + REG,  A
 260                                 DEC         \TX + nBT
 261                                 INC         \TX + iRD
 262                                 MOV         \TX + CNT,  #8
 263                                 MOV         \TX + STA,  #1
 264                                 TE_1_TX_0   \TX,        \MODE
 265                                 IRET
 266                                 ;--------------------------------------------------------
 267                                 \@BITS:
 268                                 TX_1        \TX,        \MODE
 269                                 BTJT        \TX + STA,  #1,         \@PARI
 270                                 SRL         \TX + REG
 271                                 JRC         \@BITS_1
 272                                 TX_0        \TX,        \MODE
 273                                 IF          \MODE & PARI_ENB
 274                                 BCPL        \TX + STA,  #7
 275                                 ENDIF
 276                                 \@BITS_1:
 277                                 DEC         \TX + CNT
 278                                 JRNE        \@EXIT
 279                                 IF          \MODE & PARI_ENB
 280                                 BSET        \TX + STA,  #1
 281                                 ELSE
 282                                 MOV         \TX + CNT,  #3
 283                                 MOV         \TX + STA,  #7
 284                                 ENDIF
 285                                 IRET
 286                                 ;--------------------------------------------------------
 287                                 \@PARI:
 288                                 BTJT        \TX + STA,  #2,         \@STOP
 289                                 IF          \MODE & PARI_ODD
 290                                 BTJF        \TX + STA,  #7,         \@PARI_1
 291                                 ENDIF
 292                                 IF          \MODE & PARI_EVEN
 293                                 BTJT        \TX + STA,  #7,         \@PARI_1
 294                                 ENDIF
 295                                 TX_0        \TX,        \MODE
 296                                 \@PARI_1:
 297                                 MOV         \TX + CNT,  #2
 298                                 BSET        \TX + STA,  #2
 299                                 IRET
 300                                 ;--------------------------------------------------------
 301                                 \@STOP:
 302                                 DEC         \TX + CNT
 303                                 JRNE        \@EXIT
 304                                 MOV         \TX + STA,  #0
 305                                 ;--------------------------------------------------------
 306                                 \@EXIT:
 307                                 ENDIF
 308                                 ENDM
 309                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 310                     RX_PROC:    MACRO       \RX,        \BUF,       \LEN,       \MODE
 311                                 IF          ( \MODE & RX_ENB ) && ( \LEN != 0 )
 312                                 ;--------------------------------------------------------
 313                                 LD          A,          \RX + IOR
 314                                 AND         A,          #15
 315                                 CLRW        X
 316                                 LD          XL,         A
 317                                 LD          A,          (FILT,X)
 318                                 LD          \RX + IOR,  A
 319                                 ;--------------------------------------------------------
 320                                 \@START:
 321                                 BTJT        \RX + STA,  #0,         \@BITS
 322                                 BTJT        \RX + IOR,  #6,         \@START_1
 323                                 BTJT        \RX + CNT,  #7,         \@EXIT
 324                                 INC         \RX + CNT
 325                                 JRA         \@EXIT
 326                                 \@START_1:
 327                                 MOV         \RX + CNT,  #6
 328                                 MOV         \RX + REG,  #0X80
 329                                 MOV         \RX + STA,  #1
 330                                 JRA         \@EXIT
 331                                 ;--------------------------------------------------------
 332                                 \@BITS:
 333                                 DEC         \RX + CNT
 334                                 JRNE        \@EXIT
 335                                 MOV         \RX + CNT,  #5
 336                                 BTJT        \RX + STA,  #1,         \@PARI
 337                                 LD          A,          \RX + IOR
 338                                 RLC         A
 339                                 IF          \MODE & PARI_ENB
 340                                 JRNC        \@BITS_1
 341                                 BCPL        \RX + STA,  #7
 342                                 \@BITS_1:
 343                                 ENDIF
 344                                 RRC         \RX + REG
 345                                 JRNC        \@EXIT
 346                                 IF          \MODE & PARI_ENB
 347                                 BSET        \RX + STA,  #1
 348                                 ELSE
 349                                 BSET        \RX + STA,  #1
 350                                 BSET        \RX + STA,  #2
 351                                 ENDIF
 352                                 JRA         \@EXIT
 353                                 ;--------------------------------------------------------
 354                                 \@PARI:
 355                                 BTJT        \RX + STA,  #2,         \@STOP
 356                                 BTJF        \RX + IOR,  #7,         \@PARI_1
 357                                 BCPL        \RX + STA,  #7
 358                                 \@PARI_1:
 359                                 BSET        \RX + STA,  #2
 360                                 JRA         \@EXIT
 361                                 ;--------------------------------------------------------
 362                                 \@STOP:
 363                                 MOV         \RX + CNT,  #0
 364                                 BRES        \RX + STA,  #0
 365                                 IF          \MODE & PARI_ODD
 366                                 BTJF        \RX + STA,  #7,         \@EXIT
 367                                 ENDIF
 368                                 IF          \MODE & PARI_EVEN
 369                                 BTJT        \RX + STA,  #7,         \@EXIT
 370                                 ENDIF
 371                                 BTJF        \RX + IOR,  #7,         \@EXIT
 372                                 LD          A,          \RX + nBT
 373                                 CP          A,          #\LEN
 374                                 JRUGE       \@EXIT
 375                                 LD          A,          \RX + iWR
 376                                 AND         A,          #\LEN-1
 377                                 CLRW        X
 378                                 LD          XL,         A
 379                                 LD          A,          \RX + REG
 380                                 LD          (\BUF,X),   A
 381                                 INC         \RX + iWR
 382                                 INC         \RX + nBT
 383                                 ;--------------------------------------------------------
 384                                 \@EXIT:
 385                                 ENDIF
 386                                 ENDM
 387                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 388                                 SWITCH      .text
 389                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 390  0000                FILT:
 391  0000               _FILT:
 392  0000 00                        DC.B        00000000B   ; 0000
 393  0001 02                        DC.B        00000010B   ; 0001
 394  0002 04                        DC.B        00000100B   ; 0010
 395  0003 86                        DC.B        10000110B   ; 0011
 396  0004 00                        DC.B        00000000B   ; 0100
 397  0005 82                        DC.B        10000010B   ; 0101
 398  0006 84                        DC.B        10000100B   ; 0110
 399  0007 de                        DC.B        11011110B   ; 0111
 400  0008 60                        DC.B        01100000B   ; 1000
 401  0009 0a                        DC.B        00001010B   ; 1001
 402  000a 0c                        DC.B        00001100B   ; 1010
 403  000b 8e                        DC.B        10001110B   ; 1011
 404  000c 08                        DC.B        00001000B   ; 1100
 405  000d 8a                        DC.B        10001010B   ; 1101
 406  000e 8c                        DC.B        10001100B   ; 1110
 407  000f 8e                        DC.B        10001110B   ; 1111
 408                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 409                     ; void SoftSerialISR( void );
 410  0010               f_SoftSerialISR:
 411                                 TMR_CLR_IF
 411  0010 35fe0000                  MOV         _TIM4_SR,   #0XFE
 412                                 ;--------------------------------------------------------
 413                                 TE_PORT     SU0_TX,     SU0_TE_ODR, SU0_TE_PIN, SU0_MODE
 413                                 IF          SU0_MODE & TE_ENB
 413                                 LD          A,          \TX + IOR
 413                                 RLC         A
 413                                 BCCM        \ODR,       #\PIN
 413                                 ENDIF
 414                                 TX_PORT     SU0_TX,     SU0_TX_ODR, SU0_TX_PIN, SU0_MODE
 414                                 IF          SU0_MODE & TX_ENB
 414  0014 c6002d                    LD          A,          SU0_TX + IOR
 414  0017 46                        RRC         A
 414  0018 90110000                  BCCM        SU0_TX_ODR,       #SU0_TX_PIN
 414                                 ENDIF
 415                                 RX_PORT     SU0_RX,     SU0_RX_IDR, SU0_RX_PIN, SU0_MODE
 415                                 IF          SU0_MODE & RX_ENB
 415  001c c60000                    LD          A,          SU0_RX_IDR
 415                                 IF          SU0_MODE & RX_INV
 415                                 AND         A,          #(1<<\PIN)
 415                                 SUB         A,          #1
 415                                 ELSE
 415  001f aafe                      OR          A,          #(1<<SU0_RX_PIN)^0XFF
 415  0021 ab01                      ADD         A,          #1
 415                                 ENDIF
 415  0023 90110005                  BCCM        SU0_RX + IOR,  #0
 415                                 ENDIF
 416                                 ;--------------------------------------------------------
 417                                 TE_PORT     SU1_TX,     SU1_TE_ODR, SU1_TE_PIN, SU1_MODE
 417                                 IF          SU1_MODE & TE_ENB
 417  0027 c6007d                    LD          A,          SU1_TX + IOR
 417  002a 49                        RLC         A
 417  002b 901f0000                  BCCM        SU1_TE_ODR,       #SU1_TE_PIN
 417                                 ENDIF
 418                                 TX_PORT     SU1_TX,     SU1_TX_ODR, SU1_TX_PIN, SU1_MODE
 418                                 IF          SU1_MODE & TX_ENB
 418  002f c6007d                    LD          A,          SU1_TX + IOR
 418  0032 46                        RRC         A
 418  0033 901b0000                  BCCM        SU1_TX_ODR,       #SU1_TX_PIN
 418                                 ENDIF
 419                                 RX_PORT     SU1_RX,     SU1_RX_IDR, SU1_RX_PIN, SU1_MODE
 419                                 IF          SU1_MODE & RX_ENB
 419  0037 c60000                    LD          A,          SU1_RX_IDR
 419                                 IF          SU1_MODE & RX_INV
 419                                 AND         A,          #(1<<\PIN)
 419                                 SUB         A,          #1
 419                                 ELSE
 419  003a aabf                      OR          A,          #(1<<SU1_RX_PIN)^0XFF
 419  003c ab01                      ADD         A,          #1
 419                                 ENDIF
 419  003e 90110055                  BCCM        SU1_RX + IOR,  #0
 419                                 ENDIF
 420                                 ;--------------------------------------------------------
 421                                 TE_PORT     SU2_TX,     SU2_TE_ODR, SU2_TE_PIN, SU2_MODE
 421                                 IF          SU2_MODE & TE_ENB
 421                                 LD          A,          \TX + IOR
 421                                 RLC         A
 421                                 BCCM        \ODR,       #\PIN
 421                                 ENDIF
 422                                 TX_PORT     SU2_TX,     SU2_TX_ODR, SU2_TX_PIN, SU2_MODE
 422                                 IF          SU2_MODE & TX_ENB
 422  0042 c600cd                    LD          A,          SU2_TX + IOR
 422  0045 46                        RRC         A
 422  0046 90170000                  BCCM        SU2_TX_ODR,       #SU2_TX_PIN
 422                                 ENDIF
 423                                 RX_PORT     SU2_RX,     SU2_RX_IDR, SU2_RX_PIN, SU2_MODE
 423                                 IF          SU2_MODE & RX_ENB
 423  004a c60000                    LD          A,          SU2_RX_IDR
 423                                 IF          SU2_MODE & RX_INV
 423                                 AND         A,          #(1<<\PIN)
 423                                 SUB         A,          #1
 423                                 ELSE
 423  004d aaef                      OR          A,          #(1<<SU2_RX_PIN)^0XFF
 423  004f ab01                      ADD         A,          #1
 423                                 ENDIF
 423  0051 901100a5                  BCCM        SU2_RX + IOR,  #0
 423                                 ENDIF
 424                                 ;--------------------------------------------------------
 425                                 TE_PORT     SU3_TX,     SU3_TE_ODR, SU3_TE_PIN, SU3_MODE
 425                                 IF          SU3_MODE & TE_ENB
 425                                 LD          A,          \TX + IOR
 425                                 RLC         A
 425                                 BCCM        \ODR,       #\PIN
 425                                 ENDIF
 426                                 TX_PORT     SU3_TX,     SU3_TX_ODR, SU3_TX_PIN, SU3_MODE
 426                                 IF          SU3_MODE & TX_ENB
 426                                 LD          A,          \TX + IOR
 426                                 RRC         A
 426                                 BCCM        \ODR,       #\PIN
 426                                 ENDIF
 427                                 RX_PORT     SU3_RX,     SU3_RX_IDR, SU3_RX_PIN, SU3_MODE
 427                                 IF          SU3_MODE & RX_ENB
 427                                 LD          A,          \IDR
 427                                 IF          \MODE & RX_INV
 427                                 AND         A,          #(1<<\PIN)
 427                                 SUB         A,          #1
 427                                 ELSE
 427                                 OR          A,          #(1<<\PIN)^0XFF
 427                                 ADD         A,          #1
 427                                 ENDIF
 427                                 BCCM        \RX + IOR,  #0
 427                                 ENDIF
 428                                 ;--------------------------------------------------------
 429                                 RX_PROC     SU0_RX,     SU0_RX_BUF, SU0_RX_LEN, SU0_MODE
 429                                 IF          ( SU0_MODE & RX_ENB ) && ( SU0_RX_LEN != 0 )
 429  0055 c60005                    LD          A,          SU0_RX + IOR
 429  0058 a40f                      AND         A,          #15
 429  005a 5f                        CLRW        X
 429  005b 97                        LD          XL,         A
 429  005c d60000                    LD          A,          (FILT,X)
 429  005f c70005                    LD          SU0_RX + IOR,  A
 429  0062                           .15START:
 429  0062 720000041e                BTJT        SU0_RX + STA,  #0,         .15BITS
 429  0067 720c00050b                BTJT        SU0_RX + IOR,  #6,         .15START_1
 429  006c 720e000674                BTJT        SU0_RX + CNT,  #7,         .15EXIT
 429  0071 725c0006                  INC         SU0_RX + CNT
 429  0075 206e                      JRA         .15EXIT
 429  0077                           .15START_1:
 429  0077 35060006                  MOV         SU0_RX + CNT,  #6
 429  007b 35800007                  MOV         SU0_RX + REG,  #0X80
 429  007f 35010004                  MOV         SU0_RX + STA,  #1
 429  0083 2060                      JRA         .15EXIT
 429  0085                           .15BITS:
 429  0085 725a0006                  DEC         SU0_RX + CNT
 429  0089 265a                      JRNE        .15EXIT
 429  008b 35050006                  MOV         SU0_RX + CNT,  #5
 429  008f 7202000414                BTJT        SU0_RX + STA,  #1,         .15PARI
 429  0094 c60005                    LD          A,          SU0_RX + IOR
 429  0097 49                        RLC         A
 429                                 IF          SU0_MODE & PARI_ENB
 429                                 JRNC        \@BITS_1
 429                                 BCPL        \RX + STA,  #7
 429                                 \@BITS_1:
 429                                 ENDIF
 429  0098 72560007                  RRC         SU0_RX + REG
 429  009c 2447                      JRNC        .15EXIT
 429                                 IF          SU0_MODE & PARI_ENB
 429                                 BSET        \RX + STA,  #1
 429                                 ELSE
 429  009e 72120004                  BSET        SU0_RX + STA,  #1
 429  00a2 72140004                  BSET        SU0_RX + STA,  #2
 429                                 ENDIF
 429  00a6 203d                      JRA         .15EXIT
 429  00a8                           .15PARI:
 429  00a8 720400040f                BTJT        SU0_RX + STA,  #2,         .15STOP
 429  00ad 720f000504                BTJF        SU0_RX + IOR,  #7,         .15PARI_1
 429  00b2 901e0004                  BCPL        SU0_RX + STA,  #7
 429  00b6                           .15PARI_1:
 429  00b6 72140004                  BSET        SU0_RX + STA,  #2
 429  00ba 2029                      JRA         .15EXIT
 429  00bc                           .15STOP:
 429  00bc 35000006                  MOV         SU0_RX + CNT,  #0
 429  00c0 72110004                  BRES        SU0_RX + STA,  #0
 429                                 IF          SU0_MODE & PARI_ODD
 429                                 BTJF        \RX + STA,  #7,         \@EXIT
 429                                 ENDIF
 429                                 IF          SU0_MODE & PARI_EVEN
 429                                 BTJT        \RX + STA,  #7,         \@EXIT
 429                                 ENDIF
 429  00c4 720f00051c                BTJF        SU0_RX + IOR,  #7,         .15EXIT
 429  00c9 c60008                    LD          A,          SU0_RX + nBT
 429  00cc a120                      CP          A,          #SU0_RX_LEN
 429  00ce 2415                      JRUGE       .15EXIT
 429  00d0 c60009                    LD          A,          SU0_RX + iWR
 429  00d3 a41f                      AND         A,          #SU0_RX_LEN-1
 429  00d5 5f                        CLRW        X
 429  00d6 97                        LD          XL,         A
 429  00d7 c60007                    LD          A,          SU0_RX + REG
 429  00da d7000c                    LD          (SU0_RX_BUF,X),   A
 429  00dd 725c0009                  INC         SU0_RX + iWR
 429  00e1 725c0008                  INC         SU0_RX + nBT
 429  00e5                           .15EXIT:
 429                                 ENDIF
 430                                 RX_PROC     SU1_RX,     SU1_RX_BUF, SU1_RX_LEN, SU1_MODE
 430                                 IF          ( SU1_MODE & RX_ENB ) && ( SU1_RX_LEN != 0 )
 430  00e5 c60055                    LD          A,          SU1_RX + IOR
 430  00e8 a40f                      AND         A,          #15
 430  00ea 5f                        CLRW        X
 430  00eb 97                        LD          XL,         A
 430  00ec d60000                    LD          A,          (FILT,X)
 430  00ef c70055                    LD          SU1_RX + IOR,  A
 430  00f2                           .16START:
 430  00f2 720000541e                BTJT        SU1_RX + STA,  #0,         .16BITS
 430  00f7 720c00550b                BTJT        SU1_RX + IOR,  #6,         .16START_1
 430  00fc 720e005674                BTJT        SU1_RX + CNT,  #7,         .16EXIT
 430  0101 725c0056                  INC         SU1_RX + CNT
 430  0105 206e                      JRA         .16EXIT
 430  0107                           .16START_1:
 430  0107 35060056                  MOV         SU1_RX + CNT,  #6
 430  010b 35800057                  MOV         SU1_RX + REG,  #0X80
 430  010f 35010054                  MOV         SU1_RX + STA,  #1
 430  0113 2060                      JRA         .16EXIT
 430  0115                           .16BITS:
 430  0115 725a0056                  DEC         SU1_RX + CNT
 430  0119 265a                      JRNE        .16EXIT
 430  011b 35050056                  MOV         SU1_RX + CNT,  #5
 430  011f 7202005414                BTJT        SU1_RX + STA,  #1,         .16PARI
 430  0124 c60055                    LD          A,          SU1_RX + IOR
 430  0127 49                        RLC         A
 430                                 IF          SU1_MODE & PARI_ENB
 430                                 JRNC        \@BITS_1
 430                                 BCPL        \RX + STA,  #7
 430                                 \@BITS_1:
 430                                 ENDIF
 430  0128 72560057                  RRC         SU1_RX + REG
 430  012c 2447                      JRNC        .16EXIT
 430                                 IF          SU1_MODE & PARI_ENB
 430                                 BSET        \RX + STA,  #1
 430                                 ELSE
 430  012e 72120054                  BSET        SU1_RX + STA,  #1
 430  0132 72140054                  BSET        SU1_RX + STA,  #2
 430                                 ENDIF
 430  0136 203d                      JRA         .16EXIT
 430  0138                           .16PARI:
 430  0138 720400540f                BTJT        SU1_RX + STA,  #2,         .16STOP
 430  013d 720f005504                BTJF        SU1_RX + IOR,  #7,         .16PARI_1
 430  0142 901e0054                  BCPL        SU1_RX + STA,  #7
 430  0146                           .16PARI_1:
 430  0146 72140054                  BSET        SU1_RX + STA,  #2
 430  014a 2029                      JRA         .16EXIT
 430  014c                           .16STOP:
 430  014c 35000056                  MOV         SU1_RX + CNT,  #0
 430  0150 72110054                  BRES        SU1_RX + STA,  #0
 430                                 IF          SU1_MODE & PARI_ODD
 430                                 BTJF        \RX + STA,  #7,         \@EXIT
 430                                 ENDIF
 430                                 IF          SU1_MODE & PARI_EVEN
 430                                 BTJT        \RX + STA,  #7,         \@EXIT
 430                                 ENDIF
 430  0154 720f00551c                BTJF        SU1_RX + IOR,  #7,         .16EXIT
 430  0159 c60058                    LD          A,          SU1_RX + nBT
 430  015c a120                      CP          A,          #SU1_RX_LEN
 430  015e 2415                      JRUGE       .16EXIT
 430  0160 c60059                    LD          A,          SU1_RX + iWR
 430  0163 a41f                      AND         A,          #SU1_RX_LEN-1
 430  0165 5f                        CLRW        X
 430  0166 97                        LD          XL,         A
 430  0167 c60057                    LD          A,          SU1_RX + REG
 430  016a d7005c                    LD          (SU1_RX_BUF,X),   A
 430  016d 725c0059                  INC         SU1_RX + iWR
 430  0171 725c0058                  INC         SU1_RX + nBT
 430  0175                           .16EXIT:
 430                                 ENDIF
 431                                 RX_PROC     SU2_RX,     SU2_RX_BUF, SU2_RX_LEN, SU2_MODE
 431                                 IF          ( SU2_MODE & RX_ENB ) && ( SU2_RX_LEN != 0 )
 431  0175 c600a5                    LD          A,          SU2_RX + IOR
 431  0178 a40f                      AND         A,          #15
 431  017a 5f                        CLRW        X
 431  017b 97                        LD          XL,         A
 431  017c d60000                    LD          A,          (FILT,X)
 431  017f c700a5                    LD          SU2_RX + IOR,  A
 431  0182                           .17START:
 431  0182 720000a41e                BTJT        SU2_RX + STA,  #0,         .17BITS
 431  0187 720c00a50b                BTJT        SU2_RX + IOR,  #6,         .17START_1
 431  018c 720e00a674                BTJT        SU2_RX + CNT,  #7,         .17EXIT
 431  0191 725c00a6                  INC         SU2_RX + CNT
 431  0195 206e                      JRA         .17EXIT
 431  0197                           .17START_1:
 431  0197 350600a6                  MOV         SU2_RX + CNT,  #6
 431  019b 358000a7                  MOV         SU2_RX + REG,  #0X80
 431  019f 350100a4                  MOV         SU2_RX + STA,  #1
 431  01a3 2060                      JRA         .17EXIT
 431  01a5                           .17BITS:
 431  01a5 725a00a6                  DEC         SU2_RX + CNT
 431  01a9 265a                      JRNE        .17EXIT
 431  01ab 350500a6                  MOV         SU2_RX + CNT,  #5
 431  01af 720200a414                BTJT        SU2_RX + STA,  #1,         .17PARI
 431  01b4 c600a5                    LD          A,          SU2_RX + IOR
 431  01b7 49                        RLC         A
 431                                 IF          SU2_MODE & PARI_ENB
 431                                 JRNC        \@BITS_1
 431                                 BCPL        \RX + STA,  #7
 431                                 \@BITS_1:
 431                                 ENDIF
 431  01b8 725600a7                  RRC         SU2_RX + REG
 431  01bc 2447                      JRNC        .17EXIT
 431                                 IF          SU2_MODE & PARI_ENB
 431                                 BSET        \RX + STA,  #1
 431                                 ELSE
 431  01be 721200a4                  BSET        SU2_RX + STA,  #1
 431  01c2 721400a4                  BSET        SU2_RX + STA,  #2
 431                                 ENDIF
 431  01c6 203d                      JRA         .17EXIT
 431  01c8                           .17PARI:
 431  01c8 720400a40f                BTJT        SU2_RX + STA,  #2,         .17STOP
 431  01cd 720f00a504                BTJF        SU2_RX + IOR,  #7,         .17PARI_1
 431  01d2 901e00a4                  BCPL        SU2_RX + STA,  #7
 431  01d6                           .17PARI_1:
 431  01d6 721400a4                  BSET        SU2_RX + STA,  #2
 431  01da 2029                      JRA         .17EXIT
 431  01dc                           .17STOP:
 431  01dc 350000a6                  MOV         SU2_RX + CNT,  #0
 431  01e0 721100a4                  BRES        SU2_RX + STA,  #0
 431                                 IF          SU2_MODE & PARI_ODD
 431                                 BTJF        \RX + STA,  #7,         \@EXIT
 431                                 ENDIF
 431                                 IF          SU2_MODE & PARI_EVEN
 431                                 BTJT        \RX + STA,  #7,         \@EXIT
 431                                 ENDIF
 431  01e4 720f00a51c                BTJF        SU2_RX + IOR,  #7,         .17EXIT
 431  01e9 c600a8                    LD          A,          SU2_RX + nBT
 431  01ec a120                      CP          A,          #SU2_RX_LEN
 431  01ee 2415                      JRUGE       .17EXIT
 431  01f0 c600a9                    LD          A,          SU2_RX + iWR
 431  01f3 a41f                      AND         A,          #SU2_RX_LEN-1
 431  01f5 5f                        CLRW        X
 431  01f6 97                        LD          XL,         A
 431  01f7 c600a7                    LD          A,          SU2_RX + REG
 431  01fa d700ac                    LD          (SU2_RX_BUF,X),   A
 431  01fd 725c00a9                  INC         SU2_RX + iWR
 431  0201 725c00a8                  INC         SU2_RX + nBT
 431  0205                           .17EXIT:
 431                                 ENDIF
 432                                 RX_PROC     SU3_RX,     SU3_RX_BUF, SU3_RX_LEN, SU3_MODE
 432                                 IF          ( SU3_MODE & RX_ENB ) && ( SU3_RX_LEN != 0 )
 432                                 LD          A,          \RX + IOR
 432                                 AND         A,          #15
 432                                 CLRW        X
 432                                 LD          XL,         A
 432                                 LD          A,          (FILT,X)
 432                                 LD          \RX + IOR,  A
 432                                 \@START:
 432                                 BTJT        \RX + STA,  #0,         \@BITS
 432                                 BTJT        \RX + IOR,  #6,         \@START_1
 432                                 BTJT        \RX + CNT,  #7,         \@EXIT
 432                                 INC         \RX + CNT
 432                                 JRA         \@EXIT
 432                                 \@START_1:
 432                                 MOV         \RX + CNT,  #6
 432                                 MOV         \RX + REG,  #0X80
 432                                 MOV         \RX + STA,  #1
 432                                 JRA         \@EXIT
 432                                 \@BITS:
 432                                 DEC         \RX + CNT
 432                                 JRNE        \@EXIT
 432                                 MOV         \RX + CNT,  #5
 432                                 BTJT        \RX + STA,  #1,         \@PARI
 432                                 LD          A,          \RX + IOR
 432                                 RLC         A
 432                                 IF          \MODE & PARI_ENB
 432                                 JRNC        \@BITS_1
 432                                 BCPL        \RX + STA,  #7
 432                                 \@BITS_1:
 432                                 ENDIF
 432                                 RRC         \RX + REG
 432                                 JRNC        \@EXIT
 432                                 IF          \MODE & PARI_ENB
 432                                 BSET        \RX + STA,  #1
 432                                 ELSE
 432                                 BSET        \RX + STA,  #1
 432                                 BSET        \RX + STA,  #2
 432                                 ENDIF
 432                                 JRA         \@EXIT
 432                                 \@PARI:
 432                                 BTJT        \RX + STA,  #2,         \@STOP
 432                                 BTJF        \RX + IOR,  #7,         \@PARI_1
 432                                 BCPL        \RX + STA,  #7
 432                                 \@PARI_1:
 432                                 BSET        \RX + STA,  #2
 432                                 JRA         \@EXIT
 432                                 \@STOP:
 432                                 MOV         \RX + CNT,  #0
 432                                 BRES        \RX + STA,  #0
 432                                 IF          \MODE & PARI_ODD
 432                                 BTJF        \RX + STA,  #7,         \@EXIT
 432                                 ENDIF
 432                                 IF          \MODE & PARI_EVEN
 432                                 BTJT        \RX + STA,  #7,         \@EXIT
 432                                 ENDIF
 432                                 BTJF        \RX + IOR,  #7,         \@EXIT
 432                                 LD          A,          \RX + nBT
 432                                 CP          A,          #\LEN
 432                                 JRUGE       \@EXIT
 432                                 LD          A,          \RX + iWR
 432                                 AND         A,          #\LEN-1
 432                                 CLRW        X
 432                                 LD          XL,         A
 432                                 LD          A,          \RX + REG
 432                                 LD          (\BUF,X),   A
 432                                 INC         \RX + iWR
 432                                 INC         \RX + nBT
 432                                 \@EXIT:
 432                                 ENDIF
 433                                 ;--------------------------------------------------------
 434  0205 725c0000                  INC         PRSCL
 435  0209 c60000                    LD          A,          PRSCL
 436  020c a407                      AND         A,          #7
 437  020e 48                        SLA         A
 438  020f 5f                        CLRW        X
 439  0210 97                        LD          XL,         A
 440  0211 de0215                    LDW         X,          (JMPT,X)
 441  0214 fc                        JP          (X)
 442  0215                           JMPT:
 443  0215 0225                      DC.W        PRSCL_RST
 444  0217 0231                      DC.W        SU0_TX_PROC
 445  0219 02ac                      DC.W        SU1_TX_PROC
 446  021b 0327                      DC.W        SU2_TX_PROC
 447  021d 03a2                      DC.W        SU3_TX_PROC
 448  021f 0225                      DC.W        PRSCL_RST
 449  0221 0225                      DC.W        PRSCL_RST
 450  0223 0225                      DC.W        PRSCL_RST
 451                                 ;--------------------------------------------------------
 452  0225                           PRSCL_RST:
 453  0225 725f0000                  CLR         PRSCL
 454  0229 ce0002                    LDW         X,          TMR_CNT
 455  022c 5c                        INCW        X
 456  022d cf0002                    LDW         TMR_CNT,    X
 457  0230 80                        IRET
 458                                 ;--------------------------------------------------------
 459  0231                           SU0_TX_PROC:
 460                                 TX_PROC     SU0_TX,     SU0_TX_BUF, SU0_TX_LEN, SU0_MODE
 460                                 IF          ( SU0_MODE & TX_ENB ) && ( SU0_TX_LEN != 0 )
 460  0231                           .19START:
 460  0231 7200002c37                BTJT        SU0_TX + STA,  #0,         .19BITS
 460  0236 725d0030                  TNZ         SU0_TX + nBT
 460  023a 260f                      JRNE        .19START_1
 460                                 TE_0_TX_1   SU0_TX,        SU0_MODE
 460                                 IF          !( SU0_MODE & TE_INV ) && !( SU0_MODE & TX_INV )
 460  023c 3501002d                  MOV         SU0_TX + IOR,  #0X01
 460                                 ENDIF
 460                                 IF          !( SU0_MODE & TE_INV ) &&  ( SU0_MODE & TX_INV )
 460                                 MOV         \TX + IOR,  #0X00
 460                                 ENDIF
 460                                 IF           ( SU0_MODE & TE_INV ) && !( SU0_MODE & TX_INV )
 460                                 MOV         \TX + IOR,  #0X81
 460                                 ENDIF
 460                                 IF           ( SU0_MODE & TE_INV ) &&  ( SU0_MODE & TX_INV )
 460                                 MOV         \TX + IOR,  #0X80
 460                                 ENDIF
 460  0240 725d002e                  TNZ         SU0_TX + CNT
 460  0244 2b65                      JRMI        .19EXIT
 460  0246 725c002e                  INC         SU0_TX + CNT
 460  024a 80                        IRET
 460  024b                           .19START_1:
 460  024b c60032                    LD          A,          SU0_TX + iRD
 460  024e a41f                      AND         A,          #SU0_TX_LEN-1
 460  0250 5f                        CLRW        X
 460  0251 97                        LD          XL,         A
 460  0252 d60034                    LD          A,          (SU0_TX_BUF,X)
 460  0255 c7002f                    LD          SU0_TX + REG,  A
 460  0258 725a0030                  DEC         SU0_TX + nBT
 460  025c 725c0032                  INC         SU0_TX + iRD
 460  0260 3508002e                  MOV         SU0_TX + CNT,  #8
 460  0264 3501002c                  MOV         SU0_TX + STA,  #1
 460                                 TE_1_TX_0   SU0_TX,        SU0_MODE
 460                                 IF          !( SU0_MODE & TE_INV ) && !( SU0_MODE & TX_INV )
 460  0268 3580002d                  MOV         SU0_TX + IOR,  #0X80
 460                                 ENDIF
 460                                 IF          !( SU0_MODE & TE_INV ) &&  ( SU0_MODE & TX_INV )
 460                                 MOV         \TX + IOR,  #0X81
 460                                 ENDIF
 460                                 IF           ( SU0_MODE & TE_INV ) && !( SU0_MODE & TX_INV )
 460                                 MOV         \TX + IOR,  #0X00
 460                                 ENDIF
 460                                 IF           ( SU0_MODE & TE_INV ) &&  ( SU0_MODE & TX_INV )
 460                                 MOV         \TX + IOR,  #0X01
 460                                 ENDIF
 460  026c 80                        IRET
 460  026d                           .19BITS:
 460                                 TX_1        SU0_TX,        SU0_MODE
 460                                 IF          SU0_MODE & TX_INV
 460                                 BRES        \TX + IOR,  #0
 460                                 ELSE
 460  026d 7210002d                  BSET        SU0_TX + IOR,  #0
 460                                 ENDIF
 460  0271 7202002c19                BTJT        SU0_TX + STA,  #1,         .19PARI
 460  0276 7254002f                  SRL         SU0_TX + REG
 460  027a 2504                      JRC         .19BITS_1
 460                                 TX_0        SU0_TX,        SU0_MODE
 460                                 IF          SU0_MODE & TX_INV
 460                                 BSET        \TX + IOR,  #0
 460                                 ELSE
 460  027c 7211002d                  BRES        SU0_TX + IOR,  #0
 460                                 ENDIF
 460                                 IF          SU0_MODE & PARI_ENB
 460                                 BCPL        \TX + STA,  #7
 460                                 ENDIF
 460  0280                           .19BITS_1:
 460  0280 725a002e                  DEC         SU0_TX + CNT
 460  0284 2625                      JRNE        .19EXIT
 460                                 IF          SU0_MODE & PARI_ENB
 460                                 BSET        \TX + STA,  #1
 460                                 ELSE
 460  0286 3503002e                  MOV         SU0_TX + CNT,  #3
 460  028a 3507002c                  MOV         SU0_TX + STA,  #7
 460                                 ENDIF
 460  028e 80                        IRET
 460  028f                           .19PARI:
 460  028f 7204002c0d                BTJT        SU0_TX + STA,  #2,         .19STOP
 460                                 IF          SU0_MODE & PARI_ODD
 460                                 BTJF        \TX + STA,  #7,         \@PARI_1
 460                                 ENDIF
 460                                 IF          SU0_MODE & PARI_EVEN
 460                                 BTJT        \TX + STA,  #7,         \@PARI_1
 460                                 ENDIF
 460                                 TX_0        SU0_TX,        SU0_MODE
 460                                 IF          SU0_MODE & TX_INV
 460                                 BSET        \TX + IOR,  #0
 460                                 ELSE
 460  0294 7211002d                  BRES        SU0_TX + IOR,  #0
 460                                 ENDIF
 460  0298                           .19PARI_1:
 460  0298 3502002e                  MOV         SU0_TX + CNT,  #2
 460  029c 7214002c                  BSET        SU0_TX + STA,  #2
 460  02a0 80                        IRET
 460  02a1                           .19STOP:
 460  02a1 725a002e                  DEC         SU0_TX + CNT
 460  02a5 2604                      JRNE        .19EXIT
 460  02a7 3500002c                  MOV         SU0_TX + STA,  #0
 460  02ab                           .19EXIT:
 460                                 ENDIF
 461  02ab 80                        IRET
 462                                 ;--------------------------------------------------------
 463  02ac                           SU1_TX_PROC:
 464                                 TX_PROC     SU1_TX,     SU1_TX_BUF, SU1_TX_LEN, SU1_MODE
 464                                 IF          ( SU1_MODE & TX_ENB ) && ( SU1_TX_LEN != 0 )
 464  02ac                           .25START:
 464  02ac 7200007c37                BTJT        SU1_TX + STA,  #0,         .25BITS
 464  02b1 725d0080                  TNZ         SU1_TX + nBT
 464  02b5 260f                      JRNE        .25START_1
 464                                 TE_0_TX_1   SU1_TX,        SU1_MODE
 464                                 IF          !( SU1_MODE & TE_INV ) && !( SU1_MODE & TX_INV )
 464  02b7 3501007d                  MOV         SU1_TX + IOR,  #0X01
 464                                 ENDIF
 464                                 IF          !( SU1_MODE & TE_INV ) &&  ( SU1_MODE & TX_INV )
 464                                 MOV         \TX + IOR,  #0X00
 464                                 ENDIF
 464                                 IF           ( SU1_MODE & TE_INV ) && !( SU1_MODE & TX_INV )
 464                                 MOV         \TX + IOR,  #0X81
 464                                 ENDIF
 464                                 IF           ( SU1_MODE & TE_INV ) &&  ( SU1_MODE & TX_INV )
 464                                 MOV         \TX + IOR,  #0X80
 464                                 ENDIF
 464  02bb 725d007e                  TNZ         SU1_TX + CNT
 464  02bf 2b65                      JRMI        .25EXIT
 464  02c1 725c007e                  INC         SU1_TX + CNT
 464  02c5 80                        IRET
 464  02c6                           .25START_1:
 464  02c6 c60082                    LD          A,          SU1_TX + iRD
 464  02c9 a41f                      AND         A,          #SU1_TX_LEN-1
 464  02cb 5f                        CLRW        X
 464  02cc 97                        LD          XL,         A
 464  02cd d60084                    LD          A,          (SU1_TX_BUF,X)
 464  02d0 c7007f                    LD          SU1_TX + REG,  A
 464  02d3 725a0080                  DEC         SU1_TX + nBT
 464  02d7 725c0082                  INC         SU1_TX + iRD
 464  02db 3508007e                  MOV         SU1_TX + CNT,  #8
 464  02df 3501007c                  MOV         SU1_TX + STA,  #1
 464                                 TE_1_TX_0   SU1_TX,        SU1_MODE
 464                                 IF          !( SU1_MODE & TE_INV ) && !( SU1_MODE & TX_INV )
 464  02e3 3580007d                  MOV         SU1_TX + IOR,  #0X80
 464                                 ENDIF
 464                                 IF          !( SU1_MODE & TE_INV ) &&  ( SU1_MODE & TX_INV )
 464                                 MOV         \TX + IOR,  #0X81
 464                                 ENDIF
 464                                 IF           ( SU1_MODE & TE_INV ) && !( SU1_MODE & TX_INV )
 464                                 MOV         \TX + IOR,  #0X00
 464                                 ENDIF
 464                                 IF           ( SU1_MODE & TE_INV ) &&  ( SU1_MODE & TX_INV )
 464                                 MOV         \TX + IOR,  #0X01
 464                                 ENDIF
 464  02e7 80                        IRET
 464  02e8                           .25BITS:
 464                                 TX_1        SU1_TX,        SU1_MODE
 464                                 IF          SU1_MODE & TX_INV
 464                                 BRES        \TX + IOR,  #0
 464                                 ELSE
 464  02e8 7210007d                  BSET        SU1_TX + IOR,  #0
 464                                 ENDIF
 464  02ec 7202007c19                BTJT        SU1_TX + STA,  #1,         .25PARI
 464  02f1 7254007f                  SRL         SU1_TX + REG
 464  02f5 2504                      JRC         .25BITS_1
 464                                 TX_0        SU1_TX,        SU1_MODE
 464                                 IF          SU1_MODE & TX_INV
 464                                 BSET        \TX + IOR,  #0
 464                                 ELSE
 464  02f7 7211007d                  BRES        SU1_TX + IOR,  #0
 464                                 ENDIF
 464                                 IF          SU1_MODE & PARI_ENB
 464                                 BCPL        \TX + STA,  #7
 464                                 ENDIF
 464  02fb                           .25BITS_1:
 464  02fb 725a007e                  DEC         SU1_TX + CNT
 464  02ff 2625                      JRNE        .25EXIT
 464                                 IF          SU1_MODE & PARI_ENB
 464                                 BSET        \TX + STA,  #1
 464                                 ELSE
 464  0301 3503007e                  MOV         SU1_TX + CNT,  #3
 464  0305 3507007c                  MOV         SU1_TX + STA,  #7
 464                                 ENDIF
 464  0309 80                        IRET
 464  030a                           .25PARI:
 464  030a 7204007c0d                BTJT        SU1_TX + STA,  #2,         .25STOP
 464                                 IF          SU1_MODE & PARI_ODD
 464                                 BTJF        \TX + STA,  #7,         \@PARI_1
 464                                 ENDIF
 464                                 IF          SU1_MODE & PARI_EVEN
 464                                 BTJT        \TX + STA,  #7,         \@PARI_1
 464                                 ENDIF
 464                                 TX_0        SU1_TX,        SU1_MODE
 464                                 IF          SU1_MODE & TX_INV
 464                                 BSET        \TX + IOR,  #0
 464                                 ELSE
 464  030f 7211007d                  BRES        SU1_TX + IOR,  #0
 464                                 ENDIF
 464  0313                           .25PARI_1:
 464  0313 3502007e                  MOV         SU1_TX + CNT,  #2
 464  0317 7214007c                  BSET        SU1_TX + STA,  #2
 464  031b 80                        IRET
 464  031c                           .25STOP:
 464  031c 725a007e                  DEC         SU1_TX + CNT
 464  0320 2604                      JRNE        .25EXIT
 464  0322 3500007c                  MOV         SU1_TX + STA,  #0
 464  0326                           .25EXIT:
 464                                 ENDIF
 465  0326 80                        IRET
 466                                 ;--------------------------------------------------------
 467  0327                           SU2_TX_PROC:
 468                                 TX_PROC     SU2_TX,     SU2_TX_BUF, SU2_TX_LEN, SU2_MODE
 468                                 IF          ( SU2_MODE & TX_ENB ) && ( SU2_TX_LEN != 0 )
 468  0327                           .31START:
 468  0327 720000cc37                BTJT        SU2_TX + STA,  #0,         .31BITS
 468  032c 725d00d0                  TNZ         SU2_TX + nBT
 468  0330 260f                      JRNE        .31START_1
 468                                 TE_0_TX_1   SU2_TX,        SU2_MODE
 468                                 IF          !( SU2_MODE & TE_INV ) && !( SU2_MODE & TX_INV )
 468  0332 350100cd                  MOV         SU2_TX + IOR,  #0X01
 468                                 ENDIF
 468                                 IF          !( SU2_MODE & TE_INV ) &&  ( SU2_MODE & TX_INV )
 468                                 MOV         \TX + IOR,  #0X00
 468                                 ENDIF
 468                                 IF           ( SU2_MODE & TE_INV ) && !( SU2_MODE & TX_INV )
 468                                 MOV         \TX + IOR,  #0X81
 468                                 ENDIF
 468                                 IF           ( SU2_MODE & TE_INV ) &&  ( SU2_MODE & TX_INV )
 468                                 MOV         \TX + IOR,  #0X80
 468                                 ENDIF
 468  0336 725d00ce                  TNZ         SU2_TX + CNT
 468  033a 2b65                      JRMI        .31EXIT
 468  033c 725c00ce                  INC         SU2_TX + CNT
 468  0340 80                        IRET
 468  0341                           .31START_1:
 468  0341 c600d2                    LD          A,          SU2_TX + iRD
 468  0344 a41f                      AND         A,          #SU2_TX_LEN-1
 468  0346 5f                        CLRW        X
 468  0347 97                        LD          XL,         A
 468  0348 d600d4                    LD          A,          (SU2_TX_BUF,X)
 468  034b c700cf                    LD          SU2_TX + REG,  A
 468  034e 725a00d0                  DEC         SU2_TX + nBT
 468  0352 725c00d2                  INC         SU2_TX + iRD
 468  0356 350800ce                  MOV         SU2_TX + CNT,  #8
 468  035a 350100cc                  MOV         SU2_TX + STA,  #1
 468                                 TE_1_TX_0   SU2_TX,        SU2_MODE
 468                                 IF          !( SU2_MODE & TE_INV ) && !( SU2_MODE & TX_INV )
 468  035e 358000cd                  MOV         SU2_TX + IOR,  #0X80
 468                                 ENDIF
 468                                 IF          !( SU2_MODE & TE_INV ) &&  ( SU2_MODE & TX_INV )
 468                                 MOV         \TX + IOR,  #0X81
 468                                 ENDIF
 468                                 IF           ( SU2_MODE & TE_INV ) && !( SU2_MODE & TX_INV )
 468                                 MOV         \TX + IOR,  #0X00
 468                                 ENDIF
 468                                 IF           ( SU2_MODE & TE_INV ) &&  ( SU2_MODE & TX_INV )
 468                                 MOV         \TX + IOR,  #0X01
 468                                 ENDIF
 468  0362 80                        IRET
 468  0363                           .31BITS:
 468                                 TX_1        SU2_TX,        SU2_MODE
 468                                 IF          SU2_MODE & TX_INV
 468                                 BRES        \TX + IOR,  #0
 468                                 ELSE
 468  0363 721000cd                  BSET        SU2_TX + IOR,  #0
 468                                 ENDIF
 468  0367 720200cc19                BTJT        SU2_TX + STA,  #1,         .31PARI
 468  036c 725400cf                  SRL         SU2_TX + REG
 468  0370 2504                      JRC         .31BITS_1
 468                                 TX_0        SU2_TX,        SU2_MODE
 468                                 IF          SU2_MODE & TX_INV
 468                                 BSET        \TX + IOR,  #0
 468                                 ELSE
 468  0372 721100cd                  BRES        SU2_TX + IOR,  #0
 468                                 ENDIF
 468                                 IF          SU2_MODE & PARI_ENB
 468                                 BCPL        \TX + STA,  #7
 468                                 ENDIF
 468  0376                           .31BITS_1:
 468  0376 725a00ce                  DEC         SU2_TX + CNT
 468  037a 2625                      JRNE        .31EXIT
 468                                 IF          SU2_MODE & PARI_ENB
 468                                 BSET        \TX + STA,  #1
 468                                 ELSE
 468  037c 350300ce                  MOV         SU2_TX + CNT,  #3
 468  0380 350700cc                  MOV         SU2_TX + STA,  #7
 468                                 ENDIF
 468  0384 80                        IRET
 468  0385                           .31PARI:
 468  0385 720400cc0d                BTJT        SU2_TX + STA,  #2,         .31STOP
 468                                 IF          SU2_MODE & PARI_ODD
 468                                 BTJF        \TX + STA,  #7,         \@PARI_1
 468                                 ENDIF
 468                                 IF          SU2_MODE & PARI_EVEN
 468                                 BTJT        \TX + STA,  #7,         \@PARI_1
 468                                 ENDIF
 468                                 TX_0        SU2_TX,        SU2_MODE
 468                                 IF          SU2_MODE & TX_INV
 468                                 BSET        \TX + IOR,  #0
 468                                 ELSE
 468  038a 721100cd                  BRES        SU2_TX + IOR,  #0
 468                                 ENDIF
 468  038e                           .31PARI_1:
 468  038e 350200ce                  MOV         SU2_TX + CNT,  #2
 468  0392 721400cc                  BSET        SU2_TX + STA,  #2
 468  0396 80                        IRET
 468  0397                           .31STOP:
 468  0397 725a00ce                  DEC         SU2_TX + CNT
 468  039b 2604                      JRNE        .31EXIT
 468  039d 350000cc                  MOV         SU2_TX + STA,  #0
 468  03a1                           .31EXIT:
 468                                 ENDIF
 469  03a1 80                        IRET
 470                                 ;--------------------------------------------------------
 471  03a2                           SU3_TX_PROC:
 472                                 TX_PROC     SU3_TX,     SU3_TX_BUF, SU3_TX_LEN, SU3_MODE
 472                                 IF          ( SU3_MODE & TX_ENB ) && ( SU3_TX_LEN != 0 )
 472                                 \@START:
 472                                 BTJT        \TX + STA,  #0,         \@BITS
 472                                 TNZ         \TX + nBT
 472                                 JRNE        \@START_1
 472                                 TE_0_TX_1   \TX,        \MODE
 472                                 TNZ         \TX + CNT
 472                                 JRMI        \@EXIT
 472                                 INC         \TX + CNT
 472                                 IRET
 472                                 \@START_1:
 472                                 LD          A,          \TX + iRD
 472                                 AND         A,          #\LEN-1
 472                                 CLRW        X
 472                                 LD          XL,         A
 472                                 LD          A,          (\BUF,X)
 472                                 LD          \TX + REG,  A
 472                                 DEC         \TX + nBT
 472                                 INC         \TX + iRD
 472                                 MOV         \TX + CNT,  #8
 472                                 MOV         \TX + STA,  #1
 472                                 TE_1_TX_0   \TX,        \MODE
 472                                 IRET
 472                                 \@BITS:
 472                                 TX_1        \TX,        \MODE
 472                                 BTJT        \TX + STA,  #1,         \@PARI
 472                                 SRL         \TX + REG
 472                                 JRC         \@BITS_1
 472                                 TX_0        \TX,        \MODE
 472                                 IF          \MODE & PARI_ENB
 472                                 BCPL        \TX + STA,  #7
 472                                 ENDIF
 472                                 \@BITS_1:
 472                                 DEC         \TX + CNT
 472                                 JRNE        \@EXIT
 472                                 IF          \MODE & PARI_ENB
 472                                 BSET        \TX + STA,  #1
 472                                 ELSE
 472                                 MOV         \TX + CNT,  #3
 472                                 MOV         \TX + STA,  #7
 472                                 ENDIF
 472                                 IRET
 472                                 \@PARI:
 472                                 BTJT        \TX + STA,  #2,         \@STOP
 472                                 IF          \MODE & PARI_ODD
 472                                 BTJF        \TX + STA,  #7,         \@PARI_1
 472                                 ENDIF
 472                                 IF          \MODE & PARI_EVEN
 472                                 BTJT        \TX + STA,  #7,         \@PARI_1
 472                                 ENDIF
 472                                 TX_0        \TX,        \MODE
 472                                 \@PARI_1:
 472                                 MOV         \TX + CNT,  #2
 472                                 BSET        \TX + STA,  #2
 472                                 IRET
 472                                 \@STOP:
 472                                 DEC         \TX + CNT
 472                                 JRNE        \@EXIT
 472                                 MOV         \TX + STA,  #0
 472                                 \@EXIT:
 472                                 ENDIF
 473  03a2 80                        IRET
 474                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 475  03a3               UART_RX_TABLE:
 476                                 IF          ( SU0_MODE & RX_ENB ) && ( SU0_RX_LEN != 0 )
 477  03a3 20                        DC.B        SU0_RX_LEN
 478  03a4 0004                      DC.W        SU0_RX
 479  03a6 000c                      DC.W        SU0_RX_BUF
 480                                 ELSE
 481                                 DC.B        0
 482                                 DC.W        0
 483                                 DC.W        0
 484                                 ENDIF
 485                                 IF          ( SU1_MODE & RX_ENB ) && ( SU1_RX_LEN != 0 )
 486  03a8 20                        DC.B        SU1_RX_LEN
 487  03a9 0054                      DC.W        SU1_RX
 488  03ab 005c                      DC.W        SU1_RX_BUF
 489                                 ELSE
 490                                 DC.B        0
 491                                 DC.W        0
 492                                 DC.W        0
 493                                 ENDIF
 494                                 IF          ( SU2_MODE & RX_ENB ) && ( SU2_RX_LEN != 0 )
 495  03ad 20                        DC.B        SU2_RX_LEN
 496  03ae 00a4                      DC.W        SU2_RX
 497  03b0 00ac                      DC.W        SU2_RX_BUF
 498                                 ELSE
 499                                 DC.B        0
 500                                 DC.W        0
 501                                 DC.W        0
 502                                 ENDIF
 503                                 IF          ( SU3_MODE & RX_ENB ) && ( SU3_RX_LEN != 0 )
 504                                 DC.B        SU3_RX_LEN
 505                                 DC.W        SU3_RX
 506                                 DC.W        SU3_RX_BUF
 507                                 ELSE
 508  03b2 00                        DC.B        0
 509  03b3 0000                      DC.W        0
 510  03b5 0000                      DC.W        0
 511                                 ENDIF
 512  03b7               GET_UART_RX:
 513  03b7 a30004                    CPW         X,          #4
 514  03ba 2502                      JRULT       1$
 515  03bc 4f                        CLR         A
 516  03bd 81                        RET
 517  03be                           1$:
 518  03be a605                      LD          A,          #5
 519  03c0 42                        MUL         X,          A
 520  03c1 1c03a3                    ADDW        X,          #UART_RX_TABLE
 521  03c4 f6                        LD          A,          (X)
 522  03c5 9093                      LDW         Y,          X
 523  03c7 81                        RET
 524                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 525  03c8               UART_TX_TABLE:
 526                                 IF          ( SU0_MODE & TX_ENB ) && ( SU0_TX_LEN != 0 )
 527  03c8 20                        DC.B        SU0_TX_LEN
 528  03c9 002c                      DC.W        SU0_TX
 529  03cb 0034                      DC.W        SU0_TX_BUF
 530                                 ELSE
 531                                 DC.B        0
 532                                 DC.W        0
 533                                 DC.W        0
 534                                 ENDIF
 535                                 IF          ( SU1_MODE & TX_ENB ) && ( SU1_TX_LEN != 0 )
 536  03cd 20                        DC.B        SU1_TX_LEN
 537  03ce 007c                      DC.W        SU1_TX
 538  03d0 0084                      DC.W        SU1_TX_BUF
 539                                 ELSE
 540                                 DC.B        0
 541                                 DC.W        0
 542                                 DC.W        0
 543                                 ENDIF
 544                                 IF          ( SU2_MODE & TX_ENB ) && ( SU2_TX_LEN != 0 )
 545  03d2 20                        DC.B        SU2_TX_LEN
 546  03d3 00cc                      DC.W        SU2_TX
 547  03d5 00d4                      DC.W        SU2_TX_BUF
 548                                 ELSE
 549                                 DC.B        0
 550                                 DC.W        0
 551                                 DC.W        0
 552                                 ENDIF
 553                                 IF          ( SU3_MODE & TX_ENB ) && ( SU3_TX_LEN != 0 )
 554                                 DC.B        SU3_TX_LEN
 555                                 DC.W        SU3_TX
 556                                 DC.W        SU3_TX_BUF
 557                                 ELSE
 558  03d7 00                        DC.B        0
 559  03d8 0000                      DC.W        0
 560  03da 0000                      DC.W        0
 561                                 ENDIF
 562  03dc               GET_UART_TX:
 563  03dc a30004                    CPW         X,          #4
 564  03df 2502                      JRULT       1$
 565  03e1 4f                        CLR         A
 566  03e2 81                        RET
 567  03e3                           1$:
 568  03e3 a605                      LD          A,          #5
 569  03e5 42                        MUL         X,          A
 570  03e6 1c03c8                    ADDW        X,          #UART_TX_TABLE
 571  03e9 f6                        LD          A,          (X)
 572  03ea 9093                      LDW         Y,          X
 573  03ec 81                        RET
 574                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 575                     ; void SRL_Init( void );
 576  03ed               _SRL_Init:
 577                                 IF          SU0_MODE & RX_ENB
 578                                 CLR_RAM     SU0_RX
 578  03ed 725f0004                  CLR         SU0_RX + STA
 578  03f1 725f0005                  CLR         SU0_RX + IOR
 578  03f5 725f0006                  CLR         SU0_RX + CNT
 578  03f9 725f0007                  CLR         SU0_RX + REG
 578  03fd 725f0008                  CLR         SU0_RX + nBT
 578  0401 725f0009                  CLR         SU0_RX + iWR
 578  0405 725f000a                  CLR         SU0_RX + iRD
 579                                 ENDIF
 580                                 IF          SU0_MODE & TX_ENB
 581                                 CLR_RAM     SU0_TX
 581  0409 725f002c                  CLR         SU0_TX + STA
 581  040d 725f002d                  CLR         SU0_TX + IOR
 581  0411 725f002e                  CLR         SU0_TX + CNT
 581  0415 725f002f                  CLR         SU0_TX + REG
 581  0419 725f0030                  CLR         SU0_TX + nBT
 581  041d 725f0031                  CLR         SU0_TX + iWR
 581  0421 725f0032                  CLR         SU0_TX + iRD
 582                                 ENDIF
 583                                 IF          SU1_MODE & RX_ENB
 584                                 CLR_RAM     SU1_RX
 584  0425 725f0054                  CLR         SU1_RX + STA
 584  0429 725f0055                  CLR         SU1_RX + IOR
 584  042d 725f0056                  CLR         SU1_RX + CNT
 584  0431 725f0057                  CLR         SU1_RX + REG
 584  0435 725f0058                  CLR         SU1_RX + nBT
 584  0439 725f0059                  CLR         SU1_RX + iWR
 584  043d 725f005a                  CLR         SU1_RX + iRD
 585                                 ENDIF
 586                                 IF          SU1_MODE & TX_ENB
 587                                 CLR_RAM     SU1_TX
 587  0441 725f007c                  CLR         SU1_TX + STA
 587  0445 725f007d                  CLR         SU1_TX + IOR
 587  0449 725f007e                  CLR         SU1_TX + CNT
 587  044d 725f007f                  CLR         SU1_TX + REG
 587  0451 725f0080                  CLR         SU1_TX + nBT
 587  0455 725f0081                  CLR         SU1_TX + iWR
 587  0459 725f0082                  CLR         SU1_TX + iRD
 588                                 ENDIF
 589                                 IF          SU2_MODE & RX_ENB
 590                                 CLR_RAM     SU2_RX
 590  045d 725f00a4                  CLR         SU2_RX + STA
 590  0461 725f00a5                  CLR         SU2_RX + IOR
 590  0465 725f00a6                  CLR         SU2_RX + CNT
 590  0469 725f00a7                  CLR         SU2_RX + REG
 590  046d 725f00a8                  CLR         SU2_RX + nBT
 590  0471 725f00a9                  CLR         SU2_RX + iWR
 590  0475 725f00aa                  CLR         SU2_RX + iRD
 591                                 ENDIF
 592                                 IF          SU2_MODE & TX_ENB
 593                                 CLR_RAM     SU2_TX
 593  0479 725f00cc                  CLR         SU2_TX + STA
 593  047d 725f00cd                  CLR         SU2_TX + IOR
 593  0481 725f00ce                  CLR         SU2_TX + CNT
 593  0485 725f00cf                  CLR         SU2_TX + REG
 593  0489 725f00d0                  CLR         SU2_TX + nBT
 593  048d 725f00d1                  CLR         SU2_TX + iWR
 593  0491 725f00d2                  CLR         SU2_TX + iRD
 594                                 ENDIF
 595                                 IF          SU3_MODE & RX_ENB
 596                                 CLR_RAM     SU3_RX
 597                                 ENDIF
 598                                 IF          SU3_MODE & TX_ENB
 599                                 CLR_RAM     SU3_TX
 600                                 ENDIF
 601  0495 725f0000                  CLR         PRSCL
 602  0499 725f0001                  CLR         TMR_WDT
 603  049d 5f                        CLRW        X
 604  049e cf0002                    LDW         TMR_CNT,    X
 605                                 TMR_SET_UP
 605  04a1 35040000                  MOV         _TIM4_CR1,  #0X04
 605  04a5 35010000                  MOV         _TIM4_PSCR, #1
 605  04a9 35a60000                  MOV         _TIM4_ARR,  #166
 605  04ad 35000000                  MOV         _TIM4_SR,   #0X00
 605  04b1 35010000                  MOV         _TIM4_IER,  #0X01
 605  04b5 35050000                  MOV         _TIM4_CR1,  #0X05
 606  04b9 81                        RET
 607                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 608                     ; void SRL_Check( void );
 609  04ba               _SRL_Check:
 610  04ba 725c0001                  INC         TMR_WDT
 611  04be c60001                    LD          A,          TMR_WDT
 612  04c1 a164                      CP          A,          #100
 613  04c3 251b                      JRULT       1$
 614  04c5 725f0001                  CLR         TMR_WDT
 615  04c9 ce0002                    LDW         X,          TMR_CNT
 616  04cc 905f                      CLRW        Y
 617  04ce 90cf0002                  LDW         TMR_CNT,    Y
 618  04d2 a321c0                    CPW         X,          #BAUD_RATE/10*9
 619  04d5 2404aced03ed              JRULT       _SRL_Init
 620  04db a32940                    CPW         X,          #BAUD_RATE/10*11
 621  04de 22f7                      JRUGT       _SRL_Init
 622  04e0                           1$:
 623  04e0 81                        RET
 624                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 625                     ; U8   SRL_RX_Getc( U16 port, U8 * c );
 626  04e1               _SRL_RX_Getc:
 627  04e1 cd03b7                    CALL        GET_UART_RX
 628  04e4 4d                        TNZ         A
 629  04e5 2601                      JRNE        1$
 630  04e7 81                        RET
 631  04e8                           1$:
 632  04e8 ee01                      LDW         X,          (1,X)
 633  04ea 6d04                      TNZ         (nBT,X)
 634  04ec 2602                      JRNE        2$
 635  04ee 4f                        CLR         A
 636  04ef 81                        RET
 637  04f0                           2$:
 638  04f0 6a04                      DEC         (nBT,X)
 639  04f2 e106                      CP          A,          (iRD,X)
 640  04f4 2202                      JRUGT       3$
 641  04f6 6f06                      CLR         (iRD,X)
 642  04f8                           3$:
 643  04f8 e606                      LD          A,          (iRD,X)
 644  04fa 88                        PUSH        A
 645  04fb 4b00                      PUSH        #0
 646  04fd 90ee03                    LDW         Y,          (3,Y)
 647  0500 72f901                    ADDW        Y,          (1,SP)
 648  0503 84                        POP         A
 649  0504 84                        POP         A
 650  0505 90f6                      LD          A,          (Y)
 651  0507 6c06                      INC         (iRD,X)
 652  0509 1e03                      LDW         X,          (3,SP)
 653  050b f7                        LD          (X),        A
 654  050c a601                      LD          A,          #1
 655  050e 81                        RET
 656                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 657                     ; U8   SRL_RX_Gets( U16 port, U8 * s );
 658  050f               _SRL_RX_Gets:
 659  050f cd03b7                    CALL        GET_UART_RX
 660  0512 4d                        TNZ         A
 661  0513 2601                      JRNE        1$
 662  0515 81                        RET
 663  0516                           1$:
 664  0516 ee01                      LDW         X,          (1,X)
 665  0518 6d04                      TNZ         (nBT,X)
 666  051a 2602                      JRNE        2$
 667  051c 4f                        CLR         A
 668  051d 81                        RET
 669  051e                           2$:
 670  051e e602                      LD          A,          (CNT,X)
 671  0520 a124                      CP          A,          #36
 672  0522 2402                      JRUGE       3$
 673  0524 4f                        CLR         A
 674  0525 81                        RET
 675  0526                           3$:
 676  0526 4b00                      PUSH        #0
 677  0528                           4$:
 678  0528 0c01                      INC         (1,SP)
 679  052a 93                        LDW         X,          Y
 680  052b ee01                      LDW         X,          (1,X)
 681  052d e606                      LD          A,          (iRD,X)
 682  052f 90f1                      CP          A,          (Y)
 683  0531 2503                      JRULT       5$
 684  0533 4f                        CLR         A
 685  0534 e706                      LD          (iRD,X),    A
 686  0536                           5$:
 687  0536 88                        PUSH        A
 688  0537 4b00                      PUSH        #0
 689  0539 93                        LDW         X,          Y
 690  053a ee03                      LDW         X,          (3,X)
 691  053c 72fb01                    ADDW        X,          (1,SP)
 692  053f 84                        POP         A
 693  0540 84                        POP         A
 694  0541 f6                        LD          A,          (X)
 695  0542 1e04                      LDW         X,          (4,SP)
 696  0544 f7                        LD          (X),        A
 697  0545 5c                        INCW        X
 698  0546 1f04                      LDW         (4,SP),     X
 699  0548 93                        LDW         X,          Y
 700  0549 ee01                      LDW         X,          (1,X)
 701  054b 6c06                      INC         (iRD,X)
 702  054d 6a04                      DEC         (nBT,X)
 703  054f 26d7                      JRNE        4$
 704  0551 84                        POP         A
 705  0552 81                        RET
 706                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 707                     ; U8   SRL_TX_Putc( U16 port, U8 c );
 708  0553               _SRL_TX_Putc:
 709  0553 cd03dc                    CALL        GET_UART_TX
 710  0556 4d                        TNZ         A
 711  0557 2601                      JRNE        1$
 712  0559 81                        RET
 713  055a                           1$:
 714  055a ee01                      LDW         X,          (1,X)
 715  055c e104                      CP          A,          (nBT,X)
 716  055e 2202                      JRUGT       2$
 717  0560 4f                        CLR         A
 718  0561 81                        RET
 719  0562                           2$:
 720  0562 e105                      CP          A,          (iWR,X)
 721  0564 2202                      JRUGT       3$
 722  0566 6f05                      CLR         (iWR,X)
 723  0568                           3$:
 724  0568 e605                      LD          A,          (iWR,X)
 725  056a 88                        PUSH        A
 726  056b 4b00                      PUSH        #0
 727  056d 90ee03                    LDW         Y,          (3,Y)
 728  0570 72f901                    ADDW        Y,          (1,SP)
 729  0573 84                        POP         A
 730  0574 84                        POP         A
 731  0575 7b03                      LD          A,          (3,SP)
 732  0577 90f7                      LD          (Y),        A
 733  0579 6c05                      INC         (iWR,X)
 734  057b 6c04                      INC         (nBT,X)
 735  057d a601                      LD          A,          #1
 736  057f 81                        RET
 737                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 738                     ; U8   SRL_TX_Puts( U16 port, U8 * s, U8 n );
 739  0580               _SRL_TX_Puts:
 740  0580 cd03dc                    CALL        GET_UART_TX
 741  0583 4d                        TNZ         A
 742  0584 2601                      JRNE        1$
 743  0586 81                        RET
 744  0587                           1$:
 745  0587 0d05                      TNZ         (5,SP)
 746  0589 2602                      JRNE        2$
 747  058b 4f                        CLR         A
 748  058c 81                        RET
 749  058d                           2$:
 750  058d ee01                      LDW         X,          (1,X)
 751  058f 6d04                      TNZ         (nBT,X)
 752  0591 2702                      JREQ        3$
 753  0593 4f                        CLR         A
 754  0594 81                        RET
 755  0595                           3$:
 756  0595 e602                      LD          A,          (CNT,X)
 757  0597 a10c                      CP          A,          #12
 758  0599 2402                      JRUGE       4$
 759  059b 4f                        CLR         A
 760  059c 81                        RET
 761  059d                           4$:
 762  059d 93                        LDW         X,          Y
 763  059e ee01                      LDW         X,          (1,X)
 764  05a0 e604                      LD          A,          (nBT,X)
 765  05a2 90f1                      CP          A,          (Y)
 766  05a4 2502                      JRULT       5$
 767  05a6 4f                        CLR         A
 768  05a7 81                        RET
 769  05a8                           5$:
 770  05a8 e605                      LD          A,          (iWR,X)
 771  05aa 90f1                      CP          A,          (Y)
 772  05ac 2503                      JRULT       6$
 773  05ae 4f                        CLR         A
 774  05af e705                      LD          (iWR,X),    A
 775  05b1                           6$:
 776  05b1 88                        PUSH        A
 777  05b2 4b00                      PUSH        #0
 778  05b4 1e05                      LDW         X,          (5,SP)
 779  05b6 f6                        LD          A,          (X)
 780  05b7 5c                        INCW        X
 781  05b8 1f05                      LDW         (5,SP),     X
 782  05ba 93                        LDW         X,          Y
 783  05bb ee03                      LDW         X,          (3,X)
 784  05bd 72fb01                    ADDW        X,          (1,SP)
 785  05c0 f7                        LD          (X),        A
 786  05c1 85                        POPW        X
 787  05c2 93                        LDW         X,          Y
 788  05c3 ee01                      LDW         X,          (1,X)
 789  05c5 6c05                      INC         (iWR,X)
 790  05c7 6c04                      INC         (nBT,X)
 791  05c9 0a05                      DEC         (5,SP)
 792  05cb 26d0                      JRNE        4$
 793  05cd a601                      LD          A,          #1
 794  05cf 81                        RET
 795                     ;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 796                                 END

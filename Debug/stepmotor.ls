   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   3                     ; Optimizer V4.2.4 - 18 Dec 2007
2891                     ; 12 void STPM_Init( void )
2891                     ; 13   {
2893                     	switch	.text
2894  0000               _STPM_Init:
2898                     ; 14     stpm.stepNow = 0;
2900  0000 5f            	clrw	x
2901  0001 bf02          	ldw	_stpm+2,x
2902                     ; 15     stpm.stepSet = 0;
2904  0003 bf00          	ldw	_stpm,x
2905                     ; 16     STPM_Reset( &stpm, 1280, 1921 );
2907  0005 ae0781        	ldw	x,#1921
2908  0008 89            	pushw	x
2909  0009 ae0500        	ldw	x,#1280
2910  000c 89            	pushw	x
2911  000d ae0000        	ldw	x,#_stpm
2912  0010 ad75          	call	_STPM_Reset
2914  0012 5b04          	addw	sp,#4
2915                     ; 17     TIM2_CR1   = 0X00;
2917  0014 725f5300      	clr	_TIM2_CR1
2918                     ; 18     TIM2_IER   = 0X02;
2920  0018 35025301      	mov	_TIM2_IER,#2
2921                     ; 19     TIM2_SR1   = 0X00;
2923  001c 725f5302      	clr	_TIM2_SR1
2924                     ; 20     TIM2_SR2   = 0X00;
2926  0020 725f5303      	clr	_TIM2_SR2
2927                     ; 21     TIM2_CCMR1 = 0X10;
2929  0024 35105305      	mov	_TIM2_CCMR1,#16
2930                     ; 22     TIM2_CCMR2 = 0X00;
2932  0028 725f5306      	clr	_TIM2_CCMR2
2933                     ; 23     TIM2_CCMR3 = 0X00;
2935  002c 725f5307      	clr	_TIM2_CCMR3
2936                     ; 24     TIM2_CCER1 = 0X00;
2938  0030 725f5308      	clr	_TIM2_CCER1
2939                     ; 25     TIM2_CCER2 = 0X00;
2941  0034 725f5309      	clr	_TIM2_CCER2
2942                     ; 26     TIM2_CNTRH = 0;
2944  0038 725f530a      	clr	_TIM2_CNTRH
2945                     ; 27     TIM2_CNTRL = 0;
2947  003c 725f530b      	clr	_TIM2_CNTRL
2948                     ; 28     TIM2_PSCR  = 4;
2950  0040 3504530c      	mov	_TIM2_PSCR,#4
2951                     ; 29     TIM2_ARRH  = 255;
2953  0044 35ff530d      	mov	_TIM2_ARRH,#255
2954                     ; 30     TIM2_ARRL  = 255;
2956  0048 35ff530e      	mov	_TIM2_ARRL,#255
2957                     ; 31     TIM2_CCR1H = 0;
2959  004c 725f530f      	clr	_TIM2_CCR1H
2960                     ; 32     TIM2_CCR1L = 0;
2962  0050 725f5310      	clr	_TIM2_CCR1L
2963                     ; 33     TIM2_CCR2H = 0;
2965  0054 725f5311      	clr	_TIM2_CCR2H
2966                     ; 34     TIM2_CCR2L = 0;
2968  0058 725f5312      	clr	_TIM2_CCR2L
2969                     ; 35     TIM2_CCR3H = 0;
2971  005c 725f5313      	clr	_TIM2_CCR3H
2972                     ; 36     TIM2_CCR3L = 0;
2974  0060 725f5314      	clr	_TIM2_CCR3L
2975                     ; 37     TIM2_CR1  |= 0X01;
2977  0064 72105300      	bset	_TIM2_CR1,#0
2978                     ; 38   }
2981  0068 81            	ret	
3008                     ; 40 void STPM_Ctrl( void )
3008                     ; 41   {
3009                     	switch	.text
3010  0069               _STPM_Ctrl:
3014                     ; 42     TIM2_PSCR  = 4;
3016  0069 3504530c      	mov	_TIM2_PSCR,#4
3017                     ; 43     TIM2_ARRH  = 255;
3019  006d 35ff530d      	mov	_TIM2_ARRH,#255
3020                     ; 44     TIM2_ARRL  = 255;
3022  0071 35ff530e      	mov	_TIM2_ARRL,#255
3023                     ; 45     TIM2_CR1   = 0X01;
3025  0075 35015300      	mov	_TIM2_CR1,#1
3026                     ; 46   }
3029  0079 81            	ret	
3101                     ; 48 U8   STPM_IsBusy( STPM * p )
3101                     ; 49   {
3102                     	switch	.text
3103  007a               _STPM_IsBusy:
3107                     ; 50     if ( p->stepNow == p->stepSet ) return 0;
3109  007a 9093          	ldw	y,x
3110  007c ee02          	ldw	x,(2,x)
3111  007e 90f3          	cpw	x,(y)
3112  0080 2602          	jrne	L5702
3115  0082 4f            	clr	a
3118  0083 81            	ret	
3119  0084               L5702:
3120                     ; 51     return 1;
3122  0084 a601          	ld	a,#1
3125  0086 81            	ret	
3181                     ; 54 U8   STPM_Reset( STPM * p, U16 step, U16 time )
3181                     ; 55   {
3182                     	switch	.text
3183  0087               _STPM_Reset:
3185  0087 89            	pushw	x
3186       00000000      OFST:	set	0
3189                     ; 56     if ( STPM_IsBusy( p ) ) return 0;
3191  0088 adf0          	call	_STPM_IsBusy
3193  008a 4d            	tnz	a
3194  008b 2703          	jreq	L7212
3197  008d 4f            	clr	a
3199  008e 200e          	jra	L02
3200  0090               L7212:
3201                     ; 57     p->stepSet = 0;
3203  0090 1e01          	ldw	x,(OFST+1,sp)
3204  0092 905f          	clrw	y
3205  0094 ff            	ldw	(x),y
3206                     ; 58     p->stepNow = step;
3208  0095 1605          	ldw	y,(OFST+5,sp)
3209  0097 ef02          	ldw	(2,x),y
3210                     ; 59     p->stepTime = time;
3212  0099 1607          	ldw	y,(OFST+7,sp)
3213  009b ef04          	ldw	(4,x),y
3214                     ; 60     return 1;
3216  009d 4c            	inc	a
3218  009e               L02:
3220  009e 85            	popw	x
3221  009f 81            	ret	
3277                     ; 63 U8   STPM_Setup( STPM * p, U16 step, U16 time )
3277                     ; 64   {
3278                     	switch	.text
3279  00a0               _STPM_Setup:
3281  00a0 89            	pushw	x
3282       00000000      OFST:	set	0
3285                     ; 65     if ( STPM_IsBusy( p ) ) return 0;
3287  00a1 add7          	call	_STPM_IsBusy
3289  00a3 4d            	tnz	a
3290  00a4 2703          	jreq	L1612
3293  00a6 4f            	clr	a
3295  00a7 200a          	jra	L62
3296  00a9               L1612:
3297                     ; 66     p->stepSet = step;
3299  00a9 1e01          	ldw	x,(OFST+1,sp)
3300  00ab 1605          	ldw	y,(OFST+5,sp)
3301  00ad ff            	ldw	(x),y
3302                     ; 67     p->stepTime = time;
3304  00ae 1607          	ldw	y,(OFST+7,sp)
3305  00b0 ef04          	ldw	(4,x),y
3306                     ; 68     return 1;
3308  00b2 4c            	inc	a
3310  00b3               L62:
3312  00b3 85            	popw	x
3313  00b4 81            	ret	
3350                     ; 71 void STPM_ForceStop( STPM * p )
3350                     ; 72   {
3351                     	switch	.text
3352  00b5               _STPM_ForceStop:
3356                     ; 73     __DI( );
3359  00b5 9b            	sim	
3361                     ; 74     p->stepSet = p->stepNow;
3363  00b6 9093          	ldw	y,x
3364  00b8 90ee02        	ldw	y,(2,y)
3365  00bb ff            	ldw	(x),y
3366                     ; 75     __EI( );
3369  00bc 9a            	rim	
3371                     ; 76   }
3374  00bd 81            	ret	
3459                     ; 79 @far @interrupt void StepMotorISR( void )
3459                     ; 80   {
3461                     	switch	.text
3462  00be               f_StepMotorISR:
3465       00000002      OFST:	set	2
3466  00be 89            	pushw	x
3469                     ; 82     _asm(" BCPL _PG_ODR,#1 ");
3472  00bf 9012501e      	bcpl	_PG_ODR,#1
3474                     ; 83     TIM2_SR1 = 0XFD;
3476  00c3 35fd5302      	mov	_TIM2_SR1,#253
3477                     ; 84     ccr.byte.H = TIM2_CCR1H;
3479  00c7 c6530f        	ld	a,_TIM2_CCR1H
3480  00ca 6b01          	ld	(OFST-1,sp),a
3481                     ; 85     ccr.byte.L = TIM2_CCR1L;
3483  00cc c65310        	ld	a,_TIM2_CCR1L
3484  00cf 6b02          	ld	(OFST+0,sp),a
3485                     ; 86     ccr.word += stpm.stepTime;
3487  00d1 1e01          	ldw	x,(OFST-1,sp)
3488  00d3 72bb0004      	addw	x,_stpm+4
3489  00d7 1f01          	ldw	(OFST-1,sp),x
3490                     ; 87     TIM2_CCR1H = ccr.byte.H;
3492  00d9 7b01          	ld	a,(OFST-1,sp)
3493  00db c7530f        	ld	_TIM2_CCR1H,a
3494                     ; 88     TIM2_CCR1L = ccr.byte.L;
3496  00de 7b02          	ld	a,(OFST+0,sp)
3497  00e0 c75310        	ld	_TIM2_CCR1L,a
3498                     ; 89     if ( stpm.state )
3500  00e3 b606          	ld	a,_stpm+6
3501  00e5 2604ac410241  	jreq	L7052
3502                     ; 91         switch( stpm.stepNow & 7 )
3504  00eb b603          	ld	a,_stpm+3
3505  00ed a407          	and	a,#7
3507                     ; 140                 break;
3508  00ef 272d          	jreq	L1722
3509  00f1 4a            	dec	a
3510  00f2 2746          	jreq	L1132
3511  00f4 4a            	dec	a
3512  00f5 2767          	jreq	L1332
3513  00f7 4a            	dec	a
3514  00f8 2604ac820182  	jreq	L1532
3515  00fe 4a            	dec	a
3516  00ff 2604ac9c019c  	jreq	L1732
3517  0105 4a            	dec	a
3518  0106 2604acbe01be  	jreq	L1142
3519  010c 4a            	dec	a
3520  010d 2604ace001e0  	jreq	L1342
3521  0113 4a            	dec	a
3522  0114 2604acfa01fa  	jreq	L1542
3523  011a ac1a021a      	jra	L7622
3524  011e               L1722:
3525                     ; 94                 STPM_B_OFF( );
3528  011e 72155000      	bres	_PA_ODR,#2
3533  0122 721f5005      	bres	_PB_ODR,#7
3535                     ; 95                 STPM_C_OFF( );
3538  0126 72175000      	bres	_PA_ODR,#3
3543  012a 721d5000      	bres	_PA_ODR,#6
3545                     ; 96                 STPM_D_OFF( );
3548  012e 72195000      	bres	_PA_ODR,#4
3553  0132 721b5000      	bres	_PA_ODR,#5
3555                     ; 97                 STPM_A_ON( );
3563                     ; 98                 break;
3565  0136 ac120212      	jpf	LC001
3566  013a               L1132:
3567                     ; 100                 STPM_C_OFF( );
3570  013a 72175000      	bres	_PA_ODR,#3
3575  013e 721d5000      	bres	_PA_ODR,#6
3577                     ; 101                 STPM_D_OFF( );
3580  0142 72195000      	bres	_PA_ODR,#4
3585  0146 721b5000      	bres	_PA_ODR,#5
3587                     ; 102                 STPM_A_ON( );
3590  014a 72125000      	bset	_PA_ODR,#1
3595  014e 721c5005      	bset	_PB_ODR,#6
3597                     ; 103                 STPM_B_ON( );
3600  0152 72145000      	bset	_PA_ODR,#2
3605  0156 721e5005      	bset	_PB_ODR,#7
3607                     ; 104                 break;
3609  015a ac1a021a      	jra	L7622
3610  015e               L1332:
3611                     ; 106                 STPM_C_OFF( );
3614  015e 72175000      	bres	_PA_ODR,#3
3619  0162 721d5000      	bres	_PA_ODR,#6
3621                     ; 107                 STPM_D_OFF( );
3624  0166 72195000      	bres	_PA_ODR,#4
3629  016a 721b5000      	bres	_PA_ODR,#5
3631                     ; 108                 STPM_A_OFF( );
3634  016e 72135000      	bres	_PA_ODR,#1
3639  0172 721d5005      	bres	_PB_ODR,#6
3641                     ; 109                 STPM_B_ON( );
3644  0176 72145000      	bset	_PA_ODR,#2
3649  017a 721e5005      	bset	_PB_ODR,#7
3651                     ; 110                 break;
3653  017e ac1a021a      	jra	L7622
3654  0182               L1532:
3655                     ; 112                 STPM_D_OFF( );
3658  0182 72195000      	bres	_PA_ODR,#4
3663  0186 721b5000      	bres	_PA_ODR,#5
3665                     ; 113                 STPM_A_OFF( );
3668  018a 72135000      	bres	_PA_ODR,#1
3673  018e 721d5005      	bres	_PB_ODR,#6
3675                     ; 114                 STPM_B_ON( );
3678  0192 72145000      	bset	_PA_ODR,#2
3683  0196 721e5005      	bset	_PB_ODR,#7
3685                     ; 115                 STPM_C_ON( );
3693                     ; 116                 break;
3695  019a 2018          	jpf	LC003
3696  019c               L1732:
3697                     ; 118                 STPM_D_OFF( );
3700  019c 72195000      	bres	_PA_ODR,#4
3705  01a0 721b5000      	bres	_PA_ODR,#5
3707                     ; 119                 STPM_A_OFF( );
3710  01a4 72135000      	bres	_PA_ODR,#1
3715  01a8 721d5005      	bres	_PB_ODR,#6
3717                     ; 120                 STPM_B_OFF( );
3720  01ac 72155000      	bres	_PA_ODR,#2
3725  01b0 721f5005      	bres	_PB_ODR,#7
3727                     ; 121                 STPM_C_ON( );
3734  01b4               LC003:
3735  01b4 72165000      	bset	_PA_ODR,#3
3736  01b8 721c5000      	bset	_PA_ODR,#6
3738                     ; 122                 break;
3740  01bc 205c          	jra	L7622
3741  01be               L1142:
3742                     ; 124                 STPM_A_OFF( );
3745  01be 72135000      	bres	_PA_ODR,#1
3750  01c2 721d5005      	bres	_PB_ODR,#6
3752                     ; 125                 STPM_B_OFF( );
3755  01c6 72155000      	bres	_PA_ODR,#2
3760  01ca 721f5005      	bres	_PB_ODR,#7
3762                     ; 126                 STPM_C_ON( );
3765  01ce 72165000      	bset	_PA_ODR,#3
3770  01d2 721c5000      	bset	_PA_ODR,#6
3772                     ; 127                 STPM_D_ON( );
3779  01d6               LC002:
3780  01d6 72185000      	bset	_PA_ODR,#4
3781  01da 721a5000      	bset	_PA_ODR,#5
3783                     ; 128                 break;
3785  01de 203a          	jra	L7622
3786  01e0               L1342:
3787                     ; 130                 STPM_A_OFF( );
3790  01e0 72135000      	bres	_PA_ODR,#1
3795  01e4 721d5005      	bres	_PB_ODR,#6
3797                     ; 131                 STPM_B_OFF( );
3800  01e8 72155000      	bres	_PA_ODR,#2
3805  01ec 721f5005      	bres	_PB_ODR,#7
3807                     ; 132                 STPM_C_OFF( );
3810  01f0 72175000      	bres	_PA_ODR,#3
3815  01f4 721d5000      	bres	_PA_ODR,#6
3817                     ; 133                 STPM_D_ON( );
3825                     ; 134                 break;
3827  01f8 20dc          	jpf	LC002
3828  01fa               L1542:
3829                     ; 136                 STPM_B_OFF( );
3832  01fa 72155000      	bres	_PA_ODR,#2
3837  01fe 721f5005      	bres	_PB_ODR,#7
3839                     ; 137                 STPM_C_OFF( );
3842  0202 72175000      	bres	_PA_ODR,#3
3847  0206 721d5000      	bres	_PA_ODR,#6
3849                     ; 138                 STPM_D_ON( );
3852  020a 72185000      	bset	_PA_ODR,#4
3857  020e 721a5000      	bset	_PA_ODR,#5
3859                     ; 139                 STPM_A_ON( );
3866  0212               LC001:
3867  0212 72125000      	bset	_PA_ODR,#1
3868  0216 721c5005      	bset	_PB_ODR,#6
3870                     ; 140                 break;
3872  021a               L7622:
3873                     ; 142         if ( stpm.stepNow < stpm.stepSet )
3875  021a be02          	ldw	x,_stpm+2
3876  021c b300          	cpw	x,_stpm
3877  021e 2405          	jruge	L1742
3878                     ; 144             stpm.stepNow++;
3880  0220 5c            	incw	x
3881  0221 bf02          	ldw	_stpm+2,x
3883  0223 2046          	jra	L5052
3884  0225               L1742:
3885                     ; 146         else if ( stpm.stepSet < stpm.stepNow )
3887  0225 be00          	ldw	x,_stpm
3888  0227 b302          	cpw	x,_stpm+2
3889  0229 2407          	jruge	L5742
3890                     ; 148             stpm.stepNow--;
3892  022b be02          	ldw	x,_stpm+2
3893  022d 5a            	decw	x
3894  022e bf02          	ldw	_stpm+2,x
3896  0230 2039          	jra	L5052
3897  0232               L5742:
3898                     ; 150         else if ( stpm.state == 1 )
3900  0232 b606          	ld	a,_stpm+6
3901  0234 4a            	dec	a
3902  0235 2606          	jrne	L1052
3903                     ; 152             stpm.state = 2;
3905  0237 35020006      	mov	_stpm+6,#2
3907  023b 202e          	jra	L5052
3908  023d               L1052:
3909                     ; 156             stpm.state = 0;
3911  023d 3f06          	clr	_stpm+6
3912  023f 202a          	jra	L5052
3913  0241               L7052:
3914                     ; 161         STPM_A_OFF( );
3917  0241 72135000      	bres	_PA_ODR,#1
3922  0245 721d5005      	bres	_PB_ODR,#6
3924                     ; 162         STPM_B_OFF( );
3927  0249 72155000      	bres	_PA_ODR,#2
3932  024d 721f5005      	bres	_PB_ODR,#7
3934                     ; 163         STPM_C_OFF( );
3937  0251 72175000      	bres	_PA_ODR,#3
3942  0255 721d5000      	bres	_PA_ODR,#6
3944                     ; 164         STPM_D_OFF( );
3947  0259 72195000      	bres	_PA_ODR,#4
3952  025d 721b5000      	bres	_PA_ODR,#5
3954                     ; 165         if ( stpm.stepSet != stpm.stepNow )
3956  0261 be00          	ldw	x,_stpm
3957  0263 b302          	cpw	x,_stpm+2
3958  0265 2704          	jreq	L5052
3959                     ; 167             stpm.state = 1;
3961  0267 35010006      	mov	_stpm+6,#1
3962  026b               L5052:
3963                     ; 170   }
3966  026b 5b02          	addw	sp,#2
3967  026d 80            	iret	
3979                     	xdef	f_StepMotorISR
3980                     	xdef	_STPM_ForceStop
3981                     	xdef	_STPM_Setup
3982                     	xdef	_STPM_Reset
3983                     	xdef	_STPM_IsBusy
3984                     	xdef	_STPM_Ctrl
3985                     	xdef	_STPM_Init
3986                     	switch	.ubsct
3987  0000               _stpm:
3988  0000 000000000000  	ds.b	7
3989                     	xdef	_stpm
4009                     	end

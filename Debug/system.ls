   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   3                     ; Optimizer V4.2.4 - 18 Dec 2007
2811                     ; 13 void SYS_Ctrl( void )
2811                     ; 14 {
2813                     	switch	.text
2814  0000               _SYS_Ctrl:
2818                     ; 15     if( F_KEY_TEA_BASKET || TEA_KEY_FLAG)
2820  0000 7206000005    	btjt	_Ctrl_Board_Status,#3,L3002
2822  0005 720700000f    	btjf	_Disp_Flag1,#3,L1002
2823  000a               L3002:
2824                     ; 16       Brew_Select_B   = TEA;
2826  000a 35020000      	mov	_Brew_Select_B,#2
2828  000e               L5002:
2829                     ; 21     if(( Start_Status == OFF )&&( DLY_BREW_FUNC == 1))
2831  000e c60000        	ld	a,_Start_Status
2832  0011 261c          	jrne	L5102
2834  0013 720b000017    	btjf	_Disp_Flag,#5,L5102
2837  0018 81            	ret	
2838  0019               L1002:
2839                     ; 17     else if( F_KEY_COFFEE_BASKET || COFFEE_KEY_FLAG )
2841  0019 7200000005    	btjt	_Ctrl_Board_Status,#0,L1102
2843  001e 7205000006    	btjf	_Disp_Flag1,#2,L7002
2844  0023               L1102:
2845                     ; 18       Brew_Select_B   = COFFEE;
2847  0023 35010000      	mov	_Brew_Select_B,#1
2849  0027 20e5          	jra	L5002
2850  0029               L7002:
2851                     ; 20       Brew_Select_B   = NO_BASKET;
2853  0029 725f0000      	clr	_Brew_Select_B
2854  002d 20df          	jra	L5002
2855  002f               L5102:
2856                     ; 27      Brew_Select = Brew_Select_B;
2858  002f 5500000000    	mov	_Brew_Select,_Brew_Select_B
2859                     ; 31 }
2862  0034 81            	ret	
2875                     	xref	_Ctrl_Board_Status
2876                     	xref	_Disp_Flag1
2877                     	xref	_Disp_Flag
2878                     	xref	_Brew_Select_B
2879                     	xref	_Brew_Select
2880                     	xref	_Start_Status
2881                     	xdef	_SYS_Ctrl
2900                     	end

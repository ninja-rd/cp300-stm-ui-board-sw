   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   3                     ; Optimizer V4.2.4 - 18 Dec 2007
2811                     ; 17 @far @interrupt void UART2_TX_IRQHandler( void )
2811                     ; 18 {
2812                     	switch	.text
2813  0000               f_UART2_TX_IRQHandler:
2818                     ; 19     if( ++Tx_Num >= Tx_Length )
2820  0000 725c0000      	inc	_Tx_Num
2821  0004 c60000        	ld	a,_Tx_Num
2822  0007 c10000        	cp	a,_Tx_Length
2823  000a 2505          	jrult	L1002
2824                     ; 22         UART2_CR2 &= ~0X80;//��ֹ�����ж�
2826  000c 721f5245      	bres	_UART2_CR2,#7
2829  0010 80            	iret	
2830  0011               L1002:
2831                     ; 26         UART2_DR = Tx_Buffer[Tx_Num];
2833  0011 5f            	clrw	x
2834  0012 97            	ld	xl,a
2835  0013 d60000        	ld	a,(_Tx_Buffer,x)
2836  0016 c75241        	ld	_UART2_DR,a
2837                     ; 28 }
2840  0019 80            	iret	
2877                     ; 30 @far @interrupt void UART2_RX_IRQHandler( void )
2877                     ; 31 {
2878                     	switch	.text
2879  001a               f_UART2_RX_IRQHandler:
2882       00000001      OFST:	set	1
2883  001a 88            	push	a
2886                     ; 33     if ((UART2_SR & 0x20) != 0x00)
2888  001b 720b52401e    	btjf	_UART2_SR,#5,L1202
2889                     ; 35         uart_data = UART2_SR;
2891  0020 c65240        	ld	a,_UART2_SR
2892                     ; 36         uart_data = UART2_DR;
2894  0023 c65241        	ld	a,_UART2_DR
2895  0026 6b01          	ld	(OFST+0,sp),a
2896                     ; 38         Rx_Delay = 2;
2898  0028 35020000      	mov	_Rx_Delay,#2
2899                     ; 39         if( Rx_Num < 50 )
2901  002c c60000        	ld	a,_Rx_Num
2902  002f a132          	cp	a,#50
2903  0031 240b          	jruge	L1202
2904                     ; 41           Rx_Buffer[ Rx_Num ] = uart_data;
2906  0033 5f            	clrw	x
2907  0034 97            	ld	xl,a
2908  0035 7b01          	ld	a,(OFST+0,sp)
2909  0037 d70000        	ld	(_Rx_Buffer,x),a
2910                     ; 42           Rx_Num++;
2912  003a 725c0000      	inc	_Rx_Num
2913  003e               L1202:
2914                     ; 45 }
2917  003e 84            	pop	a
2918  003f 80            	iret	
2949                     ; 48 @far @interrupt void StepMotorISR( void )
2949                     ; 49 {
2950                     	switch	.text
2951  0040               f_StepMotorISR:
2954       00000001      OFST:	set	1
2955  0040 88            	push	a
2958                     ; 50   U8 MOTOR = 0;
2960  0041 0f01          	clr	(OFST+0,sp)
2961                     ; 51   MOTOR++;
2963  0043 0c01          	inc	(OFST+0,sp)
2964                     ; 52 }
2967  0045 84            	pop	a
2968  0046 80            	iret	
2980                     	xdef	f_StepMotorISR
2981                     	xdef	f_UART2_RX_IRQHandler
2982                     	xdef	f_UART2_TX_IRQHandler
2983                     	xref	_Rx_Buffer
2984                     	xref	_Rx_Num
2985                     	xref	_Rx_Delay
2986                     	xref	_Tx_Buffer
2987                     	xref	_Tx_Length
2988                     	xref	_Tx_Num
3007                     	end

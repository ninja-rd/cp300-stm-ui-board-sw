
///////////////////////////////////////////////////////////
//                                                       //
//          FileName:   Globals.h                        //
//                                                       //
///////////////////////////////////////////////////////////

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#ifdef  EXTERN
#undef  EXTERN
#endif
#ifdef  _MAIN_SRC_
#define EXTERN
#else
#define EXTERN extern
#endif

#include "STM8S005K6.h"

//#include "STM8S105C6.h"
#include "StTypes.h"
#include "Hardware.h"
#include <string.h>


#endif

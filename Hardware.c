
///////////////////////////////////////////////////////////
//                                                       //
//          FileName:   Hardware.c                       //
//                                                       //
///////////////////////////////////////////////////////////

#define  _HARDWARE_SRC_
#include "Globals.h"
#include "Hardware.h"
#include "LED_DISP.h"

#define OPTION_BYTE( ADDR )   ( *( ( @far U8* )( ADDR ) ) )
@inline void MCU_WriteOptionByte( U32 OPB_ADDR, U8 OPB_BYTE )
  {
    if ( OPTION_BYTE( OPB_ADDR ) != OPB_BYTE
      || OPTION_BYTE( OPB_ADDR+1 ) != ( U8 )~OPB_BYTE )
      {
        __WDTC( );
        OPTION_BYTE( OPB_ADDR ) = OPB_BYTE;
        __WDTC( );
        OPTION_BYTE( OPB_ADDR+1 ) = ( U8 )~OPB_BYTE;
      }
  }

void EEP_WriteByte( U8 * p, U8 byte )
  {
    FLASH_CR1   = 0X00;
    FLASH_DUKR  = 0XAE;
    FLASH_DUKR  = 0X56;
    FLASH_CR2   = 0X00;
    FLASH_NCR2  = 0XFF;
    *p = byte;
    FLASH_CR2   = 0X00;
    FLASH_NCR2  = 0XFF;
    FLASH_IAPSR = 0XE7;
  }
  
void MCU_Init( void )
  {
    __DI( );
    // Initialize System Register.
    RST_SR      = 0x00;
    CLK_ICKR    = 0x0D;
    CLK_ECKR    = 0x00;
    CLK_SWCR    = 0x02;
    CLK_SWR     = 0xE1;
    CLK_CKDIVR  = 0x00;
    CLK_PCKENR1 = 0xFF;
    CLK_PCKENR2 = 0xFF;
    CLK_CSSR    = 0x01;
    CLK_CCOR    = 0x00;
    CLK_SWIMCCR = 0x00;
    // Initialize Watchdog. Underflow Time = 250mS.
    IWDG_KR     = 0x55;
    IWDG_PR     = 0x04;
    IWDG_RLR    = 250;
    IWDG_KR     = 0xAA;
    IWDG_KR     = 0xCC;
    // MCU power on delay.
    MCU_Delay( 10000UL );
    // Check & Write optionbytes.
    #if ICP_OPB_ENABLE
    FLASH_CR1   = 0X00;
    FLASH_DUKR  = 0XAE;
    FLASH_DUKR  = 0X56;
    FLASH_CR2   = 0X80;
    FLASH_NCR2  = 0X7F;
    MCU_WriteOptionByte( 0x4801, CFG_OPB_4801 );
    MCU_WriteOptionByte( 0x4803, CFG_OPB_4803 );
    MCU_WriteOptionByte( 0x4805, CFG_OPB_4805 );
    MCU_WriteOptionByte( 0x4807, CFG_OPB_4807 );
    MCU_WriteOptionByte( 0x4809, CFG_OPB_4809 );
    MCU_WriteOptionByte( 0x480B, CFG_OPB_480B );
    MCU_WriteOptionByte( 0x480D, CFG_OPB_480D );
    MCU_WriteOptionByte( 0x487E, CFG_OPB_487E );
    FLASH_CR2   = 0X00;
    FLASH_NCR2  = 0XFF;
    FLASH_IAPSR = 0XE7;
    #endif
    // Initialize I/O port function.
    ADC_TDRL    = CFG_ADC_TDRL;
    ADC_TDRH    = CFG_ADC_TDRH;
    PA_ODR      = CFG_PA_ODR;
    PA_DDR      = CFG_PA_DDR;
    PA_CR1      = CFG_PA_CR1;
    PA_CR2      = CFG_PA_CR2;
    PB_ODR      = CFG_PB_ODR;
    PB_DDR      = CFG_PB_DDR;
    PB_CR1      = CFG_PB_CR1;
    PB_CR2      = CFG_PB_CR2;
    PC_ODR      = CFG_PC_ODR;
    PC_DDR      = CFG_PC_DDR;
    PC_CR1      = CFG_PC_CR1;
    PC_CR2      = CFG_PC_CR2;
    
    PD_ODR      = CFG_PD_ODR;
    PD_DDR      = CFG_PD_DDR;
    PD_CR1      = CFG_PD_CR1;
    PD_CR2      = CFG_PD_CR2;
    PE_ODR      = CFG_PE_ODR;
    PE_DDR      = CFG_PE_DDR;
    PE_CR1      = CFG_PE_CR1;
    PE_CR2      = CFG_PE_CR2;

    PF_ODR      = CFG_PF_ODR;
    PF_DDR      = CFG_PF_DDR;
    PF_CR1      = CFG_PF_CR1;
    PF_CR2      = CFG_PF_CR2;
  
    EXTI_CR1    = CFG_EXTI_CR1;
    EXTI_CR2    = CFG_EXTI_CR2;
    // Initialize Interrupt Level.
    ITC_SPR1    = CFG_ITC_SPR1;
    ITC_SPR2    = CFG_ITC_SPR2;
    ITC_SPR3    = CFG_ITC_SPR3;
    ITC_SPR4    = CFG_ITC_SPR4;
    ITC_SPR5    = CFG_ITC_SPR5;
    ITC_SPR6    = CFG_ITC_SPR6;
    ITC_SPR7    = CFG_ITC_SPR7;
    // Initialize ADC.
    #if 0
    ADC_HTRH   = 0XFF;
    ADC_HTRL   = 0X03;
    ADC_LTRH   = 0X00;
    ADC_LTRL   = 0X00;
    ADC_CR3    = 0X00;
    ADC_CR2    = 0X08;
    ADC_CR1    = 0X71;
    ADC_CSR    = 0X05;
    ADC_CR1    = 0X71;
    #endif
    // Initialize System Timer.
    TIM1_CR1   = 0X80;
    TIM1_CR2   = 0X00;
    TIM1_SMCR  = 0X00;
    TIM1_ETR   = 0X00;
    TIM1_SR1   = 0X00;
    TIM1_SR2   = 0X00;
    TIM1_IER   = 0X01;
    TIM1_EGR   = 0X00;
    TIM1_CCMR1 = 0X00;
    TIM1_CCMR2 = 0X00;
    TIM1_CCMR3 = 0X6C;
    TIM1_CCMR4 = 0X00;
    TIM1_CCER1 = 0X00;
    TIM1_CCER2 = 0X0C;
    TIM1_CNTR  = 0;
    TIM1_PSCRH = 0;
    TIM1_PSCRL = 15;
    //TIM1_ARRH  = 999/256;
    //TIM1_ARRL  = 999%256;
    TIM1_ARRH  = 0;
    //TIM1_ARRL  = 20;
    TIM1_ARRL  = 19;
    TIM1_RCR   = 0;//7
    TIM1_CCR1H = 0;
    TIM1_CCR1L = 0;
    TIM1_CCR2H = 0;
    TIM1_CCR2L = 0;
    TIM1_CCR3H = 0;
    TIM1_CCR3L = 0;
    TIM1_CCR4H = 0;
    TIM1_CCR4L = 0;
    TIM1_BKR   = 0X13;
    TIM1_DTR   = 16;
    TIM1_OISR  = 0X00;
    TIM1_EGR   = 0X20;
    TIM1_CR1  |= 0X01;
    TIM1_BKR  |= 0X80;

 // Initialize BZ PWM control.
    #if 0
    TIM2_CR1   = 0X80;
    TIM2_SR1   = 0X00;
    TIM2_SR2   = 0X00;
    TIM2_IER   = 0X00;
    TIM2_CCMR1 = 0X68;
    TIM2_CCMR2 = 0X00;
    TIM2_CCMR3 = 0X00;
    TIM2_CCER1 = 0X01;
    TIM2_CCER2 = 0X00;
    TIM2_CNTRH = 0;
    TIM2_CNTRL = 0;
    TIM2_PSCR  = 4;
    TIM2_ARRH  = 9999/256;
    TIM2_ARRL  = 9999%256;
    TIM2_CCR1H = 4999/256;
    TIM2_CCR1L = 4999%256;
    TIM2_CCR2H = 0;
    TIM2_CCR2L = 0;
    TIM2_CCR3H = 0;
    TIM2_CCR3L = 0;
    TIM2_EGR   = 0X0F;
    TIM2_CR1   = 0X81;/**/
    #endif
    // Initialize BZ PWM control.
    #if 1
    TIM3_CR1   = 0X00;
    TIM3_SR1   = 0X00;
    TIM3_SR2   = 0X00;
    TIM3_IER   = 0X00;
    TIM3_CCMR1 = 0X00;
    TIM3_CCMR2 = 0X68;
    TIM3_CCER1 = 0X10;
    TIM3_CNTRH = 0;
    TIM3_CNTRL = 0;
    TIM3_PSCR  = 4;        // 16M/ 2^4 = 1us
    
    TIM3_CCR1H = 0;
    TIM3_CCR1L = 0;
    TIM3_CCR2H = 0;
    TIM3_CCR2L = 0;
    TIM3_ARRH  = 0;
    TIM3_ARRL  = 237;     // 238us /    4.2khz
    TIM3_EGR   = 0X0F;
    TIM3_CR1   = 0X81;/**/
    #endif
    // Initialize uart
    UART2_CR1 = 0x00;
    UART2_CR2 = 0x2C;
    UART2_CR3 = 0x00;
    UART2_CR4 = 0x00;// NA
    UART2_CR5 = 0x00;// NA
    UART2_CR6 = 0x00;// NA
    UART2_BRR2 = 0x02;
    UART2_BRR1 = 0x68;// 682 波特率9600
    //UART2_BRR2 = 0x06;
    //UART2_BRR1 = 0x11;//  波特率57600    16000000/57600
    // Initialize Variables.
    
  }


void MCU_Refresh( void )
  {
    ADC_TDRL    = CFG_ADC_TDRL;
    ADC_TDRH    = CFG_ADC_TDRH;
    PA_DDR      = CFG_PA_DDR;
    PA_CR1      = CFG_PA_CR1;
    PA_CR2      = CFG_PA_CR2;
    PB_DDR      = CFG_PB_DDR;
    PB_CR1      = CFG_PB_CR1;
    PB_CR2      = CFG_PB_CR2;
    PC_DDR      = CFG_PC_DDR;
    PC_CR1      = CFG_PC_CR1;
    PC_CR2      = CFG_PC_CR2;
    PD_DDR      = CFG_PD_DDR;
    PD_CR1      = CFG_PD_CR1;
    PD_CR2      = CFG_PD_CR2;
    //PE_DDR      = CFG_PE_DDR;
    //PE_CR1      = CFG_PE_CR1;
    //PE_CR2      = CFG_PE_CR2;

    EXTI_CR1    = CFG_EXTI_CR1;
    EXTI_CR2    = CFG_EXTI_CR2;
    
  }

#define DELAY_1US( )  do { \
    __NOP( ); __NOP( ); __NOP( ); __NOP( ); \
    __NOP( ); __NOP( ); __NOP( ); __NOP( ); \
    __NOP( ); __NOP( ); __NOP( ); __NOP( ); \
    __NOP( ); __NOP( ); __NOP( ); __NOP( ); \
    } while(0)


static U8 Led_Drive_Step = 0;
static U8 Led_Drive_Cnt  = 0;
static U8 Led_Drive_PWM[ 7 ];

void Led_Drive_Clr( void )
{
    COM0_OFF( );
    COM1_OFF( );
    COM2_OFF( );
    COM3_OFF( );
    COM4_OFF( );
    SEG0_OFF( );
    SEG1_OFF( );
    SEG2_OFF( );
    SEG3_OFF( );
    SEG4_OFF( );
    SEG5_OFF( );
    SEG6_OFF( );
    Led_Drive_Step++;
}

void Com0_led_Init( void )
{
    COM0_ON( );
    Led_Drive_Cnt = 98;
    Led_Drive_PWM[ 0 ] = Led_Type;
    Led_Drive_PWM[ 1 ] = Led_Clean;
    Led_Drive_PWM[ 2 ] = Led_Power;
    Led_Drive_PWM[ 3 ] = Led_BAR2;
    Led_Drive_PWM[ 4 ] = Led_BAR3;
    Led_Drive_PWM[ 5 ] = Led_BAR4;
    Led_Drive_PWM[ 6 ] = Led_BAR5;
    Led_Drive_Step++;
}

void Com1_led_Init( void )
{
  COM1_ON( );
  Led_Drive_Cnt = 98;
  Led_Drive_PWM[ 0 ] = Led_White;
  Led_Drive_PWM[ 1 ] = Led_Speciality;
  Led_Drive_PWM[ 2 ] = Led_Green;
  Led_Drive_PWM[ 3 ] = Led_Immersion;
  Led_Drive_PWM[ 4 ] = Led_Delicate;
  Led_Drive_PWM[ 5 ] = Led_Style;
  Led_Drive_PWM[ 6 ] = Led_Start;
  Led_Drive_Step++;
}

void Com2_led_Init( void )
{
  COM2_ON( );
  Led_Drive_Cnt = 98;
  Led_Drive_PWM[ 0 ] = Led_Classic;
  Led_Drive_PWM[ 1 ] = Led_Herbal;
  Led_Drive_PWM[ 2 ] = Led_Rich;
  Led_Drive_PWM[ 3 ] = Led_Black;
  Led_Drive_PWM[ 4 ] = Led_Overice;
  Led_Drive_PWM[ 5 ] = Led_Oolong;
  Led_Drive_PWM[ 6 ] = Led_Coldbrew;
  Led_Drive_Step++;
}

void Com3_led_Init( void )
{
  COM3_ON( );
  Led_Drive_Cnt = 98;
  Led_Drive_PWM[ 0 ] = Led_Xltravel;
  Led_Drive_PWM[ 1 ] = Led_Xlcup;
  Led_Drive_PWM[ 2 ] = Led_Halfcarafe;
  Led_Drive_PWM[ 3 ] = Led_Cup;
  Led_Drive_PWM[ 4 ] = Led_Fullcarafe;
  Led_Drive_PWM[ 5 ] = Led_BAR1;
  Led_Drive_PWM[ 6 ] = Led_Addone;
  Led_Drive_Step++;
}

void Com4_led_Init( void )
{
  COM4_ON( );
  Led_Drive_Cnt = 98;
  Led_Drive_PWM[ 0 ] = Led_Insertbasket;
  Led_Drive_PWM[ 1 ] = Led_Delayset;
  Led_Drive_PWM[ 2 ] = Led_Clean2;
  Led_Drive_PWM[ 3 ] = Led_Keepwarm;
  Led_Drive_PWM[ 4 ] = Led_Tea;
  Led_Drive_PWM[ 5 ] = Led_Coffee;
  Led_Drive_PWM[ 6 ] = Led_Travel;
  Led_Drive_Step++;
}

void Seg_Led_Out( void )
{
   Led_Drive_Cnt--;
   if( Led_Drive_Cnt < Led_Drive_PWM[ 0 ] ) SEG0_ON( );
   if( Led_Drive_Cnt < Led_Drive_PWM[ 1 ] ) SEG1_ON( );
   if( Led_Drive_Cnt < Led_Drive_PWM[ 2 ] ) SEG2_ON( );
   if( Led_Drive_Cnt < Led_Drive_PWM[ 3 ] ) SEG3_ON( );
   if( Led_Drive_Cnt < Led_Drive_PWM[ 4 ] ) SEG4_ON( );
   if( Led_Drive_Cnt < Led_Drive_PWM[ 5 ] ) SEG5_ON( );
   if( Led_Drive_Cnt < Led_Drive_PWM[ 6 ] ) SEG6_ON( );
   if( Led_Drive_Cnt == 0 )  Led_Drive_Step++;
}

void Seg_Led_Out_E( void )
{
   Led_Drive_Cnt--;
   if( Led_Drive_Cnt < Led_Drive_PWM[ 0 ] ) SEG0_ON( );
   if( Led_Drive_Cnt < Led_Drive_PWM[ 1 ] ) SEG1_ON( );
   if( Led_Drive_Cnt < Led_Drive_PWM[ 2 ] ) SEG2_ON( );
   if( Led_Drive_Cnt < Led_Drive_PWM[ 3 ] ) SEG3_ON( );
   if( Led_Drive_Cnt < Led_Drive_PWM[ 4 ] ) SEG4_ON( );
   if( Led_Drive_Cnt < Led_Drive_PWM[ 5 ] ) SEG5_ON( );
   if( Led_Drive_Cnt < Led_Drive_PWM[ 6 ] ) SEG6_ON( );
   if( Led_Drive_Cnt == 0 )  Led_Drive_Step = 0;
}

void (* const LED_Drive_Tab[ ])( void ) = 
{
  Led_Drive_Clr,
  Com0_led_Init,
  Seg_Led_Out,
  
  Led_Drive_Clr,
  Com1_led_Init,
  Seg_Led_Out,
  
  Led_Drive_Clr,
  Com2_led_Init,
  Seg_Led_Out,
  #if 1
  Led_Drive_Clr,
  Com3_led_Init,
  Seg_Led_Out,
  
  Led_Drive_Clr,
  Com4_led_Init,
  Seg_Led_Out_E,
  #endif
  Led_Drive_Clr
  
};
// @nosvf 不保存C_X寄存器
@far @nosvf @interrupt void SystemTimerISR( void ) // 20us
//@far  @interrupt void SystemTimerISR( void ) // 20us

  {
    //_asm(" BSET _PA_ODR,#2 ");
    TIM1_SR1 = 0X00;
    sysTicks++;
    (*LED_Drive_Tab[ Led_Drive_Step & 0x0F ])( );
    //_asm(" BRES _PA_ODR,#2 ");
  }

U16  ADC_Convert( U8 chn )
  {
    U16 buf;
    ADC_HTRH = 0XFF;
    ADC_HTRL = 0X03;
    ADC_LTRH = 0X00;
    ADC_LTRL = 0X00;
    ADC_CSR  = chn;
    ADC_CR3  = 0X00;
    ADC_CR2  = 0X08;
    ADC_CR1  = 0X71;
    buf = 1000;
    while ( buf-- )
      {
        if ( ADC_CSR & 0X80 ) break;
      }
    return ADC_DR;
  }



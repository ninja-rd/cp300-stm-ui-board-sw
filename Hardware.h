
///////////////////////////////////////////////////////////
//                                                       //
//          FileName:   Hardware.h                       //
//                                                       //
///////////////////////////////////////////////////////////

#ifndef _HARDWARE_H_
#define _HARDWARE_H_

#ifdef  EXTERN
#undef  EXTERN
#endif
#ifdef  _HARDWARE_SRC_
#define EXTERN
#else
#define EXTERN extern
#endif

#define ICP_OPB_ENABLE  0
#define CFG_OPB_4801    0x00
#define CFG_OPB_4803    0x00
#define CFG_OPB_4805    0x00
#define CFG_OPB_4807    0x00
#define CFG_OPB_4809    0x00
#define CFG_OPB_480B    0x00
#define CFG_OPB_480D    0x00
#define CFG_OPB_487E    0x00

#define CFG_ADC_TDRL    0x00
#define CFG_ADC_TDRH    0x00  // 无AD采样

#define CFG_PA_ODR      0x00
#define CFG_PA_DDR      0x04 // PA1,PA2:晶振
#define CFG_PA_CR1      0x04
#define CFG_PA_CR2      0x04

#define CFG_PB_ODR      0x03
#define CFG_PB_DDR      0x03 // PB5:ENC_D, PB4:ENC_C,   PB3:ENC_B, PB2:ENC_A,  PB1:GR1,PB0: GR0 
#define CFG_PB_CR1      0x3F // PB5 ~PB1:带上拉输入 ，PB0:推挽输出
#define CFG_PB_CR2      0x03 // PB5 ~PB1:输入无中断,     PB0:输出速率10M

#define CFG_PC_ODR      0x00
#define CFG_PC_DDR      0xFE // PC7 ~ PC1 : SG1 ~SG7 用作SEG输出,PC0:无
#define CFG_PC_CR1      0xFE
#define CFG_PC_CR2      0xFE

#define CFG_PD_ODR      0xFE
#define CFG_PD_DDR      0xBF // PD7: PT6964_STB, PD6 :RX,  PD5:TX,  PD4: GR5,PD3:GR4, PD2:GR3, PD1:SWIM,PD0:BUZ
#define CFG_PD_CR1      0xBF
#define CFG_PD_CR2      0xBF

#define CFG_PE_ODR      0x00
#define CFG_PE_DDR      0x20 // PE5: PT6964_DIO
#define CFG_PE_CR1      0x20
#define CFG_PE_CR2      0x00

#define CFG_PF_ODR      0X10
#define CFG_PF_DDR      0X10 // PT6964_CLK
#define CFG_PF_CR1      0X10
#define CFG_PF_CR2      0X10

#define CFG_EXTI_CR1    0x00
#define CFG_EXTI_CR2    0x00

#define CFG_ITC_SPR1    0x55
#define CFG_ITC_SPR2    0x55
#define CFG_ITC_SPR3    0x55
#define CFG_ITC_SPR4    0x45
#define CFG_ITC_SPR5    0x55
#define CFG_ITC_SPR6    0x51
#define CFG_ITC_SPR7    0x55


//======LED灯COM & SEG===========//
#define COM0_ON( )    _asm(" BRES _PB_ODR,#1 ")
#define COM0_OFF( )   _asm(" BSET _PB_ODR,#1 ")
#define COM1_ON( )    _asm(" BRES _PB_ODR,#0 ")
#define COM1_OFF( )   _asm(" BSET _PB_ODR,#0 ")
#define COM2_ON( )    _asm(" BRES _PD_ODR,#2 ")
#define COM2_OFF( )   _asm(" BSET _PD_ODR,#2 ")
#define COM3_ON( )    _asm(" BRES _PD_ODR,#3 ")
#define COM3_OFF( )   _asm(" BSET _PD_ODR,#3 ")
#define COM4_ON( )    _asm(" BRES _PD_ODR,#4 ")
#define COM4_OFF( )   _asm(" BSET _PD_ODR,#4 ")

#define SEG0_ON( )    _asm(" BSET _PC_ODR,#7 ")
#define SEG0_OFF( )   _asm(" BRES _PC_ODR,#7 ")
#define SEG1_ON( )    _asm(" BSET _PC_ODR,#6 ")
#define SEG1_OFF( )   _asm(" BRES _PC_ODR,#6 ")
#define SEG2_ON( )    _asm(" BSET _PC_ODR,#5 ")
#define SEG2_OFF( )   _asm(" BRES _PC_ODR,#5 ")
#define SEG3_ON( )    _asm(" BSET _PC_ODR,#4 ")
#define SEG3_OFF( )   _asm(" BRES _PC_ODR,#4 ")
#define SEG4_ON( )    _asm(" BSET _PC_ODR,#3 ")
#define SEG4_OFF( )   _asm(" BRES _PC_ODR,#3 ")
#define SEG5_ON( )    _asm(" BSET _PC_ODR,#2 ")
#define SEG5_OFF( )   _asm(" BRES _PC_ODR,#2 ")
#define SEG6_ON( )    _asm(" BSET _PC_ODR,#1 ")
#define SEG6_OFF( )   _asm(" BRES _PC_ODR,#1 ")




//======================//
// STB 配置
#define PIN_TB_CPL()       _asm(" BCPL _PD_ODR,#7 ")        
#define PIN_PT_TB_SET( )   _asm(" BSET _PD_ODR,#7 ")
#define PIN_PT_TB_CLR( )   _asm(" BRES _PD_ODR,#7 ")
#define PT_TB_OUT( ) do {  _asm(" BSET _PD_DDR,#7 "); } while (0)

//  CLK 配置
#define PIN_CK_CPL()       _asm(" BCPL _PF_ODR,#4 ") 

#define PIN_PT_CK_SET( )   _asm(" BSET _PF_ODR,#4 ")
#define PIN_PT_CK_CLR( )   _asm(" BRES _PF_ODR,#4 ")
#define PT_CK_OUT( ) do {  _asm(" BSET _PF_DDR,#4 "); } while (0)

//   DI/O配置
#define PIN_DI_CPL()       _asm(" BCPL _PE_ODR,#5 ") 

#define PIN_PT_DI_SET( )   _asm(" BSET _PE_ODR,#5 ")
#define PIN_PT_DI_CLR( )   _asm(" BRES _PE_ODR,#5 ")
#define PT_DI_OUT( ) do {  _asm(" BSET _PE_DDR,#5 "); } while (0)

#define PIN_PT_DO( )   ( BTST( PE_IDR, 5 ) )
#define PT_DO_IN( )  do {  _asm(" BRES _PE_DDR,#5 "); } while (0) // 置数据口为输入

#define PIN_TEST_CPL()       _asm(" BCPL _PA_ODR,#2 ") 

EXTERN  U8  keyIDR;

EXTERN  U8  hswIDR;
EXTERN  U16 hswTime;

#define SEGA          0X01
#define SEGB          0X02
#define SEGC          0X04
#define SEGD          0X08
#define SEGE          0X10
#define SEGF          0X20
#define SEGG          0X40
#define SEGP          0X80
#define C_DIGIT_0     (SEGA|SEGB|SEGC|SEGD|SEGE|SEGF)
#define C_DIGIT_1     (SEGB|SEGC)
#define C_DIGIT_2     (SEGA|SEGB|SEGD|SEGE|SEGG)
#define C_DIGIT_3     (SEGA|SEGB|SEGC|SEGD|SEGG)
#define C_DIGIT_4     (SEGB|SEGC|SEGF|SEGG)
#define C_DIGIT_5     (SEGA|SEGC|SEGD|SEGF|SEGG)
#define C_DIGIT_6     (SEGA|SEGC|SEGD|SEGE|SEGF|SEGG)
#define C_DIGIT_7     (SEGA|SEGB|SEGC)
#define C_DIGIT_8     (SEGA|SEGB|SEGC|SEGD|SEGE|SEGF|SEGG)
#define C_DIGIT_9     (SEGA|SEGB|SEGC|SEGD|SEGF|SEGG)
#define C_DIGIT_A     (SEGA|SEGB|SEGC|SEGE|SEGF|SEGG)
#define C_DIGIT_b     (SEGC|SEGD|SEGE|SEGF|SEGG)
#define C_DIGIT_C     (SEGA|SEGD|SEGE|SEGF)
#define C_DIGIT_d     (SEGB|SEGC|SEGD|SEGE|SEGG)
#define C_DIGIT_E     (SEGA|SEGD|SEGE|SEGF|SEGG)
#define C_DIGIT_F     (SEGA|SEGE|SEGF|SEGG)

EXTERN U8  ledSegs[ 5 ];

EXTERN U8  sysTicks;

@inline void MCU_Delay( U32 n ) { while ( n-- ) __WDTC( ); }

EXTERN void MCU_Init( void );
EXTERN void MCU_Refresh( void );
EXTERN void EEP_WriteByte( U8 * p, U8 byte );
EXTERN U16  ADC_Convert( U8 chn );

#endif


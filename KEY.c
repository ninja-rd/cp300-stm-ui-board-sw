
///////////////////////////////////////////////////////////
//                                                       //
//          FileName:   COM_ODU.c                        //
//                                                       //
///////////////////////////////////////////////////////////

#define  _KEY_SRC_
#include "Globals.h"
#include "KEY.h"
#include "Pt696x.h"
#include "COM_ODU.h"
#include "Beeper.h"


#define STYLE_S   1
#define TYPE_S    2
#define DEBUG_MAX  6

U16 SRAM keyTime;
U16 SRAM keyData;
U16 SRAM keyScan;
U16 SRAM keyScan_old;
U8  SRAM keyCode;
U8  SRAM keyLast;
U8  SRAM keyCopy;


U16 Delay_Brew_Min_Total  = 0;
U16 Clock_Min_Total       = 0;
U16 dimDelay10minute      = 0;
void Key_Init( void )
{
  
    memset( KEY_Buff, 0x00, sizeof( KEY_Buff ) );
    Knob0_count [ 1 ]  = fullcarafe;
    Knob1_count [ 1 ]  = Classic;
    Knob0_count [ 2 ]  = fullcarafe;
    Knob1_count [ 2 ]  = Classic;
    Knob2_count        = Herbal;

    Clock_Set_Time  = 3;
    Clock.hour                  = 12;
    Clock.hour_b                = 12;
    Clock.min                   = 0;
    Clock.min_b                 = 0;
    Clock.second                = 0;

    Clock_DLY_BREW.hour         = 12;
    Clock_DLY_BREW.hour_b       = 12;
    Clock_DLY_BREW.min          = 0;
    Clock_DLY_BREW.min_b        = 0;
    Clock_DLY_BREW.second       = 0;

    Clock_Clean.hour            = 0;
    Clock_Clean.hour_b          = 0;
    Clock_Clean.min             = 59;
    Clock_Clean.min_b           = 0;
    Clock_Clean.second          = 59;

    Clock_Keep_Warm.hour        = 2;
    Clock_Keep_Warm.hour_b      = 0;
    Clock_Keep_Warm.min         = 0;
    Clock_Keep_Warm.min_b       = 0;
    Clock_Keep_Warm.second      = 0;
    
    System_Status = ACTIVE;
    Standby_Timer = 300;// 上电5分钟无操作进行待机模式

    Start_Status  = OFF;// 关机模式，
    //Brew_Select   = COFFEE;
    if( F_KEY_TEA_BASKET )
      Brew_Select   = TEA;
    else if( F_KEY_COFFEE_BASKET )
      Brew_Select   = COFFEE;
    else 
      Brew_Select   = NO_BASKET;
    Style_Type_Flag = 0;
    SET_Keep_Warm_Time    = 0;
    Delay_Keep_Warm_Time  = 0;
    Delay_Keep_Warm_Level = 0;

    CANCEL_DELAY_BREW_3S  = 0;
    Delay_Brew_Time       = 0;

    Keep_Warm_Class       = WARM_HI;
    Complete_Sound_Delay  = 0;
    Pause_Sound_Delay     = 0;
    brewAdjust15S         = 0;
    brewNoaction3S        = 0;

    delayBrewPause        = 0;
    delayAdjust15S        = 0;
    delayNoaction3S       = 0;
    delayExitCnt          = 0;
    cleanAlarm            = 0;
    cleanStart            = 0;
    forcePump             = 0;
    debugEnable           = 0;
    
}

void Clock_Cnt( void )
{
    if( ++Clock.second> 59)
    {
      Clock.second = 0;
      if(++Clock.min > 59)
      {
        Clock.min = 0;
        if( Clock.hour == 11 )
        {
          Clock.hour = 12;
          AM_PM_FLAG ^= 1;
        }
        else if( Clock.hour == 12)
          Clock.hour = 1;
        else
          Clock.hour++;
      }
    }
}
void Delay_Brew_Sure( void )
{
    DLY_BREW_FLAG = 0;
    Delay_Brew_Time = 0;
    Style_Type_Flag = 0;
    BeepStart( Confirm_Sound );
    Clock_DLY_BREW.hour = Clock_DLY_BREW.hour_b;
    Clock_DLY_BREW.min  = Clock_DLY_BREW.min_b;
    Delay_Brew_Min_Total = (U16)(Clock_DLY_BREW.hour * 60) + (U16)Clock_DLY_BREW.min;
    DLY_BREW_FUNC = 1;
    delayAdjust15S = DELAY_ADJUST_TIME;
}

void Time_Deal(void)
{
   static U8 t400ms = 0;
   static U8 t500ms = 0;
   static U8 time   = 0;
   static U8 t600ms = 0;
   static U8 t750ms = 0;
   DECEX(Clock_Stay_Time);
   //======400MS==============//
   if( ++t400ms >= 40 )
   {
      t400ms = 0;
      _400ms_flag ^= 1;
   }
   //======500MS==============//
   if( ++t500ms >= 50 )
   {
      t500ms = 0;
      _500ms_flag ^= 1;
   }
   //=======1S=================//
   if( ++time >= 100)
   {
      time = 0;
      _1s_flag ^= 1;
      
      if( System_Status == ACTIVE  && Start_Status == OFF )
      {
         //if( CLEAN_DISP_FLAG  ||  KEEP_WARM_FUNC  || DLY_BREW_FUNC )
         if( CLEAN_DISP_FLAG  ||  KEEP_WARM_FUNC )
         {
           Standby_Timer = 300;
         }
         else
         {
          if( Standby_Timer )
          {
            Standby_Timer--;
            if( Standby_Timer == 0 )
            {
              //System_Status = STANDBY;
              System_Status = DIM_STATUS;
              dimDelay10minute = 600;
            }
          }
         }
        
      }
      else
      {
        Standby_Timer = 300;
      }

      if( System_Status == DIM_STATUS)
      {
        if( dimDelay10minute ) dimDelay10minute--;
        if( dimDelay10minute == 0) System_Status = STANDBY;
      }
      
      if( Clock_Set_Time )
      {
         Clock_Set_Time--;
         if( Clock_Set_Time == 0 )
         {
           Clock.hour = Clock.hour_b;
           Clock.min  = Clock.min_b;
           Clock.second = 0;
         }
      }
      else
      Clock_Cnt( );

      if ( Paused_Delay ) 
      {
        Paused_Delay--;
        if( Paused_Delay <= PAUSE_TIME-20 )
        {
          if( Pause_Sound_Delay )
          {
           Pause_Sound_Delay--;
           BeepStart( Reminder_Sound );
          }
        }
        if( 0 == Paused_Delay )
        {
          //Start_Status = OFF;
          Start_Status = CANCEL;
          KEEP_WARM_FUNC  = 0;
          WARM_LEVEL_FUNC = 0;
          CLEAN_FUNC_FLAG = 0;
          BeepStart( Cancel_Sound );
        }
        if( Brew_Select_B != NO_BASKET )
        { 
          
          if( Brew_Select_B == Brew_Select )
          {
            if( NO_BASKET_PAUSE )
            {
             NO_BASKET_PAUSE = 0;
             Paused_Delay = 0;
             Start_Status = UNPAUSE;
            }
          }
          else
          {
            Start_Status = CANCEL;
            KEEP_WARM_FUNC  = 0;
            WARM_LEVEL_FUNC = 0;
            CLEAN_FUNC_FLAG = 0;
            BeepStart( Cancel_Sound );
          }
        }
        
      }
      else
      {
        if(( Start_Status == ON && Brew_Select_B == NO_BASKET )
          ||( Start_Status == UNPAUSE && Brew_Select_B == NO_BASKET ))
        {
          NO_BASKET_PAUSE = 1;
          Start_Status = PAUSE;
          Paused_Delay = PAUSE_TIME;
        }
      }
      DECEX( brewAdjust15S );
      DECEX( brewNoaction3S );
      if( brewAdjust15S == 0)
      {
        if( brewNoaction3S )
        {
          BeepStart( Cancel_Sound );
          Start_Status = OFF;
          KEEP_WARM_FUNC = 0;
        }
      }
      if( Delay_Keep_Warm_Time )
      {
        Delay_Keep_Warm_Time--;
        if( Delay_Keep_Warm_Time == 0 )
        {
          
          BeepStart( Confirm_Sound );
        }
      }
      if( Delay_Keep_Warm_Level )
      {
        Delay_Keep_Warm_Level--;
        if( Delay_Keep_Warm_Level == 0 )
        {
          BeepStart( Confirm_Sound );
        }
      }
      // 完成烹煮后，开始保温功能
      if((Halfcarafe == Knob0_count [ Brew_Select ]  || fullcarafe == Knob0_count [ Brew_Select ]  )
                   && (Classic == Knob1_count [ Brew_Select ]  || Rich == Knob1_count [ Brew_Select ] ))
      {
        if( Keep_Warm_Class == WARM_OFF )
        {
          F_KEEP_WARM_START = 0;
          if( F_KEEP_WARM_START )
            KEEP_WARM_FUNC = 1;
        }
        else
        {
         if( F_KEEP_WARM_START )
            KEEP_WARM_FUNC = 1;
        }
      }
      if( KEEP_WARM_FUNC )
      {
        if( ++Clock_Keep_Warm.second > 59)
        {
          Clock_Keep_Warm.second = 0;
          if( ++Clock_Keep_Warm.min_b > 59 )
          {
            Clock_Keep_Warm.min_b = 0;
            if( ++Clock_Keep_Warm.hour_b > 4 )
              Clock_Keep_Warm.hour_b = 0;
          }
        }
        if( Clock_Keep_Warm.min == Clock_Keep_Warm.min_b
          && Clock_Keep_Warm.hour == Clock_Keep_Warm.hour_b )
        {
          KEEP_WARM_FUNC = 0; 
          F_KEEP_WARM_START = 0;
          Clock_Keep_Warm.second = 0;
          Clock_Keep_Warm.min_b  = 0;
          Clock_Keep_Warm.hour_b = 0;
        }
      }
       if( DELAY_SET_TYPE && DELAY_SET_SIZE && DELAY_SET_STYLE )
       {
         if( Delay_Brew_Time > 5 )
          Delay_Brew_Time = 5;
       }
       if( Delay_Brew_Time )
       {
          Delay_Brew_Time--;
         
          if( Delay_Brew_Time == 0 )
          {
            if( DELAY_SET_TYPE && DELAY_SET_SIZE && DELAY_SET_STYLE )
            {
              if( DLY_BREW_FLAG )
              {
                Delay_Brew_Sure( );
              }
            }
            DLY_BREW_FLAG   = 0;
            DELAY_SET_TYPE  = 0;
            DELAY_SET_SIZE  = 0;
            DELAY_SET_STYLE = 0;
          }
          
      }
      if( DLY_BREW_FUNC )
      {
         Clock_Min_Total = (U16)(Clock.hour * 60) + (U16)Clock.min;
         if( AM_PM_FLAG == DELAY_AM_PM_FLAG)
         {
            if(( Clock_Min_Total == Delay_Brew_Min_Total)
              && ( Clock.second == Clock_DLY_BREW.second ))
            {
              DLY_BREW_FUNC = 0;
              Start_Status = ON;
            }
         }
         if( Brew_Select_B == NO_BASKET )
         {
          if( delayBrewPause == 0)
           {
            delayBrewPause = DELAYPUASE;
            Pause_Sound_Delay = 10;
           }
         }
         else if( Brew_Select_B == Brew_Select )
         {
           delayBrewPause = 0;
           Pause_Sound_Delay = 0;
         }
         else if( Brew_Select_B != Brew_Select )
         {
           delayBrewPause = 0;
           Pause_Sound_Delay = 0;
           DLY_BREW_FUNC = 0;
         }
        if( delayBrewPause )
        {
         delayBrewPause--;
         if( delayBrewPause <= DELAYPUASE - 20 )
         {
          if( Pause_Sound_Delay )
          {
           Pause_Sound_Delay--;
           BeepStart( Reminder_Sound );
          }
         }
         if( delayBrewPause == 0)
          DLY_BREW_FUNC = 0;
        }

        DECEX( delayAdjust15S );
        DECEX( delayNoaction3S );
        if( delayAdjust15S == 0)
        {
          if( delayNoaction3S )
          {
            BeepStart( Cancel_Sound );
            DLY_BREW_FUNC = 0;
          }
        }
          
      }
      DECEX( CANCEL_DELAY_BREW_3S );
      DECEX( CLean_Blink_10count );
      if( cleanAlarm )
      {
        cleanAlarm--;
      }
      if( cleanStart )
      {
        cleanStart--;
        if( cleanStart == 0 )
          CLEAN_DISP_FLAG = 0;
      }
      if( CLEAN_FUNC_FLAG )
      {
          if( Start_Status == ON || Start_Status == UNPAUSE )
          {
            if( Clock_Clean.second )
              Clock_Clean.second--;
            else if( Clock_Clean.min )
            {
              Clock_Clean.second = 59;
              Clock_Clean.min--;
            }
            else if( Clock_Clean.hour ) 
            {
               Clock_Clean.second = 59;
               Clock_Clean.min    = 59;
               Clock_Clean.hour--;
            }
            if(( Clock_Clean.hour == 0) && ( Clock_Clean.min == 0))
            {
                CLEAN_FUNC_FLAG = 0;
            }
          }
       }
      if( Complete_Sound_Delay )
      {
        Complete_Sound_Delay--;
        if( F_Brew_complete )
        
        BeepStart( Complete_Sound );
        else if( F_Brew_abnormal_end )
        BeepStart( Reminder_Sound );
      }
   }
   //===========================//
}

void Key_Scan(void)
{
  //static U16 SRAM	keyScanBuf[ 3 ];
  static WORD key_scan_old = { 0 };
  static U8  key_time;
  WORD key_scan = { 0 };
  U8   key[5];
  memset( key, 0x00, sizeof( key ) );
	Pt696xRd(key,sizeof( key ));
  if( key[0] & 0x01 ) key_scan.bits.b0  = 1;
	if( key[0] & 0x02 ) key_scan.bits.b1  = 1;
  if( key[0] & 0x08 ) key_scan.bits.b2  = 1;
	if( key[0] & 0x10 ) key_scan.bits.b3  = 1;
  
  if( key[1] & 0x01 ) key_scan.bits.b4  = 1;
	if( key[1] & 0x02 ) key_scan.bits.b5  = 1;
  if( key[1] & 0x08 ) key_scan.bits.b6  = 1;
	if( key[1] & 0x10 ) key_scan.bits.b7  = 1;

  if( key[2] & 0x01 ) key_scan.bits.b8  = 1;
	if( key[2] & 0x02 ) key_scan.bits.b9  = 1;
  if( key[2] & 0x08 ) key_scan.bits.b10 = 1;
	if( key[2] & 0x10 ) key_scan.bits.b11 = 1;
  
  if( key[3] & 0x01 ) key_scan.bits.b12 = 1;
	if( key[3] & 0x02 ) key_scan.bits.b13 = 1;
  if( key[3] & 0x08 ) key_scan.bits.b14 = 1;
	if( key[3] & 0x10 ) key_scan.bits.b15 = 1;

  if( key_scan_old.word != key_scan.word )
  {
    key_time = 0;
    key_scan_old.word = key_scan.word;
  }
  else
  {
    INCEX( key_time );
    if( key_time == 2 )
    {
           keyScan = key_scan.word;
           if( keyScan == 0 )
             keyScan_old = 0;
           
    }
  }
} 


void KeyGenerate( void )
  {
    if ( keyData != keyScan )
    {
        //keyData = keyScan;
        if(keyScan_old == 0)
        {
            keyScan_old= keyScan;
            keyData = keyScan;
        }
        else if( keyScan == C_SC_HOUR_STYLE 
              || keyScan == C_SC_MINUTE_TYPE 
              || keyScan == C_SC_CLEAN_STYLE
              || keyScan == C_SC_MINUTE_HOUR)
        {
          keyData = keyScan;
        }
        else
          keyData = 0;
          
        keyTime = C_KT_REPEAT;
    }
    else
    {
        INCEX( keyTime );
        switch ( keyTime )
          {
            case C_KT_SHORT:
               {
                  keyLast = keyCopy;
                  switch( keyData )
                  {
                    case C_SC_NONE:       keyCode = C_KC_NONE;                              break;

                    case C_SC_HOUR:       keyCode = C_KC_HOUR;       KEY_Buff[ 0 ] = 0x01;  break;
                    case C_SC_MINUTE:     keyCode = C_KC_MINUTE;     KEY_Buff[ 1 ] = 0x01;  break;
                    case C_SC_KEEPWARM:   keyCode = C_KC_KEEPWARM;   KEY_Buff[ 2 ] = 0x01;  break;
                    case C_SC_DELAYSET:   keyCode = C_KC_DELAYSET;   KEY_Buff[ 3 ] = 0x01;  break;

                    case C_SC_STYLE:      keyCode = C_KC_STYLE;      KEY_Buff[ 4 ] = 0x01;  break;
                    case C_SC_CLEAN:      keyCode = C_KC_CLEAN;      KEY_Buff[ 5 ] = 0x01;  break;
                    case C_SC_START:      keyCode = C_KC_START;      KEY_Buff[ 6 ] = 0x01;  break;
                    case C_SC_TYPE:       keyCode = C_KC_TYPE;       KEY_Buff[ 7 ] = 0x01;  break;

                    case C_SC_POWER:      keyCode = C_KC_POWER;      KEY_Buff[ 8 ] = 0x01;  break;
                    case C_SC_HOUR_STYLE: keyCode = C_KC_HOUR_STYLE;                        break;
                    case C_SC_MINUTE_TYPE: keyCode = C_KC_MINUTE_TYPE;                      break;
                    case C_SC_CLEAN_STYLE: keyCode = C_KC_CLEAN_STYLE;                      break;
                    case C_SC_MINUTE_HOUR: keyCode = C_KC_MINUTE_HOUR;                      break;
                    default:              keyCode = C_KC_UNDEF;         break;
                  }
               }
               keyCopy = keyCode;
               break;
            case C_KT_1SEC:
                keyCode = ( keyCode & 0x1f ) | C_KS_1SEC;
                keyCopy = keyCode;
                break;
            case C_KT_2SEC:
                keyCode = ( keyCode & 0x1f ) | C_KS_2SEC;
                keyCopy = keyCode;
                break;
            case C_KT_5SEC:
                keyCode = ( keyCode & 0x1f ) | C_KS_5SEC;
                keyCopy = keyCode;
                break;
            case C_KT_10SEC:
                keyCode = ( keyCode & 0x1f ) | C_KS_10SEC;
                keyCopy = keyCode;
                break;
            case C_KT_30SEC:
                keyCode = ( keyCode & 0x1f ) | C_KS_30SEC;
                keyCopy = keyCode;
                break;
            default:
                break;

          }
    }
}
void KeyRepeat( void )
  {
    if ( keyTime >= C_KT_REPEAT )
      {
        keyTime -= C_KT_REPEAT;
      }
  }

void KeyHandler( void )
{
  
  U8  key;
  
  if ( C_KS_DEALED != ( keyCode & 0xe0 ) ) 
  {
        key = keyCode;
        keyCode |= C_KS_DEALED;
        switch( key ) 
        {
          case C_KC_NONE:
               if( SET_WARM_LEVEL_FLAG )
               {
                 Delay_Keep_Warm_Time = 5;
                 SET_WARM_LEVEL_FLAG =0;
               }
               if( keyLast == C_KC_KEEPWARM )
               {
                 //Delay_Brew_Time = 0;
                 Standby_Timer = 300;
                 if( System_Status == DIM_STATUS)
                 {
                  System_Status = ACTIVE;
                  break;
                 }
                 if( DLY_BREW_FUNC )
                 {
                  DLY_BREW_FUNC = 0;
                  break;
                 }
                 
                 //if( NO_BASKET == Brew_Select )
                  //break;
                 //if( System_Status == STANDBY )
                  //break;
                 if( CLEAN_DISP_FLAG || CLEAN_FUNC_FLAG)
                   break;
                 if((Halfcarafe == Knob0_count [ Brew_Select ]  || fullcarafe == Knob0_count [ Brew_Select ]  )
                   && (Classic == Knob1_count [ Brew_Select ]  || Rich == Knob1_count [ Brew_Select ] ))
                 {
                     if( Delay_Keep_Warm_Time )
                     {
                      Delay_Keep_Warm_Time = 0;
                      BeepStart( Confirm_Sound );
                      break;
                     }
                     if( Delay_Keep_Warm_Level )
                     {
                      Delay_Keep_Warm_Level = 0;
                      BeepStart( Confirm_Sound );
                      break;
                     }
                     KEEP_WARM_FUNC ^= 1;
                     if( F_KEEP_WARM_START )
                           F_KEEP_WARM_START = 0;// 自动保温后，再按时停止保温
                     if( KEEP_WARM_FUNC == 0 )
                     {
                      Clock_Keep_Warm.second = 0;
                      Clock_Keep_Warm.min_b  = 0;
                      Clock_Keep_Warm.hour_b = 0;
                     }
                 }
               }
               else if( keyLast == C_KC_START)
               {
                   Standby_Timer = 300;
                   Delay_Keep_Warm_Time = 0;
                   Delay_Keep_Warm_Level = 0;
                   if( System_Status == DIM_STATUS)
                   {
                    System_Status = ACTIVE;
                    break;
                   }
                   if( Brew_Select == NO_BASKET )
                    return;
                   //if( DLY_BREW_FUNC )
                     // break;
                   if( System_Status == ACTIVE )
                   {
                     if( OFF == Start_Status )
                     {
                       Start_Status    = ON;
                       Style_Type_Flag = 0;
                       Delay_Brew_Time       = 0;
                       brewAdjust15S    = BREW_ADJUST_TIME;
                       DLY_BREW_FLAG         = 0;
                       DLY_BREW_FUNC   = 0;
                       
                       if( CLEAN_DISP_FLAG )
                       {
                        CLEAN_DISP_FLAG = 0;
                        cleanStart      = 0;
                        CLEAN_FUNC_FLAG = 1;
                       }
                     }
                     else
                     {
                          if( F_Brew_complete ||  F_Brew_abnormal_end || errorCode0 )
                          {
                            //errorCode0          = 0;
                            F_Brew_complete     = 0;
                            F_Brew_abnormal_end = 0;
                            Complete_Sound_Delay = 0;
                            Start_Status        = OFF;
                          }
                          else if( Paused_Delay )
                          {
                           Paused_Delay = 0;
                           Start_Status = UNPAUSE;
                           BeepStart( Confirm_Sound );
                          }
                          else
                          {
                           Paused_Delay = PAUSE_TIME;
                           Pause_Sound_Delay = 10;
                           Start_Status = PAUSE;
                           BeepStart( Confirm_Sound );
                          }
                     }
                   }
               }
               break;
          case C_KC_POWER:
               Standby_Timer = 300;
               Delay_Keep_Warm_Time = 0;
               Delay_Keep_Warm_Level = 0;
               Delay_Brew_Time       = 0;
               DLY_BREW_FLAG         = 0;
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( System_Status == STANDBY)
               {
                  System_Status = ACTIVE;
               }
               else
               {
                    DLY_BREW_FUNC = 0;
                    KEEP_WARM_FUNC = 0;
                    WARM_LEVEL_FUNC = 0;
                    System_Status = STANDBY;
                    Paused_Delay = 0;
                    Start_Status = OFF;
                    Style_Type_Flag = 0;
                    CLEAN_FUNC_FLAG = 0;
                    CLEAN_DISP_FLAG = 0;
                    Clock_Clean.hour            = 0;
                    Clock_Clean.min             = 59;
                    Clock_Clean.second          = 59;
                    F_Brew_complete     = 0;
                    F_Brew_abnormal_end = 0;
                    Complete_Sound_Delay = 0;
                    //errorCode0          = 0;
               }
               break;
          case C_KC_HOUR|C_KS_2SEC: // 连续加操作
               if(0 == Delay_Keep_Warm_Time ) // 保温设置时间不进行连加操作
                 KeyRepeat( );   
          case C_KC_HOUR:
               Standby_Timer = 300;
               if( debugEnable )
               {
                  if( debugIndex < DEBUG_MAX )
                    debugIndex++;
                  debugValueEnable = 0;
                  break;
               }
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( Delay_Brew_Time )
               {
                  if( DLY_BREW_FLAG )
                  Delay_Brew_Time = 30;
                  Clock_Stay_Time = 50;
                  if( Clock_DLY_BREW.hour_b == 11 )
                   {
                     Clock_DLY_BREW.hour_b = 12;
                     DELAY_AM_PM_FLAG ^= 1;
                   }
                   else if( Clock_DLY_BREW.hour_b == 12)
                     Clock_DLY_BREW.hour_b = 1;
                   else
                      Clock_DLY_BREW.hour_b++;
               }
               else if( Delay_Keep_Warm_Time )
               {
                 Clock_Stay_Time = 50;
                 Delay_Keep_Warm_Time = 5;
                 if( Clock_Keep_Warm.min == 0 )
                 {
                   if( ++Clock_Keep_Warm.hour > 4)
                    Clock_Keep_Warm.hour = 1;
                 }
                 else
                 {
                   if( ++Clock_Keep_Warm.hour > 3)
                    Clock_Keep_Warm.hour = 0;
                 }
                 Clock_Keep_Warm.second = 0;
                 Clock_Keep_Warm.min_b  = 0;
                 Clock_Keep_Warm.hour_b = 0;
               }
               else if( Delay_Keep_Warm_Level )
               {
                 Delay_Keep_Warm_Level = 5;
                 if( Keep_Warm_Class < WARM_HI )
                 Keep_Warm_Class++;
               }
               else 
               {
                 if( Clock_Set_Time == 0)
                 {
                  Clock_Set_Time = 3;
                  Clock.hour_b = Clock.hour;
                  break;
                 }
                 Clock_Set_Time = 3;
                 Clock_Stay_Time = 50;
                 if( Clock.hour_b == 11 )
                 {
                   Clock.hour_b = 12;
                   AM_PM_FLAG ^= 1;
                 }
                 else if( Clock.hour_b == 12)
                   Clock.hour_b = 1;
                 else
                    Clock.hour_b++;
               }
               break;
          case C_KC_MINUTE|C_KS_2SEC:
                if(0 == Delay_Keep_Warm_Time ) // 保温设置时间不进行连加操作
                  KeyRepeat( );  
          case C_KC_MINUTE:
              Standby_Timer = 300;
              if( debugEnable )
              {
                if( debugIndex > 0 )
                  debugIndex--;
                debugValueEnable = 0;
                break;
              }
              if( System_Status == DIM_STATUS)
              {
                System_Status = ACTIVE;
                break;
              }
              if( Delay_Brew_Time )
               {
                  if( DLY_BREW_FLAG )
                  Delay_Brew_Time = 30;
                  Clock_Stay_Time = 50;//连续按时不闪烁
                  if( ++Clock_DLY_BREW.min_b > 59 )
                     Clock_DLY_BREW.min_b = 0;
               }
               else if( Delay_Keep_Warm_Time )
               {
                 Delay_Keep_Warm_Time = 5;
                 Clock_Stay_Time = 50;
                 if( Clock_Keep_Warm.hour == 4 )
                 {
                   Clock_Keep_Warm.hour = 0;
                 }
                 Clock_Keep_Warm.min += 15;
                 if( Clock_Keep_Warm.min > 45 )
                 {
                  if( Clock_Keep_Warm.hour )
                    Clock_Keep_Warm.min = 0;
                  else
                    Clock_Keep_Warm.min = 15;
                 }
                 
                 Clock_Keep_Warm.second = 0;
                 Clock_Keep_Warm.min_b  = 0;
                 Clock_Keep_Warm.hour_b = 0;
               }
               else if( Delay_Keep_Warm_Level )
               {
                 Delay_Keep_Warm_Level = 5;
                 if( Keep_Warm_Class > WARM_OFF )
                 Keep_Warm_Class--;
               }
              else
              {
                 if( Clock_Set_Time == 0)
                 {
                  Clock_Set_Time = 3;
                  Clock.min_b = Clock.min;
                  break;
                 }
                 Clock_Set_Time  = 3;
                 Clock_Stay_Time = 50;//连续按时不闪烁
                 if( ++Clock.min_b > 59 )
                  Clock.min_b = 0;
                 break;
              }
              break;
         
          case C_KC_KEEPWARM | C_KS_2SEC:
               Standby_Timer = 300;
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( NO_BASKET == Brew_Select )
                break;
               //if( System_Status == STANDBY )
               // break;
               if( Start_Status != OFF )
                break;
               if( CLEAN_DISP_FLAG || CLEAN_FUNC_FLAG)
                break;
               if( DLY_BREW_FUNC )
                  break;
               if((Halfcarafe == Knob0_count [ Brew_Select ]  || fullcarafe == Knob0_count [ Brew_Select ]  )
                   && (Classic == Knob1_count [ Brew_Select ] || Rich == Knob1_count [ Brew_Select ] ))
                {
                  //
                  Clock_Set_Time = 0;
                  SET_WARM_LEVEL_FLAG = 1;
                }
               break;
          case C_KC_KEEPWARM | C_KS_5SEC:
               Standby_Timer = 300;
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               //if( NO_BASKET == Brew_Select )
                //break;
               //if( System_Status == STANDBY )
               // break;
               //if( Start_Status != OFF )
               // break;
               if( CLEAN_DISP_FLAG || CLEAN_FUNC_FLAG)
                break;
               if( DLY_BREW_FUNC )
                  break;
               if((Halfcarafe == Knob0_count [ Brew_Select ]  || fullcarafe == Knob0_count [ Brew_Select ]  )
                   && (Classic == Knob1_count [ Brew_Select ]  || Rich == Knob1_count [ Brew_Select ] ))
               {
                Delay_Keep_Warm_Level = 5;
                Clock_Set_Time = 0;
                SET_WARM_LEVEL_FLAG = 0;// 超过5秒就清除设置保温时间.
                if( WARM_LEVEL_FUNC )
                  WARM_LEVEL_FUNC = 0;
                else
                  WARM_LEVEL_FUNC = 1;
               }
               break;
          case C_KC_DELAYSET:
               Standby_Timer = 300;
               Clock_Set_Time = 0;
               Delay_Keep_Warm_Time = 0;
               Delay_Keep_Warm_Level = 0;
               if( debugEnable )
               {
                  debugValueEnable = 1;
                  break;
               }
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( NO_BASKET == Brew_Select )
                break;
               if( System_Status == STANDBY )
                break;
               if( Start_Status != OFF )
                break;
               if( CLEAN_DISP_FLAG || CLEAN_FUNC_FLAG)
                break;
               if( ACTIVE == System_Status && OFF == Start_Status )
               {
                  if( DELAY_SET_TYPE && DELAY_SET_SIZE && DELAY_SET_STYLE )
                  {
                    if( Delay_Brew_Time )
                    {
                      if( DLY_BREW_FLAG )
                      {
                        Delay_Brew_Sure( );
                      }
                      break;
                    }
                    DELAY_SET_TYPE = 0;
                    DELAY_SET_TYPE = 0;
                    DELAY_SET_TYPE = 0;
                  }
                  else
                  {
                    if( DLY_BREW_FLAG )
                    {
                      Delay_Brew_Time = 0;
                      DLY_BREW_FLAG   = 0;
                      Delay_Keep_Warm_Time = 0;
                      SET_WARM_LEVEL_FLAG = 0;
                      DELAY_SET_SIZE  = 0;
                      DELAY_SET_STYLE = 0;
                      DELAY_SET_TYPE  = 0;
                      break;
                    }
                  }
                  
                 if( DLY_BREW_FUNC )
                 {
                  DLY_BREW_FUNC = 0;
                  BeepStart( Confirm_Sound );
                 }
                 else
                 {
                   Delay_Brew_Time = 30;
                   DLY_BREW_FLAG   = 1;
                   DELAY_SET_SIZE  = 0;
                   DELAY_SET_STYLE = 0;
                   DELAY_SET_TYPE  = 0;
                   Delay_Keep_Warm_Time = 0;
                   SET_WARM_LEVEL_FLAG = 0;
                   Style_Type_Flag = 0;
                 }
               }
               break;
          case C_KC_STYLE:
               Standby_Timer = 300;
               Delay_Keep_Warm_Time = 0;
               Delay_Keep_Warm_Level = 0;
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( System_Status == STANDBY )
                break;
               if( NO_BASKET == Brew_Select || COFFEE == Brew_Select)
                break;
               if( DLY_BREW_FUNC )
               {
                  if( delayAdjust15S == 0)
                  {
                    BeepStart( Cancel_Sound );
                    delayExitCnt  = 3;
                    break;
                  }
               }
               if( Start_Status != OFF )
               {
                 if( brewAdjust15S == 0)
                 {
                    BeepStart( Cancel_Sound );
                    
                    KEEP_WARM_FUNC = 0;
                    brewExitCnt    = 3;
                    break;
                 }
               }
               Clock_Set_Time = 0;
               
               if( Delay_Brew_Time )
                Delay_Brew_Time = 30;
               if( brewAdjust15S )
                 brewNoaction3S = BREW_NOACTION_TIME;
               if( delayAdjust15S )
                 delayNoaction3S = DELAY_NOACTION_TIME;
               if( STYLE_S == Style_Type_Flag )
                Style_Type_Flag = 0;
               else
                Style_Type_Flag = STYLE_S;
               break;
          case C_KC_TYPE:
               Standby_Timer = 300;
               Delay_Keep_Warm_Time = 0;
               Delay_Keep_Warm_Level = 0;
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( System_Status == STANDBY )
                break;
               if( NO_BASKET == Brew_Select || COFFEE == Brew_Select)
                break;
               if( DLY_BREW_FUNC )
               {
                  if( delayAdjust15S == 0)
                  {
                    BeepStart( Cancel_Sound );
                    delayExitCnt  = 3;
                    break;
                  }
               }
               if( Start_Status != OFF )
               {
                 if( brewAdjust15S == 0)
                 {
                    BeepStart( Cancel_Sound );
                    
                    KEEP_WARM_FUNC = 0;
                    brewExitCnt    = 3;
                    break;
                 }
               }
               Clock_Set_Time = 0;
               if( Delay_Brew_Time )
                Delay_Brew_Time = 30;
               if( brewAdjust15S )
                 brewNoaction3S = BREW_NOACTION_TIME;
               if( delayAdjust15S )
                 delayNoaction3S = DELAY_NOACTION_TIME;
               if( Brew_Select == TEA )
               {
                 if( TYPE_S == Style_Type_Flag )
                  Style_Type_Flag = 0;
                 else
                  Style_Type_Flag = TYPE_S;
               }
               
               break;
          case C_KC_CLEAN:
               Clock_Set_Time = 0;
               //Delay_Brew_Time = 0;
               Standby_Timer = 300;
               Delay_Keep_Warm_Time = 0;
               Delay_Keep_Warm_Level = 0;
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( NO_BASKET == Brew_Select )
                break;
               if( System_Status == STANDBY )
                break;
               //if( Start_Status != OFF )
               // break;
               
               if( DLY_BREW_FUNC )
               {
                  DLY_BREW_FUNC = 0;
                  break;
               }
               if( OFF == Start_Status)
               {
                if( Knob0_count [ Brew_Select ] !=  fullcarafe )
                {
                  cleanAlarm = 5;
                }
                else
                {
                  CLEAN_DISP_FLAG ^= 1;
                  if( CLEAN_DISP_FLAG )
                    cleanStart = 5;
                }
                
               }
               else if( CLEAN_FUNC_FLAG )
               {
                CLEAN_FUNC_FLAG = 0;
                //Start_Status = CANCALL;
                CLEAN_DISP_FLAG = 0;
                Clock_Clean.hour            = 0;
                Clock_Clean.min             = 59;
                Clock_Clean.second          = 59;
                Start_Status = OFF;
                BeepStart( Cancel_Sound );
               }
               
               break;
          case C_KC_START | C_KS_2SEC:
               Standby_Timer = 300;
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( Brew_Select == NO_BASKET )
                  return;
               if( Start_Status == PAUSE )
               {
                 Start_Status  = OFF;
                 Paused_Delay  = 0;
                 KEEP_WARM_FUNC = 0;
                 CLEAN_FUNC_FLAG = 0;
               }
               break;

          case C_KC_HOUR_STYLE | C_KS_2SEC:
               Standby_Timer = 300;
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( COFFEE_KEY_FLAG )
               {
                COFFEE_KEY_FLAG = 0;
               }
               else
                COFFEE_KEY_FLAG = 1;
               TEA_KEY_FLAG    = 0;
               break;
          case C_KC_MINUTE_TYPE | C_KS_2SEC:
               Standby_Timer = 300;
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( TEA_KEY_FLAG )
               {
                TEA_KEY_FLAG = 0;
               }
               else
                TEA_KEY_FLAG = 1;
               COFFEE_KEY_FLAG    = 0;
               break;
          case C_KC_CLEAN_STYLE | C_KS_2SEC:
               if( forcePump == 0 )
                 forcePump = 1;
               else
                 forcePump = 0;
               break;
          case C_KC_MINUTE_HOUR | C_KS_2SEC:
            if( debugEnable == 0 )
               debugEnable = 1;
             else
               debugEnable = 0;
             debugIndex = 0;
             debugValueEnable = 0;
               break;
          default: break;
        }
  }
}

void Key_Deal( void )
{
 Key_Scan( );
 KeyGenerate( );
 KeyHandler( );
}

U8 TEST_SIZE;

void Size_Rotary_Code( void )
{
  static U8 Size_Value;
         U8 Size_Value_temp  = 0;
         U8 PB_DATA = PB_IDR;
  Size_Value_temp = ( PB_DATA & 0X30 ) >> 4 ;
  if(( Size_Value ^ Size_Value_temp) & 0x03)
  {
    Size_Value <<= 2;
    Size_Value |= Size_Value_temp;
    TEST_SIZE = Size_Value & 0X3F;
    switch( TEST_SIZE )
    {
      case 0x0B:
      case 0x34:
              
               Clock_Set_Time = 0;
               //Delay_Brew_Time = 0;
               Standby_Timer = 300;
               Delay_Keep_Warm_Time = 0;
               Delay_Keep_Warm_Level = 0;
               
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( Delay_Brew_Time )
                Delay_Brew_Time = 30;
               if( NO_BASKET == Brew_Select )
                break;
               if( System_Status == STANDBY )
                break;
               
               if( Start_Status != OFF )
               {
                if( brewAdjust15S == 0)
                 {
                    BeepStart( Cancel_Sound );
                    
                    KEEP_WARM_FUNC = 0;
                    brewExitCnt    = 3;
                    break;
                 }
               }
                
               DELAY_SET_SIZE = 1;
               if( Speciality == Knob1_count [ Brew_Select ]  || Immersion == Knob1_count [ Brew_Select ]  )
                break;
               if( DLY_BREW_FUNC )
               {
                  if( delayAdjust15S == 0)
                  {
                    BeepStart( Cancel_Sound );
                    delayExitCnt  = 3;
                    break;
                  }
               }
               if( brewAdjust15S )
                 brewNoaction3S = BREW_NOACTION_TIME;
               if( delayAdjust15S )
                 delayNoaction3S = DELAY_NOACTION_TIME;
               if( Knob0_count [ Brew_Select ] < fullcarafe )
                Knob0_count [ Brew_Select ]++;
               break;
      
      case 0x07:
      case 0x38:
               Clock_Set_Time = 0;
               //Delay_Brew_Time = 0;
               Standby_Timer = 300;
               Delay_Keep_Warm_Time = 0;
               Delay_Keep_Warm_Level = 0;
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( Delay_Brew_Time )
                Delay_Brew_Time = 30;
               if( NO_BASKET == Brew_Select )
                break;
               if( System_Status == STANDBY )
                break;
               if( Start_Status != OFF )
               {
                if( brewAdjust15S == 0)
                 {
                    BeepStart( Cancel_Sound );
                    
                    KEEP_WARM_FUNC = 0;
                    brewExitCnt    = 3;
                    break;
                 }
               }
               if( Speciality == Knob1_count [ Brew_Select ]  || Immersion == Knob1_count [ Brew_Select ]  )
                break;
               if( DLY_BREW_FUNC )
               {
                  if( delayAdjust15S == 0)
                  {
                    BeepStart( Cancel_Sound );
                    delayExitCnt  = 3;
                    break;
                  }
               }
               DELAY_SET_SIZE = 1;
               if( brewAdjust15S )
                 brewNoaction3S = BREW_NOACTION_TIME;
               if( delayAdjust15S )
                 delayNoaction3S = DELAY_NOACTION_TIME;
               if( Knob0_count [ Brew_Select ]  > Cup )
                Knob0_count [ Brew_Select ]--;
               break;
      default:break;
    }
  }
}


U8 TEST_STYLE;
void Style_Type_Rotary_Code( void )
{
  static U8 Style_Type;
         U8 Style_Type_temp  = 0;
         U8 PB_DATA = PB_IDR;
  Style_Type_temp = ( PB_DATA & 0X0C ) >> 2 ;
  if(( Style_Type ^ Style_Type_temp) & 0x03)
  {
    Style_Type <<= 2;
    Style_Type |= Style_Type_temp;
    TEST_STYLE = Style_Type & 0X3F;
    switch(  TEST_STYLE )
    {
      case 0x07:
      case 0x38:
               Clock_Set_Time = 0;
               //Delay_Brew_Time = 0;
               Standby_Timer = 300;
               Delay_Keep_Warm_Time = 0;
               Delay_Keep_Warm_Level = 0;
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( Delay_Brew_Time )
                Delay_Brew_Time = 30;
               if( NO_BASKET == Brew_Select )
                break;
               if( System_Status == STANDBY )
                break;
               if( Start_Status != OFF )
               {
                if( brewAdjust15S == 0)
                 {
                    BeepStart( Cancel_Sound );
                    
                    KEEP_WARM_FUNC = 0;
                    brewExitCnt    = 3;
                    break;
                 }
               }
               if( DLY_BREW_FUNC )
               {
                  if( delayAdjust15S == 0)
                  {
                    BeepStart( Cancel_Sound );
                    delayExitCnt  = 3;
                    break;
                  }
               }
               if( Brew_Select == TEA )
               {
                 if( Style_Type_Flag == 0 )
                 {
                  Style_Type_Flag = STYLE_S;
                  //break;
                 }
               }
               if ( Brew_Select == COFFEE )
               {
                DELAY_SET_STYLE = 1;
                if( brewAdjust15S )
                 brewNoaction3S = BREW_NOACTION_TIME;
                if( delayAdjust15S )
                 delayNoaction3S = DELAY_NOACTION_TIME;
                if( Knob1_count [ Brew_Select ]  < Immersion )
                     Knob1_count [ Brew_Select ] ++;
               }
               else
               {
                 if( brewAdjust15S )
                 brewNoaction3S = BREW_NOACTION_TIME;
                 if( delayAdjust15S )
                 delayNoaction3S = DELAY_NOACTION_TIME;
                 if( Style_Type_Flag == STYLE_S )
                 {
                   DELAY_SET_STYLE = 1;
                   if( Knob1_count [ Brew_Select ]  < Speciality )
                     Knob1_count [ Brew_Select ]++;
                 }
                 else if( Style_Type_Flag == TYPE_S )
                 {
                   DELAY_SET_TYPE = 1;
                   if( Knob2_count < Delicate )
                     Knob2_count++;
                 }
               }
               break;
      case 0x0B:
      case 0x34:
               Clock_Set_Time = 0;
               //Delay_Brew_Time = 0;
               Standby_Timer = 300;
               Delay_Keep_Warm_Time = 0;
               Delay_Keep_Warm_Level = 0;
               if( System_Status == DIM_STATUS)
               {
                System_Status = ACTIVE;
                break;
               }
               if( Delay_Brew_Time )
                Delay_Brew_Time = 30;
               if( NO_BASKET == Brew_Select )
                break;
               if( System_Status == STANDBY )
                break;
               if( Start_Status != OFF )
               {
                 if( brewAdjust15S == 0)
                 {
                    BeepStart( Cancel_Sound );
                    
                    KEEP_WARM_FUNC = 0;
                    brewExitCnt    = 3;
                    break;
                 }
               }
               if( DLY_BREW_FUNC )
               {
                  if( delayAdjust15S == 0)
                  {
                    BeepStart( Cancel_Sound );
                    delayExitCnt  = 3;
                    break;
                  }
               }
               if( Brew_Select == TEA )
               {
                 if( Style_Type_Flag == 0 )
                 {
                  Style_Type_Flag = STYLE_S;
                  //break;
                 }
               }
               if ( Brew_Select == COFFEE )
               {
                DELAY_SET_STYLE = 1;
                if( brewAdjust15S )
                 brewNoaction3S = BREW_NOACTION_TIME;
                if( delayAdjust15S )
                 delayNoaction3S = DELAY_NOACTION_TIME;
                if( Knob1_count [ Brew_Select ]  > Classic )
                   Knob1_count [ Brew_Select ] --;
               }
               else
               {
                 if( brewAdjust15S )
                 brewNoaction3S = BREW_NOACTION_TIME;
                 if( delayAdjust15S )
                 delayNoaction3S = DELAY_NOACTION_TIME;
                 if( Style_Type_Flag == STYLE_S )
                 {
                   DELAY_SET_STYLE = 1;
                   if( Knob1_count [ Brew_Select ]  > Classic )
                     Knob1_count [ Brew_Select ]--;
                 }
                 else if( Style_Type_Flag == TYPE_S )
                 {
                   DELAY_SET_TYPE = 1;
                   if( Knob2_count > Herbal)
                     Knob2_count--;
                 }
               }
               break;
      default:break;
    }
  }
}


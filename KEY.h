
///////////////////////////////////////////////////////////
//                                                       //
//          FileName:   COM_ODU.h                        //
//                                                       //
///////////////////////////////////////////////////////////

#ifndef _KEY_H_
#define _KEY_H_

#ifdef  EXTERN
#undef  EXTERN
#endif
#ifdef  _KEY_SRC_
#define EXTERN
#else
#define EXTERN extern
#endif

#define C_KT_REPEAT   20
#define C_KT_REPEAT1  10
#define C_KT_SHORT  ( C_KT_REPEAT + 4 )
#define C_KT_1SEC  ( C_KT_REPEAT + 100 )
#define C_KT_2SEC  ( C_KT_REPEAT + 200 )
#define C_KT_5SEC  ( C_KT_REPEAT + 500 )
#define C_KT_10SEC  ( C_KT_REPEAT + 1000 )
#define C_KT_30SEC  ( C_KT_REPEAT + 3000 )

#define C_KS_SHORT   0X00
#define C_KS_1SEC    0X20
#define C_KS_2SEC    0X40
#define C_KS_5SEC    0X60
#define C_KS_10SEC   0X80
#define C_KS_30SEC   0XA0
#define C_KS_DEALED  0XE0

#define C_KC_UNDEF             0XFF
#define C_KC_NONE              0

#define C_KC_HOUR              1
#define C_KC_MINUTE            2
#define C_KC_KEEPWARM          3
#define C_KC_DELAYSET          4

#define C_KC_STYLE             5
#define C_KC_TYPE              6
#define C_KC_POWER             7
#define C_KC_CLEAN             8
#define C_KC_START             9

#define C_KC_HOUR_STYLE        10
#define C_KC_MINUTE_TYPE       11
#define C_KC_CLEAN_STYLE       12
#define C_KC_MINUTE_HOUR       13


#define C_SC_UNDEF  0XFFFF
#define C_SC_NONE   0X0000


#define C_SC_HOUR              0x0200
#define C_SC_MINUTE            0x0800
#define C_SC_KEEPWARM          0x8000
#define C_SC_DELAYSET          0x2000

#define C_SC_STYLE             0x0100
#define C_SC_TYPE              0x4000
#define C_SC_POWER             0x0001
#define C_SC_CLEAN             0x0400

#define C_SC_START             0x1000

#define C_SC_HOUR_STYLE        0x0300
#define C_SC_MINUTE_TYPE       0x4800
#define C_SC_CLEAN_STYLE       0X0500
#define C_SC_MINUTE_HOUR       0X0A00





#define PAUSE_TIME             300
#define BREW_ADJUST_TIME       15
#define BREW_NOACTION_TIME     3;

#define  DELAYPUASE            300
#define DELAY_ADJUST_TIME       15
#define DELAY_NOACTION_TIME     3;



typedef enum Size_Count
{
 Cup,
 Xlcup,
 Travel,
 Xltrael,
 Halfcarafe,
 fullcarafe
}KNOB0_ENUM;

typedef enum Style_Count
{
 Classic,
 Rich,
 Overice,
 Coldbrew,
 Speciality,
 Immersion
}KNOB1_ENUM;

typedef enum Type_Count
{
 Herbal,
 Black,
 Oolong,
 White,
 Green,
 Delicate
}KNOB2_ENUM;

EXTERN KNOB0_ENUM Knob0_count[ 3 ];
EXTERN KNOB1_ENUM Knob1_count[ 3 ];
EXTERN KNOB2_ENUM Knob2_count;

typedef struct 
{
  U8 second;
  U8 min;
  U8 hour;
  U8 min_b;
  U8 hour_b;
}CLOCK_STRUCT;
EXTERN CLOCK_STRUCT Clock;
EXTERN CLOCK_STRUCT Clock_DLY_BREW;
EXTERN CLOCK_STRUCT Clock_Clean;
EXTERN CLOCK_STRUCT Clock_Keep_Warm;

typedef enum
{
  STANDBY,
  DIM_STATUS,
  ACTIVE
}SYSTEM_STATUS;
EXTERN SYSTEM_STATUS System_Status;

typedef enum
{
  OFF,
  ON,
  PAUSE,
  CANCEL,
  UNPAUSE
}START_STATUS;

EXTERN START_STATUS Start_Status;

typedef enum
{
  NO_BASKET,
  COFFEE,
  TEA
}BREW_SELECT;
EXTERN BREW_SELECT Brew_Select;
EXTERN BREW_SELECT Brew_Select_B;

typedef enum
{
  WARM_OFF,
  WARM_LO,
  WARM_HI
}KEEP_WARM_CLASS;
EXTERN KEEP_WARM_CLASS Keep_Warm_Class;

EXTERN U8 Style_Type_Flag;
EXTERN U16 Paused_Delay;
EXTERN U8 Clock_Set_Time;
EXTERN U8 Clock_Stay_Time;
EXTERN U16 Standby_Timer;

EXTERN U8 Delay_Keep_Warm_Time;
EXTERN U8 Delay_Keep_Warm_Level;
EXTERN U8 SET_Keep_Warm_Time;

EXTERN U8 Delay_Brew_Time;
EXTERN U8 CANCEL_DELAY_BREW_3S;

EXTERN U8 Complete_Sound_Delay;
EXTERN U8 Pause_Sound_Delay;
EXTERN U8 brewAdjust15S;
EXTERN U8 brewNoaction3S;
EXTERN U8 brewExitCnt;

EXTERN U16 delayBrewPause;
EXTERN U8 delayAdjust15S;
EXTERN U8 delayNoaction3S;
EXTERN U8 delayExitCnt;


EXTERN U8 cleanAlarm;
EXTERN U8 cleanStart;
EXTERN U8 forcePump;
EXTERN U8 debugEnable;
EXTERN U8 debugIndex;
EXTERN U8 debugValueEnable;
EXTERN BYTE   Time_Flag;
#define _500ms_flag           Time_Flag.bits.b0
#define _400ms_flag           Time_Flag.bits.b1
#define _10s_flag             Time_Flag.bits.b2
#define _1s_flag              Time_Flag.bits.b3
#define _300ms_flag           Time_Flag.bits.b4
#define _1s_preheat_flag      Time_Flag.bits.b5
#define _075s_preheat_disp    Time_Flag.bits.b6

EXTERN BYTE   Disp_Flag;
#define      AM_PM_FLAG           Disp_Flag.bits.b0
#define      KEEP_WARM_FUNC       Disp_Flag.bits.b1
#define      SET_WARM_LEVEL_FLAG  Disp_Flag.bits.b2
#define      WARM_LEVEL_FUNC      Disp_Flag.bits.b3
#define      DLY_BREW_FLAG        Disp_Flag.bits.b4
#define      DLY_BREW_FUNC        Disp_Flag.bits.b5
#define      DELAY_AM_PM_FLAG     Disp_Flag.bits.b6

EXTERN BYTE   Disp_Flag1;
#define      CLEAN_DISP_FLAG           Disp_Flag1.bits.b0
#define      CLEAN_FUNC_FLAG           Disp_Flag1.bits.b1
#define      COFFEE_KEY_FLAG           Disp_Flag1.bits.b2
#define      TEA_KEY_FLAG              Disp_Flag1.bits.b3
#define      DELAY_SET_SIZE            Disp_Flag1.bits.b4
#define      DELAY_SET_STYLE           Disp_Flag1.bits.b5
#define      DELAY_SET_TYPE            Disp_Flag1.bits.b6

EXTERN BYTE   Sys_Flag1;
#define      NO_BASKET_PAUSE           Sys_Flag1.bits.b0










//EXTERN BYTE Key_Flag[2 ];
EXTERN WORD Key_Value;

EXTERN U8 KEY_Buff[ 16 ];


EXTERN void Time_Deal(void);
EXTERN void Key_Init( void );
EXTERN void Key_Scan(void);
EXTERN void Key_Deal( void );

EXTERN void Size_Rotary_Code( void );
EXTERN void Style_Type_Rotary_Code( void );

#endif


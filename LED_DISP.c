
///////////////////////////////////////////////////////////
//                                                       //
//          FileName:   COM_ODU.c                        //
//                                                       //
///////////////////////////////////////////////////////////

#define  _LED_DISP_SRC_
#include "Globals.h"
#include "LED_DISP.h"
#include "Pt696x.h"
#include "COM_ODU.h"
#include "KEY.h"

static U8 SRAM ledBreathe  = 0;

const U8 digCode[ ] =
  {
    C_CODE_0, C_CODE_1, C_CODE_2, C_CODE_3,
    C_CODE_4, C_CODE_5, C_CODE_6, C_CODE_7,
    C_CODE_8, C_CODE_9, C_CODE_A, C_CODE_b,
    C_CODE_C, C_CODE_d, C_CODE_E, C_CODE_F
  };
  
U8 SRAM digImage[ 4 ] ;


void DisplayDriver( void )
  {
      Pt696xCmd( C_PT_DIG4);
      Pt696xWr( 0, ledMap, sizeof( ledMap ) );
      if( System_Status == ACTIVE )
        Pt696xCmd( C_PT_ON7);
      else
        Pt696xCmd( C_PT_ON1);
  }

//================================//


void hour_min( U8 hour,U8 min )
  {
    if( hour < 10)
      digImage[ 3 ] = 0;
    else
      digImage[ 3 ] = digCode[ hour / 10 ];
    digImage[ 2 ] = digCode[ hour % 10 ];
    
    digImage[ 1 ] = digCode[ min / 10 ];
    min = min % 10;
    digImage[ 0 ] = digCode[ min ];
  }

void Clock_Disp(void)
{
  if( Clock_Set_Time )
  {
    if( _500ms_flag  || Clock_Stay_Time)
    {
      hour_min(Clock.hour_b,Clock.min_b);
      BSETEX( ledMap, LM_2DP);
      if( AM_PM_FLAG )
       BSETEX( ledMap, LM_PM);
      else
       BSETEX( ledMap, LM_AM);
    }
  }
  else
  {
      hour_min(Clock.hour,Clock.min);
      if( _500ms_flag )
      {
        BSETEX( ledMap, LM_2DP);
      }
      if( AM_PM_FLAG )
       BSETEX( ledMap, LM_PM);
      else
       BSETEX( ledMap, LM_AM);
  }
}


#if 0
void Keep_Warm_Clock_Disp(void)
{
    if( _500ms_flag  || Clock_Stay_Time)
    {
      hour_min(Clock_Keep_Warm.hour,Clock_Keep_Warm.min);
      BSETEX( ledMap, LM_2DP); //
    }
    if( _500ms_flag )// 闪烁
     Led_Keepwarm = LED_ON;// STAY WARM_LED
}

void Delay_Brew_Clock_Disp(void)
{

    if( COFFEE == Brew_Select )
      Led_Coffee = LED_ON;
    else if( TEA == Brew_Select )
      Led_Tea = LED_ON;
    if( _500ms_flag  || Clock_Stay_Time)
    {
      hour_min(Clock_DLY_BREW.hour_b,Clock_DLY_BREW.min_b);
      BSETEX( ledMap, LM_2DP); //
      if( DELAY_AM_PM_FLAG )
        BSETEX( ledMap, LM_PM);
      else
       BSETEX( ledMap, LM_AM);
    }
    if( _500ms_flag )
        Led_Delayset = LED_ON;
    if( DELAY_SET_SIZE == 0 )
    {
      if( _500ms_flag )
      {
        switch ( Knob0_count [ Brew_Select ]  )
        {
           case Cup:        Led_Cup = LED_ON;break;
           case Xlcup:      Led_Xlcup = LED_ON;break;
           case Travel:     Led_Travel = LED_ON;break;
           case Xltrael:    Led_Xltravel = LED_ON;break;
           case Halfcarafe: Led_Halfcarafe = LED_ON;break;
           case fullcarafe: Led_Fullcarafe = LED_ON;break;
           default:break;
        }
      }
    }
    else
    {
        switch ( Knob0_count [ Brew_Select ]  )
        {
           case Cup:        Led_Cup = LED_ON;break;
           case Xlcup:      Led_Xlcup = LED_ON;break;
           case Travel:     Led_Travel = LED_ON;break;
           case Xltrael:    Led_Xltravel = LED_ON;break;
           case Halfcarafe: Led_Halfcarafe = LED_ON;break;
           case fullcarafe: Led_Fullcarafe = LED_ON;break;
           default:break;
        }
    }
    if( DELAY_SET_STYLE == 0 )
    {
      if( _500ms_flag )
        {
          switch ( Knob1_count [ Brew_Select ]  )
          {
             case Classic:    Led_Classic= LED_ON;break;
             case Rich:       Led_Rich= LED_ON;break;
             case Overice:    Led_Overice= LED_ON;break;
             case Coldbrew:   Led_Coldbrew= LED_ON;break;
             case Speciality: Led_Speciality= LED_ON;break;
             case Immersion:  Led_Immersion= LED_ON;break;
             default:break;
          }
        }
    }
    else
    {
          switch ( Knob1_count [ Brew_Select ]  )
          {
             case Classic:    Led_Classic= LED_ON;break;
             case Rich:       Led_Rich= LED_ON;break;
             case Overice:    Led_Overice= LED_ON;break;
             case Coldbrew:   Led_Coldbrew= LED_ON;break;
             case Speciality: Led_Speciality= LED_ON;break;
             case Immersion:  Led_Immersion= LED_ON;break;
             default:break;
          }
    }
    if( Brew_Select == TEA )
    {
      if( Style_Type_Flag == 1 )
        Led_Style = LED_ON;
      else if( 2 == Style_Type_Flag )
        Led_Type = LED_ON;
      if( DELAY_SET_TYPE == 0 )
      {
        if( _500ms_flag )
        {
          switch ( Knob2_count )
          {
             case Herbal:   Led_Herbal = LED_ON;  break;
             case Black:    Led_Black= LED_ON;    break;
             case Oolong:   Led_Oolong= LED_ON;   break;
             case White:    Led_White= LED_ON;    break;
             case Green:    Led_Green= LED_ON;    break;
             case Delicate: Led_Delicate = LED_ON;break;
             default:break;
          }
        }
      }
      else
      {
          switch ( Knob2_count )
          {
             case Herbal:   Led_Herbal = LED_ON;  break;
             case Black:    Led_Black= LED_ON;    break;
             case Oolong:   Led_Oolong= LED_ON;   break;
             case White:    Led_White= LED_ON;    break;
             case Green:    Led_Green= LED_ON;    break;
             case Delicate: Led_Delicate = LED_ON;break;
             default:break;
          }
      }
    }
    else
    {
     DELAY_SET_TYPE = 1;
    }
}

void CLean_Disp( void )
{

  if( CLEAN_DISP_FLAG )
  {
     if( _500ms_flag )
     {
      if( Clock_Clean.hour > 0 )
        hour_min(Clock_Clean.hour,Clock_Clean.min);
      else
        hour_min(Clock_Clean.min,Clock_Clean.second);
      BSETEX( ledMap, LM_2DP); //
      Led_Clean = LED_ON;
      Led_Fullcarafe = LED_ON;
      Led_Start      = LED_ON;
     }
     
  }
  else if( CLEAN_FUNC_FLAG )
  {
     if( Clock_Clean.hour > 0 )
        hour_min(Clock_Clean.hour,Clock_Clean.min);
      else
        hour_min(Clock_Clean.min,Clock_Clean.second);
      BSETEX( ledMap, LM_2DP); //
      Led_Clean = LED_ON;
      Led_Fullcarafe = LED_ON;
      Led_Start      = LED_ON;
  }
}
void Tea_Coffee_Same_Disp( void )
{
  switch ( Knob1_count [ Brew_Select ]  )
  {
     case Classic:    Led_Classic= LED_ON;break;
     case Rich:       Led_Rich= LED_ON;break;
     case Overice:    Led_Overice= LED_ON;break;
     case Coldbrew:   Led_Coldbrew= LED_ON;break;
     case Speciality: Led_Speciality= LED_ON;break;
     case Immersion:  Led_Immersion= LED_ON;break;
     default:break;
  }
  if( Speciality == Knob1_count [ Brew_Select ]  || Immersion == Knob1_count [ Brew_Select ]  )
  {
    //if( Cup == Knob0_count )
      Led_Cup = LED_ON;
  }
  else
  {
    switch ( Knob0_count [ Brew_Select ]  )
    {
       case Cup:        Led_Cup = LED_ON;break;
       case Xlcup:      Led_Xlcup = LED_ON;break;
       case Travel:     Led_Travel = LED_ON;break;
       case Xltrael:    Led_Xltravel = LED_ON;break;
       case Halfcarafe: Led_Halfcarafe = LED_ON;break;
       case fullcarafe: Led_Fullcarafe = LED_ON;break;
       default:break;
    }
  }
  
  
}
void Coffee_Disp( void )
{
  Led_Coffee = LED_ON;
  Tea_Coffee_Same_Disp( );
  if( Coldbrew == Knob1_count [ Brew_Select ] )
    Led_Addone = LED_ON;
  
}

void Tea_Disp( void )
{ 
   Led_Tea = LED_ON;
   Tea_Coffee_Same_Disp( );
   switch ( Knob2_count )
    {
       case Herbal:   Led_Herbal = LED_ON;  break;
       case Black:    Led_Black= LED_ON;    break;
       case Oolong:   Led_Oolong= LED_ON;   break;
       case White:    Led_White= LED_ON;    break;
       case Green:    Led_Green= LED_ON;    break;
       case Delicate: Led_Delicate = LED_ON;break;
       default:break;
    }
  if( Style_Type_Flag == 1 )
    Led_Style = LED_ON;
  else if( 2 == Style_Type_Flag )
    Led_Type = LED_ON;
}

#endif
//===============================//
void Digit_Figure( void )
{
      if ( BTST( digImage[ 2 ], 0 ) )  BSETEX( ledMap, LM_3A );
      if ( BTST( digImage[ 2 ], 1 ) )  BSETEX( ledMap, LM_3B );
      if ( BTST( digImage[ 2 ], 2 ) )  BSETEX( ledMap, LM_3C );
      if ( BTST( digImage[ 2 ], 3 ) )  BSETEX( ledMap, LM_3D );
      if ( BTST( digImage[ 2 ], 4 ) )  BSETEX( ledMap, LM_3E );
      if ( BTST( digImage[ 2 ], 5 ) )  BSETEX( ledMap, LM_3F );  
      if ( BTST( digImage[ 2 ], 6 ) )  BSETEX( ledMap, LM_3G );
      if ( BTST( digImage[ 1 ], 0 ) )  BSETEX( ledMap, LM_2A );
      if ( BTST( digImage[ 1 ], 1 ) )  BSETEX( ledMap, LM_2B );
      if ( BTST( digImage[ 1 ], 2 ) )  BSETEX( ledMap, LM_2C );
      if ( BTST( digImage[ 1 ], 3 ) )  BSETEX( ledMap, LM_2D );
      if ( BTST( digImage[ 1 ], 4 ) )  BSETEX( ledMap, LM_2E );
      if ( BTST( digImage[ 1 ], 5 ) )  BSETEX( ledMap, LM_2F );  
      if ( BTST( digImage[ 1 ], 6 ) )  BSETEX( ledMap, LM_2G );
      if ( BTST( digImage[ 0 ], 0 ) )  BSETEX( ledMap, LM_1A );
      if ( BTST( digImage[ 0 ], 1 ) )  BSETEX( ledMap, LM_1B );
      if ( BTST( digImage[ 0 ], 2 ) )  BSETEX( ledMap, LM_1C );
      if ( BTST( digImage[ 0 ], 3 ) )  BSETEX( ledMap, LM_1D );
      if ( BTST( digImage[ 0 ], 4 ) )  BSETEX( ledMap, LM_1E );
      if ( BTST( digImage[ 0 ], 5 ) )  BSETEX( ledMap, LM_1F );
      if ( BTST( digImage[ 0 ], 6 ) )  BSETEX( ledMap, LM_1G );
      if ( BTST( digImage[ 3 ], 0 ) )  BSETEX( ledMap, LM_4A );
      if ( BTST( digImage[ 3 ], 1 ) )  BSETEX( ledMap, LM_4B );
      if ( BTST( digImage[ 3 ], 2 ) )  BSETEX( ledMap, LM_4C );
      if ( BTST( digImage[ 3 ], 3 ) )  BSETEX( ledMap, LM_4D );
      if ( BTST( digImage[ 3 ], 4 ) )  BSETEX( ledMap, LM_4E );
      if ( BTST( digImage[ 3 ], 5 ) )  BSETEX( ledMap, LM_4F );
      if ( BTST( digImage[ 3 ], 6 ) )  BSETEX( ledMap, LM_4G );
}


#if 0
void LED_DISP( void )
{
   static U8 time400ms = 0;
   led[ 0 ].byte = 0;
   led[ 1 ].byte = 0;
   led[ 2 ].byte = 0;
   led[ 3 ].byte = 0;
   led[ 4 ].byte = 0;
   memset( ledMap, 0x00, sizeof( ledMap ) );
   memset( digImage, 0x00, sizeof( digImage ) );

   if( CLEAN_DISP_FLAG )
   {}
   else if( Delay_Keep_Warm_Time  && Delay_Brew_Time == 0)
   Keep_Warm_Clock_Disp( );
   else if( Delay_Keep_Warm_Time == 0 && Delay_Brew_Time == 0 )
   {
     Clock_Disp( );
     if( KEEP_WARM_FUNC )
      Led_Keepwarm = LED_ON;
     if( Delay_Keep_Warm_Level )
     {
       if( _500ms_flag )
       {
         
         Led_Keepwarm = LED_ON;
         if( WARM_LEVEL_FUNC )
          Led_Extrahot = LED_ON;
       }
     }
     else
     {
        if((Halfcarafe == Knob0_count [ Brew_Select ]  || fullcarafe == Knob0_count [ Brew_Select ]  )
                   && (Classic == Knob1_count [ Brew_Select ]  || Rich == Knob1_count [ Brew_Select ] ))
        {
            if( WARM_LEVEL_FUNC )
                Led_Extrahot = LED_ON;
        }
         
     }
   }
   if( ACTIVE == System_Status )
   {
     Led_Power = LED_ON;
     if( NO_BASKET == Brew_Select ) // 无篮时闪烁指示灯
     {
      if( _500ms_flag )
       Led_Insertbasket = LED_ON;
     }
     else
     {
       if( 0 == CLEAN_DISP_FLAG && 0 == CLEAN_FUNC_FLAG && Delay_Brew_Time == 0 )
       {
         if( COFFEE == Brew_Select )
           Coffee_Disp( );
         else
           Tea_Disp( );
       }
       
       if( ON == Start_Status || PAUSE == Start_Status || UNPAUSE== Start_Status ) // 关机模式下显示图标
       {
         if( CLEAN_DISP_FLAG || CLEAN_FUNC_FLAG)
         {
              CLean_Disp( );
         }
         else
         {
           if( Paused_Delay )
           {
            if( _500ms_flag )
             Led_Start = LED_ON;
           }
           else
            Led_Start = LED_ON;
         }
       }
       else 
       {
            if( CLEAN_DISP_FLAG || CLEAN_FUNC_FLAG)
            {
              CLean_Disp( );
            }
            else
            {
              if( Delay_Brew_Time )
              {
               Delay_Brew_Clock_Disp( );
              }
              else if( DLY_BREW_FUNC )
              {
                Led_Delayset = LED_ON;
              }
              if( CANCEL_DELAY_BREW_3S )
              {
                if( _500ms_flag )
                  Led_Delayset = LED_ON;
              }
            }
       }
       
     }
     
   }
   #if 1
   __DI( );
   ledSegs[ 0 ]     = led[ 0 ].byte;
   ledSegs[ 1 ]     = led[ 1 ].byte;
   ledSegs[ 2 ]     = led[ 2 ].byte;
   ledSegs[ 3 ]     = led[ 3 ].byte;
   ledSegs[ 4 ]     = led[ 4 ].byte;
   
   
   __EI( );
   #endif
   #if 0
   ledSegs[ 0 ]     = 0XFF;
   ledSegs[ 1 ]     = 0XFF;
   ledSegs[ 2 ]     = 0XFF;
   ledSegs[ 3 ]     = 0XFF;
   ledSegs[ 4 ]     = 0XFF;
   #endif
   Digit_Figure( );
   DisplayDriver( );
}
#endif

void Tea_Coffee_Dim_Disp( U8 dim )
{
  
  Led_Cup        = dim;
  Led_Xlcup      = dim;
  Led_Travel     = dim;
  Led_Xltravel   = dim;
  Led_Halfcarafe = dim;
  Led_Fullcarafe = dim;
}

void Coffee_Style_Dim_Disp( U8 dim )
{
  Led_Classic    = dim;
  Led_Rich       = dim;
  Led_Overice    = dim;
  Led_Coldbrew   = dim;
  Led_Speciality = dim;
  Led_Immersion  = dim;
}

void Tea_Style_Type_Dim_Disp( U8 dim )
{
  

  Led_Style       = dim;
  Led_Type        = dim;
  if( Style_Type_Flag == 1 )
  {
    if( dim == DIM10)
      Led_Style = LED_ON;
    else if( dim == DIM)
      Led_Style = DIM10;
    Led_Classic    = dim;
    Led_Rich       = dim;
    Led_Overice    = dim;
    Led_Coldbrew   = dim;
    Led_Speciality = dim;
    //Led_Immersion  = dim;
  }
  else if( 2 == Style_Type_Flag )
  {
    
    if( dim == DIM10)
      Led_Type  = LED_ON;
    else if( dim == DIM)
      Led_Type  = DIM10;
    Led_Herbal      = dim;
    Led_Black       = dim;
    Led_Oolong      = dim;
    Led_White       = dim;
    Led_Green       = dim;
    Led_Delicate    = dim;
  }
}

void DelayBrew_Tea_Dim_Disp( U8 dim )
{
  Led_Style       = dim;
  Led_Type        = dim;
  Led_Classic     = dim;
  Led_Rich        = dim;
  Led_Overice     = dim;
  Led_Coldbrew    = dim;
  Led_Speciality  = dim;
  //Led_Immersion   = dim;
  Led_Herbal      = dim;
  Led_Black       = dim;
  Led_Oolong      = dim;
  Led_White       = dim;
  Led_Green       = dim;
  Led_Delicate    = dim;
  if( Style_Type_Flag == 1 )
  {
    if( dim == DIM10)
      Led_Style = LED_ON;
    else if( dim == DIM)
      Led_Style = DIM10;
  }
  else if( 2 == Style_Type_Flag )
  {
    if( dim == DIM10)
      Led_Type  = LED_ON;
    else if( dim == DIM)
      Led_Type  = DIM10;
  }
}

void Tea_Coffee_Same_Disp( U8 dim )
{
  switch ( Knob1_count [ Brew_Select ]  )
  {
     case Classic:    Led_Classic     = dim;break;
     case Rich:       Led_Rich        = dim;break;
     case Overice:    Led_Overice     = dim;break;
     case Coldbrew:   Led_Coldbrew    = dim;break;
     case Speciality: Led_Speciality  = dim;break;
     case Immersion:  Led_Immersion   = dim;break;
     default:break;
  }
  if( Speciality == Knob1_count [ Brew_Select ]  
    || Immersion == Knob1_count [ Brew_Select ]  )
  {
    //if( Cup == Knob0_count )
    //  Led_Cup = dim;
    Led_Cup        = LED_OFF;
    Led_Xlcup      = LED_OFF;
    Led_Travel     = LED_OFF;
    Led_Xltravel   = LED_OFF;
    Led_Halfcarafe = LED_OFF;
    Led_Fullcarafe = LED_OFF;
  }
  else
  {
    switch ( Knob0_count [ Brew_Select ]  )
    {
       case Cup:        Led_Cup = dim;break;
       case Xlcup:      Led_Xlcup = dim;break;
       case Travel:     Led_Travel = dim;break;
       case Xltrael:    Led_Xltravel = dim;break;
       case Halfcarafe: Led_Halfcarafe = dim;break;
       case fullcarafe: Led_Fullcarafe = dim;break;
       default:break;
    }
  }
}
void Coffee_Disp( U8 dim )
{
  Led_Coffee = dim;
  //if( Coldbrew == Knob1_count [ Brew_Select ] )
   // Led_Addone = LED_ON;
  
}

void Tea_Disp( U8 dim )
{ 
   Led_Tea = dim;
   switch ( Knob2_count )
    {
       case Herbal:   Led_Herbal   = dim;break;
       case Black:    Led_Black    = dim;break;
       case Oolong:   Led_Oolong   = dim;break;
       case White:    Led_White    = dim;break;
       case Green:    Led_Green    = dim;break;
       case Delicate: Led_Delicate = dim;break;
       default:break;
    }
}

void Tea_Type_Disp( U8 dim)
{
 switch ( Knob2_count )
  {
     case Herbal:   Led_Herbal   = dim;break;
     case Black:    Led_Black    = dim;break;
     case Oolong:   Led_Oolong   = dim;break;
     case White:    Led_White    = dim;break;
     case Green:    Led_Green    = dim;break;
     case Delicate: Led_Delicate = dim;break;
     default:break;
  }
}
void Coffee_DIM_Disp( void )
{
  Led_Coffee = DIM;
  //if( Coldbrew == Knob1_count [ Brew_Select ] )
   // Led_Addone = LED_ON;
  
}

void Tea_DIM_Disp( void )
{ 
   Led_Tea = DIM;
   switch ( Knob2_count )
    {
       case Herbal:   Led_Herbal   = DIM10;break;
       case Black:    Led_Black    = DIM10;break;
       case Oolong:   Led_Oolong   = DIM10;break;
       case White:    Led_White    = DIM10;break;
       case Green:    Led_Green    = DIM10;break;
       case Delicate: Led_Delicate = DIM10;break;
       default:break;
    }
}

void CLean_Disp_Standby( void )
{
     if( CLEAN_DISP_FLAG )
     {
       if( _500ms_flag )
       {
          hour_min(Clock_Clean.min,Clock_Clean.second);
          BSETEX( ledMap, LM_2DP); //
          Led_Fullcarafe = LED_ON;
          Led_Start      = LED_ON;
          if( CLean_Class == 0 || CLean_Class == 1 )
            Led_Clean2  = LED_ON;
          else
            Led_Clean = LED_ON;
       }
     }
     if( cleanAlarm )
     {
       //Tea_Coffee_Dim_Disp( DIM10 );
       if( _500ms_flag )
       {
          Led_Fullcarafe = LED_ON;
          if( CLean_Class == 0 || CLean_Class == 1 )
            Led_Clean2  = LED_ON;
          else
            Led_Clean = LED_ON;
       }
       switch ( Knob0_count [ Brew_Select ]  )
        {
           case Cup:        Led_Cup = LED_ON;
                            Led_Xlcup = DIM10;
                            Led_Travel = DIM10;
                            Led_Xltravel = DIM10;
                            Led_Halfcarafe = DIM10;
                            break;
           case Xlcup:      Led_Cup = DIM10;
                            Led_Xlcup = LED_ON;
                            Led_Travel = DIM10;
                            Led_Xltravel = DIM10;
                            Led_Halfcarafe = DIM10;
                            break;
           case Travel:     Led_Cup = DIM10;
                            Led_Xlcup = DIM10;
                            Led_Travel = LED_ON;
                            Led_Xltravel = DIM10;
                            Led_Halfcarafe = DIM10;
                            break;
           case Xltrael:    Led_Cup = DIM10;
                            Led_Xlcup = DIM10;
                            Led_Travel = DIM10;
                            Led_Xltravel = LED_ON;
                            Led_Halfcarafe = DIM10;
                            break;
           case Halfcarafe: Led_Cup = DIM10;
                            Led_Xlcup = DIM10;
                            Led_Travel = DIM10;
                            Led_Xltravel = DIM10;
                            Led_Halfcarafe = LED_ON;
                            break;
           default:break;
        }
       
     }
}
void Pause_Clock_Disp( U16 pauseclock )
{
      U8 tempM = (U8 )(pauseclock/60);
      U8 tempS = (U8 )(pauseclock%60);
      hour_min(tempM,tempS);
      BSETEX( ledMap, LM_2DP); //
}

void CLean_Disp_Run( void )
{
      
      if( Paused_Delay )
      {
        Pause_Clock_Disp( Paused_Delay );
      }
      else
      {
        hour_min(Clock_Clean.min,Clock_Clean.second);
        BSETEX( ledMap, LM_2DP); //
      }
      Led_Fullcarafe = LED_ON;
      if( CLean_Class == 0 || CLean_Class == 1 )
          Led_Clean2  = LED_ON;
      else
          Led_Clean = LED_ON;
}

void Stay_Warm_Disp( void )
{
  if( Brew_Select == NO_BASKET )
  {
    if( KEEP_WARM_FUNC )
          Led_Keepwarm = LED_ON;
      else
      {
         if( System_Status == DIM_STATUS )
          Led_Keepwarm = DIM;
         else
          Led_Keepwarm = DIM10;
      }
  }
  else
  {
      if((Halfcarafe == Knob0_count [ Brew_Select ]  || fullcarafe == Knob0_count [ Brew_Select ]  )
                       && (Classic == Knob1_count [ Brew_Select ]  || Rich == Knob1_count [ Brew_Select ] ))
      {
           
          if( KEEP_WARM_FUNC )
              Led_Keepwarm = LED_ON;
          else
          {
             if( System_Status == DIM_STATUS )
              Led_Keepwarm = DIM;
             else
              Led_Keepwarm = DIM10;
          }
      }
      else
      {
        KEEP_WARM_FUNC = 0;// any change to setting which disable stay warm will switch stay warm off
      }
   }
}
void Keep_Warm_Clock_Disp(void)
{
    if( _500ms_flag  || Clock_Stay_Time)
    {
      hour_min(Clock_Keep_Warm.hour,Clock_Keep_Warm.min);
      BSETEX( ledMap, LM_2DP); //
    }
    if( _500ms_flag )// 闪烁
     Led_Keepwarm = LED_ON;// STAY WARM_LED
}
void Keep_Warm_Level_Disp(void)
{
     if( _500ms_flag )
     {
        Led_Keepwarm = LED_ON;
        if( Keep_Warm_Class == WARM_HI)
        {
          digImage[ 3 ] = C_CODE_h;
          digImage[ 2 ] = SEGC;
          digImage[ 1 ] = 0;
          digImage[ 0 ] = 0;
        }
        else if(  Keep_Warm_Class == WARM_LO )
        {
          digImage[ 3 ] = C_CODE_L;
          digImage[ 2 ] = C_CODE_o;
          digImage[ 1 ] = 0;
          digImage[ 0 ] = 0;
        }
        else
        {
          digImage[ 3 ] = C_CODE_o;
          digImage[ 2 ] = C_CODE_F;
          digImage[ 1 ] = C_CODE_F;
          digImage[ 0 ] = 0;
        }
     }
}

void Delay_Brew_Clock_Disp( void )
{
    Tea_Coffee_Dim_Disp( DIM10 );
    if( COFFEE == Brew_Select )
     {
      Led_Coffee = DIM10;
      Coffee_Style_Dim_Disp( DIM10 );
     }
    else if( TEA == Brew_Select )
     {
      Led_Tea = DIM10;
      //Tea_Style_Type_Dim_Disp( );
      DelayBrew_Tea_Dim_Disp( DIM10);
     }
    if( _500ms_flag  || Clock_Stay_Time)
    {
      hour_min(Clock_DLY_BREW.hour_b,Clock_DLY_BREW.min_b);
      BSETEX( ledMap, LM_2DP); //
      if( DELAY_AM_PM_FLAG )
        BSETEX( ledMap, LM_PM);
      else
       BSETEX( ledMap, LM_AM);
    }
    if( _500ms_flag )
      Led_Delayset = LED_ON;
    if( DELAY_SET_STYLE == 0 )
    {
      if( _500ms_flag )
        {
          switch ( Knob1_count [ Brew_Select ]  )
          {
             case Classic:    Led_Classic     = LED_ON;break;
             case Rich:       Led_Rich        = LED_ON;break;
             case Overice:    Led_Overice     = LED_ON;break;
             case Coldbrew:   Led_Coldbrew    = LED_ON;break;
             case Speciality: Led_Speciality  = LED_ON;break;
             case Immersion:  Led_Immersion   = LED_ON;break;
             default:break;
          }
        }
        else
        {
          switch ( Knob1_count [ Brew_Select ]  )
          {
             case Classic:    Led_Classic     = LED_OFF;break;
             case Rich:       Led_Rich        = LED_OFF;break;
             case Overice:    Led_Overice     = LED_OFF;break;
             case Coldbrew:   Led_Coldbrew    = LED_OFF;break;
             case Speciality: Led_Speciality  = LED_OFF;break;
             case Immersion:  Led_Immersion   = LED_OFF;break;
             default:break;
          }
        }
    }
    else
    {
          switch ( Knob1_count [ Brew_Select ]  )
          {
             case Classic:    Led_Classic= LED_ON;break;
             case Rich:       Led_Rich= LED_ON;break;
             case Overice:    Led_Overice= LED_ON;break;
             case Coldbrew:   Led_Coldbrew= LED_ON;break;
             case Speciality: Led_Speciality= LED_ON;break;
             case Immersion:  Led_Immersion= LED_ON;break;
             default:break;
          }
    }
    
    if( DELAY_SET_SIZE == 0 )
    {
      if( Speciality == Knob1_count [ Brew_Select ]  
        || Immersion == Knob1_count [ Brew_Select ]  )
      {
        if( _500ms_flag )
          Led_Cup = LED_ON;
        else
          Led_Cup = LED_OFF;
      }
      else
      {
          if( _500ms_flag )
          {
            switch ( Knob0_count [ Brew_Select ]  )
            {
               case Cup:        Led_Cup        = LED_ON;break;
               case Xlcup:      Led_Xlcup      = LED_ON;break;
               case Travel:     Led_Travel     = LED_ON;break;
               case Xltrael:    Led_Xltravel   = LED_ON;break;
               case Halfcarafe: Led_Halfcarafe = LED_ON;break;
               case fullcarafe: Led_Fullcarafe = LED_ON;break;
               default:break;
            }
          }
          else
          {
            switch ( Knob0_count [ Brew_Select ]  )
            {
               case Cup:        Led_Cup        = LED_OFF;break;
               case Xlcup:      Led_Xlcup      = LED_OFF;break;
               case Travel:     Led_Travel     = LED_OFF;break;
               case Xltrael:    Led_Xltravel   = LED_OFF;break;
               case Halfcarafe: Led_Halfcarafe = LED_OFF;break;
               case fullcarafe: Led_Fullcarafe = LED_OFF;break;
               default:break;
            }
          }
      }
    }
    else
    {
        if( Speciality == Knob1_count [ Brew_Select ]  
        || Immersion == Knob1_count [ Brew_Select ]  )
        {
            Led_Cup = LED_ON;
        }
        else
        {
            switch ( Knob0_count [ Brew_Select ]  )
            {
               case Cup:        Led_Cup        = LED_ON;break;
               case Xlcup:      Led_Xlcup      = LED_ON;break;
               case Travel:     Led_Travel     = LED_ON;break;
               case Xltrael:    Led_Xltravel   = LED_ON;break;
               case Halfcarafe: Led_Halfcarafe = LED_ON;break;
               case fullcarafe: Led_Fullcarafe = LED_ON;break;
               default:break;
            }
        }
    }

    if( Brew_Select == TEA )
    {
      if( Style_Type_Flag == 1 )
        Led_Style = LED_ON;
      else if( 2 == Style_Type_Flag )
        Led_Type = LED_ON;
      if( DELAY_SET_TYPE == 0 )
      {
        if( _500ms_flag )
        {
          switch ( Knob2_count )
          {
             case Herbal:   Led_Herbal   = LED_ON;break;
             case Black:    Led_Black    = LED_ON;break;
             case Oolong:   Led_Oolong   = LED_ON;break;
             case White:    Led_White    = LED_ON;break;
             case Green:    Led_Green    = LED_ON;break;
             case Delicate: Led_Delicate = LED_ON;break;
             default:break;
          }
        }
        else
        {
          switch ( Knob2_count )
          {
             case Herbal:   Led_Herbal   = LED_OFF;break;
             case Black:    Led_Black    = LED_OFF;break;
             case Oolong:   Led_Oolong   = LED_OFF;break;
             case White:    Led_White    = LED_OFF;break;
             case Green:    Led_Green    = LED_OFF;break;
             case Delicate: Led_Delicate = LED_OFF;break;
             default:break;
          }
        }
      }
      else
      {
          switch ( Knob2_count )
          {
             case Herbal:   Led_Herbal = LED_ON;  break;
             case Black:    Led_Black= LED_ON;    break;
             case Oolong:   Led_Oolong= LED_ON;   break;
             case White:    Led_White= LED_ON;    break;
             case Green:    Led_Green= LED_ON;    break;
             case Delicate: Led_Delicate = LED_ON;break;
             default:break;
          }
      }
    }
    else
    {
     DELAY_SET_TYPE = 1;
    }
    
}
void Delay_Brew_Disp( void )
{
  static U8 delayAlarm  = 0;
  if( DLY_BREW_FUNC )
  {
    if( delayBrewPause )
    {
      if( _500ms_flag )
        Led_Delayset = LED_ON;
    }
    else
      Led_Delayset = LED_ON;
    

      if( delayAdjust15S )
      {
        if( delayNoaction3S )
        {
           if( COFFEE == Brew_Select )
           {
             Tea_Coffee_Dim_Disp( DIM10 );
             Coffee_Style_Dim_Disp( DIM10 );
             Tea_Coffee_Same_Disp( LED_ON);
             Coffee_Disp( LED_ON );
           }
           else
           {
             Tea_Coffee_Dim_Disp( DIM10 );
             Tea_Style_Type_Dim_Disp( DIM10 );
             Tea_Coffee_Same_Disp( LED_ON );
             Tea_Disp( LED_ON );
           }
        }
        else if( System_Status == ACTIVE )
        {
          Tea_Coffee_Same_Disp( LED_ON );
          if( Brew_Select == TEA )
          {
             Tea_Disp( LED_ON );
          }
          else
            Coffee_Disp( LED_ON );
        }
          
      }
      else
      {

        if( delayExitCnt )
        {
           delayAlarm++;
           if( delayAlarm <= 20)
           {
            Tea_Coffee_Same_Disp( LED_OFF);
            if( Brew_Select == TEA )
              Tea_Type_Disp( LED_OFF);
           }
           else if( delayAlarm <= 60 )
           {
            Tea_Coffee_Same_Disp( LED_ON);
            if( Brew_Select == TEA )
              Tea_Type_Disp( LED_ON);
           }
           else
           {
            delayAlarm = 0;
            delayExitCnt--;
            if( delayExitCnt == 0 )
              DLY_BREW_FUNC = 0;
           }
        }
        else if( System_Status == ACTIVE )
        {
          Tea_Coffee_Same_Disp( LED_ON );
          if( Brew_Select == TEA )
          {
             Tea_Disp( LED_ON );
          }
          else
            Coffee_Disp( LED_ON );
        }
      }
  }
  else
  {
    if( Brew_Select != NO_BASKET && Start_Status == OFF )
    {
      if( System_Status == ACTIVE)
        Led_Delayset = DIM10;
      else if( System_Status == DIM_STATUS )
        Led_Delayset = DIM;
    }
  }
}

void Disp_Temperature( S16 Temp )
{
  U8 num = Temp / 1000;
  digImage[ 3 ] = 0;
  if( num == 0 )
  digImage[ 2 ] = 0;
  else
  digImage[ 2 ] = digCode[ Temp / 1000 ];
  Temp %= 1000;
  digImage[ 1 ] = digCode[ Temp / 100 ];
  Temp %= 100;
  digImage[ 0 ] = digCode[ Temp / 10 ];
   
}
void FlowRate_Disp( S16 flow)
{
  U8 num = flow / 100;
  digImage[ 3 ] = 0;
  if( num == 0 )
  digImage[ 2 ] = 0;
  else
  digImage[ 2 ] = digCode[ flow / 100 ];
  flow %= 100;
  digImage[ 1 ] = digCode[ flow / 10 ];
  digImage[ 0 ] = digCode[ flow % 10 ];
}
void Error_Code_Disp( void )
{
  if( errorCode0 & 0x01)
  {
     digImage[ 3 ] = C_CODE_E;
     digImage[ 2 ] = C_CODE_r;
     digImage[ 1 ] = C_CODE_r;
     digImage[ 0 ] = C_CODE_1;
  }
  else if( errorCode0 & 0x02)
  {
     digImage[ 3 ] = C_CODE_E;
     digImage[ 2 ] = C_CODE_r;
     digImage[ 1 ] = C_CODE_r;
     digImage[ 0 ] = C_CODE_2;
  }
  else if( errorCode0 & 0x04)
  {
     digImage[ 3 ] = C_CODE_E;
     digImage[ 2 ] = C_CODE_r;
     digImage[ 1 ] = C_CODE_r;
     digImage[ 0 ] = C_CODE_3;
  }
  else if( errorCode0 & 0x08)
  {
     digImage[ 3 ] = C_CODE_E;
     digImage[ 2 ] = C_CODE_r;
     digImage[ 1 ] = C_CODE_r;
     digImage[ 0 ] = C_CODE_4;
  }
  else if( errorCode0 & 0x10)
  {
     digImage[ 3 ] = C_CODE_E;
     digImage[ 2 ] = C_CODE_r;
     digImage[ 1 ] = C_CODE_r;
     digImage[ 0 ] = C_CODE_5;
  }
  else if( errorCode0 & 0x20)
  {
     digImage[ 3 ] = C_CODE_E;
     digImage[ 2 ] = C_CODE_r;
     digImage[ 1 ] = C_CODE_r;
     digImage[ 0 ] = C_CODE_6;
  }
  else if( errorCode0 & 0x40)
  {
     digImage[ 3 ] = C_CODE_E;
     digImage[ 2 ] = C_CODE_r;
     digImage[ 1 ] = C_CODE_r;
     digImage[ 0 ] = C_CODE_7;
  }
  else if( errorCode0 & 0x80)
  {
     digImage[ 3 ] = C_CODE_E;
     digImage[ 2 ] = C_CODE_r;
     digImage[ 1 ] = C_CODE_r;
     digImage[ 0 ] = C_CODE_8;
  }
}

void DebugMessage_Disp( void )
{
   if( debugEnable )
   {
      BCLREX( ledMap, LM_2DP);
      BCLREX( ledMap, LM_PM);
      BCLREX( ledMap, LM_AM);
      if( debugValueEnable )
      {
         switch( debugIndex )
         {
           case 0:
                 digImage[ 3 ] = 0;
                 digImage[ 2 ] = 0;
                 digImage[ 1 ] = digCode[ 0 ];
                 digImage[ 0 ] = digCode[ debugMessage.commSoftVersion ];
                break;
           case 1:
                 digImage[ 3 ] = 0;
                 digImage[ 2 ] = 0;
                 digImage[ 1 ] = C_CODE_b;
                 digImage[ 0 ] = digCode[ debugMessage.commBlock + 1 ];
                break;
           case 2:
                Disp_Temperature( debugMessage.commTempBoiler + 5);
                break;
           case 3:
                Disp_Temperature( debugMessage.commTempWarmPlate +5 );
                break;
           case 4:
                Disp_Temperature( debugMessage.commTempInlet + 5 );
                break;
           case 5:
                Disp_Temperature( debugMessage.commTempWaterout + 5);
                break;
           case 6:
                FlowRate_Disp( debugMessage.commflowRate );
                break;
         }
      }
      else
      {
        digImage[ 3 ] = C_SEGG;
        digImage[ 2 ] = C_CODE_0;
        digImage[ 0 ] = C_SEGG;
        digImage[ 1 ] = digCode[ debugIndex ];
      }
   }
}
void Pause_Disp( void )
{
   if( CLEAN_FUNC_FLAG == 1)
   {
     if( _500ms_flag )
      Led_Start = LED_ON;
   }
   else if( _500ms_flag )
    {
       Led_Start = LED_ON;
       Led_BAR1 = LED_ON;
       Led_BAR2 = LED_ON;
       Led_BAR3 = LED_ON;
       Led_BAR4 = LED_ON;
       Led_BAR5 = LED_ON;
    }
}

void Run_Disp(void)
{
     Led_Start = LED_ON;
     if( CLEAN_FUNC_FLAG == 1)
      {}
     else if( F_Brew_complete == 0 && F_Brew_abnormal_end == 0 )
     {
         
         switch( Brew_Bar )
         {
           case 0: Led_BAR1 = ledBreathe;
                   Led_BAR2 = DIM;
                   Led_BAR3 = DIM;
                   Led_BAR4 = DIM;
                   Led_BAR5 = DIM;
                   break;
           case 1:   
                   Led_BAR1 = LED_ON;
                   Led_BAR2 = ledBreathe;
                   Led_BAR3 = DIM;
                   Led_BAR4 = DIM;
                   Led_BAR5 = DIM;
                   break;
           case 2: 
                   Led_BAR1 = LED_ON;
                   Led_BAR2 = LED_ON;
                   Led_BAR3 = ledBreathe;
                   Led_BAR4 = DIM;
                   Led_BAR5 = DIM;
                   break;
           case 3: 
                   Led_BAR1 = LED_ON;
                   Led_BAR2 = LED_ON;
                   Led_BAR3 = LED_ON;
                   Led_BAR4 = ledBreathe;
                   Led_BAR5 = DIM;
                   break;
           case 4: 
                   Led_BAR1 = LED_ON;
                   Led_BAR2 = LED_ON;
                   Led_BAR3 = LED_ON;
                   Led_BAR4 = LED_ON;
                   Led_BAR5 = ledBreathe;
                   break;
           case 5: Led_BAR1 = ledBreathe;
                   Led_BAR2 = ledBreathe;
                   Led_BAR3 = ledBreathe;
                   Led_BAR4 = ledBreathe;
                   Led_BAR5 = ledBreathe;
                   break;
         }
    }
    if( brewAdjust15S )
    {
      if( brewNoaction3S )
      {
         if( COFFEE == Brew_Select )
         {
           Tea_Coffee_Dim_Disp( DIM10 );
           Coffee_Style_Dim_Disp( DIM10 );
           Tea_Coffee_Same_Disp( LED_ON);
           Coffee_Disp( LED_ON );
         }
         else if( TEA == Brew_Select)
         {
           Tea_Coffee_Dim_Disp( DIM10 );
           Tea_Style_Type_Dim_Disp( DIM10 );
           Tea_Coffee_Same_Disp( LED_ON );
           Tea_Disp( LED_ON );
         }
      }
    }
}


void LED_DISP( void )
{
   
   static U8 SRAM timBreathe  = 0;
   static U8 blinkAlarm       = 0;
   if ( ++timBreathe > 200 ) timBreathe = 0;
   if( timBreathe <= DIM )
    ledBreathe = DIM;
   else if( timBreathe <= 100 )
    ledBreathe = timBreathe;
   else if( timBreathe <= 200-DIM)
    ledBreathe = 200 - timBreathe;
   else
    ledBreathe = DIM;
#if 1
    Led_Keepwarm         = LED_OFF;
    Led_Type             = LED_OFF;
    Led_Clean            = LED_OFF;
    Led_Power            = LED_OFF;
    Led_BAR2             = LED_OFF;
    Led_BAR3             = LED_OFF;
    Led_BAR4             = LED_OFF;
    Led_BAR5             = LED_OFF;

    Led_White            = LED_OFF;
    Led_Speciality       = LED_OFF;
    Led_Green            = LED_OFF;
    Led_Immersion        = LED_OFF;
    Led_Delicate         = LED_OFF;
    Led_Style            = LED_OFF;
    Led_Start            = LED_OFF;

    Led_Classic          = LED_OFF;
    Led_Herbal           = LED_OFF;
    Led_Rich             = LED_OFF;
    Led_Black            = LED_OFF;
    Led_Overice          = LED_OFF;
    Led_Oolong           = LED_OFF;
    Led_Coldbrew         = LED_OFF;

    Led_Xltravel         = LED_OFF;
    Led_Xlcup            = LED_OFF;
    Led_Halfcarafe       = LED_OFF;
    Led_Cup              = LED_OFF;
    Led_Fullcarafe       = LED_OFF;
    Led_BAR1             = LED_OFF;
    Led_Addone           = LED_OFF;


    Led_Insertbasket     = LED_OFF;
    Led_Delayset         = LED_OFF;
    Led_Clean2           = LED_OFF;
    //Led_Keepwarm         = LED_OFF;
    Led_Tea              = LED_OFF;
    Led_Coffee           = LED_OFF;
    Led_Travel           = LED_OFF;
#endif
   memset( ledMap, 0x00, sizeof( ledMap ) );
   memset( digImage, 0x00, sizeof( digImage ) );
   if( CLEAN_DISP_FLAG || CLEAN_FUNC_FLAG || F_Brew_complete || F_Brew_abnormal_end )
    {
      Error_Code_Disp( );
    }
   else if( Delay_Keep_Warm_Time )
    {
      Keep_Warm_Clock_Disp( );
    }
   else if( Delay_Keep_Warm_Level )
    {
      Keep_Warm_Level_Disp( );
    }
   else if( Delay_Brew_Time )
    {
     Delay_Brew_Clock_Disp( );
    }
   else 
    {
         if( errorCode0 )
         Error_Code_Disp();
         else if( Paused_Delay )
          Pause_Clock_Disp( Paused_Delay );
         else if( delayBrewPause )
          Pause_Clock_Disp( delayBrewPause );
         else
          Clock_Disp( );
         Stay_Warm_Disp( );
         Delay_Brew_Disp( );
    }
    //Disp_Temperature(T_BOIL);
   if( ACTIVE == System_Status )
   {
     Led_Power = LED_ON;
     if(( NO_BASKET == Brew_Select )&& ( OFF == Start_Status)) // 无篮时闪烁指示灯
     {
      //if( _500ms_flag )
       //Led_Insertbasket = LED_ON;
     }
     else
     {
       if( ON == Start_Status || PAUSE == Start_Status || UNPAUSE== Start_Status )
       {
         switch( Start_Status )
         {
             case PAUSE:  Pause_Disp();break;
             case ON:
             case UNPAUSE: Run_Disp();break;
         }
         if( F_Brew_complete || F_Brew_abnormal_end )
         {
             if( _500ms_flag )
              {
                 switch( Brew_Bar )
                 {
                   case 0: break;
                   case 1: Led_BAR1 = LED_ON;                   
                           break;
                   case 2: Led_BAR1 = LED_ON;
                           Led_BAR2 = LED_ON;
                           break;
                   case 3: Led_BAR1 = LED_ON;
                           Led_BAR2 = LED_ON;
                           Led_BAR3 = LED_ON;
                           break;
                   case 4: Led_BAR1 = LED_ON;
                           Led_BAR2 = LED_ON;
                           Led_BAR3 = LED_ON;
                           Led_BAR4 = LED_ON;
                           break;
                   case 5: Led_BAR1 = LED_ON;
                           Led_BAR2 = LED_ON;
                           Led_BAR3 = LED_ON;
                           Led_BAR4 = LED_ON;
                           Led_BAR5 = LED_ON;
                           break;
                 }
                 digImage[ 3 ] = C_CODE_E;
                 digImage[ 2 ] = C_CODE_n;
                 digImage[ 1 ] = C_CODE_d;
                 digImage[ 0 ] = 0;

                 if( CLEAN_FUNC_FLAG )
                 {
                    Led_Fullcarafe = LED_ON;
                   if( CLean_Class == 0 || CLean_Class == 1 )
                    Led_Clean2  = LED_ON;
                   else
                    Led_Clean = LED_ON;
                 }
              }

         }
         if( Brew_Select_B == NO_BASKET )
         {}
         else if( CLEAN_FUNC_FLAG )
         {
            if( F_Brew_complete || F_Brew_abnormal_end )
            {
            }
            else
           CLean_Disp_Run( );
         }
         else if( COFFEE == Brew_Select )
         {
           //Tea_Coffee_Dim_Disp( );
           //Coffee_Style_Dim_Disp( );
           if( brewExitCnt )
           {
             blinkAlarm++;
             if( blinkAlarm <= 20)
              Tea_Coffee_Same_Disp( LED_OFF);
             else if( blinkAlarm <= 60 )
              Tea_Coffee_Same_Disp( LED_ON);
             else
             {
              blinkAlarm = 0;
              brewExitCnt--;
              if( brewExitCnt == 0 )
                Start_Status = OFF;
              blinkAlarm = 0;
             }
             
           }
           else
           {
            Tea_Coffee_Same_Disp( LED_ON);
           }
           Coffee_Disp( LED_ON);
         }
         else
         {
           //Tea_Coffee_Dim_Disp( );
           //Tea_Style_Type_Dim_Disp( );
           if( brewExitCnt )
           {
             blinkAlarm++;
             if( blinkAlarm <= 20)
             {
              Tea_Coffee_Same_Disp( LED_OFF);
              if( Brew_Select == TEA )
              Tea_Type_Disp( LED_OFF);
             }
             else if( blinkAlarm <= 60 )
             {
              Tea_Coffee_Same_Disp( LED_ON);
              if( Brew_Select == TEA )
              Tea_Type_Disp( LED_ON);
             }
             else
             {
              blinkAlarm = 0;
              brewExitCnt--;
              if( brewExitCnt == 0 )
                Start_Status = OFF;
              blinkAlarm = 0;
             }
             
           }
           else
           {
            Tea_Coffee_Same_Disp( LED_ON);
           }
           Tea_Disp( LED_ON );
         }
       }
       else
       {
         if( DLY_BREW_FUNC || DLY_BREW_FLAG )
         {
         }
         else if( CLEAN_DISP_FLAG || cleanAlarm )
         {
           CLean_Disp_Standby( );
         }
         else 
         {
             if( Brew_Select == NO_BASKET )
             {
             }
             else
             {
                 if( COFFEE == Brew_Select )
                 {
                   Tea_Coffee_Dim_Disp( DIM10 );
                   Coffee_Style_Dim_Disp( DIM10 );
                   Tea_Coffee_Same_Disp( LED_ON);
                   Coffee_Disp( LED_ON );
                 }
                 else
                 {
                   Tea_Coffee_Dim_Disp( DIM10 );
                   Tea_Style_Type_Dim_Disp( DIM10 );
                   Tea_Coffee_Same_Disp( LED_ON );
                   Tea_Disp( LED_ON );
                 }
                 if( CLean_Blink_10count )
                 {
                   if( _400ms_flag )
                   {
                     if( CLean_Class == 1 )
                      Led_Clean2  = LED_ON;
                     else if( CLean_Class == 2 )
                      Led_Clean   = LED_ON;
                   }
                 }
                 else
                 {
                     if( CLean_Class == 0 )
                      Led_Clean2  = DIM;
                     else if( CLean_Class == 1 )
                      Led_Clean2  = LED_ON;
                     else if( CLean_Class == 2 )
                      Led_Clean   = LED_ON;
                 }
             }
         }
       }
     }
   }
   else if( DIM_STATUS ==System_Status )
   {
          if( Brew_Select_B == NO_BASKET )
          {}
          else
          {
              if( DLY_BREW_FUNC )
              {
              }
              else
              {
                if( COFFEE == Brew_Select )
                 {
                   Tea_Coffee_Dim_Disp( DIM );
                   Coffee_Style_Dim_Disp( DIM );
                   Tea_Coffee_Same_Disp( DIM10);
                   Coffee_Disp( DIM10 );
                 }
                 else
                 {
                   Tea_Coffee_Dim_Disp( DIM );
                   Tea_Style_Type_Dim_Disp( DIM );
                   Tea_Coffee_Same_Disp( DIM10 );
                   Tea_Disp( DIM10 );
                 }
                 Led_Clean2  = DIM;
              }
              Led_Power = DIM10;
          }
   }

   //Disp_Temperature(T_BOIL);
   DebugMessage_Disp( );
   Digit_Figure( );
   DisplayDriver( );
}


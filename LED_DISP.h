
///////////////////////////////////////////////////////////
//                                                       //
//          FileName:   COM_ODU.h                        //
//                                                       //
///////////////////////////////////////////////////////////

#ifndef _LED_DISP_H_
#define _LED_DISP_H_

#ifdef  EXTERN
#undef  EXTERN
#endif
#ifdef  _LED_DISP_SRC_
#define EXTERN
#else
#define EXTERN extern
#endif

#define C_SEGA  0X01
#define C_SEGB  0X02
#define C_SEGC  0X04
#define C_SEGD  0X08
#define C_SEGE  0X10
#define C_SEGF  0X20
#define C_SEGG  0X40
#define C_SEGP  0X80

#define C_CODE_0    (C_SEGA|C_SEGB|C_SEGC|C_SEGD|C_SEGE|C_SEGF)
#define C_CODE_1    (C_SEGB|C_SEGC)
#define C_CODE_2    (C_SEGA|C_SEGB|C_SEGD|C_SEGE|C_SEGG)
#define C_CODE_3    (C_SEGA|C_SEGB|C_SEGC|C_SEGD|C_SEGG)
#define C_CODE_4    (C_SEGB|C_SEGC|C_SEGF|C_SEGG)
#define C_CODE_5    (C_SEGA|C_SEGC|C_SEGD|C_SEGF|C_SEGG)
#define C_CODE_6    (C_SEGA|C_SEGC|C_SEGD|C_SEGE|C_SEGF|C_SEGG)
#define C_CODE_7    (C_SEGA|C_SEGB|C_SEGC)
#define C_CODE_8    (C_SEGA|C_SEGB|C_SEGC|C_SEGD|C_SEGE|C_SEGF|C_SEGG)
#define C_CODE_9    (C_SEGA|C_SEGB|C_SEGC|C_SEGD|C_SEGF|C_SEGG)
#define C_CODE_A    (C_SEGA|C_SEGB|C_SEGC|C_SEGE|C_SEGF|C_SEGG)
#define C_CODE_a    (C_SEGA|C_SEGB|C_SEGC|C_SEGD|C_SEGE|C_SEGG)
#define C_CODE_b    (C_SEGC|C_SEGD|C_SEGE|C_SEGF|C_SEGG)
#define C_CODE_C    (C_SEGA|C_SEGD|C_SEGE|C_SEGF)
#define C_CODE_c    (C_SEGD|C_SEGE|C_SEGG)
#define C_CODE_d    (C_SEGB|C_SEGC|C_SEGD|C_SEGE|C_SEGG)
#define C_CODE_E    (C_SEGA|C_SEGD|C_SEGE|C_SEGF|C_SEGG)
#define C_CODE_e    (C_SEGA|C_SEGB|C_SEGD|C_SEGE|C_SEGF|C_SEGG)
#define C_CODE_F    (C_SEGA|C_SEGE|C_SEGF|C_SEGG)
#define C_CODE_G    (C_SEGA|C_SEGC|C_SEGD|C_SEGE|C_SEGF)
#define C_CODE_g    (C_SEGA|C_SEGB|C_SEGC|C_SEGD|C_SEGF|C_SEGG)
#define C_CODE_H    (C_SEGB|C_SEGC|C_SEGE|C_SEGF|C_SEGG)
#define C_CODE_h    (C_SEGC|C_SEGE|C_SEGF|C_SEGG)
#define C_CODE_I    (C_SEGE|C_SEGF)
#define C_CODE_i    (C_SEGA|C_SEGC|C_SEGG)
#define C_CODE_J    (C_SEGB|C_SEGC|C_SEGD|C_SEGE)
#define C_CODE_L    (C_SEGD|C_SEGE|C_SEGF)
#define C_CODE_N    (C_SEGA|C_SEGB|C_SEGC|C_SEGE|C_SEGF)
#define C_CODE_n    (C_SEGC|C_SEGE|C_SEGG)
#define C_CODE_O    (C_SEGA|C_SEGB|C_SEGC|C_SEGD|C_SEGE|C_SEGF)
#define C_CODE_o    (C_SEGC|C_SEGD|C_SEGE|C_SEGG)
#define C_CODE_P    (C_SEGA|C_SEGB|C_SEGE|C_SEGF|C_SEGG)
#define C_CODE_q    (C_SEGA|C_SEGB|C_SEGC|C_SEGF|C_SEGG)
#define C_CODE_r    (C_SEGE|C_SEGG)
#define C_CODE_S    (C_SEGA|C_SEGC|C_SEGD|C_SEGF|C_SEGG)
#define C_CODE_T    (C_SEGA|C_SEGE|C_SEGF)
#define C_CODE_t    (C_SEGD|C_SEGE|C_SEGF|C_SEGG)
#define C_CODE_U    (C_SEGB|C_SEGC|C_SEGD|C_SEGE|C_SEGF)
#define C_CODE_u    (C_SEGC|C_SEGD|C_SEGE)
#define C_CODE_y    (C_SEGB|C_SEGC|C_SEGD|C_SEGF|C_SEGG)
#define C_CODE_X    (C_SEGE|C_SEGF|C_SEGG)

#if 0
enum id_LED_MAP
{ 
    //LM_GR1 = 0X01,LM_PM,LM_WARM,LM_FULL,LM_TRAVEL,LM_DLYBREW,          // COM1
    //LM_GR2 = 0X11,LM_AM,LM_DRIP,LM_HALF,LM_CUP,LM_ONOFF,               // COM2
    LM_DLYBREW,LM_TRAVEL,LM_FULL,LM_WARM,LM_PM,LM_GR1 = 0X0F,          // COM1
    LM_ONOFF,LM_CUP,LM_HALF,LM_DRIP,LM_AM,LM_GR2 = 0X1F,               // COM2
    LM_4E,LM_4D,LM_4DP,LM_4C,LM_4G,LM_4A,LM_4F,LM_4B,LM_GR3 = 0X2F,    // COM3
    LM_3E,LM_3D,LM_3DP,LM_3C,LM_3G,LM_3A,LM_3F,LM_3B,LM_GR4 = 0X3F,    // COM4
    LM_2E,LM_2D,LM_CLOCK_POINT,LM_2C,LM_2G,LM_2A,LM_2F,LM_2B,LM_GR5 = 0X4F,// COM5
    LM_1E,LM_1D,LM_1DP,LM_1C,LM_1G,LM_1A,LM_1F,LM_1B,                  // COM6
    LM_MAX
};
#endif
enum id_LED_MAP
{ 
    LM_AM = 0X05,LM_4G,LM_4F,LM_4E,LM_4D,LM_4C = 0X0B,LM_4B,LM_4A,    // COM1
    LM_PM = 0X15,LM_3G,LM_3F,LM_3E,LM_3D,LM_3C = 0X1B,LM_3B,LM_3A,    // COM2
    LM_2DP = 0X25,LM_2G,LM_2F,LM_2E,LM_2D,LM_2C = 0X2B,LM_2B,LM_2A,    // COM3
    LM_1DP = 0X35,LM_1G,LM_1F,LM_1E,LM_1D,LM_1C = 0X3B,LM_1B,LM_1A,    // COM4
    LM_MAX
};

EXTERN U8  ledMap[ ( LM_MAX + 7 ) >> 3 ];


#define LED_ON    100
#define LED_OFF   0
#define DIM       5
#define DIM10     10



EXTERN U8 Led_Type;
EXTERN U8 Led_Clean;
EXTERN U8 Led_Power;
EXTERN U8 Led_BAR2;
EXTERN U8 Led_BAR3;
EXTERN U8 Led_BAR4;
EXTERN U8 Led_BAR5;

EXTERN U8 Led_White;
EXTERN U8 Led_Speciality;
EXTERN U8 Led_Green;
EXTERN U8 Led_Immersion;
EXTERN U8 Led_Delicate;
EXTERN U8 Led_Style;
EXTERN U8 Led_Start;

EXTERN U8 Led_Classic;
EXTERN U8 Led_Herbal;
EXTERN U8 Led_Rich;
EXTERN U8 Led_Black;
EXTERN U8 Led_Overice;
EXTERN U8 Led_Oolong;
EXTERN U8 Led_Coldbrew;

EXTERN U8 Led_Xltravel;
EXTERN U8 Led_Xlcup;
EXTERN U8 Led_Halfcarafe;
EXTERN U8 Led_Cup;
EXTERN U8 Led_Fullcarafe;
EXTERN U8 Led_BAR1;
EXTERN U8 Led_Addone;


EXTERN U8 Led_Insertbasket;
EXTERN U8 Led_Delayset;
EXTERN U8 Led_Clean2;
EXTERN U8 Led_Keepwarm;
EXTERN U8 Led_Tea;
EXTERN U8 Led_Coffee;
EXTERN U8 Led_Travel;






//#define F_LED_AM                     led[ 4 ].bits.b0
//#define F_LED_PM                     led[ 4 ].bits.b1

EXTERN void LED_DISP( void );

#endif


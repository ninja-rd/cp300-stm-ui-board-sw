/*
  File name:pt6964.c
 */

#define _PT696X_SRC_
#include "Globals.h"
#include "Pt696x.h"
  /*
void Pt696xDelay( void )
  {
    __NOP( );
   // __NOP( );
   // __NOP( );
   // __NOP( );
   // __NOP( );

  }*/


  
void Pt696xStart( void )
  {
    PT_TB_OUT( );
    PT_CK_OUT( );
    PIN_PT_TB_SET( );
    Pt696xDelay( );
    PIN_PT_TB_CLR( );
  }
  
#define Pt696xStop( ) PIN_PT_TB_SET( )

void Pt696xSend( U8 dat )
  {
    U8  bit = 0x01;
    PT_DI_OUT( );
    do{
        PIN_PT_CK_CLR( );
        if ( dat & bit )
          {
            PIN_PT_DI_SET( );
          }
        else
          {
            PIN_PT_DI_CLR( );
          }
        Pt696xDelay( );
        PIN_PT_CK_SET( );
        Pt696xDelay( );
      } while ( bit<<=1 );
  }
  
void Pt696xCmd( U8 cmd )
  {
    Pt696xStart( );
    Pt696xSend( cmd );
    Pt696xStop( );
  }
  
void Pt696xWr( U8 addr, U8 *pImage, U8 size )
  {
    Pt696xStart( );
    Pt696xSend( 0x40 );
    Pt696xStop( );
    Pt696xStart( );
    Pt696xSend( 0xf0 | addr );
    while ( size-- )
      {
        __WDTC( );
        Pt696xSend( *pImage );
        pImage++;
      }
    Pt696xStop( );
  }
U8   Pt696xRecieve( void )
  {
    U8  dat = 0;
    U8  bit = 0x01;
    PT_DO_IN( );
    Pt696xDelay( );
    do{
        PIN_PT_CK_CLR( );
        Pt696xDelay( );
        PIN_PT_CK_SET( );
        Pt696xDelay( );
        if ( PIN_PT_DO() ) dat |= bit;
      } while ( bit<<=1 );
    return ( dat );
  }
void Pt696xRd( U8 *pImage, U8 size )
  {
    Pt696xStart( );
    Pt696xSend( 0x42 );
    while ( size-- )
      {
        __WDTC( );
        *pImage = Pt696xRecieve( );
        pImage++;
      }
    Pt696xStop( );
  }  
  


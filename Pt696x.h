/*
  File name:pt6964.h
 */

#ifndef _PT696X_H_
#define _PT696X_H_

#ifdef EXTERN
#undef EXTERN
#endif
#ifdef _PT696X_SRC_
#define EXTERN
#else
#define EXTERN extern
#endif

//#define PT696X_KEYBOARD

#define C_PT_DIG4   0X00
#define C_PT_DIG5   0X01
#define C_PT_DIG6   0X02
#define C_PT_DIG7   0X03
#define C_PT_OFF    0X80
#define C_PT_ON0    0X88  // 1/16
#define C_PT_ON1    0X89  // 2/16
#define C_PT_ON2    0X8A  // 4/16
#define C_PT_ON3    0X8B  // 10/16
#define C_PT_ON4    0X8C  // 11/16
#define C_PT_ON5    0X8D  // 12/16
#define C_PT_ON6    0X8E  // 13/16
#define C_PT_ON7    0X8F  // 14/16

#define C_PT_WRITE   0X40
#define C_PT_READ   0X42
#define Pt696xDelay( )  do { \
    __NOP( );\
    __NOP( );\
    __NOP( );\
    __NOP( );\
  } while(0)

EXTERN void Pt696xCmd( U8 cmd );
EXTERN void Pt696xWr( U8 addr, U8 *pImage, U8 size );
EXTERN void Pt696xRd( U8 *pImage, U8 size );

#endif


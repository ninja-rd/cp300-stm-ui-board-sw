
///////////////////////////////////////////////////////////
//                                                       //
//          FileName:   StTypes.h                        //
//                                                       //
///////////////////////////////////////////////////////////

#ifndef _ST_TYPES_H_
#define _ST_TYPES_H_

typedef unsigned char   U8;
typedef unsigned short  U16;
typedef unsigned long   U32;
typedef signed char     S8;
typedef signed short    S16;
typedef signed long     S32;

#ifndef NULL
//#define NULL  ((void*)0)
#endif

typedef union
  {
    U8 byte;
    struct
      {
        unsigned char b0 : 1;
        unsigned char b1 : 1;
        unsigned char b2 : 1;
        unsigned char b3 : 1;
        unsigned char b4 : 1;
        unsigned char b5 : 1;
        unsigned char b6 : 1;
        unsigned char b7 : 1;
      } bits;
  } BYTE;

typedef union
  {
    U16 word;
    struct
      {
        U8 H;
        U8 L;
      } byte;
    struct
      {
        unsigned char b0  : 1;
        unsigned char b1  : 1;
        unsigned char b2  : 1;
        unsigned char b3  : 1;
        unsigned char b4  : 1;
        unsigned char b5  : 1;
        unsigned char b6  : 1;
        unsigned char b7  : 1;
        unsigned char b8  : 1;
        unsigned char b9  : 1;
        unsigned char b10 : 1;
        unsigned char b11 : 1;
        unsigned char b12 : 1;
        unsigned char b13 : 1;
        unsigned char b14 : 1;
        unsigned char b15 : 1;
      } bits;
  } WORD;

typedef union
{
   float FLOAT_TYPE;
   struct
      {
        U8 HH;
        U8 HL;
        U8 LH;
        U8 LL;
      } FL;
}FLOAT;

#define SRAM          @tiny
#define XRAM          @near
#define CROM          const


#define INCEX(X)     if(++X==0)X--//(X=((X)+1>(X))?(X)+1:(X))
#define DECEX(X)     if(X!=0)X--//(X=((X)-1<(X))?(X)-1:(X))

#define BMSK(N)       (1U<<(N))
#define BSET(X,N)     (X)|=BMSK(N)
#define BCLR(X,N)     (X)&=(BMSK(N)^-1)
#define BCPL(X,N)     (X)^=BMSK(N)
#define BTST(X,N)     ((X)&BMSK(N))
#define BSETEX(X,N)   X[(N)>>3]|=((U8)BMSK((N)&7))
#define BCLREX(X,N)   X[(N)>>3]&=(((U8)BMSK((N)&7))^-1)
#define BCPLEX(X,N)   X[(N)>>3]^=((U8)BMSK((N)&7))
#define BTSTEX(X,N)   (X[(N)>>3]&((U8)BMSK((N)&7)))
#define LENOF(ARR)    ((U8)(sizeof(ARR)/sizeof(ARR[0])))

#define __DI( )       _asm(" sim ")
#define __EI( )       _asm(" rim ")
#define __NOP( )      _asm(" nop ")
#define __WDTC( )     IWDG_KR = 0xAA

#endif



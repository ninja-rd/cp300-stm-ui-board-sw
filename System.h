
///////////////////////////////////////////////////////////
//                                                       //
//          FileName:   System.h                         //
//                                                       //
///////////////////////////////////////////////////////////

#ifndef _SYSTEM_H_
#define _SYSTEM_H_

#ifdef  EXTERN
#undef  EXTERN
#endif
#ifdef  _SYSTEM_SRC_
#define EXTERN
#else
#define EXTERN extern
#endif


EXTERN void SYS_Init( void );
EXTERN void SYS_Ctrl( void );

#endif


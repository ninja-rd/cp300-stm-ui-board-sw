
///////////////////////////////////////////////////////////
//                                                       //
//          FileName:   COM_ODU.c                        //
//                                                       //
///////////////////////////////////////////////////////////

#define  _UART_SRC_
#include "Globals.h"
#include "UART.h"
#include "COM_ODU.h"

#ifndef TEST_INTERRUPT
#define TEST_INTERRUPT
#endif

@far @interrupt void UART2_TX_IRQHandler( void )
{
    if( ++Tx_Num >= Tx_Length )
    {
        //Tx_Delay = 5;
        UART2_CR2 &= ~0X80;//��ֹ�����ж�
    }
    else
    {
        UART2_DR = Tx_Buffer[Tx_Num];
    }
}

@far @interrupt void UART2_RX_IRQHandler( void )
{
    U8 uart_data;
    if ((UART2_SR & 0x20) != 0x00)
    {
        uart_data = UART2_SR;
        uart_data = UART2_DR;
        //UART2_SR &= ~0x20;
        Rx_Delay = 2;
        if( Rx_Num < 50 )
        {
          Rx_Buffer[ Rx_Num ] = uart_data;
          Rx_Num++;
        }
    }
}

#ifdef TEST_INTERRUPT
@far @interrupt void StepMotorISR( void )
{
  U8 MOTOR = 0;
  MOTOR++;
}
#endif

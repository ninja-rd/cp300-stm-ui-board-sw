
///////////////////////////////////////////////////////////
//                                                       //
//          FileName:   Main.c                           //
//                                                       //
///////////////////////////////////////////////////////////

#define  _MAIN_SRC_
#include "Globals.h"

#include "System.h"
#include "COM_ODU.h"
#include "KEY.h"
#include "LED_DISP.h"
#include "Pt696x.h"
#include "Beeper.h"


void main( void )
  {
    static U8 sysTicks10ms = 0;
    MCU_Init( );
    COM_ODU_Init( );
    Key_Init( );
    //BeepStart( Cancel_Sound );
    sysTicks = 0;
    while ( 1 )
      {
        __EI( );
        if ( sysTicks >= 50 )
          {
            __DI( );
            sysTicks -= 50;
            __EI( );
            
            if( ++sysTicks10ms >= 10 )
            {
              __WDTC( );
              sysTicks10ms = 0;
             MCU_Refresh( );
             Time_Deal( );
             Key_Deal( );
             SYS_Ctrl( );
             LED_DISP( );
             BeepCtrl( );
            }
            Size_Rotary_Code( );
            Style_Type_Rotary_Code( );
            COM_ODU_Ctrl( );
          }
      }
  }
  

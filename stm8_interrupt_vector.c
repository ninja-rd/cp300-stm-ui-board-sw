/*	BASIC INTERRUPT VECTOR TABLE FOR STM8 devices
 *	Copyright (c) 2007 STMicroelectronics
 */

typedef void @far (*interrupt_handler_t)(void);

struct interrupt_vector {
	unsigned char interrupt_instruction;
	interrupt_handler_t interrupt_handler;
};

@far @interrupt void NonHandledInterrupt (void)
{
	/*
	  in order to detect unexpected events during development, 
	  it is recommended to set a breakpoint on the following instruction
	*/

	return;
}

extern void _stext();         /* startup routine */
extern @far @interrupt void SystemTimerISR( void );
extern @far @interrupt void StepMotorISR( void );
extern @far @interrupt void SoftSerialISR( void );
extern @far @interrupt void UART2_TX_IRQHandler( void );
extern @far @interrupt void UART2_RX_IRQHandler( void );





struct interrupt_vector const _vectab[] = {
	{0x82, (interrupt_handler_t)_stext},  /* RESET */
	{0x82, NonHandledInterrupt},          /* TRAP  */
	{0x82, NonHandledInterrupt}, /* 0  TLI       External Top Level Interrupt            */
	{0x82, NonHandledInterrupt}, /* 1  AWU       Auto Wake Up From Halt                  */
	{0x82, NonHandledInterrupt}, /* 2  CLK       Clock Controller                        */
	{0x82, NonHandledInterrupt}, /* 3  EXTI0     Port A External Interrupt               */
	{0x82, NonHandledInterrupt}, /* 4  EXTI1     Port B External Interrupt               */
	{0x82, NonHandledInterrupt}, /* 5  EXTI2     Port C External Interrupt               */
	{0x82, NonHandledInterrupt}, /* 6  EXTI3     Port D External Interrupt               */
	{0x82, NonHandledInterrupt}, /* 7  EXTI4     Port E External Interrupt               */
	{0x82, NonHandledInterrupt}, /* 8  EXTI5     Port F External Interrupt               */
	{0x82, NonHandledInterrupt}, /* 9  Reserved */
	{0x82, NonHandledInterrupt}, /* 10 SPI       End Of Chansfer                         */
	{0x82, SystemTimerISR     }, /* 11 TIM1      Update/Overflow/Underflow/Trigger/Break */
	{0x82, NonHandledInterrupt}, /* 12 TIM1      Capture/Compare                         */
	{0x82, NonHandledInterrupt}, /* 13 TIM2/TIM5 Update/Overflow/Trigger                 */
	{0x82, NonHandledInterrupt}, /* 14 TIM2/TIM5 Capture/Compare                         */
	{0x82, NonHandledInterrupt}, /* 15 TIM3      Update/Overflow/Trigger                 */
	{0x82, NonHandledInterrupt}, /* 16 TIM3      Capture/Compare                         */
  {0x82, NonHandledInterrupt}, /* 17 UART1     Tx Complete                             */
  {0x82, NonHandledInterrupt}, /* 18 UART1     Rx DATA full                            */
  {0x82, NonHandledInterrupt}, /* 19 I2C       Interrupt                               */
	{0x82, UART2_TX_IRQHandler}, /* 20 UART2     Tx Complete                             */
	{0x82, UART2_RX_IRQHandler}, /* 21 UART2     Rx DATA full                            */
	{0x82, NonHandledInterrupt}, /* 22 ADC1      End Of Conversion                       */
	{0x82, NonHandledInterrupt}, /* 23 TIM4/TIM6 Update/Overflow/Trigger                 */
	{0x82, NonHandledInterrupt}, /* 24 FLASH     EOP/WR_PG_DIS                           */
	{0x82, NonHandledInterrupt}, /* 25 Reserved */
	{0x82, NonHandledInterrupt}, /* 26 Reserved */
	{0x82, NonHandledInterrupt}, /* 27 Reserved */
	{0x82, NonHandledInterrupt}, /* 28 Reserved */
	{0x82, NonHandledInterrupt}, /* 29 Reserved */
};

